//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_NewBillingItemInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingItemAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingItemTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingItemTypeSelection" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscNewBillingItemInd",
    "comCscBillingItemAmt",
    "comCscBillingItemTypeCd",
    "effectiveDt",
    "expirationDt",
    "comCscBillingItemTypeSelection"
})
@XmlRootElement(name = "com.csc_NewBillingItem")
public class ComCscNewBillingItem {

    @XmlElement(name = "com.csc_NewBillingItemInd")
    protected Boolean comCscNewBillingItemInd;
    @XmlElement(name = "com.csc_BillingItemAmt")
    protected BigDecimal comCscBillingItemAmt;
    @XmlElement(name = "com.csc_BillingItemTypeCd")
    protected String comCscBillingItemTypeCd;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDt;
    @XmlElement(name = "com.csc_BillingItemTypeSelection")
    protected ComCscBillingItemTypeSelection comCscBillingItemTypeSelection;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscNewBillingItemInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscNewBillingItemInd() {
        return comCscNewBillingItemInd;
    }

    /**
     * Sets the value of the comCscNewBillingItemInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscNewBillingItemInd(Boolean value) {
        this.comCscNewBillingItemInd = value;
    }

    /**
     * Gets the value of the comCscBillingItemAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscBillingItemAmt() {
        return comCscBillingItemAmt;
    }

    /**
     * Sets the value of the comCscBillingItemAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscBillingItemAmt(BigDecimal value) {
        this.comCscBillingItemAmt = value;
    }

    /**
     * Gets the value of the comCscBillingItemTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillingItemTypeCd() {
        return comCscBillingItemTypeCd;
    }

    /**
     * Sets the value of the comCscBillingItemTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillingItemTypeCd(String value) {
        this.comCscBillingItemTypeCd = value;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDt() {
        return expirationDt;
    }

    /**
     * Sets the value of the expirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDt(XMLGregorianCalendar value) {
        this.expirationDt = value;
    }

    /**
     * Gets the value of the comCscBillingItemTypeSelection property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscBillingItemTypeSelection }
     *     
     */
    public ComCscBillingItemTypeSelection getComCscBillingItemTypeSelection() {
        return comCscBillingItemTypeSelection;
    }

    /**
     * Sets the value of the comCscBillingItemTypeSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscBillingItemTypeSelection }
     *     
     */
    public void setComCscBillingItemTypeSelection(ComCscBillingItemTypeSelection value) {
        this.comCscBillingItemTypeSelection = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
