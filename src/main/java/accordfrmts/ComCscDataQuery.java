//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SystemTimestamp" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SystemErrorInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_QueryName" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_QueryParameter" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CacheDataInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReadCacheInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ResultSet" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "comCscSystemTimestamp",
    "comCscSystemErrorInfo",
    "comCscQueryName",
    "comCscQueryParameter",
    "comCscCacheDataInd",
    "comCscReadCacheInd",
    "comCscResultSet"
})
@XmlRootElement(name = "com.csc_DataQuery")
public class ComCscDataQuery {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "com.csc_SystemTimestamp")
    protected String comCscSystemTimestamp;
    @XmlElement(name = "com.csc_SystemErrorInfo")
    protected ComCscSystemErrorInfo comCscSystemErrorInfo;
    @XmlElement(name = "com.csc_QueryName")
    protected String comCscQueryName;
    @XmlElement(name = "com.csc_QueryParameter")
    protected List<ComCscQueryParameter> comCscQueryParameter;
    @XmlElement(name = "com.csc_CacheDataInd")
    protected Boolean comCscCacheDataInd;
    @XmlElement(name = "com.csc_ReadCacheInd")
    protected Boolean comCscReadCacheInd;
    @XmlElement(name = "com.csc_ResultSet")
    protected ComCscResultSet comCscResultSet;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the comCscSystemTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSystemTimestamp() {
        return comCscSystemTimestamp;
    }

    /**
     * Sets the value of the comCscSystemTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSystemTimestamp(String value) {
        this.comCscSystemTimestamp = value;
    }

    /**
     * Gets the value of the comCscSystemErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscSystemErrorInfo }
     *     
     */
    public ComCscSystemErrorInfo getComCscSystemErrorInfo() {
        return comCscSystemErrorInfo;
    }

    /**
     * Sets the value of the comCscSystemErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscSystemErrorInfo }
     *     
     */
    public void setComCscSystemErrorInfo(ComCscSystemErrorInfo value) {
        this.comCscSystemErrorInfo = value;
    }

    /**
     * Gets the value of the comCscQueryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscQueryName() {
        return comCscQueryName;
    }

    /**
     * Sets the value of the comCscQueryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscQueryName(String value) {
        this.comCscQueryName = value;
    }

    /**
     * Gets the value of the comCscQueryParameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscQueryParameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscQueryParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscQueryParameter }
     * 
     * 
     */
    public List<ComCscQueryParameter> getComCscQueryParameter() {
        if (comCscQueryParameter == null) {
            comCscQueryParameter = new ArrayList<ComCscQueryParameter>();
        }
        return this.comCscQueryParameter;
    }

    /**
     * Gets the value of the comCscCacheDataInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscCacheDataInd() {
        return comCscCacheDataInd;
    }

    /**
     * Sets the value of the comCscCacheDataInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscCacheDataInd(Boolean value) {
        this.comCscCacheDataInd = value;
    }

    /**
     * Gets the value of the comCscReadCacheInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscReadCacheInd() {
        return comCscReadCacheInd;
    }

    /**
     * Sets the value of the comCscReadCacheInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscReadCacheInd(Boolean value) {
        this.comCscReadCacheInd = value;
    }

    /**
     * Gets the value of the comCscResultSet property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscResultSet }
     *     
     */
    public ComCscResultSet getComCscResultSet() {
        return comCscResultSet;
    }

    /**
     * Sets the value of the comCscResultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscResultSet }
     *     
     */
    public void setComCscResultSet(ComCscResultSet value) {
        this.comCscResultSet = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
