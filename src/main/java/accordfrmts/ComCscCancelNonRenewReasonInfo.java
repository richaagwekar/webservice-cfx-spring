//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}CancelReasonCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NonRenewalInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PriorityNbr" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="IdRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "cancelReasonCd",
    "comCscNonRenewalInd",
    "comCscPriorityNbr"
})
@XmlRootElement(name = "com.csc_CancelNonRenewReasonInfo")
public class ComCscCancelNonRenewReasonInfo {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "CancelReasonCd")
    protected CancellationReason cancelReasonCd;
    @XmlElement(name = "com.csc_NonRenewalInd")
    protected Boolean comCscNonRenewalInd;
    @XmlElement(name = "com.csc_PriorityNbr")
    protected String comCscPriorityNbr;
    @XmlAttribute
    @XmlSchemaType(name = "anySimpleType")
    protected String id;
    @XmlAttribute(name = "IdRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object idRef;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the cancelReasonCd property.
     * 
     * @return
     *     possible object is
     *     {@link CancellationReason }
     *     
     */
    public CancellationReason getCancelReasonCd() {
        return cancelReasonCd;
    }

    /**
     * Sets the value of the cancelReasonCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancellationReason }
     *     
     */
    public void setCancelReasonCd(CancellationReason value) {
        this.cancelReasonCd = value;
    }

    /**
     * Gets the value of the comCscNonRenewalInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscNonRenewalInd() {
        return comCscNonRenewalInd;
    }

    /**
     * Sets the value of the comCscNonRenewalInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscNonRenewalInd(Boolean value) {
        this.comCscNonRenewalInd = value;
    }

    /**
     * Gets the value of the comCscPriorityNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPriorityNbr() {
        return comCscPriorityNbr;
    }

    /**
     * Sets the value of the comCscPriorityNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPriorityNbr(String value) {
        this.comCscPriorityNbr = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the idRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdRef() {
        return idRef;
    }

    /**
     * Sets the value of the idRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdRef(Object value) {
        this.idRef = value;
    }

}
