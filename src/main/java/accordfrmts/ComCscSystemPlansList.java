//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}Description" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillTypeDesc" minOccurs="0"/>
 *         &lt;element ref="{}BillFrequencyCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillFrequencyDesc" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "description",
    "comCscBillTypeCd",
    "comCscBillTypeDesc",
    "billFrequencyCd",
    "comCscBillFrequencyDesc"
})
@XmlRootElement(name = "com.csc_SystemPlansList")
public class ComCscSystemPlansList {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "com.csc_BillTypeCd")
    protected String comCscBillTypeCd;
    @XmlElement(name = "com.csc_BillTypeDesc")
    protected String comCscBillTypeDesc;
    @XmlElement(name = "BillFrequencyCd")
    protected Frequency billFrequencyCd;
    @XmlElement(name = "com.csc_BillFrequencyDesc")
    protected String comCscBillFrequencyDesc;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the comCscBillTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillTypeCd() {
        return comCscBillTypeCd;
    }

    /**
     * Sets the value of the comCscBillTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillTypeCd(String value) {
        this.comCscBillTypeCd = value;
    }

    /**
     * Gets the value of the comCscBillTypeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillTypeDesc() {
        return comCscBillTypeDesc;
    }

    /**
     * Sets the value of the comCscBillTypeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillTypeDesc(String value) {
        this.comCscBillTypeDesc = value;
    }

    /**
     * Gets the value of the billFrequencyCd property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getBillFrequencyCd() {
        return billFrequencyCd;
    }

    /**
     * Sets the value of the billFrequencyCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setBillFrequencyCd(Frequency value) {
        this.billFrequencyCd = value;
    }

    /**
     * Gets the value of the comCscBillFrequencyDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillFrequencyDesc() {
        return comCscBillFrequencyDesc;
    }

    /**
     * Sets the value of the comCscBillFrequencyDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillFrequencyDesc(String value) {
        this.comCscBillFrequencyDesc = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
