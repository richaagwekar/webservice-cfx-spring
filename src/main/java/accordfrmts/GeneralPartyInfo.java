//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}NameInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}Addr" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}PreferredContact" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}Communications" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_License" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditScoreInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientTypeCd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nameInfo",
    "addr",
    "preferredContact",
    "communications",
    "comCscLicense",
    "comCscCreditScoreInfo",
    "comCscClientRelation",
    "comCscItemIdInfo",
    "comCscActionInfo",
    "comCscClientTypeCd"
})
@XmlRootElement(name = "GeneralPartyInfo")
public class GeneralPartyInfo {

    @XmlElement(name = "NameInfo")
    protected List<NameInfo> nameInfo;
    @XmlElement(name = "Addr")
    protected List<Addr> addr;
    @XmlElement(name = "PreferredContact")
    protected List<PreferredContact> preferredContact;
    @XmlElement(name = "Communications")
    protected Communications communications;
    @XmlElement(name = "com.csc_License")
    protected List<ComCscLicense> comCscLicense;
    @XmlElement(name = "com.csc_CreditScoreInfo")
    protected ComCscCreditScoreInfo comCscCreditScoreInfo;
    @XmlElement(name = "com.csc_ClientRelation")
    protected List<ComCscClientRelation> comCscClientRelation;
    @XmlElement(name = "com.csc_ItemIdInfo")
    protected ComCscItemIdInfo comCscItemIdInfo;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlElement(name = "com.csc_ClientTypeCd")
    protected String comCscClientTypeCd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the nameInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameInfo }
     * 
     * 
     */
    public List<NameInfo> getNameInfo() {
        if (nameInfo == null) {
            nameInfo = new ArrayList<NameInfo>();
        }
        return this.nameInfo;
    }

    /**
     * Gets the value of the addr property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addr property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddr().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Addr }
     * 
     * 
     */
    public List<Addr> getAddr() {
        if (addr == null) {
            addr = new ArrayList<Addr>();
        }
        return this.addr;
    }

    /**
     * Gets the value of the preferredContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preferredContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreferredContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PreferredContact }
     * 
     * 
     */
    public List<PreferredContact> getPreferredContact() {
        if (preferredContact == null) {
            preferredContact = new ArrayList<PreferredContact>();
        }
        return this.preferredContact;
    }

    /**
     * Gets the value of the communications property.
     * 
     * @return
     *     possible object is
     *     {@link Communications }
     *     
     */
    public Communications getCommunications() {
        return communications;
    }

    /**
     * Sets the value of the communications property.
     * 
     * @param value
     *     allowed object is
     *     {@link Communications }
     *     
     */
    public void setCommunications(Communications value) {
        this.communications = value;
    }

    /**
     * Gets the value of the comCscLicense property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscLicense property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscLicense().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscLicense }
     * 
     * 
     */
    public List<ComCscLicense> getComCscLicense() {
        if (comCscLicense == null) {
            comCscLicense = new ArrayList<ComCscLicense>();
        }
        return this.comCscLicense;
    }

    /**
     * Gets the value of the comCscCreditScoreInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscCreditScoreInfo }
     *     
     */
    public ComCscCreditScoreInfo getComCscCreditScoreInfo() {
        return comCscCreditScoreInfo;
    }

    /**
     * Sets the value of the comCscCreditScoreInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscCreditScoreInfo }
     *     
     */
    public void setComCscCreditScoreInfo(ComCscCreditScoreInfo value) {
        this.comCscCreditScoreInfo = value;
    }

    /**
     * Gets the value of the comCscClientRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientRelation }
     * 
     * 
     */
    public List<ComCscClientRelation> getComCscClientRelation() {
        if (comCscClientRelation == null) {
            comCscClientRelation = new ArrayList<ComCscClientRelation>();
        }
        return this.comCscClientRelation;
    }

    /**
     * Gets the value of the comCscItemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public ComCscItemIdInfo getComCscItemIdInfo() {
        return comCscItemIdInfo;
    }

    /**
     * Sets the value of the comCscItemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public void setComCscItemIdInfo(ComCscItemIdInfo value) {
        this.comCscItemIdInfo = value;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the comCscClientTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscClientTypeCd() {
        return comCscClientTypeCd;
    }

    /**
     * Sets the value of the comCscClientTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscClientTypeCd(String value) {
        this.comCscClientTypeCd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
