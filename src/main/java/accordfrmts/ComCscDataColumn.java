//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_ColumnSequenceNbr" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ColumnName" minOccurs="0"/>
 *         &lt;element ref="{}DataTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}DataTypeLength" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Scale" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Value" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscColumnSequenceNbr",
    "comCscColumnName",
    "dataTypeCd",
    "dataTypeLength",
    "comCscScale",
    "comCscValue"
})
@XmlRootElement(name = "com.csc_DataColumn")
public class ComCscDataColumn {

    @XmlElement(name = "com.csc_ColumnSequenceNbr")
    protected String comCscColumnSequenceNbr;
    @XmlElement(name = "com.csc_ColumnName")
    protected String comCscColumnName;
    @XmlElement(name = "DataTypeCd")
    protected String dataTypeCd;
    @XmlElement(name = "DataTypeLength")
    protected String dataTypeLength;
    @XmlElement(name = "com.csc_Scale")
    protected String comCscScale;
    @XmlElement(name = "com.csc_Value")
    protected String comCscValue;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscColumnSequenceNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscColumnSequenceNbr() {
        return comCscColumnSequenceNbr;
    }

    /**
     * Sets the value of the comCscColumnSequenceNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscColumnSequenceNbr(String value) {
        this.comCscColumnSequenceNbr = value;
    }

    /**
     * Gets the value of the comCscColumnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscColumnName() {
        return comCscColumnName;
    }

    /**
     * Sets the value of the comCscColumnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscColumnName(String value) {
        this.comCscColumnName = value;
    }

    /**
     * Gets the value of the dataTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeCd() {
        return dataTypeCd;
    }

    /**
     * Sets the value of the dataTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeCd(String value) {
        this.dataTypeCd = value;
    }

    /**
     * Gets the value of the dataTypeLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeLength() {
        return dataTypeLength;
    }

    /**
     * Sets the value of the dataTypeLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeLength(String value) {
        this.dataTypeLength = value;
    }

    /**
     * Gets the value of the comCscScale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscScale() {
        return comCscScale;
    }

    /**
     * Sets the value of the comCscScale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscScale(String value) {
        this.comCscScale = value;
    }

    /**
     * Gets the value of the comCscValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscValue() {
        return comCscValue;
    }

    /**
     * Sets the value of the comCscValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscValue(String value) {
        this.comCscValue = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
