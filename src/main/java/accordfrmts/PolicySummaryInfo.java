//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}PolicyNumber" minOccurs="0"/>
 *         &lt;element ref="{}FullTermAmt" minOccurs="0"/>
 *         &lt;element ref="{}PolicyStatusCd"/>
 *         &lt;element ref="{}com.csc_LastAcyDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TransAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReasonAmendedCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TransactionStatusDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DiscountedTermAmt" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "policyNumber",
    "fullTermAmt",
    "policyStatusCd",
    "comCscLastAcyDt",
    "comCscTransAmt",
    "comCscReasonAmendedCd",
    "comCscTransactionStatusDesc",
    "comCscDiscountedTermAmt"
})
@XmlRootElement(name = "PolicySummaryInfo")
public class PolicySummaryInfo {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "PolicyNumber")
    protected String policyNumber;
    @XmlElement(name = "FullTermAmt")
    protected BigDecimal fullTermAmt;
    @XmlElement(name = "PolicyStatusCd", required = true)
    protected String policyStatusCd;
    @XmlElement(name = "com.csc_LastAcyDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscLastAcyDt;
    @XmlElement(name = "com.csc_TransAmt")
    protected BigDecimal comCscTransAmt;
    @XmlElement(name = "com.csc_ReasonAmendedCd")
    protected String comCscReasonAmendedCd;
    @XmlElement(name = "com.csc_TransactionStatusDesc")
    protected String comCscTransactionStatusDesc;
    @XmlElement(name = "com.csc_DiscountedTermAmt")
    protected BigDecimal comCscDiscountedTermAmt;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the fullTermAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFullTermAmt() {
        return fullTermAmt;
    }

    /**
     * Sets the value of the fullTermAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFullTermAmt(BigDecimal value) {
        this.fullTermAmt = value;
    }

    /**
     * Gets the value of the policyStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyStatusCd() {
        return policyStatusCd;
    }

    /**
     * Sets the value of the policyStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyStatusCd(String value) {
        this.policyStatusCd = value;
    }

    /**
     * Gets the value of the comCscLastAcyDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscLastAcyDt() {
        return comCscLastAcyDt;
    }

    /**
     * Sets the value of the comCscLastAcyDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscLastAcyDt(XMLGregorianCalendar value) {
        this.comCscLastAcyDt = value;
    }

    /**
     * Gets the value of the comCscTransAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTransAmt() {
        return comCscTransAmt;
    }

    /**
     * Sets the value of the comCscTransAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTransAmt(BigDecimal value) {
        this.comCscTransAmt = value;
    }

    /**
     * Gets the value of the comCscReasonAmendedCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReasonAmendedCd() {
        return comCscReasonAmendedCd;
    }

    /**
     * Sets the value of the comCscReasonAmendedCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReasonAmendedCd(String value) {
        this.comCscReasonAmendedCd = value;
    }

    /**
     * Gets the value of the comCscTransactionStatusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscTransactionStatusDesc() {
        return comCscTransactionStatusDesc;
    }

    /**
     * Sets the value of the comCscTransactionStatusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscTransactionStatusDesc(String value) {
        this.comCscTransactionStatusDesc = value;
    }

    /**
     * Gets the value of the comCscDiscountedTermAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscDiscountedTermAmt() {
        return comCscDiscountedTermAmt;
    }

    /**
     * Sets the value of the comCscDiscountedTermAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscDiscountedTermAmt(BigDecimal value) {
        this.comCscDiscountedTermAmt = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
