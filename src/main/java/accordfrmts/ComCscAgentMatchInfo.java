//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_NumDaysAgentPaymentDue" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NumDaysAgentReturnDue" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_MatchIdentificationCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_MatchToleranceAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DetailMatchTolerance" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscNumDaysAgentPaymentDue",
    "comCscNumDaysAgentReturnDue",
    "comCscMatchIdentificationCd",
    "comCscMatchToleranceAmt",
    "comCscDetailMatchTolerance"
})
@XmlRootElement(name = "com.csc_AgentMatchInfo")
public class ComCscAgentMatchInfo {

    @XmlElement(name = "com.csc_NumDaysAgentPaymentDue")
    protected BigInteger comCscNumDaysAgentPaymentDue;
    @XmlElement(name = "com.csc_NumDaysAgentReturnDue")
    protected BigInteger comCscNumDaysAgentReturnDue;
    @XmlElement(name = "com.csc_MatchIdentificationCd")
    protected String comCscMatchIdentificationCd;
    @XmlElement(name = "com.csc_MatchToleranceAmt")
    protected BigDecimal comCscMatchToleranceAmt;
    @XmlElement(name = "com.csc_DetailMatchTolerance")
    protected String comCscDetailMatchTolerance;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscNumDaysAgentPaymentDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComCscNumDaysAgentPaymentDue() {
        return comCscNumDaysAgentPaymentDue;
    }

    /**
     * Sets the value of the comCscNumDaysAgentPaymentDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComCscNumDaysAgentPaymentDue(BigInteger value) {
        this.comCscNumDaysAgentPaymentDue = value;
    }

    /**
     * Gets the value of the comCscNumDaysAgentReturnDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComCscNumDaysAgentReturnDue() {
        return comCscNumDaysAgentReturnDue;
    }

    /**
     * Sets the value of the comCscNumDaysAgentReturnDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComCscNumDaysAgentReturnDue(BigInteger value) {
        this.comCscNumDaysAgentReturnDue = value;
    }

    /**
     * Gets the value of the comCscMatchIdentificationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscMatchIdentificationCd() {
        return comCscMatchIdentificationCd;
    }

    /**
     * Sets the value of the comCscMatchIdentificationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscMatchIdentificationCd(String value) {
        this.comCscMatchIdentificationCd = value;
    }

    /**
     * Gets the value of the comCscMatchToleranceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscMatchToleranceAmt() {
        return comCscMatchToleranceAmt;
    }

    /**
     * Sets the value of the comCscMatchToleranceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscMatchToleranceAmt(BigDecimal value) {
        this.comCscMatchToleranceAmt = value;
    }

    /**
     * Gets the value of the comCscDetailMatchTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDetailMatchTolerance() {
        return comCscDetailMatchTolerance;
    }

    /**
     * Sets the value of the comCscDetailMatchTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDetailMatchTolerance(String value) {
        this.comCscDetailMatchTolerance = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
