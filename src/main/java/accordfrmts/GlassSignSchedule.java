//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}ScheduleTypeCd"/>
 *         &lt;element ref="{}NumMotelRooms" minOccurs="0"/>
 *         &lt;element ref="{}NumPlates" minOccurs="0"/>
 *         &lt;element ref="{}PlateSizeLength" minOccurs="0"/>
 *         &lt;element ref="{}PlateSizeWidth" minOccurs="0"/>
 *         &lt;element ref="{}PlateSizeArea" minOccurs="0"/>
 *         &lt;element ref="{}SafetyGlassInd" minOccurs="0"/>
 *         &lt;element ref="{}KindGlassClassCd" minOccurs="0"/>
 *         &lt;element ref="{}GlassPositionAndUseInBldgCd" minOccurs="0"/>
 *         &lt;element ref="{}Limit" minOccurs="0"/>
 *         &lt;element ref="{}ItemDesc" minOccurs="0"/>
 *         &lt;element ref="{}UseAndPositionInBldgDesc" minOccurs="0"/>
 *         &lt;element ref="{}NumPlatesLargePlateLossPayment" minOccurs="0"/>
 *         &lt;element ref="{}InsideOutsideSignCd" minOccurs="0"/>
 *         &lt;element ref="{}GlassBldgInteriorInd" minOccurs="0"/>
 *         &lt;element ref="{}GlassTenantsExteriorInd" minOccurs="0"/>
 *         &lt;element ref="{}Length" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "scheduleTypeCd",
    "numMotelRooms",
    "numPlates",
    "plateSizeLength",
    "plateSizeWidth",
    "plateSizeArea",
    "safetyGlassInd",
    "kindGlassClassCd",
    "glassPositionAndUseInBldgCd",
    "limit",
    "itemDesc",
    "useAndPositionInBldgDesc",
    "numPlatesLargePlateLossPayment",
    "insideOutsideSignCd",
    "glassBldgInteriorInd",
    "glassTenantsExteriorInd",
    "length"
})
@XmlRootElement(name = "GlassSignSchedule")
public class GlassSignSchedule {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "ScheduleTypeCd", required = true)
    protected String scheduleTypeCd;
    @XmlElement(name = "NumMotelRooms")
    protected BigInteger numMotelRooms;
    @XmlElement(name = "NumPlates")
    protected BigInteger numPlates;
    @XmlElement(name = "PlateSizeLength")
    protected String plateSizeLength;
    @XmlElement(name = "PlateSizeWidth")
    protected String plateSizeWidth;
    @XmlElement(name = "PlateSizeArea")
    protected String plateSizeArea;
    @XmlElement(name = "SafetyGlassInd")
    protected Boolean safetyGlassInd;
    @XmlElement(name = "KindGlassClassCd")
    protected Boolean kindGlassClassCd;
    @XmlElement(name = "GlassPositionAndUseInBldgCd")
    protected String glassPositionAndUseInBldgCd;
    @XmlElement(name = "Limit")
    protected Limit limit;
    @XmlElement(name = "ItemDesc")
    protected String itemDesc;
    @XmlElement(name = "UseAndPositionInBldgDesc")
    protected String useAndPositionInBldgDesc;
    @XmlElement(name = "NumPlatesLargePlateLossPayment")
    protected String numPlatesLargePlateLossPayment;
    @XmlElement(name = "InsideOutsideSignCd")
    protected String insideOutsideSignCd;
    @XmlElement(name = "GlassBldgInteriorInd")
    protected Boolean glassBldgInteriorInd;
    @XmlElement(name = "GlassTenantsExteriorInd")
    protected Boolean glassTenantsExteriorInd;
    @XmlElement(name = "Length")
    protected String length;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the scheduleTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleTypeCd() {
        return scheduleTypeCd;
    }

    /**
     * Sets the value of the scheduleTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleTypeCd(String value) {
        this.scheduleTypeCd = value;
    }

    /**
     * Gets the value of the numMotelRooms property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumMotelRooms() {
        return numMotelRooms;
    }

    /**
     * Sets the value of the numMotelRooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumMotelRooms(BigInteger value) {
        this.numMotelRooms = value;
    }

    /**
     * Gets the value of the numPlates property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumPlates() {
        return numPlates;
    }

    /**
     * Sets the value of the numPlates property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumPlates(BigInteger value) {
        this.numPlates = value;
    }

    /**
     * Gets the value of the plateSizeLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateSizeLength() {
        return plateSizeLength;
    }

    /**
     * Sets the value of the plateSizeLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateSizeLength(String value) {
        this.plateSizeLength = value;
    }

    /**
     * Gets the value of the plateSizeWidth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateSizeWidth() {
        return plateSizeWidth;
    }

    /**
     * Sets the value of the plateSizeWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateSizeWidth(String value) {
        this.plateSizeWidth = value;
    }

    /**
     * Gets the value of the plateSizeArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateSizeArea() {
        return plateSizeArea;
    }

    /**
     * Sets the value of the plateSizeArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateSizeArea(String value) {
        this.plateSizeArea = value;
    }

    /**
     * Gets the value of the safetyGlassInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSafetyGlassInd() {
        return safetyGlassInd;
    }

    /**
     * Sets the value of the safetyGlassInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSafetyGlassInd(Boolean value) {
        this.safetyGlassInd = value;
    }

    /**
     * Gets the value of the kindGlassClassCd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKindGlassClassCd() {
        return kindGlassClassCd;
    }

    /**
     * Sets the value of the kindGlassClassCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKindGlassClassCd(Boolean value) {
        this.kindGlassClassCd = value;
    }

    /**
     * Gets the value of the glassPositionAndUseInBldgCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlassPositionAndUseInBldgCd() {
        return glassPositionAndUseInBldgCd;
    }

    /**
     * Sets the value of the glassPositionAndUseInBldgCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlassPositionAndUseInBldgCd(String value) {
        this.glassPositionAndUseInBldgCd = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link Limit }
     *     
     */
    public Limit getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Limit }
     *     
     */
    public void setLimit(Limit value) {
        this.limit = value;
    }

    /**
     * Gets the value of the itemDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Sets the value of the itemDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDesc(String value) {
        this.itemDesc = value;
    }

    /**
     * Gets the value of the useAndPositionInBldgDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseAndPositionInBldgDesc() {
        return useAndPositionInBldgDesc;
    }

    /**
     * Sets the value of the useAndPositionInBldgDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseAndPositionInBldgDesc(String value) {
        this.useAndPositionInBldgDesc = value;
    }

    /**
     * Gets the value of the numPlatesLargePlateLossPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumPlatesLargePlateLossPayment() {
        return numPlatesLargePlateLossPayment;
    }

    /**
     * Sets the value of the numPlatesLargePlateLossPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumPlatesLargePlateLossPayment(String value) {
        this.numPlatesLargePlateLossPayment = value;
    }

    /**
     * Gets the value of the insideOutsideSignCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsideOutsideSignCd() {
        return insideOutsideSignCd;
    }

    /**
     * Sets the value of the insideOutsideSignCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsideOutsideSignCd(String value) {
        this.insideOutsideSignCd = value;
    }

    /**
     * Gets the value of the glassBldgInteriorInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGlassBldgInteriorInd() {
        return glassBldgInteriorInd;
    }

    /**
     * Sets the value of the glassBldgInteriorInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGlassBldgInteriorInd(Boolean value) {
        this.glassBldgInteriorInd = value;
    }

    /**
     * Gets the value of the glassTenantsExteriorInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGlassTenantsExteriorInd() {
        return glassTenantsExteriorInd;
    }

    /**
     * Sets the value of the glassTenantsExteriorInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGlassTenantsExteriorInd(Boolean value) {
        this.glassTenantsExteriorInd = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLength(String value) {
        this.length = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
