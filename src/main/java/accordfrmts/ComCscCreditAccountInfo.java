//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}MiscParty" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_GrantorSubscriberNbr" minOccurs="0"/>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}AccountNumberId" minOccurs="0"/>
 *         &lt;element ref="{}AcctTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountBalanceAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountOpenedDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountClosedDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_HighCredit" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PastDueAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}InstallmentAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LengthTimeAccountReview" minOccurs="0"/>
 *         &lt;element ref="{}RemarkText" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "miscParty",
    "comCscGrantorSubscriberNbr",
    "otherIdentifier",
    "accountNumberId",
    "acctTypeCd",
    "comCscAccountBalanceAmt",
    "comCscAccountOpenedDt",
    "comCscAccountClosedDt",
    "comCscHighCredit",
    "comCscPastDueAmt",
    "comCscCreditTypeCd",
    "installmentAmt",
    "comCscLengthTimeAccountReview",
    "remarkText"
})
@XmlRootElement(name = "com.csc_CreditAccountInfo")
public class ComCscCreditAccountInfo {

    @XmlElement(name = "MiscParty")
    protected MiscParty miscParty;
    @XmlElement(name = "com.csc_GrantorSubscriberNbr")
    protected String comCscGrantorSubscriberNbr;
    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "AccountNumberId")
    protected String accountNumberId;
    @XmlElement(name = "AcctTypeCd")
    protected MethodOfPayment acctTypeCd;
    @XmlElement(name = "com.csc_AccountBalanceAmt")
    protected String comCscAccountBalanceAmt;
    @XmlElement(name = "com.csc_AccountOpenedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscAccountOpenedDt;
    @XmlElement(name = "com.csc_AccountClosedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscAccountClosedDt;
    @XmlElement(name = "com.csc_HighCredit")
    protected BigDecimal comCscHighCredit;
    @XmlElement(name = "com.csc_PastDueAmt")
    protected BigDecimal comCscPastDueAmt;
    @XmlElement(name = "com.csc_CreditTypeCd")
    protected String comCscCreditTypeCd;
    @XmlElement(name = "InstallmentAmt")
    protected BigDecimal installmentAmt;
    @XmlElement(name = "com.csc_LengthTimeAccountReview")
    protected String comCscLengthTimeAccountReview;
    @XmlElement(name = "RemarkText")
    protected String remarkText;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the miscParty property.
     * 
     * @return
     *     possible object is
     *     {@link MiscParty }
     *     
     */
    public MiscParty getMiscParty() {
        return miscParty;
    }

    /**
     * Sets the value of the miscParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link MiscParty }
     *     
     */
    public void setMiscParty(MiscParty value) {
        this.miscParty = value;
    }

    /**
     * Gets the value of the comCscGrantorSubscriberNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscGrantorSubscriberNbr() {
        return comCscGrantorSubscriberNbr;
    }

    /**
     * Sets the value of the comCscGrantorSubscriberNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscGrantorSubscriberNbr(String value) {
        this.comCscGrantorSubscriberNbr = value;
    }

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the accountNumberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumberId() {
        return accountNumberId;
    }

    /**
     * Sets the value of the accountNumberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumberId(String value) {
        this.accountNumberId = value;
    }

    /**
     * Gets the value of the acctTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link MethodOfPayment }
     *     
     */
    public MethodOfPayment getAcctTypeCd() {
        return acctTypeCd;
    }

    /**
     * Sets the value of the acctTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link MethodOfPayment }
     *     
     */
    public void setAcctTypeCd(MethodOfPayment value) {
        this.acctTypeCd = value;
    }

    /**
     * Gets the value of the comCscAccountBalanceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAccountBalanceAmt() {
        return comCscAccountBalanceAmt;
    }

    /**
     * Sets the value of the comCscAccountBalanceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAccountBalanceAmt(String value) {
        this.comCscAccountBalanceAmt = value;
    }

    /**
     * Gets the value of the comCscAccountOpenedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscAccountOpenedDt() {
        return comCscAccountOpenedDt;
    }

    /**
     * Sets the value of the comCscAccountOpenedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscAccountOpenedDt(XMLGregorianCalendar value) {
        this.comCscAccountOpenedDt = value;
    }

    /**
     * Gets the value of the comCscAccountClosedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscAccountClosedDt() {
        return comCscAccountClosedDt;
    }

    /**
     * Sets the value of the comCscAccountClosedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscAccountClosedDt(XMLGregorianCalendar value) {
        this.comCscAccountClosedDt = value;
    }

    /**
     * Gets the value of the comCscHighCredit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscHighCredit() {
        return comCscHighCredit;
    }

    /**
     * Sets the value of the comCscHighCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscHighCredit(BigDecimal value) {
        this.comCscHighCredit = value;
    }

    /**
     * Gets the value of the comCscPastDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscPastDueAmt() {
        return comCscPastDueAmt;
    }

    /**
     * Sets the value of the comCscPastDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscPastDueAmt(BigDecimal value) {
        this.comCscPastDueAmt = value;
    }

    /**
     * Gets the value of the comCscCreditTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCreditTypeCd() {
        return comCscCreditTypeCd;
    }

    /**
     * Sets the value of the comCscCreditTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCreditTypeCd(String value) {
        this.comCscCreditTypeCd = value;
    }

    /**
     * Gets the value of the installmentAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInstallmentAmt() {
        return installmentAmt;
    }

    /**
     * Sets the value of the installmentAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInstallmentAmt(BigDecimal value) {
        this.installmentAmt = value;
    }

    /**
     * Gets the value of the comCscLengthTimeAccountReview property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscLengthTimeAccountReview() {
        return comCscLengthTimeAccountReview;
    }

    /**
     * Sets the value of the comCscLengthTimeAccountReview property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscLengthTimeAccountReview(String value) {
        this.comCscLengthTimeAccountReview = value;
    }

    /**
     * Gets the value of the remarkText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkText() {
        return remarkText;
    }

    /**
     * Sets the value of the remarkText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkText(String value) {
        this.remarkText = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
