//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActivityType" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ListPositionInd" minOccurs="0"/>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptsPostedDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DistributionSequenceNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptSuspenseAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptTypeDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DispositionTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReceiptDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CashEntryMethodCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CashEntryMethodDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EntryDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EntryTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionCd",
    "comCscActivityType",
    "comCscListPositionInd",
    "otherIdentifier",
    "comCscReceiptsPostedDt",
    "comCscDistributionSequenceNumber",
    "comCscReceiptAmt",
    "comCscReceiptSuspenseAmt",
    "comCscReceiptTypeCd",
    "comCscReceiptTypeDesc",
    "comCscDispositionTypeCd",
    "comCscReceiptDesc",
    "comCscCashEntryMethodCd",
    "comCscCashEntryMethodDesc",
    "comCscEntryDt",
    "comCscEntryTime"
})
@XmlRootElement(name = "com.csc_BillCashReceipts")
public class ComCscBillCashReceipts {

    @XmlElement(name = "ActionCd")
    protected Action actionCd;
    @XmlElement(name = "com.csc_ActivityType")
    protected String comCscActivityType;
    @XmlElement(name = "com.csc_ListPositionInd")
    protected Boolean comCscListPositionInd;
    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "com.csc_ReceiptsPostedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscReceiptsPostedDt;
    @XmlElement(name = "com.csc_DistributionSequenceNumber")
    protected String comCscDistributionSequenceNumber;
    @XmlElement(name = "com.csc_ReceiptAmt")
    protected BigDecimal comCscReceiptAmt;
    @XmlElement(name = "com.csc_ReceiptSuspenseAmt")
    protected BigDecimal comCscReceiptSuspenseAmt;
    @XmlElement(name = "com.csc_ReceiptTypeCd")
    protected String comCscReceiptTypeCd;
    @XmlElement(name = "com.csc_ReceiptTypeDesc")
    protected String comCscReceiptTypeDesc;
    @XmlElement(name = "com.csc_DispositionTypeCd")
    protected String comCscDispositionTypeCd;
    @XmlElement(name = "com.csc_ReceiptDesc")
    protected String comCscReceiptDesc;
    @XmlElement(name = "com.csc_CashEntryMethodCd")
    protected String comCscCashEntryMethodCd;
    @XmlElement(name = "com.csc_CashEntryMethodDesc")
    protected String comCscCashEntryMethodDesc;
    @XmlElement(name = "com.csc_EntryDt")
    protected String comCscEntryDt;
    @XmlElement(name = "com.csc_EntryTime")
    protected String comCscEntryTime;

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

    /**
     * Gets the value of the comCscActivityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscActivityType() {
        return comCscActivityType;
    }

    /**
     * Sets the value of the comCscActivityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscActivityType(String value) {
        this.comCscActivityType = value;
    }

    /**
     * Gets the value of the comCscListPositionInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscListPositionInd() {
        return comCscListPositionInd;
    }

    /**
     * Sets the value of the comCscListPositionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscListPositionInd(Boolean value) {
        this.comCscListPositionInd = value;
    }

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the comCscReceiptsPostedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscReceiptsPostedDt() {
        return comCscReceiptsPostedDt;
    }

    /**
     * Sets the value of the comCscReceiptsPostedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscReceiptsPostedDt(XMLGregorianCalendar value) {
        this.comCscReceiptsPostedDt = value;
    }

    /**
     * Gets the value of the comCscDistributionSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDistributionSequenceNumber() {
        return comCscDistributionSequenceNumber;
    }

    /**
     * Sets the value of the comCscDistributionSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDistributionSequenceNumber(String value) {
        this.comCscDistributionSequenceNumber = value;
    }

    /**
     * Gets the value of the comCscReceiptAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscReceiptAmt() {
        return comCscReceiptAmt;
    }

    /**
     * Sets the value of the comCscReceiptAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscReceiptAmt(BigDecimal value) {
        this.comCscReceiptAmt = value;
    }

    /**
     * Gets the value of the comCscReceiptSuspenseAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscReceiptSuspenseAmt() {
        return comCscReceiptSuspenseAmt;
    }

    /**
     * Sets the value of the comCscReceiptSuspenseAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscReceiptSuspenseAmt(BigDecimal value) {
        this.comCscReceiptSuspenseAmt = value;
    }

    /**
     * Gets the value of the comCscReceiptTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReceiptTypeCd() {
        return comCscReceiptTypeCd;
    }

    /**
     * Sets the value of the comCscReceiptTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReceiptTypeCd(String value) {
        this.comCscReceiptTypeCd = value;
    }

    /**
     * Gets the value of the comCscReceiptTypeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReceiptTypeDesc() {
        return comCscReceiptTypeDesc;
    }

    /**
     * Sets the value of the comCscReceiptTypeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReceiptTypeDesc(String value) {
        this.comCscReceiptTypeDesc = value;
    }

    /**
     * Gets the value of the comCscDispositionTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDispositionTypeCd() {
        return comCscDispositionTypeCd;
    }

    /**
     * Sets the value of the comCscDispositionTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDispositionTypeCd(String value) {
        this.comCscDispositionTypeCd = value;
    }

    /**
     * Gets the value of the comCscReceiptDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReceiptDesc() {
        return comCscReceiptDesc;
    }

    /**
     * Sets the value of the comCscReceiptDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReceiptDesc(String value) {
        this.comCscReceiptDesc = value;
    }

    /**
     * Gets the value of the comCscCashEntryMethodCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCashEntryMethodCd() {
        return comCscCashEntryMethodCd;
    }

    /**
     * Sets the value of the comCscCashEntryMethodCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCashEntryMethodCd(String value) {
        this.comCscCashEntryMethodCd = value;
    }

    /**
     * Gets the value of the comCscCashEntryMethodDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCashEntryMethodDesc() {
        return comCscCashEntryMethodDesc;
    }

    /**
     * Sets the value of the comCscCashEntryMethodDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCashEntryMethodDesc(String value) {
        this.comCscCashEntryMethodDesc = value;
    }

    /**
     * Gets the value of the comCscEntryDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryDt() {
        return comCscEntryDt;
    }

    /**
     * Sets the value of the comCscEntryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryDt(String value) {
        this.comCscEntryDt = value;
    }

    /**
     * Gets the value of the comCscEntryTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryTime() {
        return comCscEntryTime;
    }

    /**
     * Sets the value of the comCscEntryTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryTime(String value) {
        this.comCscEntryTime = value;
    }

}
