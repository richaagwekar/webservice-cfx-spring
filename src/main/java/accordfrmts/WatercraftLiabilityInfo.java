//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}NumWatercraftOwned" minOccurs="0"/>
 *         &lt;element ref="{}Length" minOccurs="0"/>
 *         &lt;element ref="{}Horsepower" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="OtherOrPriorPolicyRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *       &lt;attribute name="LocationRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *       &lt;attribute name="SubLocationRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numWatercraftOwned",
    "length",
    "horsepower"
})
@XmlRootElement(name = "WatercraftLiabilityInfo")
public class WatercraftLiabilityInfo {

    @XmlElement(name = "NumWatercraftOwned")
    protected String numWatercraftOwned;
    @XmlElement(name = "Length")
    protected String length;
    @XmlElement(name = "Horsepower")
    protected String horsepower;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "OtherOrPriorPolicyRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object otherOrPriorPolicyRef;
    @XmlAttribute(name = "LocationRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object locationRef;
    @XmlAttribute(name = "SubLocationRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object subLocationRef;

    /**
     * Gets the value of the numWatercraftOwned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumWatercraftOwned() {
        return numWatercraftOwned;
    }

    /**
     * Sets the value of the numWatercraftOwned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumWatercraftOwned(String value) {
        this.numWatercraftOwned = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLength(String value) {
        this.length = value;
    }

    /**
     * Gets the value of the horsepower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHorsepower() {
        return horsepower;
    }

    /**
     * Sets the value of the horsepower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHorsepower(String value) {
        this.horsepower = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the otherOrPriorPolicyRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getOtherOrPriorPolicyRef() {
        return otherOrPriorPolicyRef;
    }

    /**
     * Sets the value of the otherOrPriorPolicyRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOtherOrPriorPolicyRef(Object value) {
        this.otherOrPriorPolicyRef = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setLocationRef(Object value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the subLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSubLocationRef() {
        return subLocationRef;
    }

    /**
     * Sets the value of the subLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSubLocationRef(Object value) {
        this.subLocationRef = value;
    }

}
