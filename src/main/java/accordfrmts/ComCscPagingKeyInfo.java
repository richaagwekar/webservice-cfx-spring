//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisbursementTypeCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisbursementLocationCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisbursementPrintLocationCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BankNumber" minOccurs="0"/>
 *         &lt;element ref="{}AccountNumberId" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_MediumCd" minOccurs="0"/>
 *         &lt;element ref="{}CheckNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BeginCheckNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EndCheckNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BeginDtTime" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EndDtTime" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActivityDtTime" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisbursementStatusCd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "comCscDisbursementTypeCd",
    "effectiveDt",
    "comCscDisbursementLocationCd",
    "comCscDisbursementPrintLocationCd",
    "comCscBankNumber",
    "accountNumberId",
    "comCscMediumCd",
    "checkNumber",
    "comCscBeginCheckNumber",
    "comCscEndCheckNumber",
    "comCscBeginDtTime",
    "comCscEndDtTime",
    "comCscActivityDtTime",
    "comCscDisbursementStatusCd"
})
@XmlRootElement(name = "com.csc_PagingKeyInfo")
public class ComCscPagingKeyInfo {

    @XmlElement(name = "ItemIdInfo")
    protected List<ItemIdInfo> itemIdInfo;
    @XmlElement(name = "com.csc_DisbursementTypeCd")
    protected List<String> comCscDisbursementTypeCd;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "com.csc_DisbursementLocationCd")
    protected String comCscDisbursementLocationCd;
    @XmlElement(name = "com.csc_DisbursementPrintLocationCd")
    protected String comCscDisbursementPrintLocationCd;
    @XmlElement(name = "com.csc_BankNumber")
    protected String comCscBankNumber;
    @XmlElement(name = "AccountNumberId")
    protected String accountNumberId;
    @XmlElement(name = "com.csc_MediumCd")
    protected String comCscMediumCd;
    @XmlElement(name = "CheckNumber")
    protected String checkNumber;
    @XmlElement(name = "com.csc_BeginCheckNumber")
    protected String comCscBeginCheckNumber;
    @XmlElement(name = "com.csc_EndCheckNumber")
    protected String comCscEndCheckNumber;
    @XmlElement(name = "com.csc_BeginDtTime")
    protected Boolean comCscBeginDtTime;
    @XmlElement(name = "com.csc_EndDtTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar comCscEndDtTime;
    @XmlElement(name = "com.csc_ActivityDtTime")
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar comCscActivityDtTime;
    @XmlElement(name = "com.csc_DisbursementStatusCd")
    protected String comCscDisbursementStatusCd;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIdInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIdInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemIdInfo }
     * 
     * 
     */
    public List<ItemIdInfo> getItemIdInfo() {
        if (itemIdInfo == null) {
            itemIdInfo = new ArrayList<ItemIdInfo>();
        }
        return this.itemIdInfo;
    }

    /**
     * Gets the value of the comCscDisbursementTypeCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscDisbursementTypeCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscDisbursementTypeCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getComCscDisbursementTypeCd() {
        if (comCscDisbursementTypeCd == null) {
            comCscDisbursementTypeCd = new ArrayList<String>();
        }
        return this.comCscDisbursementTypeCd;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the comCscDisbursementLocationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDisbursementLocationCd() {
        return comCscDisbursementLocationCd;
    }

    /**
     * Sets the value of the comCscDisbursementLocationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDisbursementLocationCd(String value) {
        this.comCscDisbursementLocationCd = value;
    }

    /**
     * Gets the value of the comCscDisbursementPrintLocationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDisbursementPrintLocationCd() {
        return comCscDisbursementPrintLocationCd;
    }

    /**
     * Sets the value of the comCscDisbursementPrintLocationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDisbursementPrintLocationCd(String value) {
        this.comCscDisbursementPrintLocationCd = value;
    }

    /**
     * Gets the value of the comCscBankNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBankNumber() {
        return comCscBankNumber;
    }

    /**
     * Sets the value of the comCscBankNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBankNumber(String value) {
        this.comCscBankNumber = value;
    }

    /**
     * Gets the value of the accountNumberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumberId() {
        return accountNumberId;
    }

    /**
     * Sets the value of the accountNumberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumberId(String value) {
        this.accountNumberId = value;
    }

    /**
     * Gets the value of the comCscMediumCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscMediumCd() {
        return comCscMediumCd;
    }

    /**
     * Sets the value of the comCscMediumCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscMediumCd(String value) {
        this.comCscMediumCd = value;
    }

    /**
     * Gets the value of the checkNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckNumber() {
        return checkNumber;
    }

    /**
     * Sets the value of the checkNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckNumber(String value) {
        this.checkNumber = value;
    }

    /**
     * Gets the value of the comCscBeginCheckNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBeginCheckNumber() {
        return comCscBeginCheckNumber;
    }

    /**
     * Sets the value of the comCscBeginCheckNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBeginCheckNumber(String value) {
        this.comCscBeginCheckNumber = value;
    }

    /**
     * Gets the value of the comCscEndCheckNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEndCheckNumber() {
        return comCscEndCheckNumber;
    }

    /**
     * Sets the value of the comCscEndCheckNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEndCheckNumber(String value) {
        this.comCscEndCheckNumber = value;
    }

    /**
     * Gets the value of the comCscBeginDtTime property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscBeginDtTime() {
        return comCscBeginDtTime;
    }

    /**
     * Sets the value of the comCscBeginDtTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscBeginDtTime(Boolean value) {
        this.comCscBeginDtTime = value;
    }

    /**
     * Gets the value of the comCscEndDtTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscEndDtTime() {
        return comCscEndDtTime;
    }

    /**
     * Sets the value of the comCscEndDtTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscEndDtTime(XMLGregorianCalendar value) {
        this.comCscEndDtTime = value;
    }

    /**
     * Gets the value of the comCscActivityDtTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscActivityDtTime() {
        return comCscActivityDtTime;
    }

    /**
     * Sets the value of the comCscActivityDtTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscActivityDtTime(XMLGregorianCalendar value) {
        this.comCscActivityDtTime = value;
    }

    /**
     * Gets the value of the comCscDisbursementStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscDisbursementStatusCd() {
        return comCscDisbursementStatusCd;
    }

    /**
     * Sets the value of the comCscDisbursementStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscDisbursementStatusCd(String value) {
        this.comCscDisbursementStatusCd = value;
    }

}
