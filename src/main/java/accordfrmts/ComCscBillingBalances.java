//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_TotalPastDueAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TotalCurrentDueAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TotalFutureDueAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountBalanceAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TotalSuspenseAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CurrentInvoiceDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CurrentInvoiceDueDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CurrentInvoiceDueAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NextInvoiceDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NextInvoiceDueDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NextInvoiceAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LastAdvanceNoticeDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LastDebitDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LastDebitAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PendingDebitDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PendingDebitAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FuturePendingDebitDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FuturePendingDebitAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TotalHoldSuspenseAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_UnMatchedAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CurrentAccountDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NextAccountDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditCardLastAdvanceNoticeDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditCardLastDebitDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditCardLastDebitAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditCardPendingDebitDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CreditCardPendingDebitAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CollectionAmt" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscTotalPastDueAmt",
    "comCscTotalCurrentDueAmt",
    "comCscTotalFutureDueAmt",
    "comCscAccountBalanceAmt",
    "comCscTotalSuspenseAmt",
    "comCscCurrentInvoiceDt",
    "comCscCurrentInvoiceDueDt",
    "comCscCurrentInvoiceDueAmt",
    "comCscNextInvoiceDt",
    "comCscNextInvoiceDueDt",
    "comCscNextInvoiceAmt",
    "comCscLastAdvanceNoticeDt",
    "comCscLastDebitDt",
    "comCscLastDebitAmt",
    "comCscPendingDebitDt",
    "comCscPendingDebitAmt",
    "comCscFuturePendingDebitDt",
    "comCscFuturePendingDebitAmt",
    "comCscTotalHoldSuspenseAmt",
    "comCscUnMatchedAmt",
    "comCscCurrentAccountDt",
    "comCscNextAccountDt",
    "comCscCreditCardLastAdvanceNoticeDt",
    "comCscCreditCardLastDebitDt",
    "comCscCreditCardLastDebitAmt",
    "comCscCreditCardPendingDebitDt",
    "comCscCreditCardPendingDebitAmt",
    "comCscCollectionAmt"
})
@XmlRootElement(name = "com.csc_BillingBalances")
public class ComCscBillingBalances {

    @XmlElement(name = "com.csc_TotalPastDueAmt")
    protected BigDecimal comCscTotalPastDueAmt;
    @XmlElement(name = "com.csc_TotalCurrentDueAmt")
    protected BigDecimal comCscTotalCurrentDueAmt;
    @XmlElement(name = "com.csc_TotalFutureDueAmt")
    protected BigDecimal comCscTotalFutureDueAmt;
    @XmlElement(name = "com.csc_AccountBalanceAmt")
    protected String comCscAccountBalanceAmt;
    @XmlElement(name = "com.csc_TotalSuspenseAmt")
    protected BigDecimal comCscTotalSuspenseAmt;
    @XmlElement(name = "com.csc_CurrentInvoiceDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCurrentInvoiceDt;
    @XmlElement(name = "com.csc_CurrentInvoiceDueDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCurrentInvoiceDueDt;
    @XmlElement(name = "com.csc_CurrentInvoiceDueAmt")
    protected BigDecimal comCscCurrentInvoiceDueAmt;
    @XmlElement(name = "com.csc_NextInvoiceDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscNextInvoiceDt;
    @XmlElement(name = "com.csc_NextInvoiceDueDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscNextInvoiceDueDt;
    @XmlElement(name = "com.csc_NextInvoiceAmt")
    protected BigDecimal comCscNextInvoiceAmt;
    @XmlElement(name = "com.csc_LastAdvanceNoticeDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscLastAdvanceNoticeDt;
    @XmlElement(name = "com.csc_LastDebitDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscLastDebitDt;
    @XmlElement(name = "com.csc_LastDebitAmt")
    protected String comCscLastDebitAmt;
    @XmlElement(name = "com.csc_PendingDebitDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscPendingDebitDt;
    @XmlElement(name = "com.csc_PendingDebitAmt")
    protected String comCscPendingDebitAmt;
    @XmlElement(name = "com.csc_FuturePendingDebitDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscFuturePendingDebitDt;
    @XmlElement(name = "com.csc_FuturePendingDebitAmt")
    protected BigDecimal comCscFuturePendingDebitAmt;
    @XmlElement(name = "com.csc_TotalHoldSuspenseAmt")
    protected BigDecimal comCscTotalHoldSuspenseAmt;
    @XmlElement(name = "com.csc_UnMatchedAmt")
    protected BigDecimal comCscUnMatchedAmt;
    @XmlElement(name = "com.csc_CurrentAccountDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCurrentAccountDt;
    @XmlElement(name = "com.csc_NextAccountDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscNextAccountDt;
    @XmlElement(name = "com.csc_CreditCardLastAdvanceNoticeDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCreditCardLastAdvanceNoticeDt;
    @XmlElement(name = "com.csc_CreditCardLastDebitDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCreditCardLastDebitDt;
    @XmlElement(name = "com.csc_CreditCardLastDebitAmt")
    protected BigDecimal comCscCreditCardLastDebitAmt;
    @XmlElement(name = "com.csc_CreditCardPendingDebitDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscCreditCardPendingDebitDt;
    @XmlElement(name = "com.csc_CreditCardPendingDebitAmt")
    protected String comCscCreditCardPendingDebitAmt;
    @XmlElement(name = "com.csc_CollectionAmt")
    protected BigDecimal comCscCollectionAmt;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscTotalPastDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTotalPastDueAmt() {
        return comCscTotalPastDueAmt;
    }

    /**
     * Sets the value of the comCscTotalPastDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTotalPastDueAmt(BigDecimal value) {
        this.comCscTotalPastDueAmt = value;
    }

    /**
     * Gets the value of the comCscTotalCurrentDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTotalCurrentDueAmt() {
        return comCscTotalCurrentDueAmt;
    }

    /**
     * Sets the value of the comCscTotalCurrentDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTotalCurrentDueAmt(BigDecimal value) {
        this.comCscTotalCurrentDueAmt = value;
    }

    /**
     * Gets the value of the comCscTotalFutureDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTotalFutureDueAmt() {
        return comCscTotalFutureDueAmt;
    }

    /**
     * Sets the value of the comCscTotalFutureDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTotalFutureDueAmt(BigDecimal value) {
        this.comCscTotalFutureDueAmt = value;
    }

    /**
     * Gets the value of the comCscAccountBalanceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAccountBalanceAmt() {
        return comCscAccountBalanceAmt;
    }

    /**
     * Sets the value of the comCscAccountBalanceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAccountBalanceAmt(String value) {
        this.comCscAccountBalanceAmt = value;
    }

    /**
     * Gets the value of the comCscTotalSuspenseAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTotalSuspenseAmt() {
        return comCscTotalSuspenseAmt;
    }

    /**
     * Sets the value of the comCscTotalSuspenseAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTotalSuspenseAmt(BigDecimal value) {
        this.comCscTotalSuspenseAmt = value;
    }

    /**
     * Gets the value of the comCscCurrentInvoiceDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCurrentInvoiceDt() {
        return comCscCurrentInvoiceDt;
    }

    /**
     * Sets the value of the comCscCurrentInvoiceDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCurrentInvoiceDt(XMLGregorianCalendar value) {
        this.comCscCurrentInvoiceDt = value;
    }

    /**
     * Gets the value of the comCscCurrentInvoiceDueDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCurrentInvoiceDueDt() {
        return comCscCurrentInvoiceDueDt;
    }

    /**
     * Sets the value of the comCscCurrentInvoiceDueDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCurrentInvoiceDueDt(XMLGregorianCalendar value) {
        this.comCscCurrentInvoiceDueDt = value;
    }

    /**
     * Gets the value of the comCscCurrentInvoiceDueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscCurrentInvoiceDueAmt() {
        return comCscCurrentInvoiceDueAmt;
    }

    /**
     * Sets the value of the comCscCurrentInvoiceDueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscCurrentInvoiceDueAmt(BigDecimal value) {
        this.comCscCurrentInvoiceDueAmt = value;
    }

    /**
     * Gets the value of the comCscNextInvoiceDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscNextInvoiceDt() {
        return comCscNextInvoiceDt;
    }

    /**
     * Sets the value of the comCscNextInvoiceDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscNextInvoiceDt(XMLGregorianCalendar value) {
        this.comCscNextInvoiceDt = value;
    }

    /**
     * Gets the value of the comCscNextInvoiceDueDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscNextInvoiceDueDt() {
        return comCscNextInvoiceDueDt;
    }

    /**
     * Sets the value of the comCscNextInvoiceDueDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscNextInvoiceDueDt(XMLGregorianCalendar value) {
        this.comCscNextInvoiceDueDt = value;
    }

    /**
     * Gets the value of the comCscNextInvoiceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscNextInvoiceAmt() {
        return comCscNextInvoiceAmt;
    }

    /**
     * Sets the value of the comCscNextInvoiceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscNextInvoiceAmt(BigDecimal value) {
        this.comCscNextInvoiceAmt = value;
    }

    /**
     * Gets the value of the comCscLastAdvanceNoticeDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscLastAdvanceNoticeDt() {
        return comCscLastAdvanceNoticeDt;
    }

    /**
     * Sets the value of the comCscLastAdvanceNoticeDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscLastAdvanceNoticeDt(XMLGregorianCalendar value) {
        this.comCscLastAdvanceNoticeDt = value;
    }

    /**
     * Gets the value of the comCscLastDebitDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscLastDebitDt() {
        return comCscLastDebitDt;
    }

    /**
     * Sets the value of the comCscLastDebitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscLastDebitDt(XMLGregorianCalendar value) {
        this.comCscLastDebitDt = value;
    }

    /**
     * Gets the value of the comCscLastDebitAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscLastDebitAmt() {
        return comCscLastDebitAmt;
    }

    /**
     * Sets the value of the comCscLastDebitAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscLastDebitAmt(String value) {
        this.comCscLastDebitAmt = value;
    }

    /**
     * Gets the value of the comCscPendingDebitDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscPendingDebitDt() {
        return comCscPendingDebitDt;
    }

    /**
     * Sets the value of the comCscPendingDebitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscPendingDebitDt(XMLGregorianCalendar value) {
        this.comCscPendingDebitDt = value;
    }

    /**
     * Gets the value of the comCscPendingDebitAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPendingDebitAmt() {
        return comCscPendingDebitAmt;
    }

    /**
     * Sets the value of the comCscPendingDebitAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPendingDebitAmt(String value) {
        this.comCscPendingDebitAmt = value;
    }

    /**
     * Gets the value of the comCscFuturePendingDebitDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscFuturePendingDebitDt() {
        return comCscFuturePendingDebitDt;
    }

    /**
     * Sets the value of the comCscFuturePendingDebitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscFuturePendingDebitDt(XMLGregorianCalendar value) {
        this.comCscFuturePendingDebitDt = value;
    }

    /**
     * Gets the value of the comCscFuturePendingDebitAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscFuturePendingDebitAmt() {
        return comCscFuturePendingDebitAmt;
    }

    /**
     * Sets the value of the comCscFuturePendingDebitAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscFuturePendingDebitAmt(BigDecimal value) {
        this.comCscFuturePendingDebitAmt = value;
    }

    /**
     * Gets the value of the comCscTotalHoldSuspenseAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscTotalHoldSuspenseAmt() {
        return comCscTotalHoldSuspenseAmt;
    }

    /**
     * Sets the value of the comCscTotalHoldSuspenseAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscTotalHoldSuspenseAmt(BigDecimal value) {
        this.comCscTotalHoldSuspenseAmt = value;
    }

    /**
     * Gets the value of the comCscUnMatchedAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscUnMatchedAmt() {
        return comCscUnMatchedAmt;
    }

    /**
     * Sets the value of the comCscUnMatchedAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscUnMatchedAmt(BigDecimal value) {
        this.comCscUnMatchedAmt = value;
    }

    /**
     * Gets the value of the comCscCurrentAccountDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCurrentAccountDt() {
        return comCscCurrentAccountDt;
    }

    /**
     * Sets the value of the comCscCurrentAccountDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCurrentAccountDt(XMLGregorianCalendar value) {
        this.comCscCurrentAccountDt = value;
    }

    /**
     * Gets the value of the comCscNextAccountDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscNextAccountDt() {
        return comCscNextAccountDt;
    }

    /**
     * Sets the value of the comCscNextAccountDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscNextAccountDt(XMLGregorianCalendar value) {
        this.comCscNextAccountDt = value;
    }

    /**
     * Gets the value of the comCscCreditCardLastAdvanceNoticeDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCreditCardLastAdvanceNoticeDt() {
        return comCscCreditCardLastAdvanceNoticeDt;
    }

    /**
     * Sets the value of the comCscCreditCardLastAdvanceNoticeDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCreditCardLastAdvanceNoticeDt(XMLGregorianCalendar value) {
        this.comCscCreditCardLastAdvanceNoticeDt = value;
    }

    /**
     * Gets the value of the comCscCreditCardLastDebitDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCreditCardLastDebitDt() {
        return comCscCreditCardLastDebitDt;
    }

    /**
     * Sets the value of the comCscCreditCardLastDebitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCreditCardLastDebitDt(XMLGregorianCalendar value) {
        this.comCscCreditCardLastDebitDt = value;
    }

    /**
     * Gets the value of the comCscCreditCardLastDebitAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscCreditCardLastDebitAmt() {
        return comCscCreditCardLastDebitAmt;
    }

    /**
     * Sets the value of the comCscCreditCardLastDebitAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscCreditCardLastDebitAmt(BigDecimal value) {
        this.comCscCreditCardLastDebitAmt = value;
    }

    /**
     * Gets the value of the comCscCreditCardPendingDebitDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscCreditCardPendingDebitDt() {
        return comCscCreditCardPendingDebitDt;
    }

    /**
     * Sets the value of the comCscCreditCardPendingDebitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscCreditCardPendingDebitDt(XMLGregorianCalendar value) {
        this.comCscCreditCardPendingDebitDt = value;
    }

    /**
     * Gets the value of the comCscCreditCardPendingDebitAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCreditCardPendingDebitAmt() {
        return comCscCreditCardPendingDebitAmt;
    }

    /**
     * Sets the value of the comCscCreditCardPendingDebitAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCreditCardPendingDebitAmt(String value) {
        this.comCscCreditCardPendingDebitAmt = value;
    }

    /**
     * Gets the value of the comCscCollectionAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscCollectionAmt() {
        return comCscCollectionAmt;
    }

    /**
     * Sets the value of the comCscCollectionAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscCollectionAmt(BigDecimal value) {
        this.comCscCollectionAmt = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
