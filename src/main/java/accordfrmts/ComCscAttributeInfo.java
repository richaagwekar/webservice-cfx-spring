//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_AttributeGroupCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AttributeTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Value" minOccurs="0"/>
 *         &lt;element ref="{}ExcludedInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PriorityNbr" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscAttributeGroupCd",
    "comCscAttributeTypeCd",
    "comCscValue",
    "excludedInd",
    "comCscPriorityNbr"
})
@XmlRootElement(name = "com.csc_AttributeInfo")
public class ComCscAttributeInfo {

    @XmlElement(name = "com.csc_AttributeGroupCd")
    protected String comCscAttributeGroupCd;
    @XmlElement(name = "com.csc_AttributeTypeCd")
    protected String comCscAttributeTypeCd;
    @XmlElement(name = "com.csc_Value")
    protected String comCscValue;
    @XmlElement(name = "ExcludedInd")
    protected Boolean excludedInd;
    @XmlElement(name = "com.csc_PriorityNbr")
    protected String comCscPriorityNbr;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscAttributeGroupCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAttributeGroupCd() {
        return comCscAttributeGroupCd;
    }

    /**
     * Sets the value of the comCscAttributeGroupCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAttributeGroupCd(String value) {
        this.comCscAttributeGroupCd = value;
    }

    /**
     * Gets the value of the comCscAttributeTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAttributeTypeCd() {
        return comCscAttributeTypeCd;
    }

    /**
     * Sets the value of the comCscAttributeTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAttributeTypeCd(String value) {
        this.comCscAttributeTypeCd = value;
    }

    /**
     * Gets the value of the comCscValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscValue() {
        return comCscValue;
    }

    /**
     * Sets the value of the comCscValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscValue(String value) {
        this.comCscValue = value;
    }

    /**
     * Gets the value of the excludedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExcludedInd() {
        return excludedInd;
    }

    /**
     * Sets the value of the excludedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludedInd(Boolean value) {
        this.excludedInd = value;
    }

    /**
     * Gets the value of the comCscPriorityNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPriorityNbr() {
        return comCscPriorityNbr;
    }

    /**
     * Sets the value of the comCscPriorityNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPriorityNbr(String value) {
        this.comCscPriorityNbr = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
