//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FarthestTerminalZoneCd" minOccurs="0"/>
 *         &lt;element ref="{}NumDays" minOccurs="0"/>
 *         &lt;element ref="{}NumTrailers" minOccurs="0"/>
 *         &lt;element ref="{}RadiusUse" minOccurs="0"/>
 *         &lt;element ref="{}RadiusCd" minOccurs="0"/>
 *         &lt;element ref="{}CommlCoverage" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}CreditOrSurcharge" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "farthestTerminalZoneCd",
    "numDays",
    "numTrailers",
    "radiusUse",
    "radiusCd",
    "commlCoverage",
    "creditOrSurcharge"
})
@XmlRootElement(name = "TruckersTrailerInterchange")
public class TruckersTrailerInterchange {

    @XmlElement(name = "FarthestTerminalZoneCd")
    protected String farthestTerminalZoneCd;
    @XmlElement(name = "NumDays")
    protected BigInteger numDays;
    @XmlElement(name = "NumTrailers")
    protected BigInteger numTrailers;
    @XmlElement(name = "RadiusUse")
    protected String radiusUse;
    @XmlElement(name = "RadiusCd")
    protected String radiusCd;
    @XmlElement(name = "CommlCoverage")
    protected List<CommlCoverage> commlCoverage;
    @XmlElement(name = "CreditOrSurcharge")
    protected List<CreditOrSurcharge> creditOrSurcharge;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the farthestTerminalZoneCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFarthestTerminalZoneCd() {
        return farthestTerminalZoneCd;
    }

    /**
     * Sets the value of the farthestTerminalZoneCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFarthestTerminalZoneCd(String value) {
        this.farthestTerminalZoneCd = value;
    }

    /**
     * Gets the value of the numDays property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumDays() {
        return numDays;
    }

    /**
     * Sets the value of the numDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumDays(BigInteger value) {
        this.numDays = value;
    }

    /**
     * Gets the value of the numTrailers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumTrailers() {
        return numTrailers;
    }

    /**
     * Sets the value of the numTrailers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumTrailers(BigInteger value) {
        this.numTrailers = value;
    }

    /**
     * Gets the value of the radiusUse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusUse() {
        return radiusUse;
    }

    /**
     * Sets the value of the radiusUse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusUse(String value) {
        this.radiusUse = value;
    }

    /**
     * Gets the value of the radiusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusCd() {
        return radiusCd;
    }

    /**
     * Sets the value of the radiusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusCd(String value) {
        this.radiusCd = value;
    }

    /**
     * Gets the value of the commlCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commlCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommlCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommlCoverage }
     * 
     * 
     */
    public List<CommlCoverage> getCommlCoverage() {
        if (commlCoverage == null) {
            commlCoverage = new ArrayList<CommlCoverage>();
        }
        return this.commlCoverage;
    }

    /**
     * Gets the value of the creditOrSurcharge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditOrSurcharge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditOrSurcharge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditOrSurcharge }
     * 
     * 
     */
    public List<CreditOrSurcharge> getCreditOrSurcharge() {
        if (creditOrSurcharge == null) {
            creditOrSurcharge = new ArrayList<CreditOrSurcharge>();
        }
        return this.creditOrSurcharge;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
