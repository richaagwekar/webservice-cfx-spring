//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}InspectionCd" minOccurs="0"/>
 *         &lt;element ref="{}InspectionDt" minOccurs="0"/>
 *         &lt;element ref="{}InspectionStatusCd" minOccurs="0"/>
 *         &lt;element ref="{}InspectionReportDt" minOccurs="0"/>
 *         &lt;element ref="{}VehInspectionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inspectionCd",
    "inspectionDt",
    "inspectionStatusCd",
    "inspectionReportDt",
    "vehInspectionInfo"
})
@XmlRootElement(name = "InspectionInfo")
public class InspectionInfo {

    @XmlElement(name = "InspectionCd")
    protected String inspectionCd;
    @XmlElement(name = "InspectionDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar inspectionDt;
    @XmlElement(name = "InspectionStatusCd")
    protected String inspectionStatusCd;
    @XmlElement(name = "InspectionReportDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar inspectionReportDt;
    @XmlElement(name = "VehInspectionInfo")
    protected VehInspectionInfo vehInspectionInfo;

    /**
     * Gets the value of the inspectionCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionCd() {
        return inspectionCd;
    }

    /**
     * Sets the value of the inspectionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionCd(String value) {
        this.inspectionCd = value;
    }

    /**
     * Gets the value of the inspectionDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInspectionDt() {
        return inspectionDt;
    }

    /**
     * Sets the value of the inspectionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInspectionDt(XMLGregorianCalendar value) {
        this.inspectionDt = value;
    }

    /**
     * Gets the value of the inspectionStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionStatusCd() {
        return inspectionStatusCd;
    }

    /**
     * Sets the value of the inspectionStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionStatusCd(String value) {
        this.inspectionStatusCd = value;
    }

    /**
     * Gets the value of the inspectionReportDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInspectionReportDt() {
        return inspectionReportDt;
    }

    /**
     * Sets the value of the inspectionReportDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInspectionReportDt(XMLGregorianCalendar value) {
        this.inspectionReportDt = value;
    }

    /**
     * Gets the value of the vehInspectionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link VehInspectionInfo }
     *     
     */
    public VehInspectionInfo getVehInspectionInfo() {
        return vehInspectionInfo;
    }

    /**
     * Sets the value of the vehInspectionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehInspectionInfo }
     *     
     */
    public void setVehInspectionInfo(VehInspectionInfo value) {
        this.vehInspectionInfo = value;
    }

}
