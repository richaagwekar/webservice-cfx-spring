//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}AuditPeriod" minOccurs="0"/>
 *         &lt;element ref="{}RatingClassificationCd" minOccurs="0"/>
 *         &lt;element ref="{}RatingClassificationDescCd" minOccurs="0"/>
 *         &lt;element ref="{}CommlCoverage" minOccurs="0"/>
 *         &lt;element ref="{}PremiumBasisCd" minOccurs="0"/>
 *         &lt;element ref="{}Exposure" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="EmployeeRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "auditPeriod",
    "ratingClassificationCd",
    "ratingClassificationDescCd",
    "commlCoverage",
    "premiumBasisCd",
    "exposure"
})
@XmlRootElement(name = "WorkCompAuditExposureInfo")
public class WorkCompAuditExposureInfo {

    @XmlElement(name = "AuditPeriod")
    protected String auditPeriod;
    @XmlElement(name = "RatingClassificationCd")
    protected String ratingClassificationCd;
    @XmlElement(name = "RatingClassificationDescCd")
    protected String ratingClassificationDescCd;
    @XmlElement(name = "CommlCoverage")
    protected CommlCoverage commlCoverage;
    @XmlElement(name = "PremiumBasisCd")
    protected String premiumBasisCd;
    @XmlElement(name = "Exposure")
    protected String exposure;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "EmployeeRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object employeeRef;

    /**
     * Gets the value of the auditPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditPeriod() {
        return auditPeriod;
    }

    /**
     * Sets the value of the auditPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditPeriod(String value) {
        this.auditPeriod = value;
    }

    /**
     * Gets the value of the ratingClassificationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatingClassificationCd() {
        return ratingClassificationCd;
    }

    /**
     * Sets the value of the ratingClassificationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatingClassificationCd(String value) {
        this.ratingClassificationCd = value;
    }

    /**
     * Gets the value of the ratingClassificationDescCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRatingClassificationDescCd() {
        return ratingClassificationDescCd;
    }

    /**
     * Sets the value of the ratingClassificationDescCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRatingClassificationDescCd(String value) {
        this.ratingClassificationDescCd = value;
    }

    /**
     * Gets the value of the commlCoverage property.
     * 
     * @return
     *     possible object is
     *     {@link CommlCoverage }
     *     
     */
    public CommlCoverage getCommlCoverage() {
        return commlCoverage;
    }

    /**
     * Sets the value of the commlCoverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommlCoverage }
     *     
     */
    public void setCommlCoverage(CommlCoverage value) {
        this.commlCoverage = value;
    }

    /**
     * Gets the value of the premiumBasisCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumBasisCd() {
        return premiumBasisCd;
    }

    /**
     * Sets the value of the premiumBasisCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumBasisCd(String value) {
        this.premiumBasisCd = value;
    }

    /**
     * Gets the value of the exposure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExposure() {
        return exposure;
    }

    /**
     * Sets the value of the exposure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExposure(String value) {
        this.exposure = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the employeeRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getEmployeeRef() {
        return employeeRef;
    }

    /**
     * Sets the value of the employeeRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setEmployeeRef(Object value) {
        this.employeeRef = value;
    }

}
