//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StorageTankInfo_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StorageTankInfo_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}AboveGroundInd" minOccurs="0"/>
 *         &lt;element ref="{}DikedInd" minOccurs="0"/>
 *         &lt;element ref="{}IndoorInd" minOccurs="0"/>
 *         &lt;element ref="{}MobileInd" minOccurs="0"/>
 *         &lt;element ref="{}ContentCd" minOccurs="0"/>
 *         &lt;element ref="{}ContentDesc" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StorageTankInfo_Type", propOrder = {
    "aboveGroundInd",
    "dikedInd",
    "indoorInd",
    "mobileInd",
    "contentCd",
    "contentDesc"
})
public class StorageTankInfoType {

    @XmlElement(name = "AboveGroundInd")
    protected Boolean aboveGroundInd;
    @XmlElement(name = "DikedInd")
    protected Boolean dikedInd;
    @XmlElement(name = "IndoorInd")
    protected Boolean indoorInd;
    @XmlElement(name = "MobileInd")
    protected Boolean mobileInd;
    @XmlElement(name = "ContentCd")
    protected String contentCd;
    @XmlElement(name = "ContentDesc")
    protected String contentDesc;

    /**
     * Gets the value of the aboveGroundInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAboveGroundInd() {
        return aboveGroundInd;
    }

    /**
     * Sets the value of the aboveGroundInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAboveGroundInd(Boolean value) {
        this.aboveGroundInd = value;
    }

    /**
     * Gets the value of the dikedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDikedInd() {
        return dikedInd;
    }

    /**
     * Sets the value of the dikedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDikedInd(Boolean value) {
        this.dikedInd = value;
    }

    /**
     * Gets the value of the indoorInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIndoorInd() {
        return indoorInd;
    }

    /**
     * Sets the value of the indoorInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndoorInd(Boolean value) {
        this.indoorInd = value;
    }

    /**
     * Gets the value of the mobileInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMobileInd() {
        return mobileInd;
    }

    /**
     * Sets the value of the mobileInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMobileInd(Boolean value) {
        this.mobileInd = value;
    }

    /**
     * Gets the value of the contentCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentCd() {
        return contentCd;
    }

    /**
     * Sets the value of the contentCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentCd(String value) {
        this.contentCd = value;
    }

    /**
     * Gets the value of the contentDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentDesc() {
        return contentDesc;
    }

    /**
     * Sets the value of the contentDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentDesc(String value) {
        this.contentDesc = value;
    }

}
