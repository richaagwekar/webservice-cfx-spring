//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ObjectSequenceNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RelationTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ObjectTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AddressSequenceNumber" minOccurs="0"/>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ObjectSystemId" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountName" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_MemberName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "effectiveDt",
    "expirationDt",
    "comCscObjectSequenceNumber",
    "comCscRelationTypeCd",
    "comCscObjectTypeCd",
    "comCscAddressSequenceNumber",
    "actionCd",
    "comCscObjectSystemId",
    "comCscAccountName",
    "comCscMemberName"
})
@XmlRootElement(name = "com.csc_ObjectRelation")
public class ComCscObjectRelation {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDt;
    @XmlElement(name = "com.csc_ObjectSequenceNumber")
    protected String comCscObjectSequenceNumber;
    @XmlElement(name = "com.csc_RelationTypeCd")
    protected String comCscRelationTypeCd;
    @XmlElement(name = "com.csc_ObjectTypeCd")
    protected String comCscObjectTypeCd;
    @XmlElement(name = "com.csc_AddressSequenceNumber")
    protected String comCscAddressSequenceNumber;
    @XmlElement(name = "ActionCd")
    protected Action actionCd;
    @XmlElement(name = "com.csc_ObjectSystemId")
    protected String comCscObjectSystemId;
    @XmlElement(name = "com.csc_AccountName")
    protected ComCscAccountName comCscAccountName;
    @XmlElement(name = "com.csc_MemberName")
    protected ComCscMemberName comCscMemberName;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDt() {
        return expirationDt;
    }

    /**
     * Sets the value of the expirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDt(XMLGregorianCalendar value) {
        this.expirationDt = value;
    }

    /**
     * Gets the value of the comCscObjectSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscObjectSequenceNumber() {
        return comCscObjectSequenceNumber;
    }

    /**
     * Sets the value of the comCscObjectSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscObjectSequenceNumber(String value) {
        this.comCscObjectSequenceNumber = value;
    }

    /**
     * Gets the value of the comCscRelationTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRelationTypeCd() {
        return comCscRelationTypeCd;
    }

    /**
     * Sets the value of the comCscRelationTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRelationTypeCd(String value) {
        this.comCscRelationTypeCd = value;
    }

    /**
     * Gets the value of the comCscObjectTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscObjectTypeCd() {
        return comCscObjectTypeCd;
    }

    /**
     * Sets the value of the comCscObjectTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscObjectTypeCd(String value) {
        this.comCscObjectTypeCd = value;
    }

    /**
     * Gets the value of the comCscAddressSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAddressSequenceNumber() {
        return comCscAddressSequenceNumber;
    }

    /**
     * Sets the value of the comCscAddressSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAddressSequenceNumber(String value) {
        this.comCscAddressSequenceNumber = value;
    }

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

    /**
     * Gets the value of the comCscObjectSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscObjectSystemId() {
        return comCscObjectSystemId;
    }

    /**
     * Sets the value of the comCscObjectSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscObjectSystemId(String value) {
        this.comCscObjectSystemId = value;
    }

    /**
     * Gets the value of the comCscAccountName property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAccountName }
     *     
     */
    public ComCscAccountName getComCscAccountName() {
        return comCscAccountName;
    }

    /**
     * Sets the value of the comCscAccountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAccountName }
     *     
     */
    public void setComCscAccountName(ComCscAccountName value) {
        this.comCscAccountName = value;
    }

    /**
     * Gets the value of the comCscMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscMemberName }
     *     
     */
    public ComCscMemberName getComCscMemberName() {
        return comCscMemberName;
    }

    /**
     * Sets the value of the comCscMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscMemberName }
     *     
     */
    public void setComCscMemberName(ComCscMemberName value) {
        this.comCscMemberName = value;
    }

}
