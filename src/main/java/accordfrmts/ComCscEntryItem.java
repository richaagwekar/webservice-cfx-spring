//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AnswerDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Dependency" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Constraints" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Selection" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="SequenceNbr" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_EntryItemCd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_EntryItemTypeCd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_EntryItemText" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_EntryItemRequiredCd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_AnswerTypeCd" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_RowNbr" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_ColumnNbr" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="com.csc_EntryFieldId" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "comCscAnswerDesc",
    "comCscDependency",
    "comCscConstraints",
    "comCscSelection",
    "comCscActionInfo"
})
@XmlRootElement(name = "com.csc_EntryItem")
public class ComCscEntryItem {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "com.csc_AnswerDesc")
    protected String comCscAnswerDesc;
    @XmlElement(name = "com.csc_Dependency")
    protected List<ComCscDependency> comCscDependency;
    @XmlElement(name = "com.csc_Constraints")
    protected ComCscConstraints comCscConstraints;
    @XmlElement(name = "com.csc_Selection")
    protected List<ComCscSelection> comCscSelection;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "SequenceNbr")
    @XmlSchemaType(name = "anySimpleType")
    protected String sequenceNbr;
    @XmlAttribute(name = "com.csc_EntryItemCd")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscEntryItemCd;
    @XmlAttribute(name = "com.csc_EntryItemTypeCd")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscEntryItemTypeCd;
    @XmlAttribute(name = "com.csc_EntryItemText")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscEntryItemText;
    @XmlAttribute(name = "com.csc_EntryItemRequiredCd")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscEntryItemRequiredCd;
    @XmlAttribute(name = "com.csc_AnswerTypeCd")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscAnswerTypeCd;
    @XmlAttribute(name = "com.csc_RowNbr")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscRowNbr;
    @XmlAttribute(name = "com.csc_ColumnNbr")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscColumnNbr;
    @XmlAttribute(name = "com.csc_EntryFieldId")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscEntryFieldId;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the comCscAnswerDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAnswerDesc() {
        return comCscAnswerDesc;
    }

    /**
     * Sets the value of the comCscAnswerDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAnswerDesc(String value) {
        this.comCscAnswerDesc = value;
    }

    /**
     * Gets the value of the comCscDependency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscDependency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscDependency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscDependency }
     * 
     * 
     */
    public List<ComCscDependency> getComCscDependency() {
        if (comCscDependency == null) {
            comCscDependency = new ArrayList<ComCscDependency>();
        }
        return this.comCscDependency;
    }

    /**
     * Gets the value of the comCscConstraints property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscConstraints }
     *     
     */
    public ComCscConstraints getComCscConstraints() {
        return comCscConstraints;
    }

    /**
     * Sets the value of the comCscConstraints property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscConstraints }
     *     
     */
    public void setComCscConstraints(ComCscConstraints value) {
        this.comCscConstraints = value;
    }

    /**
     * Gets the value of the comCscSelection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscSelection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscSelection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscSelection }
     * 
     * 
     */
    public List<ComCscSelection> getComCscSelection() {
        if (comCscSelection == null) {
            comCscSelection = new ArrayList<ComCscSelection>();
        }
        return this.comCscSelection;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the sequenceNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNbr() {
        return sequenceNbr;
    }

    /**
     * Sets the value of the sequenceNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNbr(String value) {
        this.sequenceNbr = value;
    }

    /**
     * Gets the value of the comCscEntryItemCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryItemCd() {
        return comCscEntryItemCd;
    }

    /**
     * Sets the value of the comCscEntryItemCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryItemCd(String value) {
        this.comCscEntryItemCd = value;
    }

    /**
     * Gets the value of the comCscEntryItemTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryItemTypeCd() {
        return comCscEntryItemTypeCd;
    }

    /**
     * Sets the value of the comCscEntryItemTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryItemTypeCd(String value) {
        this.comCscEntryItemTypeCd = value;
    }

    /**
     * Gets the value of the comCscEntryItemText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryItemText() {
        return comCscEntryItemText;
    }

    /**
     * Sets the value of the comCscEntryItemText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryItemText(String value) {
        this.comCscEntryItemText = value;
    }

    /**
     * Gets the value of the comCscEntryItemRequiredCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryItemRequiredCd() {
        return comCscEntryItemRequiredCd;
    }

    /**
     * Sets the value of the comCscEntryItemRequiredCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryItemRequiredCd(String value) {
        this.comCscEntryItemRequiredCd = value;
    }

    /**
     * Gets the value of the comCscAnswerTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAnswerTypeCd() {
        return comCscAnswerTypeCd;
    }

    /**
     * Sets the value of the comCscAnswerTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAnswerTypeCd(String value) {
        this.comCscAnswerTypeCd = value;
    }

    /**
     * Gets the value of the comCscRowNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRowNbr() {
        return comCscRowNbr;
    }

    /**
     * Sets the value of the comCscRowNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRowNbr(String value) {
        this.comCscRowNbr = value;
    }

    /**
     * Gets the value of the comCscColumnNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscColumnNbr() {
        return comCscColumnNbr;
    }

    /**
     * Sets the value of the comCscColumnNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscColumnNbr(String value) {
        this.comCscColumnNbr = value;
    }

    /**
     * Gets the value of the comCscEntryFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEntryFieldId() {
        return comCscEntryFieldId;
    }

    /**
     * Sets the value of the comCscEntryFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEntryFieldId(String value) {
        this.comCscEntryFieldId = value;
    }

}
