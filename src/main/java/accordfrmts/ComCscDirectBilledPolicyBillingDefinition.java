//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_BillPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RenewalBillPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyBillPlanSelection" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RenewalPolicyBillPlanSelection" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NewBillAccountDefinition" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ExistingBillAccountDefinition" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyBillingDefinitionSectionDesc" minOccurs="0"/>
 *         &lt;element ref="{}DepositAmt" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscBillPlanCd",
    "comCscRenewalBillPlanCd",
    "comCscPolicyBillPlanSelection",
    "comCscRenewalPolicyBillPlanSelection",
    "comCscNewBillAccountDefinition",
    "comCscExistingBillAccountDefinition",
    "comCscPolicyBillingDefinitionSectionDesc",
    "depositAmt"
})
@XmlRootElement(name = "com.csc_DirectBilledPolicyBillingDefinition")
public class ComCscDirectBilledPolicyBillingDefinition {

    @XmlElement(name = "com.csc_BillPlanCd")
    protected String comCscBillPlanCd;
    @XmlElement(name = "com.csc_RenewalBillPlanCd")
    protected String comCscRenewalBillPlanCd;
    @XmlElement(name = "com.csc_PolicyBillPlanSelection")
    protected ComCscPolicyBillPlanSelection comCscPolicyBillPlanSelection;
    @XmlElement(name = "com.csc_RenewalPolicyBillPlanSelection")
    protected ComCscRenewalPolicyBillPlanSelection comCscRenewalPolicyBillPlanSelection;
    @XmlElement(name = "com.csc_NewBillAccountDefinition")
    protected ComCscNewBillAccountDefinition comCscNewBillAccountDefinition;
    @XmlElement(name = "com.csc_ExistingBillAccountDefinition")
    protected ComCscExistingBillAccountDefinition comCscExistingBillAccountDefinition;
    @XmlElement(name = "com.csc_PolicyBillingDefinitionSectionDesc")
    protected String comCscPolicyBillingDefinitionSectionDesc;
    @XmlElement(name = "DepositAmt")
    protected BigDecimal depositAmt;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscBillPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillPlanCd() {
        return comCscBillPlanCd;
    }

    /**
     * Sets the value of the comCscBillPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillPlanCd(String value) {
        this.comCscBillPlanCd = value;
    }

    /**
     * Gets the value of the comCscRenewalBillPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRenewalBillPlanCd() {
        return comCscRenewalBillPlanCd;
    }

    /**
     * Sets the value of the comCscRenewalBillPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRenewalBillPlanCd(String value) {
        this.comCscRenewalBillPlanCd = value;
    }

    /**
     * Gets the value of the comCscPolicyBillPlanSelection property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPolicyBillPlanSelection }
     *     
     */
    public ComCscPolicyBillPlanSelection getComCscPolicyBillPlanSelection() {
        return comCscPolicyBillPlanSelection;
    }

    /**
     * Sets the value of the comCscPolicyBillPlanSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPolicyBillPlanSelection }
     *     
     */
    public void setComCscPolicyBillPlanSelection(ComCscPolicyBillPlanSelection value) {
        this.comCscPolicyBillPlanSelection = value;
    }

    /**
     * Gets the value of the comCscRenewalPolicyBillPlanSelection property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscRenewalPolicyBillPlanSelection }
     *     
     */
    public ComCscRenewalPolicyBillPlanSelection getComCscRenewalPolicyBillPlanSelection() {
        return comCscRenewalPolicyBillPlanSelection;
    }

    /**
     * Sets the value of the comCscRenewalPolicyBillPlanSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscRenewalPolicyBillPlanSelection }
     *     
     */
    public void setComCscRenewalPolicyBillPlanSelection(ComCscRenewalPolicyBillPlanSelection value) {
        this.comCscRenewalPolicyBillPlanSelection = value;
    }

    /**
     * Gets the value of the comCscNewBillAccountDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscNewBillAccountDefinition }
     *     
     */
    public ComCscNewBillAccountDefinition getComCscNewBillAccountDefinition() {
        return comCscNewBillAccountDefinition;
    }

    /**
     * Sets the value of the comCscNewBillAccountDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscNewBillAccountDefinition }
     *     
     */
    public void setComCscNewBillAccountDefinition(ComCscNewBillAccountDefinition value) {
        this.comCscNewBillAccountDefinition = value;
    }

    /**
     * Gets the value of the comCscExistingBillAccountDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscExistingBillAccountDefinition }
     *     
     */
    public ComCscExistingBillAccountDefinition getComCscExistingBillAccountDefinition() {
        return comCscExistingBillAccountDefinition;
    }

    /**
     * Sets the value of the comCscExistingBillAccountDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscExistingBillAccountDefinition }
     *     
     */
    public void setComCscExistingBillAccountDefinition(ComCscExistingBillAccountDefinition value) {
        this.comCscExistingBillAccountDefinition = value;
    }

    /**
     * Gets the value of the comCscPolicyBillingDefinitionSectionDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPolicyBillingDefinitionSectionDesc() {
        return comCscPolicyBillingDefinitionSectionDesc;
    }

    /**
     * Sets the value of the comCscPolicyBillingDefinitionSectionDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPolicyBillingDefinitionSectionDesc(String value) {
        this.comCscPolicyBillingDefinitionSectionDesc = value;
    }

    /**
     * Gets the value of the depositAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepositAmt() {
        return depositAmt;
    }

    /**
     * Sets the value of the depositAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepositAmt(BigDecimal value) {
        this.depositAmt = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
