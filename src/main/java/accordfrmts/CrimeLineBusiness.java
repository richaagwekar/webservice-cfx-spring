//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}LOBCd"/>
 *         &lt;element ref="{}LOBSubCd" minOccurs="0"/>
 *         &lt;element ref="{}NAICCd" minOccurs="0"/>
 *         &lt;element ref="{}CompanyProductCd" minOccurs="0"/>
 *         &lt;element ref="{}CurrentTermAmt" minOccurs="0"/>
 *         &lt;element ref="{}NetChangeAmt" minOccurs="0"/>
 *         &lt;element ref="{}WrittenAmt" minOccurs="0"/>
 *         &lt;element ref="{}GroupId" minOccurs="0"/>
 *         &lt;element ref="{}PMACd" minOccurs="0"/>
 *         &lt;element ref="{}RateEffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}AdditionalInterest" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}MinPremInd" minOccurs="0"/>
 *         &lt;element ref="{}AuditInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}HazardGradeCd" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element ref="{}CurrentRetroactiveDt" minOccurs="0"/>
 *           &lt;element ref="{}ProposedRetroactiveDt" minOccurs="0"/>
 *           &lt;element ref="{}CrimeCoverageInfo" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{}EmployeeBenefitPlanInfo" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{}EmployeeClassInfo" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{}ClassCd" minOccurs="0"/>
 *           &lt;element ref="{}NumRetailLocations" minOccurs="0"/>
 *           &lt;element ref="{}NumLocations" minOccurs="0"/>
 *           &lt;element ref="{}QuestionAnswer" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{}CrimeInfo" minOccurs="0"/>
 *           &lt;element ref="{}NumVolunteers" minOccurs="0"/>
 *           &lt;element ref="{}NumEmployeesLeasedToOthers" minOccurs="0"/>
 *           &lt;element ref="{}NumEmployeesLeasedFromOthers" minOccurs="0"/>
 *           &lt;element ref="{}EmployeeBenefitPlanTotalAssetAmt" minOccurs="0"/>
 *           &lt;element ref="{}CrimeSchedule" minOccurs="0"/>
 *           &lt;element ref="{}CrimeMoneyAndSecurities" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element ref="{}com.csc_ItemIdInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lobCd",
    "lobSubCd",
    "naicCd",
    "companyProductCd",
    "currentTermAmt",
    "netChangeAmt",
    "writtenAmt",
    "groupId",
    "pmaCd",
    "rateEffectiveDt",
    "additionalInterest",
    "minPremInd",
    "auditInfo",
    "hazardGradeCd",
    "currentRetroactiveDt",
    "proposedRetroactiveDt",
    "crimeCoverageInfo",
    "employeeBenefitPlanInfo",
    "employeeClassInfo",
    "classCd",
    "numRetailLocations",
    "numLocations",
    "questionAnswer",
    "crimeInfo",
    "numVolunteers",
    "numEmployeesLeasedToOthers",
    "numEmployeesLeasedFromOthers",
    "employeeBenefitPlanTotalAssetAmt",
    "crimeSchedule",
    "crimeMoneyAndSecurities",
    "comCscItemIdInfo"
})
@XmlRootElement(name = "CrimeLineBusiness")
public class CrimeLineBusiness {

    @XmlElement(name = "LOBCd", required = true)
    protected PCLINEOFBUSINESS lobCd;
    @XmlElement(name = "LOBSubCd")
    protected String lobSubCd;
    @XmlElement(name = "NAICCd")
    protected PCCARRIERCODE naicCd;
    @XmlElement(name = "CompanyProductCd")
    protected String companyProductCd;
    @XmlElement(name = "CurrentTermAmt")
    protected BigDecimal currentTermAmt;
    @XmlElement(name = "NetChangeAmt")
    protected NetChangeAmt netChangeAmt;
    @XmlElement(name = "WrittenAmt")
    protected BigDecimal writtenAmt;
    @XmlElement(name = "GroupId")
    protected String groupId;
    @XmlElement(name = "PMACd")
    protected String pmaCd;
    @XmlElement(name = "RateEffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar rateEffectiveDt;
    @XmlElement(name = "AdditionalInterest")
    protected List<AdditionalInterest> additionalInterest;
    @XmlElement(name = "MinPremInd")
    protected Boolean minPremInd;
    @XmlElement(name = "AuditInfo")
    protected List<AuditInfo> auditInfo;
    @XmlElement(name = "HazardGradeCd")
    protected String hazardGradeCd;
    @XmlElement(name = "CurrentRetroactiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar currentRetroactiveDt;
    @XmlElement(name = "ProposedRetroactiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar proposedRetroactiveDt;
    @XmlElement(name = "CrimeCoverageInfo")
    protected List<CrimeCoverageInfo> crimeCoverageInfo;
    @XmlElement(name = "EmployeeBenefitPlanInfo")
    protected List<EmployeeBenefitPlanInfo> employeeBenefitPlanInfo;
    @XmlElement(name = "EmployeeClassInfo")
    protected List<EmployeeClassInfo> employeeClassInfo;
    @XmlElement(name = "ClassCd")
    protected String classCd;
    @XmlElement(name = "NumRetailLocations")
    protected BigInteger numRetailLocations;
    @XmlElement(name = "NumLocations")
    protected BigInteger numLocations;
    @XmlElement(name = "QuestionAnswer")
    protected List<QuestionAnswer> questionAnswer;
    @XmlElement(name = "CrimeInfo")
    protected CrimeInfo crimeInfo;
    @XmlElement(name = "NumVolunteers")
    protected BigInteger numVolunteers;
    @XmlElement(name = "NumEmployeesLeasedToOthers")
    protected BigInteger numEmployeesLeasedToOthers;
    @XmlElement(name = "NumEmployeesLeasedFromOthers")
    protected BigInteger numEmployeesLeasedFromOthers;
    @XmlElement(name = "EmployeeBenefitPlanTotalAssetAmt")
    protected BigDecimal employeeBenefitPlanTotalAssetAmt;
    @XmlElement(name = "CrimeSchedule")
    protected CrimeSchedule crimeSchedule;
    @XmlElement(name = "CrimeMoneyAndSecurities")
    protected CrimeMoneyAndSecurities crimeMoneyAndSecurities;
    @XmlElement(name = "com.csc_ItemIdInfo")
    protected ComCscItemIdInfo comCscItemIdInfo;

    /**
     * Gets the value of the lobCd property.
     * 
     * @return
     *     possible object is
     *     {@link PCLINEOFBUSINESS }
     *     
     */
    public PCLINEOFBUSINESS getLOBCd() {
        return lobCd;
    }

    /**
     * Sets the value of the lobCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link PCLINEOFBUSINESS }
     *     
     */
    public void setLOBCd(PCLINEOFBUSINESS value) {
        this.lobCd = value;
    }

    /**
     * Gets the value of the lobSubCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOBSubCd() {
        return lobSubCd;
    }

    /**
     * Sets the value of the lobSubCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOBSubCd(String value) {
        this.lobSubCd = value;
    }

    /**
     * Gets the value of the naicCd property.
     * 
     * @return
     *     possible object is
     *     {@link PCCARRIERCODE }
     *     
     */
    public PCCARRIERCODE getNAICCd() {
        return naicCd;
    }

    /**
     * Sets the value of the naicCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link PCCARRIERCODE }
     *     
     */
    public void setNAICCd(PCCARRIERCODE value) {
        this.naicCd = value;
    }

    /**
     * Gets the value of the companyProductCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyProductCd() {
        return companyProductCd;
    }

    /**
     * Sets the value of the companyProductCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyProductCd(String value) {
        this.companyProductCd = value;
    }

    /**
     * Gets the value of the currentTermAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrentTermAmt() {
        return currentTermAmt;
    }

    /**
     * Sets the value of the currentTermAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrentTermAmt(BigDecimal value) {
        this.currentTermAmt = value;
    }

    /**
     * Gets the value of the netChangeAmt property.
     * 
     * @return
     *     possible object is
     *     {@link NetChangeAmt }
     *     
     */
    public NetChangeAmt getNetChangeAmt() {
        return netChangeAmt;
    }

    /**
     * Sets the value of the netChangeAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetChangeAmt }
     *     
     */
    public void setNetChangeAmt(NetChangeAmt value) {
        this.netChangeAmt = value;
    }

    /**
     * Gets the value of the writtenAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWrittenAmt() {
        return writtenAmt;
    }

    /**
     * Sets the value of the writtenAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWrittenAmt(BigDecimal value) {
        this.writtenAmt = value;
    }

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the pmaCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMACd() {
        return pmaCd;
    }

    /**
     * Sets the value of the pmaCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMACd(String value) {
        this.pmaCd = value;
    }

    /**
     * Gets the value of the rateEffectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRateEffectiveDt() {
        return rateEffectiveDt;
    }

    /**
     * Sets the value of the rateEffectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRateEffectiveDt(XMLGregorianCalendar value) {
        this.rateEffectiveDt = value;
    }

    /**
     * Gets the value of the additionalInterest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInterest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInterest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInterest }
     * 
     * 
     */
    public List<AdditionalInterest> getAdditionalInterest() {
        if (additionalInterest == null) {
            additionalInterest = new ArrayList<AdditionalInterest>();
        }
        return this.additionalInterest;
    }

    /**
     * Gets the value of the minPremInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMinPremInd() {
        return minPremInd;
    }

    /**
     * Sets the value of the minPremInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMinPremInd(Boolean value) {
        this.minPremInd = value;
    }

    /**
     * Gets the value of the auditInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the auditInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAuditInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AuditInfo }
     * 
     * 
     */
    public List<AuditInfo> getAuditInfo() {
        if (auditInfo == null) {
            auditInfo = new ArrayList<AuditInfo>();
        }
        return this.auditInfo;
    }

    /**
     * Gets the value of the hazardGradeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazardGradeCd() {
        return hazardGradeCd;
    }

    /**
     * Sets the value of the hazardGradeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazardGradeCd(String value) {
        this.hazardGradeCd = value;
    }

    /**
     * Gets the value of the currentRetroactiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCurrentRetroactiveDt() {
        return currentRetroactiveDt;
    }

    /**
     * Sets the value of the currentRetroactiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCurrentRetroactiveDt(XMLGregorianCalendar value) {
        this.currentRetroactiveDt = value;
    }

    /**
     * Gets the value of the proposedRetroactiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProposedRetroactiveDt() {
        return proposedRetroactiveDt;
    }

    /**
     * Sets the value of the proposedRetroactiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProposedRetroactiveDt(XMLGregorianCalendar value) {
        this.proposedRetroactiveDt = value;
    }

    /**
     * Gets the value of the crimeCoverageInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the crimeCoverageInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCrimeCoverageInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CrimeCoverageInfo }
     * 
     * 
     */
    public List<CrimeCoverageInfo> getCrimeCoverageInfo() {
        if (crimeCoverageInfo == null) {
            crimeCoverageInfo = new ArrayList<CrimeCoverageInfo>();
        }
        return this.crimeCoverageInfo;
    }

    /**
     * Gets the value of the employeeBenefitPlanInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the employeeBenefitPlanInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmployeeBenefitPlanInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmployeeBenefitPlanInfo }
     * 
     * 
     */
    public List<EmployeeBenefitPlanInfo> getEmployeeBenefitPlanInfo() {
        if (employeeBenefitPlanInfo == null) {
            employeeBenefitPlanInfo = new ArrayList<EmployeeBenefitPlanInfo>();
        }
        return this.employeeBenefitPlanInfo;
    }

    /**
     * Gets the value of the employeeClassInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the employeeClassInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmployeeClassInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmployeeClassInfo }
     * 
     * 
     */
    public List<EmployeeClassInfo> getEmployeeClassInfo() {
        if (employeeClassInfo == null) {
            employeeClassInfo = new ArrayList<EmployeeClassInfo>();
        }
        return this.employeeClassInfo;
    }

    /**
     * Gets the value of the classCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassCd() {
        return classCd;
    }

    /**
     * Sets the value of the classCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassCd(String value) {
        this.classCd = value;
    }

    /**
     * Gets the value of the numRetailLocations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumRetailLocations() {
        return numRetailLocations;
    }

    /**
     * Sets the value of the numRetailLocations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumRetailLocations(BigInteger value) {
        this.numRetailLocations = value;
    }

    /**
     * Gets the value of the numLocations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumLocations() {
        return numLocations;
    }

    /**
     * Sets the value of the numLocations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumLocations(BigInteger value) {
        this.numLocations = value;
    }

    /**
     * Gets the value of the questionAnswer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the questionAnswer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestionAnswer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuestionAnswer }
     * 
     * 
     */
    public List<QuestionAnswer> getQuestionAnswer() {
        if (questionAnswer == null) {
            questionAnswer = new ArrayList<QuestionAnswer>();
        }
        return this.questionAnswer;
    }

    /**
     * Gets the value of the crimeInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CrimeInfo }
     *     
     */
    public CrimeInfo getCrimeInfo() {
        return crimeInfo;
    }

    /**
     * Sets the value of the crimeInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrimeInfo }
     *     
     */
    public void setCrimeInfo(CrimeInfo value) {
        this.crimeInfo = value;
    }

    /**
     * Gets the value of the numVolunteers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumVolunteers() {
        return numVolunteers;
    }

    /**
     * Sets the value of the numVolunteers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumVolunteers(BigInteger value) {
        this.numVolunteers = value;
    }

    /**
     * Gets the value of the numEmployeesLeasedToOthers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumEmployeesLeasedToOthers() {
        return numEmployeesLeasedToOthers;
    }

    /**
     * Sets the value of the numEmployeesLeasedToOthers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumEmployeesLeasedToOthers(BigInteger value) {
        this.numEmployeesLeasedToOthers = value;
    }

    /**
     * Gets the value of the numEmployeesLeasedFromOthers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumEmployeesLeasedFromOthers() {
        return numEmployeesLeasedFromOthers;
    }

    /**
     * Sets the value of the numEmployeesLeasedFromOthers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumEmployeesLeasedFromOthers(BigInteger value) {
        this.numEmployeesLeasedFromOthers = value;
    }

    /**
     * Gets the value of the employeeBenefitPlanTotalAssetAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEmployeeBenefitPlanTotalAssetAmt() {
        return employeeBenefitPlanTotalAssetAmt;
    }

    /**
     * Sets the value of the employeeBenefitPlanTotalAssetAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEmployeeBenefitPlanTotalAssetAmt(BigDecimal value) {
        this.employeeBenefitPlanTotalAssetAmt = value;
    }

    /**
     * Gets the value of the crimeSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link CrimeSchedule }
     *     
     */
    public CrimeSchedule getCrimeSchedule() {
        return crimeSchedule;
    }

    /**
     * Sets the value of the crimeSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrimeSchedule }
     *     
     */
    public void setCrimeSchedule(CrimeSchedule value) {
        this.crimeSchedule = value;
    }

    /**
     * Gets the value of the crimeMoneyAndSecurities property.
     * 
     * @return
     *     possible object is
     *     {@link CrimeMoneyAndSecurities }
     *     
     */
    public CrimeMoneyAndSecurities getCrimeMoneyAndSecurities() {
        return crimeMoneyAndSecurities;
    }

    /**
     * Sets the value of the crimeMoneyAndSecurities property.
     * 
     * @param value
     *     allowed object is
     *     {@link CrimeMoneyAndSecurities }
     *     
     */
    public void setCrimeMoneyAndSecurities(CrimeMoneyAndSecurities value) {
        this.crimeMoneyAndSecurities = value;
    }

    /**
     * Gets the value of the comCscItemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public ComCscItemIdInfo getComCscItemIdInfo() {
        return comCscItemIdInfo;
    }

    /**
     * Sets the value of the comCscItemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public void setComCscItemIdInfo(ComCscItemIdInfo value) {
        this.comCscItemIdInfo = value;
    }

}
