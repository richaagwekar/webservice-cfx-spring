//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_UpdateSuspense" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisburseSuspense" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReverseReceipt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ManuallySuspend" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WriteOffInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscUpdateSuspense",
    "comCscDisburseSuspense",
    "comCscReverseReceipt",
    "comCscManuallySuspend",
    "comCscWriteOffInfo"
})
@XmlRootElement(name = "com.csc_UnidentifiedActivities")
public class ComCscUnidentifiedActivities {

    @XmlElement(name = "com.csc_UpdateSuspense")
    protected ComCscUpdateSuspense comCscUpdateSuspense;
    @XmlElement(name = "com.csc_DisburseSuspense")
    protected ComCscDisburseSuspense comCscDisburseSuspense;
    @XmlElement(name = "com.csc_ReverseReceipt")
    protected ComCscReverseReceipt comCscReverseReceipt;
    @XmlElement(name = "com.csc_ManuallySuspend")
    protected ComCscManuallySuspend comCscManuallySuspend;
    @XmlElement(name = "com.csc_WriteOffInfo")
    protected ComCscWriteOffInfo comCscWriteOffInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscUpdateSuspense property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscUpdateSuspense }
     *     
     */
    public ComCscUpdateSuspense getComCscUpdateSuspense() {
        return comCscUpdateSuspense;
    }

    /**
     * Sets the value of the comCscUpdateSuspense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscUpdateSuspense }
     *     
     */
    public void setComCscUpdateSuspense(ComCscUpdateSuspense value) {
        this.comCscUpdateSuspense = value;
    }

    /**
     * Gets the value of the comCscDisburseSuspense property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscDisburseSuspense }
     *     
     */
    public ComCscDisburseSuspense getComCscDisburseSuspense() {
        return comCscDisburseSuspense;
    }

    /**
     * Sets the value of the comCscDisburseSuspense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscDisburseSuspense }
     *     
     */
    public void setComCscDisburseSuspense(ComCscDisburseSuspense value) {
        this.comCscDisburseSuspense = value;
    }

    /**
     * Gets the value of the comCscReverseReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscReverseReceipt }
     *     
     */
    public ComCscReverseReceipt getComCscReverseReceipt() {
        return comCscReverseReceipt;
    }

    /**
     * Sets the value of the comCscReverseReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscReverseReceipt }
     *     
     */
    public void setComCscReverseReceipt(ComCscReverseReceipt value) {
        this.comCscReverseReceipt = value;
    }

    /**
     * Gets the value of the comCscManuallySuspend property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscManuallySuspend }
     *     
     */
    public ComCscManuallySuspend getComCscManuallySuspend() {
        return comCscManuallySuspend;
    }

    /**
     * Sets the value of the comCscManuallySuspend property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscManuallySuspend }
     *     
     */
    public void setComCscManuallySuspend(ComCscManuallySuspend value) {
        this.comCscManuallySuspend = value;
    }

    /**
     * Gets the value of the comCscWriteOffInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscWriteOffInfo }
     *     
     */
    public ComCscWriteOffInfo getComCscWriteOffInfo() {
        return comCscWriteOffInfo;
    }

    /**
     * Sets the value of the comCscWriteOffInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscWriteOffInfo }
     *     
     */
    public void setComCscWriteOffInfo(ComCscWriteOffInfo value) {
        this.comCscWriteOffInfo = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
