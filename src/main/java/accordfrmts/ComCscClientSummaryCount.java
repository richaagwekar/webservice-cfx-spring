//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_ClientAddressInd"/>
 *         &lt;element ref="{}com.csc_ClientPhoneInd"/>
 *         &lt;element ref="{}com.csc_ClientReferenceInd"/>
 *         &lt;element ref="{}com.csc_ClientTaxInd"/>
 *         &lt;element ref="{}com.csc_ClientEmailInd"/>
 *         &lt;element ref="{}com.csc_HouseholdClientInd"/>
 *         &lt;element ref="{}com.csc_CommentsInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdditionalNameInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LicenseInd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscClientAddressInd",
    "comCscClientPhoneInd",
    "comCscClientReferenceInd",
    "comCscClientTaxInd",
    "comCscClientEmailInd",
    "comCscHouseholdClientInd",
    "comCscCommentsInd",
    "comCscAdditionalNameInd",
    "comCscLicenseInd"
})
@XmlRootElement(name = "com.csc_ClientSummaryCount")
public class ComCscClientSummaryCount {

    @XmlElement(name = "com.csc_ClientAddressInd", required = true)
    protected Boolean comCscClientAddressInd;
    @XmlElement(name = "com.csc_ClientPhoneInd", required = true)
    protected Boolean comCscClientPhoneInd;
    @XmlElement(name = "com.csc_ClientReferenceInd", required = true)
    protected Boolean comCscClientReferenceInd;
    @XmlElement(name = "com.csc_ClientTaxInd", required = true)
    protected Boolean comCscClientTaxInd;
    @XmlElement(name = "com.csc_ClientEmailInd", required = true)
    protected Boolean comCscClientEmailInd;
    @XmlElement(name = "com.csc_HouseholdClientInd", required = true)
    protected Boolean comCscHouseholdClientInd;
    @XmlElement(name = "com.csc_CommentsInd")
    protected Boolean comCscCommentsInd;
    @XmlElement(name = "com.csc_AdditionalNameInd")
    protected Boolean comCscAdditionalNameInd;
    @XmlElement(name = "com.csc_LicenseInd")
    protected Boolean comCscLicenseInd;

    /**
     * Gets the value of the comCscClientAddressInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscClientAddressInd() {
        return comCscClientAddressInd;
    }

    /**
     * Sets the value of the comCscClientAddressInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscClientAddressInd(Boolean value) {
        this.comCscClientAddressInd = value;
    }

    /**
     * Gets the value of the comCscClientPhoneInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscClientPhoneInd() {
        return comCscClientPhoneInd;
    }

    /**
     * Sets the value of the comCscClientPhoneInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscClientPhoneInd(Boolean value) {
        this.comCscClientPhoneInd = value;
    }

    /**
     * Gets the value of the comCscClientReferenceInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscClientReferenceInd() {
        return comCscClientReferenceInd;
    }

    /**
     * Sets the value of the comCscClientReferenceInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscClientReferenceInd(Boolean value) {
        this.comCscClientReferenceInd = value;
    }

    /**
     * Gets the value of the comCscClientTaxInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscClientTaxInd() {
        return comCscClientTaxInd;
    }

    /**
     * Sets the value of the comCscClientTaxInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscClientTaxInd(Boolean value) {
        this.comCscClientTaxInd = value;
    }

    /**
     * Gets the value of the comCscClientEmailInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscClientEmailInd() {
        return comCscClientEmailInd;
    }

    /**
     * Sets the value of the comCscClientEmailInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscClientEmailInd(Boolean value) {
        this.comCscClientEmailInd = value;
    }

    /**
     * Gets the value of the comCscHouseholdClientInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscHouseholdClientInd() {
        return comCscHouseholdClientInd;
    }

    /**
     * Sets the value of the comCscHouseholdClientInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscHouseholdClientInd(Boolean value) {
        this.comCscHouseholdClientInd = value;
    }

    /**
     * Gets the value of the comCscCommentsInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscCommentsInd() {
        return comCscCommentsInd;
    }

    /**
     * Sets the value of the comCscCommentsInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscCommentsInd(Boolean value) {
        this.comCscCommentsInd = value;
    }

    /**
     * Gets the value of the comCscAdditionalNameInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscAdditionalNameInd() {
        return comCscAdditionalNameInd;
    }

    /**
     * Sets the value of the comCscAdditionalNameInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscAdditionalNameInd(Boolean value) {
        this.comCscAdditionalNameInd = value;
    }

    /**
     * Gets the value of the comCscLicenseInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscLicenseInd() {
        return comCscLicenseInd;
    }

    /**
     * Sets the value of the comCscLicenseInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscLicenseInd(Boolean value) {
        this.comCscLicenseInd = value;
    }

}
