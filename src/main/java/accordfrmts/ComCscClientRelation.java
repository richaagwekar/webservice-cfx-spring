//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}NameInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CrossReferenceTypeCd"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "nameInfo",
    "comCscClientTypeCd",
    "comCscCrossReferenceTypeCd",
    "effectiveDt",
    "expirationDt",
    "actionCd"
})
@XmlRootElement(name = "com.csc_ClientRelation")
public class ComCscClientRelation {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "NameInfo")
    protected NameInfo nameInfo;
    @XmlElement(name = "com.csc_ClientTypeCd")
    protected String comCscClientTypeCd;
    @XmlElement(name = "com.csc_CrossReferenceTypeCd", required = true)
    protected String comCscCrossReferenceTypeCd;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDt;
    @XmlElement(name = "ActionCd")
    protected Action actionCd;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the nameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NameInfo }
     *     
     */
    public NameInfo getNameInfo() {
        return nameInfo;
    }

    /**
     * Sets the value of the nameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameInfo }
     *     
     */
    public void setNameInfo(NameInfo value) {
        this.nameInfo = value;
    }

    /**
     * Gets the value of the comCscClientTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscClientTypeCd() {
        return comCscClientTypeCd;
    }

    /**
     * Sets the value of the comCscClientTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscClientTypeCd(String value) {
        this.comCscClientTypeCd = value;
    }

    /**
     * Gets the value of the comCscCrossReferenceTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCrossReferenceTypeCd() {
        return comCscCrossReferenceTypeCd;
    }

    /**
     * Sets the value of the comCscCrossReferenceTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCrossReferenceTypeCd(String value) {
        this.comCscCrossReferenceTypeCd = value;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDt() {
        return expirationDt;
    }

    /**
     * Sets the value of the expirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDt(XMLGregorianCalendar value) {
        this.expirationDt = value;
    }

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

}
