//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_CashOverpaymentInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_OverPaymentMinimumDisbursementAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_OverPaymentMaximumDisbursementAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NumDaysOverPaymentHold" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscCashOverpaymentInd",
    "comCscOverPaymentMinimumDisbursementAmt",
    "comCscOverPaymentMaximumDisbursementAmt",
    "comCscNumDaysOverPaymentHold"
})
@XmlRootElement(name = "com.csc_OverPaymentInfo")
public class ComCscOverPaymentInfo {

    @XmlElement(name = "com.csc_CashOverpaymentInd")
    protected Boolean comCscCashOverpaymentInd;
    @XmlElement(name = "com.csc_OverPaymentMinimumDisbursementAmt")
    protected BigDecimal comCscOverPaymentMinimumDisbursementAmt;
    @XmlElement(name = "com.csc_OverPaymentMaximumDisbursementAmt")
    protected BigDecimal comCscOverPaymentMaximumDisbursementAmt;
    @XmlElement(name = "com.csc_NumDaysOverPaymentHold")
    protected BigInteger comCscNumDaysOverPaymentHold;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscCashOverpaymentInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscCashOverpaymentInd() {
        return comCscCashOverpaymentInd;
    }

    /**
     * Sets the value of the comCscCashOverpaymentInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscCashOverpaymentInd(Boolean value) {
        this.comCscCashOverpaymentInd = value;
    }

    /**
     * Gets the value of the comCscOverPaymentMinimumDisbursementAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscOverPaymentMinimumDisbursementAmt() {
        return comCscOverPaymentMinimumDisbursementAmt;
    }

    /**
     * Sets the value of the comCscOverPaymentMinimumDisbursementAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscOverPaymentMinimumDisbursementAmt(BigDecimal value) {
        this.comCscOverPaymentMinimumDisbursementAmt = value;
    }

    /**
     * Gets the value of the comCscOverPaymentMaximumDisbursementAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscOverPaymentMaximumDisbursementAmt() {
        return comCscOverPaymentMaximumDisbursementAmt;
    }

    /**
     * Sets the value of the comCscOverPaymentMaximumDisbursementAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscOverPaymentMaximumDisbursementAmt(BigDecimal value) {
        this.comCscOverPaymentMaximumDisbursementAmt = value;
    }

    /**
     * Gets the value of the comCscNumDaysOverPaymentHold property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComCscNumDaysOverPaymentHold() {
        return comCscNumDaysOverPaymentHold;
    }

    /**
     * Sets the value of the comCscNumDaysOverPaymentHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComCscNumDaysOverPaymentHold(BigInteger value) {
        this.comCscNumDaysOverPaymentHold = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
