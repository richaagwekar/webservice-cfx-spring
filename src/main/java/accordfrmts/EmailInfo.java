//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CommunicationUseCd" minOccurs="0"/>
 *         &lt;element ref="{}EmailAddr"/>
 *         &lt;element ref="{}DoNotContactInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EmailTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PrimaryInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdditionalItemExistsInd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="PreferredContactRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "communicationUseCd",
    "emailAddr",
    "doNotContactInd",
    "comCscEmailTypeCd",
    "comCscEffectiveDt",
    "comCscExpirationDt",
    "comCscOtherIdentifier",
    "comCscActionCd",
    "comCscPrimaryInd",
    "comCscAdditionalItemExistsInd"
})
@XmlRootElement(name = "EmailInfo")
public class EmailInfo {

    @XmlElement(name = "CommunicationUseCd")
    protected String communicationUseCd;
    @XmlElement(name = "EmailAddr", required = true)
    protected String emailAddr;
    @XmlElement(name = "DoNotContactInd")
    protected Boolean doNotContactInd;
    @XmlElement(name = "com.csc_EmailTypeCd")
    protected String comCscEmailTypeCd;
    @XmlElement(name = "com.csc_EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscEffectiveDt;
    @XmlElement(name = "com.csc_ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscExpirationDt;
    @XmlElement(name = "com.csc_OtherIdentifier")
    protected List<ComCscOtherIdentifier> comCscOtherIdentifier;
    @XmlElement(name = "com.csc_ActionCd")
    protected String comCscActionCd;
    @XmlElement(name = "com.csc_PrimaryInd")
    protected Boolean comCscPrimaryInd;
    @XmlElement(name = "com.csc_AdditionalItemExistsInd")
    protected Boolean comCscAdditionalItemExistsInd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "PreferredContactRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object preferredContactRef;

    /**
     * Gets the value of the communicationUseCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommunicationUseCd() {
        return communicationUseCd;
    }

    /**
     * Sets the value of the communicationUseCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommunicationUseCd(String value) {
        this.communicationUseCd = value;
    }

    /**
     * Gets the value of the emailAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * Sets the value of the emailAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddr(String value) {
        this.emailAddr = value;
    }

    /**
     * Gets the value of the doNotContactInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDoNotContactInd() {
        return doNotContactInd;
    }

    /**
     * Sets the value of the doNotContactInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoNotContactInd(Boolean value) {
        this.doNotContactInd = value;
    }

    /**
     * Gets the value of the comCscEmailTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEmailTypeCd() {
        return comCscEmailTypeCd;
    }

    /**
     * Sets the value of the comCscEmailTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEmailTypeCd(String value) {
        this.comCscEmailTypeCd = value;
    }

    /**
     * Gets the value of the comCscEffectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscEffectiveDt() {
        return comCscEffectiveDt;
    }

    /**
     * Sets the value of the comCscEffectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscEffectiveDt(XMLGregorianCalendar value) {
        this.comCscEffectiveDt = value;
    }

    /**
     * Gets the value of the comCscExpirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscExpirationDt() {
        return comCscExpirationDt;
    }

    /**
     * Sets the value of the comCscExpirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscExpirationDt(XMLGregorianCalendar value) {
        this.comCscExpirationDt = value;
    }

    /**
     * Gets the value of the comCscOtherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscOtherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscOtherIdentifier }
     * 
     * 
     */
    public List<ComCscOtherIdentifier> getComCscOtherIdentifier() {
        if (comCscOtherIdentifier == null) {
            comCscOtherIdentifier = new ArrayList<ComCscOtherIdentifier>();
        }
        return this.comCscOtherIdentifier;
    }

    /**
     * Gets the value of the comCscActionCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscActionCd() {
        return comCscActionCd;
    }

    /**
     * Sets the value of the comCscActionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscActionCd(String value) {
        this.comCscActionCd = value;
    }

    /**
     * Gets the value of the comCscPrimaryInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscPrimaryInd() {
        return comCscPrimaryInd;
    }

    /**
     * Sets the value of the comCscPrimaryInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscPrimaryInd(Boolean value) {
        this.comCscPrimaryInd = value;
    }

    /**
     * Gets the value of the comCscAdditionalItemExistsInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscAdditionalItemExistsInd() {
        return comCscAdditionalItemExistsInd;
    }

    /**
     * Sets the value of the comCscAdditionalItemExistsInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscAdditionalItemExistsInd(Boolean value) {
        this.comCscAdditionalItemExistsInd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the preferredContactRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getPreferredContactRef() {
        return preferredContactRef;
    }

    /**
     * Sets the value of the preferredContactRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setPreferredContactRef(Object value) {
        this.preferredContactRef = value;
    }

}
