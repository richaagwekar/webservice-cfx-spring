//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormatNameTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TemplateTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormattedName" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormattedNameTemplate" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormatClient" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "effectiveDt",
    "expirationDt",
    "comCscFormatNameTypeCd",
    "comCscTemplateTypeCd",
    "comCscFormattedName",
    "comCscFormattedNameTemplate",
    "comCscFormatClient"
})
@XmlRootElement(name = "com.csc_FormatName")
public class ComCscFormatName {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDt;
    @XmlElement(name = "com.csc_FormatNameTypeCd")
    protected String comCscFormatNameTypeCd;
    @XmlElement(name = "com.csc_TemplateTypeCd")
    protected String comCscTemplateTypeCd;
    @XmlElement(name = "com.csc_FormattedName")
    protected String comCscFormattedName;
    @XmlElement(name = "com.csc_FormattedNameTemplate")
    protected String comCscFormattedNameTemplate;
    @XmlElement(name = "com.csc_FormatClient")
    protected List<ComCscFormatClient> comCscFormatClient;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDt() {
        return expirationDt;
    }

    /**
     * Sets the value of the expirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDt(XMLGregorianCalendar value) {
        this.expirationDt = value;
    }

    /**
     * Gets the value of the comCscFormatNameTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscFormatNameTypeCd() {
        return comCscFormatNameTypeCd;
    }

    /**
     * Sets the value of the comCscFormatNameTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscFormatNameTypeCd(String value) {
        this.comCscFormatNameTypeCd = value;
    }

    /**
     * Gets the value of the comCscTemplateTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscTemplateTypeCd() {
        return comCscTemplateTypeCd;
    }

    /**
     * Sets the value of the comCscTemplateTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscTemplateTypeCd(String value) {
        this.comCscTemplateTypeCd = value;
    }

    /**
     * Gets the value of the comCscFormattedName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscFormattedName() {
        return comCscFormattedName;
    }

    /**
     * Sets the value of the comCscFormattedName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscFormattedName(String value) {
        this.comCscFormattedName = value;
    }

    /**
     * Gets the value of the comCscFormattedNameTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscFormattedNameTemplate() {
        return comCscFormattedNameTemplate;
    }

    /**
     * Sets the value of the comCscFormattedNameTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscFormattedNameTemplate(String value) {
        this.comCscFormattedNameTemplate = value;
    }

    /**
     * Gets the value of the comCscFormatClient property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscFormatClient property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscFormatClient().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscFormatClient }
     * 
     * 
     */
    public List<ComCscFormatClient> getComCscFormatClient() {
        if (comCscFormatClient == null) {
            comCscFormatClient = new ArrayList<ComCscFormatClient>();
        }
        return this.comCscFormatClient;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
