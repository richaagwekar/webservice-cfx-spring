//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingAccountNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AccountPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RenewalAccountPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReportingLevel" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SubGroup" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdditionalId" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyBillPlanDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RenewalBillPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingPayorInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RenewalBillingPayorInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AllocationCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AllocationDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AllocationVal" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingThirdPartyNameInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SequenceNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TransactionProcessDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicySplitInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_OtherRemittingAccountNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_MethodOfBillingCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PresentmentMethodCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillEFTPlan" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EFTTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}FromAcct" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "comCscPolicyInfo",
    "comCscBillingAccountNumber",
    "comCscBillTypeCd",
    "comCscAccountPlanCd",
    "comCscRenewalAccountPlanCd",
    "comCscReportingLevel",
    "comCscSubGroup",
    "comCscAdditionalId",
    "comCscBillPlanCd",
    "comCscPolicyBillPlanDesc",
    "comCscRenewalBillPlanCd",
    "comCscBillingPayorInfo",
    "comCscRenewalBillingPayorInfo",
    "comCscActionInfo",
    "comCscAllocationCd",
    "comCscAllocationDesc",
    "comCscAllocationVal",
    "comCscBillingThirdPartyNameInfo",
    "comCscSequenceNumber",
    "comCscTransactionProcessDt",
    "comCscPolicySplitInfo",
    "comCscOtherRemittingAccountNumber",
    "comCscMethodOfBillingCd",
    "comCscPresentmentMethodCd",
    "comCscBillEFTPlan",
    "comCscEFTTypeCd",
    "fromAcct"
})
@XmlRootElement(name = "com.csc_PolicyBillingInfo")
public class ComCscPolicyBillingInfo {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "com.csc_PolicyInfo")
    protected ComCscPolicyInfo comCscPolicyInfo;
    @XmlElement(name = "com.csc_BillingAccountNumber")
    protected String comCscBillingAccountNumber;
    @XmlElement(name = "com.csc_BillTypeCd")
    protected String comCscBillTypeCd;
    @XmlElement(name = "com.csc_AccountPlanCd")
    protected String comCscAccountPlanCd;
    @XmlElement(name = "com.csc_RenewalAccountPlanCd")
    protected String comCscRenewalAccountPlanCd;
    @XmlElement(name = "com.csc_ReportingLevel")
    protected String comCscReportingLevel;
    @XmlElement(name = "com.csc_SubGroup")
    protected String comCscSubGroup;
    @XmlElement(name = "com.csc_AdditionalId")
    protected String comCscAdditionalId;
    @XmlElement(name = "com.csc_BillPlanCd")
    protected String comCscBillPlanCd;
    @XmlElement(name = "com.csc_PolicyBillPlanDesc")
    protected String comCscPolicyBillPlanDesc;
    @XmlElement(name = "com.csc_RenewalBillPlanCd")
    protected String comCscRenewalBillPlanCd;
    @XmlElement(name = "com.csc_BillingPayorInfo")
    protected ComCscBillingPayorInfo comCscBillingPayorInfo;
    @XmlElement(name = "com.csc_RenewalBillingPayorInfo")
    protected ComCscRenewalBillingPayorInfo comCscRenewalBillingPayorInfo;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlElement(name = "com.csc_AllocationCd")
    protected String comCscAllocationCd;
    @XmlElement(name = "com.csc_AllocationDesc")
    protected String comCscAllocationDesc;
    @XmlElement(name = "com.csc_AllocationVal")
    protected String comCscAllocationVal;
    @XmlElement(name = "com.csc_BillingThirdPartyNameInfo")
    protected ComCscBillingThirdPartyNameInfo comCscBillingThirdPartyNameInfo;
    @XmlElement(name = "com.csc_SequenceNumber")
    protected String comCscSequenceNumber;
    @XmlElement(name = "com.csc_TransactionProcessDt")
    protected String comCscTransactionProcessDt;
    @XmlElement(name = "com.csc_PolicySplitInfo")
    protected ComCscPolicySplitInfo comCscPolicySplitInfo;
    @XmlElement(name = "com.csc_OtherRemittingAccountNumber")
    protected String comCscOtherRemittingAccountNumber;
    @XmlElement(name = "com.csc_MethodOfBillingCd")
    protected String comCscMethodOfBillingCd;
    @XmlElement(name = "com.csc_PresentmentMethodCd")
    protected String comCscPresentmentMethodCd;
    @XmlElement(name = "com.csc_BillEFTPlan")
    protected String comCscBillEFTPlan;
    @XmlElement(name = "com.csc_EFTTypeCd")
    protected String comCscEFTTypeCd;
    @XmlElement(name = "FromAcct")
    protected FromAcct fromAcct;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the comCscPolicyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPolicyInfo }
     *     
     */
    public ComCscPolicyInfo getComCscPolicyInfo() {
        return comCscPolicyInfo;
    }

    /**
     * Sets the value of the comCscPolicyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPolicyInfo }
     *     
     */
    public void setComCscPolicyInfo(ComCscPolicyInfo value) {
        this.comCscPolicyInfo = value;
    }

    /**
     * Gets the value of the comCscBillingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillingAccountNumber() {
        return comCscBillingAccountNumber;
    }

    /**
     * Sets the value of the comCscBillingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillingAccountNumber(String value) {
        this.comCscBillingAccountNumber = value;
    }

    /**
     * Gets the value of the comCscBillTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillTypeCd() {
        return comCscBillTypeCd;
    }

    /**
     * Sets the value of the comCscBillTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillTypeCd(String value) {
        this.comCscBillTypeCd = value;
    }

    /**
     * Gets the value of the comCscAccountPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAccountPlanCd() {
        return comCscAccountPlanCd;
    }

    /**
     * Sets the value of the comCscAccountPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAccountPlanCd(String value) {
        this.comCscAccountPlanCd = value;
    }

    /**
     * Gets the value of the comCscRenewalAccountPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRenewalAccountPlanCd() {
        return comCscRenewalAccountPlanCd;
    }

    /**
     * Sets the value of the comCscRenewalAccountPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRenewalAccountPlanCd(String value) {
        this.comCscRenewalAccountPlanCd = value;
    }

    /**
     * Gets the value of the comCscReportingLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReportingLevel() {
        return comCscReportingLevel;
    }

    /**
     * Sets the value of the comCscReportingLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReportingLevel(String value) {
        this.comCscReportingLevel = value;
    }

    /**
     * Gets the value of the comCscSubGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSubGroup() {
        return comCscSubGroup;
    }

    /**
     * Sets the value of the comCscSubGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSubGroup(String value) {
        this.comCscSubGroup = value;
    }

    /**
     * Gets the value of the comCscAdditionalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAdditionalId() {
        return comCscAdditionalId;
    }

    /**
     * Sets the value of the comCscAdditionalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAdditionalId(String value) {
        this.comCscAdditionalId = value;
    }

    /**
     * Gets the value of the comCscBillPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillPlanCd() {
        return comCscBillPlanCd;
    }

    /**
     * Sets the value of the comCscBillPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillPlanCd(String value) {
        this.comCscBillPlanCd = value;
    }

    /**
     * Gets the value of the comCscPolicyBillPlanDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPolicyBillPlanDesc() {
        return comCscPolicyBillPlanDesc;
    }

    /**
     * Sets the value of the comCscPolicyBillPlanDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPolicyBillPlanDesc(String value) {
        this.comCscPolicyBillPlanDesc = value;
    }

    /**
     * Gets the value of the comCscRenewalBillPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRenewalBillPlanCd() {
        return comCscRenewalBillPlanCd;
    }

    /**
     * Sets the value of the comCscRenewalBillPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRenewalBillPlanCd(String value) {
        this.comCscRenewalBillPlanCd = value;
    }

    /**
     * Gets the value of the comCscBillingPayorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscBillingPayorInfo }
     *     
     */
    public ComCscBillingPayorInfo getComCscBillingPayorInfo() {
        return comCscBillingPayorInfo;
    }

    /**
     * Sets the value of the comCscBillingPayorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscBillingPayorInfo }
     *     
     */
    public void setComCscBillingPayorInfo(ComCscBillingPayorInfo value) {
        this.comCscBillingPayorInfo = value;
    }

    /**
     * Gets the value of the comCscRenewalBillingPayorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscRenewalBillingPayorInfo }
     *     
     */
    public ComCscRenewalBillingPayorInfo getComCscRenewalBillingPayorInfo() {
        return comCscRenewalBillingPayorInfo;
    }

    /**
     * Sets the value of the comCscRenewalBillingPayorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscRenewalBillingPayorInfo }
     *     
     */
    public void setComCscRenewalBillingPayorInfo(ComCscRenewalBillingPayorInfo value) {
        this.comCscRenewalBillingPayorInfo = value;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the comCscAllocationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAllocationCd() {
        return comCscAllocationCd;
    }

    /**
     * Sets the value of the comCscAllocationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAllocationCd(String value) {
        this.comCscAllocationCd = value;
    }

    /**
     * Gets the value of the comCscAllocationDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAllocationDesc() {
        return comCscAllocationDesc;
    }

    /**
     * Sets the value of the comCscAllocationDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAllocationDesc(String value) {
        this.comCscAllocationDesc = value;
    }

    /**
     * Gets the value of the comCscAllocationVal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAllocationVal() {
        return comCscAllocationVal;
    }

    /**
     * Sets the value of the comCscAllocationVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAllocationVal(String value) {
        this.comCscAllocationVal = value;
    }

    /**
     * Gets the value of the comCscBillingThirdPartyNameInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscBillingThirdPartyNameInfo }
     *     
     */
    public ComCscBillingThirdPartyNameInfo getComCscBillingThirdPartyNameInfo() {
        return comCscBillingThirdPartyNameInfo;
    }

    /**
     * Sets the value of the comCscBillingThirdPartyNameInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscBillingThirdPartyNameInfo }
     *     
     */
    public void setComCscBillingThirdPartyNameInfo(ComCscBillingThirdPartyNameInfo value) {
        this.comCscBillingThirdPartyNameInfo = value;
    }

    /**
     * Gets the value of the comCscSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSequenceNumber() {
        return comCscSequenceNumber;
    }

    /**
     * Sets the value of the comCscSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSequenceNumber(String value) {
        this.comCscSequenceNumber = value;
    }

    /**
     * Gets the value of the comCscTransactionProcessDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscTransactionProcessDt() {
        return comCscTransactionProcessDt;
    }

    /**
     * Sets the value of the comCscTransactionProcessDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscTransactionProcessDt(String value) {
        this.comCscTransactionProcessDt = value;
    }

    /**
     * Gets the value of the comCscPolicySplitInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPolicySplitInfo }
     *     
     */
    public ComCscPolicySplitInfo getComCscPolicySplitInfo() {
        return comCscPolicySplitInfo;
    }

    /**
     * Sets the value of the comCscPolicySplitInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPolicySplitInfo }
     *     
     */
    public void setComCscPolicySplitInfo(ComCscPolicySplitInfo value) {
        this.comCscPolicySplitInfo = value;
    }

    /**
     * Gets the value of the comCscOtherRemittingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscOtherRemittingAccountNumber() {
        return comCscOtherRemittingAccountNumber;
    }

    /**
     * Sets the value of the comCscOtherRemittingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscOtherRemittingAccountNumber(String value) {
        this.comCscOtherRemittingAccountNumber = value;
    }

    /**
     * Gets the value of the comCscMethodOfBillingCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscMethodOfBillingCd() {
        return comCscMethodOfBillingCd;
    }

    /**
     * Sets the value of the comCscMethodOfBillingCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscMethodOfBillingCd(String value) {
        this.comCscMethodOfBillingCd = value;
    }

    /**
     * Gets the value of the comCscPresentmentMethodCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscPresentmentMethodCd() {
        return comCscPresentmentMethodCd;
    }

    /**
     * Sets the value of the comCscPresentmentMethodCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscPresentmentMethodCd(String value) {
        this.comCscPresentmentMethodCd = value;
    }

    /**
     * Gets the value of the comCscBillEFTPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillEFTPlan() {
        return comCscBillEFTPlan;
    }

    /**
     * Sets the value of the comCscBillEFTPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillEFTPlan(String value) {
        this.comCscBillEFTPlan = value;
    }

    /**
     * Gets the value of the comCscEFTTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscEFTTypeCd() {
        return comCscEFTTypeCd;
    }

    /**
     * Sets the value of the comCscEFTTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscEFTTypeCd(String value) {
        this.comCscEFTTypeCd = value;
    }

    /**
     * Gets the value of the fromAcct property.
     * 
     * @return
     *     possible object is
     *     {@link FromAcct }
     *     
     */
    public FromAcct getFromAcct() {
        return fromAcct;
    }

    /**
     * Sets the value of the fromAcct property.
     * 
     * @param value
     *     allowed object is
     *     {@link FromAcct }
     *     
     */
    public void setFromAcct(FromAcct value) {
        this.fromAcct = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
