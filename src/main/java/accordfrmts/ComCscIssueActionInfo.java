//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{}com.csc_ScheduleIssueInd"/>
 *         &lt;element ref="{}com.csc_ScheduleIssueNPInd"/>
 *         &lt;element ref="{}com.csc_AcceptQuoteInd"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscScheduleIssueInd",
    "comCscScheduleIssueNPInd",
    "comCscAcceptQuoteInd"
})
@XmlRootElement(name = "com.csc_IssueActionInfo")
public class ComCscIssueActionInfo {

    @XmlElement(name = "com.csc_ScheduleIssueInd")
    protected Boolean comCscScheduleIssueInd;
    @XmlElement(name = "com.csc_ScheduleIssueNPInd")
    protected Boolean comCscScheduleIssueNPInd;
    @XmlElement(name = "com.csc_AcceptQuoteInd")
    protected Boolean comCscAcceptQuoteInd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscScheduleIssueInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscScheduleIssueInd() {
        return comCscScheduleIssueInd;
    }

    /**
     * Sets the value of the comCscScheduleIssueInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscScheduleIssueInd(Boolean value) {
        this.comCscScheduleIssueInd = value;
    }

    /**
     * Gets the value of the comCscScheduleIssueNPInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscScheduleIssueNPInd() {
        return comCscScheduleIssueNPInd;
    }

    /**
     * Sets the value of the comCscScheduleIssueNPInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscScheduleIssueNPInd(Boolean value) {
        this.comCscScheduleIssueNPInd = value;
    }

    /**
     * Gets the value of the comCscAcceptQuoteInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscAcceptQuoteInd() {
        return comCscAcceptQuoteInd;
    }

    /**
     * Sets the value of the comCscAcceptQuoteInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscAcceptQuoteInd(Boolean value) {
        this.comCscAcceptQuoteInd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
