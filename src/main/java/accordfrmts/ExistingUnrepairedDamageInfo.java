//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ExistingUnrepairedDamageInd" minOccurs="0"/>
 *         &lt;element ref="{}ExistingUnrepairedDamageDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_VehiclePriorDamageCd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "existingUnrepairedDamageInd",
    "existingUnrepairedDamageDesc",
    "comCscVehiclePriorDamageCd"
})
@XmlRootElement(name = "ExistingUnrepairedDamageInfo")
public class ExistingUnrepairedDamageInfo {

    @XmlElement(name = "ExistingUnrepairedDamageInd")
    protected Boolean existingUnrepairedDamageInd;
    @XmlElement(name = "ExistingUnrepairedDamageDesc")
    protected String existingUnrepairedDamageDesc;
    @XmlElement(name = "com.csc_VehiclePriorDamageCd")
    protected PCVehicleDamage comCscVehiclePriorDamageCd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the existingUnrepairedDamageInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExistingUnrepairedDamageInd() {
        return existingUnrepairedDamageInd;
    }

    /**
     * Sets the value of the existingUnrepairedDamageInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExistingUnrepairedDamageInd(Boolean value) {
        this.existingUnrepairedDamageInd = value;
    }

    /**
     * Gets the value of the existingUnrepairedDamageDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExistingUnrepairedDamageDesc() {
        return existingUnrepairedDamageDesc;
    }

    /**
     * Sets the value of the existingUnrepairedDamageDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExistingUnrepairedDamageDesc(String value) {
        this.existingUnrepairedDamageDesc = value;
    }

    /**
     * Gets the value of the comCscVehiclePriorDamageCd property.
     * 
     * @return
     *     possible object is
     *     {@link PCVehicleDamage }
     *     
     */
    public PCVehicleDamage getComCscVehiclePriorDamageCd() {
        return comCscVehiclePriorDamageCd;
    }

    /**
     * Sets the value of the comCscVehiclePriorDamageCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link PCVehicleDamage }
     *     
     */
    public void setComCscVehiclePriorDamageCd(PCVehicleDamage value) {
        this.comCscVehiclePriorDamageCd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
