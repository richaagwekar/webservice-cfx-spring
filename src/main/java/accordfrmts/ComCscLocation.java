//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo"/>
 *         &lt;element ref="{}Addr"/>
 *         &lt;element ref="{}CountyTownCd" minOccurs="0"/>
 *         &lt;element ref="{}RiskLocationCd" minOccurs="0"/>
 *         &lt;element ref="{}EarthquakeZoneCd" minOccurs="0"/>
 *         &lt;element ref="{}TaxCodeInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}SubLocation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}AdditionalInterest" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}FireDistrict" minOccurs="0"/>
 *         &lt;element ref="{}FireDistrictCd" minOccurs="0"/>
 *         &lt;element ref="{}Communications" minOccurs="0"/>
 *         &lt;element ref="{}LocationName" minOccurs="0"/>
 *         &lt;element ref="{}LocationDesc" minOccurs="0"/>
 *         &lt;element ref="{}CatastropheZoneCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Form" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SupplementalDocument" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="NameInfoRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *       &lt;attribute name="NameInfoRefs" type="{http://www.w3.org/2001/XMLSchema}IDREFS" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "addr",
    "countyTownCd",
    "riskLocationCd",
    "earthquakeZoneCd",
    "taxCodeInfo",
    "subLocation",
    "additionalInterest",
    "fireDistrict",
    "fireDistrictCd",
    "communications",
    "locationName",
    "locationDesc",
    "catastropheZoneCd",
    "comCscActionInfo",
    "comCscForm",
    "comCscSupplementalDocument"
})
@XmlRootElement(name = "com.csc_Location")
public class ComCscLocation {

    @XmlElement(name = "ItemIdInfo", required = true)
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "Addr", required = true)
    protected Addr addr;
    @XmlElement(name = "CountyTownCd")
    protected String countyTownCd;
    @XmlElement(name = "RiskLocationCd")
    protected String riskLocationCd;
    @XmlElement(name = "EarthquakeZoneCd")
    protected String earthquakeZoneCd;
    @XmlElement(name = "TaxCodeInfo")
    protected List<TaxCodeInfo> taxCodeInfo;
    @XmlElement(name = "SubLocation")
    protected List<SubLocation> subLocation;
    @XmlElement(name = "AdditionalInterest")
    protected List<AdditionalInterest> additionalInterest;
    @XmlElement(name = "FireDistrict")
    protected String fireDistrict;
    @XmlElement(name = "FireDistrictCd")
    protected String fireDistrictCd;
    @XmlElement(name = "Communications")
    protected Communications communications;
    @XmlElement(name = "LocationName")
    protected String locationName;
    @XmlElement(name = "LocationDesc")
    protected String locationDesc;
    @XmlElement(name = "CatastropheZoneCd")
    protected List<String> catastropheZoneCd;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlElement(name = "com.csc_Form")
    protected List<ComCscForm> comCscForm;
    @XmlElement(name = "com.csc_SupplementalDocument")
    protected List<ComCscSupplementalDocument> comCscSupplementalDocument;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "NameInfoRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object nameInfoRef;
    @XmlAttribute(name = "NameInfoRefs")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> nameInfoRefs;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link Addr }
     *     
     */
    public Addr getAddr() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Addr }
     *     
     */
    public void setAddr(Addr value) {
        this.addr = value;
    }

    /**
     * Gets the value of the countyTownCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountyTownCd() {
        return countyTownCd;
    }

    /**
     * Sets the value of the countyTownCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountyTownCd(String value) {
        this.countyTownCd = value;
    }

    /**
     * Gets the value of the riskLocationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskLocationCd() {
        return riskLocationCd;
    }

    /**
     * Sets the value of the riskLocationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskLocationCd(String value) {
        this.riskLocationCd = value;
    }

    /**
     * Gets the value of the earthquakeZoneCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarthquakeZoneCd() {
        return earthquakeZoneCd;
    }

    /**
     * Sets the value of the earthquakeZoneCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarthquakeZoneCd(String value) {
        this.earthquakeZoneCd = value;
    }

    /**
     * Gets the value of the taxCodeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxCodeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxCodeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxCodeInfo }
     * 
     * 
     */
    public List<TaxCodeInfo> getTaxCodeInfo() {
        if (taxCodeInfo == null) {
            taxCodeInfo = new ArrayList<TaxCodeInfo>();
        }
        return this.taxCodeInfo;
    }

    /**
     * Gets the value of the subLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubLocation }
     * 
     * 
     */
    public List<SubLocation> getSubLocation() {
        if (subLocation == null) {
            subLocation = new ArrayList<SubLocation>();
        }
        return this.subLocation;
    }

    /**
     * Gets the value of the additionalInterest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInterest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInterest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInterest }
     * 
     * 
     */
    public List<AdditionalInterest> getAdditionalInterest() {
        if (additionalInterest == null) {
            additionalInterest = new ArrayList<AdditionalInterest>();
        }
        return this.additionalInterest;
    }

    /**
     * Gets the value of the fireDistrict property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFireDistrict() {
        return fireDistrict;
    }

    /**
     * Sets the value of the fireDistrict property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFireDistrict(String value) {
        this.fireDistrict = value;
    }

    /**
     * Gets the value of the fireDistrictCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFireDistrictCd() {
        return fireDistrictCd;
    }

    /**
     * Sets the value of the fireDistrictCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFireDistrictCd(String value) {
        this.fireDistrictCd = value;
    }

    /**
     * Gets the value of the communications property.
     * 
     * @return
     *     possible object is
     *     {@link Communications }
     *     
     */
    public Communications getCommunications() {
        return communications;
    }

    /**
     * Sets the value of the communications property.
     * 
     * @param value
     *     allowed object is
     *     {@link Communications }
     *     
     */
    public void setCommunications(Communications value) {
        this.communications = value;
    }

    /**
     * Gets the value of the locationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Sets the value of the locationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Gets the value of the locationDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationDesc() {
        return locationDesc;
    }

    /**
     * Sets the value of the locationDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationDesc(String value) {
        this.locationDesc = value;
    }

    /**
     * Gets the value of the catastropheZoneCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the catastropheZoneCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCatastropheZoneCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCatastropheZoneCd() {
        if (catastropheZoneCd == null) {
            catastropheZoneCd = new ArrayList<String>();
        }
        return this.catastropheZoneCd;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the comCscForm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscForm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscForm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscForm }
     * 
     * 
     */
    public List<ComCscForm> getComCscForm() {
        if (comCscForm == null) {
            comCscForm = new ArrayList<ComCscForm>();
        }
        return this.comCscForm;
    }

    /**
     * Gets the value of the comCscSupplementalDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscSupplementalDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscSupplementalDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscSupplementalDocument }
     * 
     * 
     */
    public List<ComCscSupplementalDocument> getComCscSupplementalDocument() {
        if (comCscSupplementalDocument == null) {
            comCscSupplementalDocument = new ArrayList<ComCscSupplementalDocument>();
        }
        return this.comCscSupplementalDocument;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the nameInfoRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getNameInfoRef() {
        return nameInfoRef;
    }

    /**
     * Sets the value of the nameInfoRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setNameInfoRef(Object value) {
        this.nameInfoRef = value;
    }

    /**
     * Gets the value of the nameInfoRefs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameInfoRefs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameInfoRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getNameInfoRefs() {
        if (nameInfoRefs == null) {
            nameInfoRefs = new ArrayList<Object>();
        }
        return this.nameInfoRefs;
    }

}
