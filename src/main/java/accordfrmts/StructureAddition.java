//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}StructureDesc" minOccurs="0"/>
 *         &lt;element ref="{}StructureUseDesc" minOccurs="0"/>
 *         &lt;element ref="{}Length" minOccurs="0"/>
 *         &lt;element ref="{}Width" minOccurs="0"/>
 *         &lt;element ref="{}Height" minOccurs="0"/>
 *         &lt;element ref="{}StructureConditionCd" minOccurs="0"/>
 *         &lt;element ref="{}Construction" minOccurs="0"/>
 *         &lt;element ref="{}BldgExposures" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}BldgProtection" minOccurs="0"/>
 *         &lt;element ref="{}AlarmAndSecurity" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}BldgImprovements" minOccurs="0"/>
 *         &lt;element ref="{}BldgFeatures" minOccurs="0"/>
 *         &lt;element ref="{}HeatingUnitInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}StorageTankInfo" minOccurs="0"/>
 *         &lt;element ref="{}InspectionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "structureDesc",
    "structureUseDesc",
    "length",
    "width",
    "height",
    "structureConditionCd",
    "construction",
    "bldgExposures",
    "bldgProtection",
    "alarmAndSecurity",
    "bldgImprovements",
    "bldgFeatures",
    "heatingUnitInfo",
    "storageTankInfo",
    "inspectionInfo"
})
@XmlRootElement(name = "StructureAddition")
public class StructureAddition {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "StructureDesc")
    protected String structureDesc;
    @XmlElement(name = "StructureUseDesc")
    protected String structureUseDesc;
    @XmlElement(name = "Length")
    protected String length;
    @XmlElement(name = "Width")
    protected String width;
    @XmlElement(name = "Height")
    protected String height;
    @XmlElement(name = "StructureConditionCd")
    protected String structureConditionCd;
    @XmlElement(name = "Construction")
    protected Construction construction;
    @XmlElement(name = "BldgExposures")
    protected List<BldgExposures> bldgExposures;
    @XmlElement(name = "BldgProtection")
    protected BldgProtection bldgProtection;
    @XmlElement(name = "AlarmAndSecurity")
    protected List<AlarmAndSecurity> alarmAndSecurity;
    @XmlElement(name = "BldgImprovements")
    protected BldgImprovements bldgImprovements;
    @XmlElement(name = "BldgFeatures")
    protected BldgFeatures bldgFeatures;
    @XmlElement(name = "HeatingUnitInfo")
    protected List<HeatingUnitInfo> heatingUnitInfo;
    @XmlElement(name = "StorageTankInfo")
    protected String storageTankInfo;
    @XmlElement(name = "InspectionInfo")
    protected List<InspectionInfo> inspectionInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the structureDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureDesc() {
        return structureDesc;
    }

    /**
     * Sets the value of the structureDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureDesc(String value) {
        this.structureDesc = value;
    }

    /**
     * Gets the value of the structureUseDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureUseDesc() {
        return structureUseDesc;
    }

    /**
     * Sets the value of the structureUseDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureUseDesc(String value) {
        this.structureUseDesc = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLength(String value) {
        this.length = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeight(String value) {
        this.height = value;
    }

    /**
     * Gets the value of the structureConditionCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureConditionCd() {
        return structureConditionCd;
    }

    /**
     * Sets the value of the structureConditionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureConditionCd(String value) {
        this.structureConditionCd = value;
    }

    /**
     * Gets the value of the construction property.
     * 
     * @return
     *     possible object is
     *     {@link Construction }
     *     
     */
    public Construction getConstruction() {
        return construction;
    }

    /**
     * Sets the value of the construction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Construction }
     *     
     */
    public void setConstruction(Construction value) {
        this.construction = value;
    }

    /**
     * Gets the value of the bldgExposures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bldgExposures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBldgExposures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BldgExposures }
     * 
     * 
     */
    public List<BldgExposures> getBldgExposures() {
        if (bldgExposures == null) {
            bldgExposures = new ArrayList<BldgExposures>();
        }
        return this.bldgExposures;
    }

    /**
     * Gets the value of the bldgProtection property.
     * 
     * @return
     *     possible object is
     *     {@link BldgProtection }
     *     
     */
    public BldgProtection getBldgProtection() {
        return bldgProtection;
    }

    /**
     * Sets the value of the bldgProtection property.
     * 
     * @param value
     *     allowed object is
     *     {@link BldgProtection }
     *     
     */
    public void setBldgProtection(BldgProtection value) {
        this.bldgProtection = value;
    }

    /**
     * Gets the value of the alarmAndSecurity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alarmAndSecurity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlarmAndSecurity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlarmAndSecurity }
     * 
     * 
     */
    public List<AlarmAndSecurity> getAlarmAndSecurity() {
        if (alarmAndSecurity == null) {
            alarmAndSecurity = new ArrayList<AlarmAndSecurity>();
        }
        return this.alarmAndSecurity;
    }

    /**
     * Gets the value of the bldgImprovements property.
     * 
     * @return
     *     possible object is
     *     {@link BldgImprovements }
     *     
     */
    public BldgImprovements getBldgImprovements() {
        return bldgImprovements;
    }

    /**
     * Sets the value of the bldgImprovements property.
     * 
     * @param value
     *     allowed object is
     *     {@link BldgImprovements }
     *     
     */
    public void setBldgImprovements(BldgImprovements value) {
        this.bldgImprovements = value;
    }

    /**
     * Gets the value of the bldgFeatures property.
     * 
     * @return
     *     possible object is
     *     {@link BldgFeatures }
     *     
     */
    public BldgFeatures getBldgFeatures() {
        return bldgFeatures;
    }

    /**
     * Sets the value of the bldgFeatures property.
     * 
     * @param value
     *     allowed object is
     *     {@link BldgFeatures }
     *     
     */
    public void setBldgFeatures(BldgFeatures value) {
        this.bldgFeatures = value;
    }

    /**
     * Gets the value of the heatingUnitInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the heatingUnitInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHeatingUnitInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HeatingUnitInfo }
     * 
     * 
     */
    public List<HeatingUnitInfo> getHeatingUnitInfo() {
        if (heatingUnitInfo == null) {
            heatingUnitInfo = new ArrayList<HeatingUnitInfo>();
        }
        return this.heatingUnitInfo;
    }

    /**
     * Gets the value of the storageTankInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStorageTankInfo() {
        return storageTankInfo;
    }

    /**
     * Sets the value of the storageTankInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStorageTankInfo(String value) {
        this.storageTankInfo = value;
    }

    /**
     * Gets the value of the inspectionInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inspectionInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInspectionInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InspectionInfo }
     * 
     * 
     */
    public List<InspectionInfo> getInspectionInfo() {
        if (inspectionInfo == null) {
            inspectionInfo = new ArrayList<InspectionInfo>();
        }
        return this.inspectionInfo;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
