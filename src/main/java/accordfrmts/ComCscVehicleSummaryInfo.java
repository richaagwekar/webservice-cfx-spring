//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_DamagesInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdditionalInformationInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_VehicleLossHistoryInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_VehicleFormsInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_VehicleSupplementalDocInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TaxFeeSurchargeInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Rate_Ind" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ResultInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AvailableDiscount" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AvailableCoverages" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WaivableCoverages" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisplayPreviousAdditionalInfoInd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscDamagesInd",
    "comCscAdditionalInformationInd",
    "comCscVehicleLossHistoryInd",
    "comCscVehicleFormsInd",
    "comCscVehicleSupplementalDocInd",
    "comCscTaxFeeSurchargeInd",
    "comCscRateInd",
    "comCscResultInd",
    "comCscAvailableDiscount",
    "comCscAvailableCoverages",
    "comCscWaivableCoverages",
    "comCscDisplayPreviousAdditionalInfoInd"
})
@XmlRootElement(name = "com.csc_VehicleSummaryInfo")
public class ComCscVehicleSummaryInfo {

    @XmlElement(name = "com.csc_DamagesInd")
    protected Boolean comCscDamagesInd;
    @XmlElement(name = "com.csc_AdditionalInformationInd")
    protected Boolean comCscAdditionalInformationInd;
    @XmlElement(name = "com.csc_VehicleLossHistoryInd")
    protected Boolean comCscVehicleLossHistoryInd;
    @XmlElement(name = "com.csc_VehicleFormsInd")
    protected Boolean comCscVehicleFormsInd;
    @XmlElement(name = "com.csc_VehicleSupplementalDocInd")
    protected Boolean comCscVehicleSupplementalDocInd;
    @XmlElement(name = "com.csc_TaxFeeSurchargeInd")
    protected Boolean comCscTaxFeeSurchargeInd;
    @XmlElement(name = "com.csc_Rate_Ind")
    protected Boolean comCscRateInd;
    @XmlElement(name = "com.csc_ResultInd")
    protected Boolean comCscResultInd;
    @XmlElement(name = "com.csc_AvailableDiscount")
    protected ComCscAvailableDiscount comCscAvailableDiscount;
    @XmlElement(name = "com.csc_AvailableCoverages")
    protected ComCscAvailableCoverages comCscAvailableCoverages;
    @XmlElement(name = "com.csc_WaivableCoverages")
    protected ComCscWaivableCoverages comCscWaivableCoverages;
    @XmlElement(name = "com.csc_DisplayPreviousAdditionalInfoInd")
    protected Boolean comCscDisplayPreviousAdditionalInfoInd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscDamagesInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscDamagesInd() {
        return comCscDamagesInd;
    }

    /**
     * Sets the value of the comCscDamagesInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscDamagesInd(Boolean value) {
        this.comCscDamagesInd = value;
    }

    /**
     * Gets the value of the comCscAdditionalInformationInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscAdditionalInformationInd() {
        return comCscAdditionalInformationInd;
    }

    /**
     * Sets the value of the comCscAdditionalInformationInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscAdditionalInformationInd(Boolean value) {
        this.comCscAdditionalInformationInd = value;
    }

    /**
     * Gets the value of the comCscVehicleLossHistoryInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscVehicleLossHistoryInd() {
        return comCscVehicleLossHistoryInd;
    }

    /**
     * Sets the value of the comCscVehicleLossHistoryInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscVehicleLossHistoryInd(Boolean value) {
        this.comCscVehicleLossHistoryInd = value;
    }

    /**
     * Gets the value of the comCscVehicleFormsInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscVehicleFormsInd() {
        return comCscVehicleFormsInd;
    }

    /**
     * Sets the value of the comCscVehicleFormsInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscVehicleFormsInd(Boolean value) {
        this.comCscVehicleFormsInd = value;
    }

    /**
     * Gets the value of the comCscVehicleSupplementalDocInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscVehicleSupplementalDocInd() {
        return comCscVehicleSupplementalDocInd;
    }

    /**
     * Sets the value of the comCscVehicleSupplementalDocInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscVehicleSupplementalDocInd(Boolean value) {
        this.comCscVehicleSupplementalDocInd = value;
    }

    /**
     * Gets the value of the comCscTaxFeeSurchargeInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscTaxFeeSurchargeInd() {
        return comCscTaxFeeSurchargeInd;
    }

    /**
     * Sets the value of the comCscTaxFeeSurchargeInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscTaxFeeSurchargeInd(Boolean value) {
        this.comCscTaxFeeSurchargeInd = value;
    }

    /**
     * Gets the value of the comCscRateInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscRateInd() {
        return comCscRateInd;
    }

    /**
     * Sets the value of the comCscRateInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscRateInd(Boolean value) {
        this.comCscRateInd = value;
    }

    /**
     * Gets the value of the comCscResultInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscResultInd() {
        return comCscResultInd;
    }

    /**
     * Sets the value of the comCscResultInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscResultInd(Boolean value) {
        this.comCscResultInd = value;
    }

    /**
     * Gets the value of the comCscAvailableDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAvailableDiscount }
     *     
     */
    public ComCscAvailableDiscount getComCscAvailableDiscount() {
        return comCscAvailableDiscount;
    }

    /**
     * Sets the value of the comCscAvailableDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAvailableDiscount }
     *     
     */
    public void setComCscAvailableDiscount(ComCscAvailableDiscount value) {
        this.comCscAvailableDiscount = value;
    }

    /**
     * Gets the value of the comCscAvailableCoverages property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAvailableCoverages }
     *     
     */
    public ComCscAvailableCoverages getComCscAvailableCoverages() {
        return comCscAvailableCoverages;
    }

    /**
     * Sets the value of the comCscAvailableCoverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAvailableCoverages }
     *     
     */
    public void setComCscAvailableCoverages(ComCscAvailableCoverages value) {
        this.comCscAvailableCoverages = value;
    }

    /**
     * Gets the value of the comCscWaivableCoverages property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscWaivableCoverages }
     *     
     */
    public ComCscWaivableCoverages getComCscWaivableCoverages() {
        return comCscWaivableCoverages;
    }

    /**
     * Sets the value of the comCscWaivableCoverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscWaivableCoverages }
     *     
     */
    public void setComCscWaivableCoverages(ComCscWaivableCoverages value) {
        this.comCscWaivableCoverages = value;
    }

    /**
     * Gets the value of the comCscDisplayPreviousAdditionalInfoInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscDisplayPreviousAdditionalInfoInd() {
        return comCscDisplayPreviousAdditionalInfoInd;
    }

    /**
     * Sets the value of the comCscDisplayPreviousAdditionalInfoInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscDisplayPreviousAdditionalInfoInd(Boolean value) {
        this.comCscDisplayPreviousAdditionalInfoInd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
