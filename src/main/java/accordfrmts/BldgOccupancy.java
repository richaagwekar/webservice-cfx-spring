//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}NumUnitsOwnerOccupied" minOccurs="0"/>
 *         &lt;element ref="{}VacantUnoccupiedCd" minOccurs="0"/>
 *         &lt;element ref="{}OccupiedPct" minOccurs="0"/>
 *         &lt;element ref="{}AreaOccupied" minOccurs="0"/>
 *         &lt;element ref="{}OccupancyDesc" minOccurs="0"/>
 *         &lt;element ref="{}NatureBusinessCd" minOccurs="0"/>
 *         &lt;element ref="{}OperationsDesc" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "numUnitsOwnerOccupied",
    "vacantUnoccupiedCd",
    "occupiedPct",
    "areaOccupied",
    "occupancyDesc",
    "natureBusinessCd",
    "operationsDesc"
})
@XmlRootElement(name = "BldgOccupancy")
public class BldgOccupancy {

    @XmlElement(name = "NumUnitsOwnerOccupied")
    protected BigInteger numUnitsOwnerOccupied;
    @XmlElement(name = "VacantUnoccupiedCd")
    protected String vacantUnoccupiedCd;
    @XmlElement(name = "OccupiedPct")
    protected Double occupiedPct;
    @XmlElement(name = "AreaOccupied")
    protected String areaOccupied;
    @XmlElement(name = "OccupancyDesc")
    protected String occupancyDesc;
    @XmlElement(name = "NatureBusinessCd")
    protected String natureBusinessCd;
    @XmlElement(name = "OperationsDesc")
    protected String operationsDesc;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the numUnitsOwnerOccupied property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumUnitsOwnerOccupied() {
        return numUnitsOwnerOccupied;
    }

    /**
     * Sets the value of the numUnitsOwnerOccupied property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumUnitsOwnerOccupied(BigInteger value) {
        this.numUnitsOwnerOccupied = value;
    }

    /**
     * Gets the value of the vacantUnoccupiedCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVacantUnoccupiedCd() {
        return vacantUnoccupiedCd;
    }

    /**
     * Sets the value of the vacantUnoccupiedCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVacantUnoccupiedCd(String value) {
        this.vacantUnoccupiedCd = value;
    }

    /**
     * Gets the value of the occupiedPct property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOccupiedPct() {
        return occupiedPct;
    }

    /**
     * Sets the value of the occupiedPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOccupiedPct(Double value) {
        this.occupiedPct = value;
    }

    /**
     * Gets the value of the areaOccupied property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaOccupied() {
        return areaOccupied;
    }

    /**
     * Sets the value of the areaOccupied property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaOccupied(String value) {
        this.areaOccupied = value;
    }

    /**
     * Gets the value of the occupancyDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupancyDesc() {
        return occupancyDesc;
    }

    /**
     * Sets the value of the occupancyDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupancyDesc(String value) {
        this.occupancyDesc = value;
    }

    /**
     * Gets the value of the natureBusinessCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatureBusinessCd() {
        return natureBusinessCd;
    }

    /**
     * Sets the value of the natureBusinessCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatureBusinessCd(String value) {
        this.natureBusinessCd = value;
    }

    /**
     * Gets the value of the operationsDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationsDesc() {
        return operationsDesc;
    }

    /**
     * Sets the value of the operationsDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationsDesc(String value) {
        this.operationsDesc = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
