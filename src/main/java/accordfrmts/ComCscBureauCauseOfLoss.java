//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BureauCauseOfLossCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BureauCauseOfLossDesc" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FinancialInd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "actionCd",
    "comCscBureauCauseOfLossCd",
    "comCscBureauCauseOfLossDesc",
    "comCscFinancialInd"
})
@XmlRootElement(name = "com.csc_BureauCauseOfLoss")
public class ComCscBureauCauseOfLoss {

    @XmlElement(name = "ItemIdInfo")
    protected List<ItemIdInfo> itemIdInfo;
    @XmlElement(name = "ActionCd")
    protected Action actionCd;
    @XmlElement(name = "com.csc_BureauCauseOfLossCd")
    protected String comCscBureauCauseOfLossCd;
    @XmlElement(name = "com.csc_BureauCauseOfLossDesc")
    protected String comCscBureauCauseOfLossDesc;
    @XmlElement(name = "com.csc_FinancialInd")
    protected Boolean comCscFinancialInd;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIdInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIdInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemIdInfo }
     * 
     * 
     */
    public List<ItemIdInfo> getItemIdInfo() {
        if (itemIdInfo == null) {
            itemIdInfo = new ArrayList<ItemIdInfo>();
        }
        return this.itemIdInfo;
    }

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

    /**
     * Gets the value of the comCscBureauCauseOfLossCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBureauCauseOfLossCd() {
        return comCscBureauCauseOfLossCd;
    }

    /**
     * Sets the value of the comCscBureauCauseOfLossCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBureauCauseOfLossCd(String value) {
        this.comCscBureauCauseOfLossCd = value;
    }

    /**
     * Gets the value of the comCscBureauCauseOfLossDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBureauCauseOfLossDesc() {
        return comCscBureauCauseOfLossDesc;
    }

    /**
     * Sets the value of the comCscBureauCauseOfLossDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBureauCauseOfLossDesc(String value) {
        this.comCscBureauCauseOfLossDesc = value;
    }

    /**
     * Gets the value of the comCscFinancialInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscFinancialInd() {
        return comCscFinancialInd;
    }

    /**
     * Sets the value of the comCscFinancialInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscFinancialInd(Boolean value) {
        this.comCscFinancialInd = value;
    }

}
