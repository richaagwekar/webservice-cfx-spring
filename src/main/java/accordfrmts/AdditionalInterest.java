//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}GeneralPartyInfo" minOccurs="0"/>
 *         &lt;element ref="{}AdditionalInterestInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ItemIdInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemIdInfo",
    "generalPartyInfo",
    "additionalInterestInfo",
    "comCscItemIdInfo",
    "comCscActionInfo"
})
@XmlRootElement(name = "AdditionalInterest")
public class AdditionalInterest {

    @XmlElement(name = "ItemIdInfo")
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "GeneralPartyInfo")
    protected GeneralPartyInfo generalPartyInfo;
    @XmlElement(name = "AdditionalInterestInfo")
    protected AdditionalInterestInfo additionalInterestInfo;
    @XmlElement(name = "com.csc_ItemIdInfo")
    protected List<ComCscItemIdInfo> comCscItemIdInfo;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the generalPartyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public GeneralPartyInfo getGeneralPartyInfo() {
        return generalPartyInfo;
    }

    /**
     * Sets the value of the generalPartyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public void setGeneralPartyInfo(GeneralPartyInfo value) {
        this.generalPartyInfo = value;
    }

    /**
     * Gets the value of the additionalInterestInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalInterestInfo }
     *     
     */
    public AdditionalInterestInfo getAdditionalInterestInfo() {
        return additionalInterestInfo;
    }

    /**
     * Sets the value of the additionalInterestInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalInterestInfo }
     *     
     */
    public void setAdditionalInterestInfo(AdditionalInterestInfo value) {
        this.additionalInterestInfo = value;
    }

    /**
     * Gets the value of the comCscItemIdInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscItemIdInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscItemIdInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscItemIdInfo }
     * 
     * 
     */
    public List<ComCscItemIdInfo> getComCscItemIdInfo() {
        if (comCscItemIdInfo == null) {
            comCscItemIdInfo = new ArrayList<ComCscItemIdInfo>();
        }
        return this.comCscItemIdInfo;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
