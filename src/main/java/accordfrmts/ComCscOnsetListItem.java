//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ClaimsOccurrence" minOccurs="0"/>
 *         &lt;element ref="{}ClaimsPayment" minOccurs="0"/>
 *         &lt;element ref="{}GeneralPartyInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "claimsOccurrence",
    "claimsPayment",
    "generalPartyInfo"
})
@XmlRootElement(name = "com.csc_OnsetListItem")
public class ComCscOnsetListItem {

    @XmlElement(name = "ClaimsOccurrence")
    protected ClaimsOccurrence claimsOccurrence;
    @XmlElement(name = "ClaimsPayment")
    protected ClaimsPayment claimsPayment;
    @XmlElement(name = "GeneralPartyInfo")
    protected GeneralPartyInfo generalPartyInfo;

    /**
     * Gets the value of the claimsOccurrence property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimsOccurrence }
     *     
     */
    public ClaimsOccurrence getClaimsOccurrence() {
        return claimsOccurrence;
    }

    /**
     * Sets the value of the claimsOccurrence property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimsOccurrence }
     *     
     */
    public void setClaimsOccurrence(ClaimsOccurrence value) {
        this.claimsOccurrence = value;
    }

    /**
     * Gets the value of the claimsPayment property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimsPayment }
     *     
     */
    public ClaimsPayment getClaimsPayment() {
        return claimsPayment;
    }

    /**
     * Sets the value of the claimsPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimsPayment }
     *     
     */
    public void setClaimsPayment(ClaimsPayment value) {
        this.claimsPayment = value;
    }

    /**
     * Gets the value of the generalPartyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public GeneralPartyInfo getGeneralPartyInfo() {
        return generalPartyInfo;
    }

    /**
     * Sets the value of the generalPartyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public void setGeneralPartyInfo(GeneralPartyInfo value) {
        this.generalPartyInfo = value;
    }

}
