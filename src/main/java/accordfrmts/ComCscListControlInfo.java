//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_ReturnCount" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ForwardInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_UnitOfWorkReturnCount" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchTotalCount" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ListCompleteInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TotalNumberOfChecks" minOccurs="0"/>
 *         &lt;element ref="{}SubTotalAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ListTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RoutingInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NextPageCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PagingKeyInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscReturnCount",
    "comCscForwardInd",
    "comCscUnitOfWorkReturnCount",
    "comCscSearchTotalCount",
    "comCscListCompleteInd",
    "comCscTotalNumberOfChecks",
    "subTotalAmt",
    "comCscListTypeCd",
    "comCscRoutingInfo",
    "comCscNextPageCd",
    "comCscPagingKeyInfo"
})
@XmlRootElement(name = "com.csc_ListControlInfo")
public class ComCscListControlInfo {

    @XmlElement(name = "com.csc_ReturnCount")
    protected String comCscReturnCount;
    @XmlElement(name = "com.csc_ForwardInd")
    protected Boolean comCscForwardInd;
    @XmlElement(name = "com.csc_UnitOfWorkReturnCount")
    protected String comCscUnitOfWorkReturnCount;
    @XmlElement(name = "com.csc_SearchTotalCount")
    protected String comCscSearchTotalCount;
    @XmlElement(name = "com.csc_ListCompleteInd")
    protected Boolean comCscListCompleteInd;
    @XmlElement(name = "com.csc_TotalNumberOfChecks")
    protected String comCscTotalNumberOfChecks;
    @XmlElement(name = "SubTotalAmt")
    protected BigDecimal subTotalAmt;
    @XmlElement(name = "com.csc_ListTypeCd")
    protected String comCscListTypeCd;
    @XmlElement(name = "com.csc_RoutingInfo")
    protected String comCscRoutingInfo;
    @XmlElement(name = "com.csc_NextPageCd")
    protected String comCscNextPageCd;
    @XmlElement(name = "com.csc_PagingKeyInfo")
    protected ComCscPagingKeyInfo comCscPagingKeyInfo;

    /**
     * Gets the value of the comCscReturnCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscReturnCount() {
        return comCscReturnCount;
    }

    /**
     * Sets the value of the comCscReturnCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscReturnCount(String value) {
        this.comCscReturnCount = value;
    }

    /**
     * Gets the value of the comCscForwardInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscForwardInd() {
        return comCscForwardInd;
    }

    /**
     * Sets the value of the comCscForwardInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscForwardInd(Boolean value) {
        this.comCscForwardInd = value;
    }

    /**
     * Gets the value of the comCscUnitOfWorkReturnCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscUnitOfWorkReturnCount() {
        return comCscUnitOfWorkReturnCount;
    }

    /**
     * Sets the value of the comCscUnitOfWorkReturnCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscUnitOfWorkReturnCount(String value) {
        this.comCscUnitOfWorkReturnCount = value;
    }

    /**
     * Gets the value of the comCscSearchTotalCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSearchTotalCount() {
        return comCscSearchTotalCount;
    }

    /**
     * Sets the value of the comCscSearchTotalCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSearchTotalCount(String value) {
        this.comCscSearchTotalCount = value;
    }

    /**
     * Gets the value of the comCscListCompleteInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscListCompleteInd() {
        return comCscListCompleteInd;
    }

    /**
     * Sets the value of the comCscListCompleteInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscListCompleteInd(Boolean value) {
        this.comCscListCompleteInd = value;
    }

    /**
     * Gets the value of the comCscTotalNumberOfChecks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscTotalNumberOfChecks() {
        return comCscTotalNumberOfChecks;
    }

    /**
     * Sets the value of the comCscTotalNumberOfChecks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscTotalNumberOfChecks(String value) {
        this.comCscTotalNumberOfChecks = value;
    }

    /**
     * Gets the value of the subTotalAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubTotalAmt() {
        return subTotalAmt;
    }

    /**
     * Sets the value of the subTotalAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubTotalAmt(BigDecimal value) {
        this.subTotalAmt = value;
    }

    /**
     * Gets the value of the comCscListTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscListTypeCd() {
        return comCscListTypeCd;
    }

    /**
     * Sets the value of the comCscListTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscListTypeCd(String value) {
        this.comCscListTypeCd = value;
    }

    /**
     * Gets the value of the comCscRoutingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRoutingInfo() {
        return comCscRoutingInfo;
    }

    /**
     * Sets the value of the comCscRoutingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRoutingInfo(String value) {
        this.comCscRoutingInfo = value;
    }

    /**
     * Gets the value of the comCscNextPageCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscNextPageCd() {
        return comCscNextPageCd;
    }

    /**
     * Sets the value of the comCscNextPageCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscNextPageCd(String value) {
        this.comCscNextPageCd = value;
    }

    /**
     * Gets the value of the comCscPagingKeyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPagingKeyInfo }
     *     
     */
    public ComCscPagingKeyInfo getComCscPagingKeyInfo() {
        return comCscPagingKeyInfo;
    }

    /**
     * Sets the value of the comCscPagingKeyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPagingKeyInfo }
     *     
     */
    public void setComCscPagingKeyInfo(ComCscPagingKeyInfo value) {
        this.comCscPagingKeyInfo = value;
    }

}
