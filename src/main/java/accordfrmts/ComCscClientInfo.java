//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}OtherIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}ExpirationDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_InsuranceSupportType" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_StatutoryTitleCd" minOccurs="0"/>
 *         &lt;element ref="{}NameInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}PersonInfo" minOccurs="0"/>
 *         &lt;element ref="{}BusinessInfo" minOccurs="0"/>
 *         &lt;element ref="{}GeneralPartyInfo" minOccurs="0"/>
 *         &lt;element ref="{}LanguageCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_RoleCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ProfessionalDesignationCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NumYrsAsClient" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_LicenseInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ObjectRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientRelation" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientAdditionalName" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientAccountInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientObjectInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientSummaryCount" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClientInceptionDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ContactSuppressionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SelectList" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WipOpenInd" minOccurs="0"/>
 *         &lt;element ref="{}Option" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "otherIdentifier",
    "effectiveDt",
    "expirationDt",
    "comCscInsuranceSupportType",
    "comCscStatutoryTitleCd",
    "nameInfo",
    "personInfo",
    "businessInfo",
    "generalPartyInfo",
    "languageCd",
    "comCscRoleCd",
    "comCscProfessionalDesignationCd",
    "comCscNumYrsAsClient",
    "comCscLicenseInfo",
    "comCscObjectRelation",
    "comCscClientRelation",
    "comCscClientAdditionalName",
    "actionCd",
    "comCscClientAccountInfo",
    "comCscClientObjectInfo",
    "comCscClientSummaryCount",
    "comCscClientInceptionDt",
    "comCscContactSuppressionInfo",
    "comCscSelectList",
    "comCscWipOpenInd",
    "option"
})
@XmlRootElement(name = "com.csc_ClientInfo")
public class ComCscClientInfo {

    @XmlElement(name = "OtherIdentifier")
    protected List<OtherIdentifier> otherIdentifier;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "ExpirationDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDt;
    @XmlElement(name = "com.csc_InsuranceSupportType")
    protected String comCscInsuranceSupportType;
    @XmlElement(name = "com.csc_StatutoryTitleCd")
    protected String comCscStatutoryTitleCd;
    @XmlElement(name = "NameInfo")
    protected List<NameInfo> nameInfo;
    @XmlElement(name = "PersonInfo")
    protected PersonInfo personInfo;
    @XmlElement(name = "BusinessInfo")
    protected BusinessInfo businessInfo;
    @XmlElement(name = "GeneralPartyInfo")
    protected GeneralPartyInfo generalPartyInfo;
    @XmlElement(name = "LanguageCd")
    protected String languageCd;
    @XmlElement(name = "com.csc_RoleCd")
    protected String comCscRoleCd;
    @XmlElement(name = "com.csc_ProfessionalDesignationCd")
    protected String comCscProfessionalDesignationCd;
    @XmlElement(name = "com.csc_NumYrsAsClient")
    protected BigInteger comCscNumYrsAsClient;
    @XmlElement(name = "com.csc_LicenseInfo")
    protected List<ComCscLicenseInfo> comCscLicenseInfo;
    @XmlElement(name = "com.csc_ObjectRelation")
    protected List<ComCscObjectRelation> comCscObjectRelation;
    @XmlElement(name = "com.csc_ClientRelation")
    protected List<ComCscClientRelation> comCscClientRelation;
    @XmlElement(name = "com.csc_ClientAdditionalName")
    protected List<ComCscClientAdditionalName> comCscClientAdditionalName;
    @XmlElement(name = "ActionCd")
    protected Action actionCd;
    @XmlElement(name = "com.csc_ClientAccountInfo")
    protected List<ComCscClientAccountInfo> comCscClientAccountInfo;
    @XmlElement(name = "com.csc_ClientObjectInfo")
    protected List<ComCscClientObjectInfo> comCscClientObjectInfo;
    @XmlElement(name = "com.csc_ClientSummaryCount")
    protected List<ComCscClientSummaryCount> comCscClientSummaryCount;
    @XmlElement(name = "com.csc_ClientInceptionDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscClientInceptionDt;
    @XmlElement(name = "com.csc_ContactSuppressionInfo")
    protected List<ComCscContactSuppressionInfo> comCscContactSuppressionInfo;
    @XmlElement(name = "com.csc_SelectList")
    protected ComCscSelectList comCscSelectList;
    @XmlElement(name = "com.csc_WipOpenInd")
    protected Boolean comCscWipOpenInd;
    @XmlElement(name = "Option")
    protected List<Option> option;

    /**
     * Gets the value of the otherIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherIdentifier }
     * 
     * 
     */
    public List<OtherIdentifier> getOtherIdentifier() {
        if (otherIdentifier == null) {
            otherIdentifier = new ArrayList<OtherIdentifier>();
        }
        return this.otherIdentifier;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expirationDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDt() {
        return expirationDt;
    }

    /**
     * Sets the value of the expirationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDt(XMLGregorianCalendar value) {
        this.expirationDt = value;
    }

    /**
     * Gets the value of the comCscInsuranceSupportType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscInsuranceSupportType() {
        return comCscInsuranceSupportType;
    }

    /**
     * Sets the value of the comCscInsuranceSupportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscInsuranceSupportType(String value) {
        this.comCscInsuranceSupportType = value;
    }

    /**
     * Gets the value of the comCscStatutoryTitleCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscStatutoryTitleCd() {
        return comCscStatutoryTitleCd;
    }

    /**
     * Sets the value of the comCscStatutoryTitleCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscStatutoryTitleCd(String value) {
        this.comCscStatutoryTitleCd = value;
    }

    /**
     * Gets the value of the nameInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameInfo }
     * 
     * 
     */
    public List<NameInfo> getNameInfo() {
        if (nameInfo == null) {
            nameInfo = new ArrayList<NameInfo>();
        }
        return this.nameInfo;
    }

    /**
     * Gets the value of the personInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PersonInfo }
     *     
     */
    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    /**
     * Sets the value of the personInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonInfo }
     *     
     */
    public void setPersonInfo(PersonInfo value) {
        this.personInfo = value;
    }

    /**
     * Gets the value of the businessInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessInfo }
     *     
     */
    public BusinessInfo getBusinessInfo() {
        return businessInfo;
    }

    /**
     * Sets the value of the businessInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessInfo }
     *     
     */
    public void setBusinessInfo(BusinessInfo value) {
        this.businessInfo = value;
    }

    /**
     * Gets the value of the generalPartyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public GeneralPartyInfo getGeneralPartyInfo() {
        return generalPartyInfo;
    }

    /**
     * Sets the value of the generalPartyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPartyInfo }
     *     
     */
    public void setGeneralPartyInfo(GeneralPartyInfo value) {
        this.generalPartyInfo = value;
    }

    /**
     * Gets the value of the languageCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageCd() {
        return languageCd;
    }

    /**
     * Sets the value of the languageCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageCd(String value) {
        this.languageCd = value;
    }

    /**
     * Gets the value of the comCscRoleCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscRoleCd() {
        return comCscRoleCd;
    }

    /**
     * Sets the value of the comCscRoleCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscRoleCd(String value) {
        this.comCscRoleCd = value;
    }

    /**
     * Gets the value of the comCscProfessionalDesignationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscProfessionalDesignationCd() {
        return comCscProfessionalDesignationCd;
    }

    /**
     * Sets the value of the comCscProfessionalDesignationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscProfessionalDesignationCd(String value) {
        this.comCscProfessionalDesignationCd = value;
    }

    /**
     * Gets the value of the comCscNumYrsAsClient property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComCscNumYrsAsClient() {
        return comCscNumYrsAsClient;
    }

    /**
     * Sets the value of the comCscNumYrsAsClient property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComCscNumYrsAsClient(BigInteger value) {
        this.comCscNumYrsAsClient = value;
    }

    /**
     * Gets the value of the comCscLicenseInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscLicenseInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscLicenseInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscLicenseInfo }
     * 
     * 
     */
    public List<ComCscLicenseInfo> getComCscLicenseInfo() {
        if (comCscLicenseInfo == null) {
            comCscLicenseInfo = new ArrayList<ComCscLicenseInfo>();
        }
        return this.comCscLicenseInfo;
    }

    /**
     * Gets the value of the comCscObjectRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscObjectRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscObjectRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscObjectRelation }
     * 
     * 
     */
    public List<ComCscObjectRelation> getComCscObjectRelation() {
        if (comCscObjectRelation == null) {
            comCscObjectRelation = new ArrayList<ComCscObjectRelation>();
        }
        return this.comCscObjectRelation;
    }

    /**
     * Gets the value of the comCscClientRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientRelation }
     * 
     * 
     */
    public List<ComCscClientRelation> getComCscClientRelation() {
        if (comCscClientRelation == null) {
            comCscClientRelation = new ArrayList<ComCscClientRelation>();
        }
        return this.comCscClientRelation;
    }

    /**
     * Gets the value of the comCscClientAdditionalName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientAdditionalName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientAdditionalName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientAdditionalName }
     * 
     * 
     */
    public List<ComCscClientAdditionalName> getComCscClientAdditionalName() {
        if (comCscClientAdditionalName == null) {
            comCscClientAdditionalName = new ArrayList<ComCscClientAdditionalName>();
        }
        return this.comCscClientAdditionalName;
    }

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

    /**
     * Gets the value of the comCscClientAccountInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientAccountInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientAccountInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientAccountInfo }
     * 
     * 
     */
    public List<ComCscClientAccountInfo> getComCscClientAccountInfo() {
        if (comCscClientAccountInfo == null) {
            comCscClientAccountInfo = new ArrayList<ComCscClientAccountInfo>();
        }
        return this.comCscClientAccountInfo;
    }

    /**
     * Gets the value of the comCscClientObjectInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientObjectInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientObjectInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientObjectInfo }
     * 
     * 
     */
    public List<ComCscClientObjectInfo> getComCscClientObjectInfo() {
        if (comCscClientObjectInfo == null) {
            comCscClientObjectInfo = new ArrayList<ComCscClientObjectInfo>();
        }
        return this.comCscClientObjectInfo;
    }

    /**
     * Gets the value of the comCscClientSummaryCount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscClientSummaryCount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscClientSummaryCount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscClientSummaryCount }
     * 
     * 
     */
    public List<ComCscClientSummaryCount> getComCscClientSummaryCount() {
        if (comCscClientSummaryCount == null) {
            comCscClientSummaryCount = new ArrayList<ComCscClientSummaryCount>();
        }
        return this.comCscClientSummaryCount;
    }

    /**
     * Gets the value of the comCscClientInceptionDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscClientInceptionDt() {
        return comCscClientInceptionDt;
    }

    /**
     * Sets the value of the comCscClientInceptionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscClientInceptionDt(XMLGregorianCalendar value) {
        this.comCscClientInceptionDt = value;
    }

    /**
     * Gets the value of the comCscContactSuppressionInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscContactSuppressionInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscContactSuppressionInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscContactSuppressionInfo }
     * 
     * 
     */
    public List<ComCscContactSuppressionInfo> getComCscContactSuppressionInfo() {
        if (comCscContactSuppressionInfo == null) {
            comCscContactSuppressionInfo = new ArrayList<ComCscContactSuppressionInfo>();
        }
        return this.comCscContactSuppressionInfo;
    }

    /**
     * Gets the value of the comCscSelectList property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscSelectList }
     *     
     */
    public ComCscSelectList getComCscSelectList() {
        return comCscSelectList;
    }

    /**
     * Sets the value of the comCscSelectList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscSelectList }
     *     
     */
    public void setComCscSelectList(ComCscSelectList value) {
        this.comCscSelectList = value;
    }

    /**
     * Gets the value of the comCscWipOpenInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscWipOpenInd() {
        return comCscWipOpenInd;
    }

    /**
     * Sets the value of the comCscWipOpenInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscWipOpenInd(Boolean value) {
        this.comCscWipOpenInd = value;
    }

    /**
     * Gets the value of the option property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the option property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Option }
     * 
     * 
     */
    public List<Option> getOption() {
        if (option == null) {
            option = new ArrayList<Option>();
        }
        return this.option;
    }

}
