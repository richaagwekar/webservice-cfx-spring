//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_BillingThirdPartyNameInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingAgentNameInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TransferredToFromAccountNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyAgreementTermBalances" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_NumPreCollectionLetters" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscBillingThirdPartyNameInfo",
    "comCscBillingAgentNameInfo",
    "comCscTransferredToFromAccountNumber",
    "comCscPolicyAgreementTermBalances",
    "comCscNumPreCollectionLetters"
})
@XmlRootElement(name = "com.csc_PolicyAgreementTermDetails")
public class ComCscPolicyAgreementTermDetails {

    @XmlElement(name = "com.csc_BillingThirdPartyNameInfo")
    protected List<ComCscBillingThirdPartyNameInfo> comCscBillingThirdPartyNameInfo;
    @XmlElement(name = "com.csc_BillingAgentNameInfo")
    protected List<ComCscBillingAgentNameInfo> comCscBillingAgentNameInfo;
    @XmlElement(name = "com.csc_TransferredToFromAccountNumber")
    protected String comCscTransferredToFromAccountNumber;
    @XmlElement(name = "com.csc_PolicyAgreementTermBalances")
    protected ComCscPolicyAgreementTermBalances comCscPolicyAgreementTermBalances;
    @XmlElement(name = "com.csc_NumPreCollectionLetters")
    protected BigInteger comCscNumPreCollectionLetters;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscBillingThirdPartyNameInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscBillingThirdPartyNameInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscBillingThirdPartyNameInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscBillingThirdPartyNameInfo }
     * 
     * 
     */
    public List<ComCscBillingThirdPartyNameInfo> getComCscBillingThirdPartyNameInfo() {
        if (comCscBillingThirdPartyNameInfo == null) {
            comCscBillingThirdPartyNameInfo = new ArrayList<ComCscBillingThirdPartyNameInfo>();
        }
        return this.comCscBillingThirdPartyNameInfo;
    }

    /**
     * Gets the value of the comCscBillingAgentNameInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscBillingAgentNameInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscBillingAgentNameInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscBillingAgentNameInfo }
     * 
     * 
     */
    public List<ComCscBillingAgentNameInfo> getComCscBillingAgentNameInfo() {
        if (comCscBillingAgentNameInfo == null) {
            comCscBillingAgentNameInfo = new ArrayList<ComCscBillingAgentNameInfo>();
        }
        return this.comCscBillingAgentNameInfo;
    }

    /**
     * Gets the value of the comCscTransferredToFromAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscTransferredToFromAccountNumber() {
        return comCscTransferredToFromAccountNumber;
    }

    /**
     * Sets the value of the comCscTransferredToFromAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscTransferredToFromAccountNumber(String value) {
        this.comCscTransferredToFromAccountNumber = value;
    }

    /**
     * Gets the value of the comCscPolicyAgreementTermBalances property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPolicyAgreementTermBalances }
     *     
     */
    public ComCscPolicyAgreementTermBalances getComCscPolicyAgreementTermBalances() {
        return comCscPolicyAgreementTermBalances;
    }

    /**
     * Sets the value of the comCscPolicyAgreementTermBalances property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPolicyAgreementTermBalances }
     *     
     */
    public void setComCscPolicyAgreementTermBalances(ComCscPolicyAgreementTermBalances value) {
        this.comCscPolicyAgreementTermBalances = value;
    }

    /**
     * Gets the value of the comCscNumPreCollectionLetters property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getComCscNumPreCollectionLetters() {
        return comCscNumPreCollectionLetters;
    }

    /**
     * Sets the value of the comCscNumPreCollectionLetters property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setComCscNumPreCollectionLetters(BigInteger value) {
        this.comCscNumPreCollectionLetters = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
