//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_CompositeTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CalculationTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CompositeRatingPremiumBasis" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ExcludedCoveragCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CompositeRatingLiabilityRiskCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CompositeRatingLiabilityClassCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CompositeRatingPhysicalDamageTypeRiskCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CompositeRatingPhysicalDamageTypeClassCd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscCompositeTypeCd",
    "comCscCalculationTypeCd",
    "comCscCompositeRatingPremiumBasis",
    "comCscExcludedCoveragCd",
    "comCscCompositeRatingLiabilityRiskCd",
    "comCscCompositeRatingLiabilityClassCd",
    "comCscCompositeRatingPhysicalDamageTypeRiskCd",
    "comCscCompositeRatingPhysicalDamageTypeClassCd"
})
@XmlRootElement(name = "com.csc_CommlAutoCompositeRatingInfo")
public class ComCscCommlAutoCompositeRatingInfo {

    @XmlElement(name = "com.csc_CompositeTypeCd")
    protected String comCscCompositeTypeCd;
    @XmlElement(name = "com.csc_CalculationTypeCd")
    protected String comCscCalculationTypeCd;
    @XmlElement(name = "com.csc_CompositeRatingPremiumBasis")
    protected ComCscCompositeRatingPremiumBasis comCscCompositeRatingPremiumBasis;
    @XmlElement(name = "com.csc_ExcludedCoveragCd")
    protected List<String> comCscExcludedCoveragCd;
    @XmlElement(name = "com.csc_CompositeRatingLiabilityRiskCd")
    protected String comCscCompositeRatingLiabilityRiskCd;
    @XmlElement(name = "com.csc_CompositeRatingLiabilityClassCd")
    protected String comCscCompositeRatingLiabilityClassCd;
    @XmlElement(name = "com.csc_CompositeRatingPhysicalDamageTypeRiskCd")
    protected String comCscCompositeRatingPhysicalDamageTypeRiskCd;
    @XmlElement(name = "com.csc_CompositeRatingPhysicalDamageTypeClassCd")
    protected String comCscCompositeRatingPhysicalDamageTypeClassCd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscCompositeTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCompositeTypeCd() {
        return comCscCompositeTypeCd;
    }

    /**
     * Sets the value of the comCscCompositeTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCompositeTypeCd(String value) {
        this.comCscCompositeTypeCd = value;
    }

    /**
     * Gets the value of the comCscCalculationTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCalculationTypeCd() {
        return comCscCalculationTypeCd;
    }

    /**
     * Sets the value of the comCscCalculationTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCalculationTypeCd(String value) {
        this.comCscCalculationTypeCd = value;
    }

    /**
     * Gets the value of the comCscCompositeRatingPremiumBasis property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscCompositeRatingPremiumBasis }
     *     
     */
    public ComCscCompositeRatingPremiumBasis getComCscCompositeRatingPremiumBasis() {
        return comCscCompositeRatingPremiumBasis;
    }

    /**
     * Sets the value of the comCscCompositeRatingPremiumBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscCompositeRatingPremiumBasis }
     *     
     */
    public void setComCscCompositeRatingPremiumBasis(ComCscCompositeRatingPremiumBasis value) {
        this.comCscCompositeRatingPremiumBasis = value;
    }

    /**
     * Gets the value of the comCscExcludedCoveragCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscExcludedCoveragCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscExcludedCoveragCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getComCscExcludedCoveragCd() {
        if (comCscExcludedCoveragCd == null) {
            comCscExcludedCoveragCd = new ArrayList<String>();
        }
        return this.comCscExcludedCoveragCd;
    }

    /**
     * Gets the value of the comCscCompositeRatingLiabilityRiskCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCompositeRatingLiabilityRiskCd() {
        return comCscCompositeRatingLiabilityRiskCd;
    }

    /**
     * Sets the value of the comCscCompositeRatingLiabilityRiskCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCompositeRatingLiabilityRiskCd(String value) {
        this.comCscCompositeRatingLiabilityRiskCd = value;
    }

    /**
     * Gets the value of the comCscCompositeRatingLiabilityClassCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCompositeRatingLiabilityClassCd() {
        return comCscCompositeRatingLiabilityClassCd;
    }

    /**
     * Sets the value of the comCscCompositeRatingLiabilityClassCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCompositeRatingLiabilityClassCd(String value) {
        this.comCscCompositeRatingLiabilityClassCd = value;
    }

    /**
     * Gets the value of the comCscCompositeRatingPhysicalDamageTypeRiskCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCompositeRatingPhysicalDamageTypeRiskCd() {
        return comCscCompositeRatingPhysicalDamageTypeRiskCd;
    }

    /**
     * Sets the value of the comCscCompositeRatingPhysicalDamageTypeRiskCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCompositeRatingPhysicalDamageTypeRiskCd(String value) {
        this.comCscCompositeRatingPhysicalDamageTypeRiskCd = value;
    }

    /**
     * Gets the value of the comCscCompositeRatingPhysicalDamageTypeClassCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscCompositeRatingPhysicalDamageTypeClassCd() {
        return comCscCompositeRatingPhysicalDamageTypeClassCd;
    }

    /**
     * Sets the value of the comCscCompositeRatingPhysicalDamageTypeClassCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscCompositeRatingPhysicalDamageTypeClassCd(String value) {
        this.comCscCompositeRatingPhysicalDamageTypeClassCd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
