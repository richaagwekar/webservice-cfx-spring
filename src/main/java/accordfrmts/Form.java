//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FormNumber" minOccurs="0"/>
 *         &lt;element ref="{}FormName" minOccurs="0"/>
 *         &lt;element ref="{}CopyrightOwnerCd" minOccurs="0"/>
 *         &lt;element ref="{}CopyrightOwnerNameCd" minOccurs="0"/>
 *         &lt;element ref="{}FormDesc" minOccurs="0"/>
 *         &lt;element ref="{}EditionDt" minOccurs="0"/>
 *         &lt;element ref="{}FormDataArea" minOccurs="0"/>
 *         &lt;element ref="{}IterationNumber" minOccurs="0"/>
 *         &lt;element ref="{}FormSequencingNumber" minOccurs="0"/>
 *         &lt;element ref="{}BondFormOwner" minOccurs="0"/>
 *         &lt;element ref="{}BondFormNumberOwner" minOccurs="0"/>
 *         &lt;element ref="{}WebsiteURL" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}FormTextContent" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SequenceNumber" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormId" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_OverrideInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ItemIdInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ActionInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormEffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_FormCategoryCd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="AggregateRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "formNumber",
    "formName",
    "copyrightOwnerCd",
    "copyrightOwnerNameCd",
    "formDesc",
    "editionDt",
    "formDataArea",
    "iterationNumber",
    "formSequencingNumber",
    "bondFormOwner",
    "bondFormNumberOwner",
    "websiteURL",
    "formTextContent",
    "comCscSequenceNumber",
    "comCscFormId",
    "comCscOverrideInd",
    "comCscItemIdInfo",
    "comCscActionInfo",
    "comCscFormEffectiveDt",
    "comCscFormCategoryCd"
})
@XmlRootElement(name = "Form")
public class Form {

    @XmlElement(name = "FormNumber")
    protected String formNumber;
    @XmlElement(name = "FormName")
    protected String formName;
    @XmlElement(name = "CopyrightOwnerCd")
    protected String copyrightOwnerCd;
    @XmlElement(name = "CopyrightOwnerNameCd")
    protected String copyrightOwnerNameCd;
    @XmlElement(name = "FormDesc")
    protected String formDesc;
    @XmlElement(name = "EditionDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar editionDt;
    @XmlElement(name = "FormDataArea")
    protected String formDataArea;
    @XmlElement(name = "IterationNumber")
    protected String iterationNumber;
    @XmlElement(name = "FormSequencingNumber")
    protected String formSequencingNumber;
    @XmlElement(name = "BondFormOwner")
    protected String bondFormOwner;
    @XmlElement(name = "BondFormNumberOwner")
    protected String bondFormNumberOwner;
    @XmlElement(name = "WebsiteURL")
    protected List<String> websiteURL;
    @XmlElement(name = "FormTextContent")
    protected String formTextContent;
    @XmlElement(name = "com.csc_SequenceNumber")
    protected String comCscSequenceNumber;
    @XmlElement(name = "com.csc_FormId")
    protected String comCscFormId;
    @XmlElement(name = "com.csc_OverrideInd")
    protected Boolean comCscOverrideInd;
    @XmlElement(name = "com.csc_ItemIdInfo")
    protected ComCscItemIdInfo comCscItemIdInfo;
    @XmlElement(name = "com.csc_ActionInfo")
    protected ComCscActionInfo comCscActionInfo;
    @XmlElement(name = "com.csc_FormEffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscFormEffectiveDt;
    @XmlElement(name = "com.csc_FormCategoryCd")
    protected String comCscFormCategoryCd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "AggregateRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object aggregateRef;

    /**
     * Gets the value of the formNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormNumber() {
        return formNumber;
    }

    /**
     * Sets the value of the formNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormNumber(String value) {
        this.formNumber = value;
    }

    /**
     * Gets the value of the formName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Sets the value of the formName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormName(String value) {
        this.formName = value;
    }

    /**
     * Gets the value of the copyrightOwnerCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopyrightOwnerCd() {
        return copyrightOwnerCd;
    }

    /**
     * Sets the value of the copyrightOwnerCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopyrightOwnerCd(String value) {
        this.copyrightOwnerCd = value;
    }

    /**
     * Gets the value of the copyrightOwnerNameCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCopyrightOwnerNameCd() {
        return copyrightOwnerNameCd;
    }

    /**
     * Sets the value of the copyrightOwnerNameCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCopyrightOwnerNameCd(String value) {
        this.copyrightOwnerNameCd = value;
    }

    /**
     * Gets the value of the formDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormDesc() {
        return formDesc;
    }

    /**
     * Sets the value of the formDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormDesc(String value) {
        this.formDesc = value;
    }

    /**
     * Gets the value of the editionDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEditionDt() {
        return editionDt;
    }

    /**
     * Sets the value of the editionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEditionDt(XMLGregorianCalendar value) {
        this.editionDt = value;
    }

    /**
     * Gets the value of the formDataArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormDataArea() {
        return formDataArea;
    }

    /**
     * Sets the value of the formDataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormDataArea(String value) {
        this.formDataArea = value;
    }

    /**
     * Gets the value of the iterationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIterationNumber() {
        return iterationNumber;
    }

    /**
     * Sets the value of the iterationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIterationNumber(String value) {
        this.iterationNumber = value;
    }

    /**
     * Gets the value of the formSequencingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormSequencingNumber() {
        return formSequencingNumber;
    }

    /**
     * Sets the value of the formSequencingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormSequencingNumber(String value) {
        this.formSequencingNumber = value;
    }

    /**
     * Gets the value of the bondFormOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBondFormOwner() {
        return bondFormOwner;
    }

    /**
     * Sets the value of the bondFormOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBondFormOwner(String value) {
        this.bondFormOwner = value;
    }

    /**
     * Gets the value of the bondFormNumberOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBondFormNumberOwner() {
        return bondFormNumberOwner;
    }

    /**
     * Sets the value of the bondFormNumberOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBondFormNumberOwner(String value) {
        this.bondFormNumberOwner = value;
    }

    /**
     * Gets the value of the websiteURL property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the websiteURL property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWebsiteURL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getWebsiteURL() {
        if (websiteURL == null) {
            websiteURL = new ArrayList<String>();
        }
        return this.websiteURL;
    }

    /**
     * Gets the value of the formTextContent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormTextContent() {
        return formTextContent;
    }

    /**
     * Sets the value of the formTextContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormTextContent(String value) {
        this.formTextContent = value;
    }

    /**
     * Gets the value of the comCscSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSequenceNumber() {
        return comCscSequenceNumber;
    }

    /**
     * Sets the value of the comCscSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSequenceNumber(String value) {
        this.comCscSequenceNumber = value;
    }

    /**
     * Gets the value of the comCscFormId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscFormId() {
        return comCscFormId;
    }

    /**
     * Sets the value of the comCscFormId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscFormId(String value) {
        this.comCscFormId = value;
    }

    /**
     * Gets the value of the comCscOverrideInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscOverrideInd() {
        return comCscOverrideInd;
    }

    /**
     * Sets the value of the comCscOverrideInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscOverrideInd(Boolean value) {
        this.comCscOverrideInd = value;
    }

    /**
     * Gets the value of the comCscItemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public ComCscItemIdInfo getComCscItemIdInfo() {
        return comCscItemIdInfo;
    }

    /**
     * Sets the value of the comCscItemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public void setComCscItemIdInfo(ComCscItemIdInfo value) {
        this.comCscItemIdInfo = value;
    }

    /**
     * Gets the value of the comCscActionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscActionInfo }
     *     
     */
    public ComCscActionInfo getComCscActionInfo() {
        return comCscActionInfo;
    }

    /**
     * Sets the value of the comCscActionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscActionInfo }
     *     
     */
    public void setComCscActionInfo(ComCscActionInfo value) {
        this.comCscActionInfo = value;
    }

    /**
     * Gets the value of the comCscFormEffectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscFormEffectiveDt() {
        return comCscFormEffectiveDt;
    }

    /**
     * Sets the value of the comCscFormEffectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscFormEffectiveDt(XMLGregorianCalendar value) {
        this.comCscFormEffectiveDt = value;
    }

    /**
     * Gets the value of the comCscFormCategoryCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscFormCategoryCd() {
        return comCscFormCategoryCd;
    }

    /**
     * Sets the value of the comCscFormCategoryCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscFormCategoryCd(String value) {
        this.comCscFormCategoryCd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the aggregateRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAggregateRef() {
        return aggregateRef;
    }

    /**
     * Sets the value of the aggregateRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAggregateRef(Object value) {
        this.aggregateRef = value;
    }

}
