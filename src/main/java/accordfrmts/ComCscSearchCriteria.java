//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_ClientSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DocumentSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdministrationGroupSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdministrationTransactionSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_CommentsSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WIPUserPreferenceSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WIPReferenceLocateSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WIPActivitySearch" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AgencyContractSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyLevel" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ClaimsSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillingAccountSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AdministrativeHoldInfo" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_TransactionErrorSearch" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_DisbursementAccountAssociationSearchInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscClientSearch",
    "comCscDocumentSearch",
    "comCscAdministrationGroupSearch",
    "comCscAdministrationTransactionSearch",
    "comCscCommentsSearch",
    "comCscWIPUserPreferenceSearch",
    "comCscWIPReferenceLocateSearch",
    "comCscWIPActivitySearch",
    "comCscAgencyContractSearch",
    "comCscPolicyLevel",
    "comCscClaimsSearch",
    "comCscBillingAccountSearch",
    "comCscAdministrativeHoldInfo",
    "comCscTransactionErrorSearch",
    "comCscDisbursementAccountAssociationSearchInfo"
})
@XmlRootElement(name = "com.csc_SearchCriteria")
public class ComCscSearchCriteria {

    @XmlElement(name = "com.csc_ClientSearch")
    protected ComCscClientSearch comCscClientSearch;
    @XmlElement(name = "com.csc_DocumentSearch")
    protected ComCscDocumentSearch comCscDocumentSearch;
    @XmlElement(name = "com.csc_AdministrationGroupSearch")
    protected ComCscAdministrationGroupSearch comCscAdministrationGroupSearch;
    @XmlElement(name = "com.csc_AdministrationTransactionSearch")
    protected ComCscAdministrationTransactionSearch comCscAdministrationTransactionSearch;
    @XmlElement(name = "com.csc_CommentsSearch")
    protected ComCscCommentsSearch comCscCommentsSearch;
    @XmlElement(name = "com.csc_WIPUserPreferenceSearch")
    protected ComCscWIPUserPreferenceSearch comCscWIPUserPreferenceSearch;
    @XmlElement(name = "com.csc_WIPReferenceLocateSearch")
    protected ComCscWIPReferenceLocateSearch comCscWIPReferenceLocateSearch;
    @XmlElement(name = "com.csc_WIPActivitySearch")
    protected List<ComCscWIPActivitySearch> comCscWIPActivitySearch;
    @XmlElement(name = "com.csc_AgencyContractSearch")
    protected ComCscAgencyContractSearch comCscAgencyContractSearch;
    @XmlElement(name = "com.csc_PolicyLevel")
    protected List<ComCscPolicyLevel> comCscPolicyLevel;
    @XmlElement(name = "com.csc_ClaimsSearch")
    protected ComCscClaimsSearch comCscClaimsSearch;
    @XmlElement(name = "com.csc_BillingAccountSearch")
    protected ComCscBillingAccountSearch comCscBillingAccountSearch;
    @XmlElement(name = "com.csc_AdministrativeHoldInfo")
    protected ComCscAdministrativeHoldInfo comCscAdministrativeHoldInfo;
    @XmlElement(name = "com.csc_TransactionErrorSearch")
    protected ComCscTransactionErrorSearch comCscTransactionErrorSearch;
    @XmlElement(name = "com.csc_DisbursementAccountAssociationSearchInfo")
    protected ComCscDisbursementAccountAssociationSearchInfo comCscDisbursementAccountAssociationSearchInfo;

    /**
     * Gets the value of the comCscClientSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscClientSearch }
     *     
     */
    public ComCscClientSearch getComCscClientSearch() {
        return comCscClientSearch;
    }

    /**
     * Sets the value of the comCscClientSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscClientSearch }
     *     
     */
    public void setComCscClientSearch(ComCscClientSearch value) {
        this.comCscClientSearch = value;
    }

    /**
     * Gets the value of the comCscDocumentSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscDocumentSearch }
     *     
     */
    public ComCscDocumentSearch getComCscDocumentSearch() {
        return comCscDocumentSearch;
    }

    /**
     * Sets the value of the comCscDocumentSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscDocumentSearch }
     *     
     */
    public void setComCscDocumentSearch(ComCscDocumentSearch value) {
        this.comCscDocumentSearch = value;
    }

    /**
     * Gets the value of the comCscAdministrationGroupSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAdministrationGroupSearch }
     *     
     */
    public ComCscAdministrationGroupSearch getComCscAdministrationGroupSearch() {
        return comCscAdministrationGroupSearch;
    }

    /**
     * Sets the value of the comCscAdministrationGroupSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAdministrationGroupSearch }
     *     
     */
    public void setComCscAdministrationGroupSearch(ComCscAdministrationGroupSearch value) {
        this.comCscAdministrationGroupSearch = value;
    }

    /**
     * Gets the value of the comCscAdministrationTransactionSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAdministrationTransactionSearch }
     *     
     */
    public ComCscAdministrationTransactionSearch getComCscAdministrationTransactionSearch() {
        return comCscAdministrationTransactionSearch;
    }

    /**
     * Sets the value of the comCscAdministrationTransactionSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAdministrationTransactionSearch }
     *     
     */
    public void setComCscAdministrationTransactionSearch(ComCscAdministrationTransactionSearch value) {
        this.comCscAdministrationTransactionSearch = value;
    }

    /**
     * Gets the value of the comCscCommentsSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscCommentsSearch }
     *     
     */
    public ComCscCommentsSearch getComCscCommentsSearch() {
        return comCscCommentsSearch;
    }

    /**
     * Sets the value of the comCscCommentsSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscCommentsSearch }
     *     
     */
    public void setComCscCommentsSearch(ComCscCommentsSearch value) {
        this.comCscCommentsSearch = value;
    }

    /**
     * Gets the value of the comCscWIPUserPreferenceSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscWIPUserPreferenceSearch }
     *     
     */
    public ComCscWIPUserPreferenceSearch getComCscWIPUserPreferenceSearch() {
        return comCscWIPUserPreferenceSearch;
    }

    /**
     * Sets the value of the comCscWIPUserPreferenceSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscWIPUserPreferenceSearch }
     *     
     */
    public void setComCscWIPUserPreferenceSearch(ComCscWIPUserPreferenceSearch value) {
        this.comCscWIPUserPreferenceSearch = value;
    }

    /**
     * Gets the value of the comCscWIPReferenceLocateSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscWIPReferenceLocateSearch }
     *     
     */
    public ComCscWIPReferenceLocateSearch getComCscWIPReferenceLocateSearch() {
        return comCscWIPReferenceLocateSearch;
    }

    /**
     * Sets the value of the comCscWIPReferenceLocateSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscWIPReferenceLocateSearch }
     *     
     */
    public void setComCscWIPReferenceLocateSearch(ComCscWIPReferenceLocateSearch value) {
        this.comCscWIPReferenceLocateSearch = value;
    }

    /**
     * Gets the value of the comCscWIPActivitySearch property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscWIPActivitySearch property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscWIPActivitySearch().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscWIPActivitySearch }
     * 
     * 
     */
    public List<ComCscWIPActivitySearch> getComCscWIPActivitySearch() {
        if (comCscWIPActivitySearch == null) {
            comCscWIPActivitySearch = new ArrayList<ComCscWIPActivitySearch>();
        }
        return this.comCscWIPActivitySearch;
    }

    /**
     * Gets the value of the comCscAgencyContractSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAgencyContractSearch }
     *     
     */
    public ComCscAgencyContractSearch getComCscAgencyContractSearch() {
        return comCscAgencyContractSearch;
    }

    /**
     * Sets the value of the comCscAgencyContractSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAgencyContractSearch }
     *     
     */
    public void setComCscAgencyContractSearch(ComCscAgencyContractSearch value) {
        this.comCscAgencyContractSearch = value;
    }

    /**
     * Gets the value of the comCscPolicyLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comCscPolicyLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComCscPolicyLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComCscPolicyLevel }
     * 
     * 
     */
    public List<ComCscPolicyLevel> getComCscPolicyLevel() {
        if (comCscPolicyLevel == null) {
            comCscPolicyLevel = new ArrayList<ComCscPolicyLevel>();
        }
        return this.comCscPolicyLevel;
    }

    /**
     * Gets the value of the comCscClaimsSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscClaimsSearch }
     *     
     */
    public ComCscClaimsSearch getComCscClaimsSearch() {
        return comCscClaimsSearch;
    }

    /**
     * Sets the value of the comCscClaimsSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscClaimsSearch }
     *     
     */
    public void setComCscClaimsSearch(ComCscClaimsSearch value) {
        this.comCscClaimsSearch = value;
    }

    /**
     * Gets the value of the comCscBillingAccountSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscBillingAccountSearch }
     *     
     */
    public ComCscBillingAccountSearch getComCscBillingAccountSearch() {
        return comCscBillingAccountSearch;
    }

    /**
     * Sets the value of the comCscBillingAccountSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscBillingAccountSearch }
     *     
     */
    public void setComCscBillingAccountSearch(ComCscBillingAccountSearch value) {
        this.comCscBillingAccountSearch = value;
    }

    /**
     * Gets the value of the comCscAdministrativeHoldInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscAdministrativeHoldInfo }
     *     
     */
    public ComCscAdministrativeHoldInfo getComCscAdministrativeHoldInfo() {
        return comCscAdministrativeHoldInfo;
    }

    /**
     * Sets the value of the comCscAdministrativeHoldInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscAdministrativeHoldInfo }
     *     
     */
    public void setComCscAdministrativeHoldInfo(ComCscAdministrativeHoldInfo value) {
        this.comCscAdministrativeHoldInfo = value;
    }

    /**
     * Gets the value of the comCscTransactionErrorSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscTransactionErrorSearch }
     *     
     */
    public ComCscTransactionErrorSearch getComCscTransactionErrorSearch() {
        return comCscTransactionErrorSearch;
    }

    /**
     * Sets the value of the comCscTransactionErrorSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscTransactionErrorSearch }
     *     
     */
    public void setComCscTransactionErrorSearch(ComCscTransactionErrorSearch value) {
        this.comCscTransactionErrorSearch = value;
    }

    /**
     * Gets the value of the comCscDisbursementAccountAssociationSearchInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscDisbursementAccountAssociationSearchInfo }
     *     
     */
    public ComCscDisbursementAccountAssociationSearchInfo getComCscDisbursementAccountAssociationSearchInfo() {
        return comCscDisbursementAccountAssociationSearchInfo;
    }

    /**
     * Sets the value of the comCscDisbursementAccountAssociationSearchInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscDisbursementAccountAssociationSearchInfo }
     *     
     */
    public void setComCscDisbursementAccountAssociationSearchInfo(ComCscDisbursementAccountAssociationSearchInfo value) {
        this.comCscDisbursementAccountAssociationSearchInfo = value;
    }

}
