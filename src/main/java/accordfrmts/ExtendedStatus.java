//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ExtendedStatusCd"/>
 *         &lt;element ref="{}ExtendedStatusDesc" minOccurs="0"/>
 *         &lt;element ref="{}MissingElementPath" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_Severity" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_StatusReference" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ItemIdInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="IdRefs" type="{http://www.w3.org/2001/XMLSchema}IDREFS" />
 *       &lt;attribute name="com.csc_NonINFError" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "extendedStatusCd",
    "extendedStatusDesc",
    "missingElementPath",
    "comCscSeverity",
    "comCscStatusReference",
    "comCscItemIdInfo"
})
@XmlRootElement(name = "ExtendedStatus")
public class ExtendedStatus {

    @XmlElement(name = "ExtendedStatusCd", required = true)
    protected String extendedStatusCd;
    @XmlElement(name = "ExtendedStatusDesc")
    protected String extendedStatusDesc;
    @XmlElement(name = "MissingElementPath")
    protected List<String> missingElementPath;
    @XmlElement(name = "com.csc_Severity")
    protected String comCscSeverity;
    @XmlElement(name = "com.csc_StatusReference")
    protected String comCscStatusReference;
    @XmlElement(name = "com.csc_ItemIdInfo")
    protected ComCscItemIdInfo comCscItemIdInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "IdRefs")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> idRefs;
    @XmlAttribute(name = "com.csc_NonINFError")
    @XmlSchemaType(name = "anySimpleType")
    protected String comCscNonINFError;

    /**
     * Gets the value of the extendedStatusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedStatusCd() {
        return extendedStatusCd;
    }

    /**
     * Sets the value of the extendedStatusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedStatusCd(String value) {
        this.extendedStatusCd = value;
    }

    /**
     * Gets the value of the extendedStatusDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedStatusDesc() {
        return extendedStatusDesc;
    }

    /**
     * Sets the value of the extendedStatusDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedStatusDesc(String value) {
        this.extendedStatusDesc = value;
    }

    /**
     * Gets the value of the missingElementPath property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the missingElementPath property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMissingElementPath().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMissingElementPath() {
        if (missingElementPath == null) {
            missingElementPath = new ArrayList<String>();
        }
        return this.missingElementPath;
    }

    /**
     * Gets the value of the comCscSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSeverity() {
        return comCscSeverity;
    }

    /**
     * Sets the value of the comCscSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSeverity(String value) {
        this.comCscSeverity = value;
    }

    /**
     * Gets the value of the comCscStatusReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscStatusReference() {
        return comCscStatusReference;
    }

    /**
     * Sets the value of the comCscStatusReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscStatusReference(String value) {
        this.comCscStatusReference = value;
    }

    /**
     * Gets the value of the comCscItemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public ComCscItemIdInfo getComCscItemIdInfo() {
        return comCscItemIdInfo;
    }

    /**
     * Sets the value of the comCscItemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscItemIdInfo }
     *     
     */
    public void setComCscItemIdInfo(ComCscItemIdInfo value) {
        this.comCscItemIdInfo = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the idRefs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idRefs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getIdRefs() {
        if (idRefs == null) {
            idRefs = new ArrayList<Object>();
        }
        return this.idRefs;
    }

    /**
     * Gets the value of the comCscNonINFError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscNonINFError() {
        return comCscNonINFError;
    }

    /**
     * Sets the value of the comCscNonINFError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscNonINFError(String value) {
        this.comCscNonINFError = value;
    }

}
