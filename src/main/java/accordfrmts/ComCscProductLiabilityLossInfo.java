//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ActionCd" minOccurs="0"/>
 *         &lt;element ref="{}ItemIdInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}Brand" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ProductLiabilityCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ProductGenericName" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_AllegedHarmDesc" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionCd",
    "itemIdInfo",
    "brand",
    "comCscProductLiabilityCd",
    "comCscProductGenericName",
    "comCscAllegedHarmDesc"
})
@XmlRootElement(name = "com.csc_ProductLiabilityLossInfo")
public class ComCscProductLiabilityLossInfo {

    @XmlElement(name = "ActionCd")
    protected Action actionCd;
    @XmlElement(name = "ItemIdInfo")
    protected List<ItemIdInfo> itemIdInfo;
    @XmlElement(name = "Brand")
    protected String brand;
    @XmlElement(name = "com.csc_ProductLiabilityCd")
    protected String comCscProductLiabilityCd;
    @XmlElement(name = "com.csc_ProductGenericName")
    protected String comCscProductGenericName;
    @XmlElement(name = "com.csc_AllegedHarmDesc")
    protected String comCscAllegedHarmDesc;

    /**
     * Gets the value of the actionCd property.
     * 
     * @return
     *     possible object is
     *     {@link Action }
     *     
     */
    public Action getActionCd() {
        return actionCd;
    }

    /**
     * Sets the value of the actionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Action }
     *     
     */
    public void setActionCd(Action value) {
        this.actionCd = value;
    }

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIdInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIdInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemIdInfo }
     * 
     * 
     */
    public List<ItemIdInfo> getItemIdInfo() {
        if (itemIdInfo == null) {
            itemIdInfo = new ArrayList<ItemIdInfo>();
        }
        return this.itemIdInfo;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the comCscProductLiabilityCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscProductLiabilityCd() {
        return comCscProductLiabilityCd;
    }

    /**
     * Sets the value of the comCscProductLiabilityCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscProductLiabilityCd(String value) {
        this.comCscProductLiabilityCd = value;
    }

    /**
     * Gets the value of the comCscProductGenericName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscProductGenericName() {
        return comCscProductGenericName;
    }

    /**
     * Sets the value of the comCscProductGenericName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscProductGenericName(String value) {
        this.comCscProductGenericName = value;
    }

    /**
     * Gets the value of the comCscAllegedHarmDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscAllegedHarmDesc() {
        return comCscAllegedHarmDesc;
    }

    /**
     * Sets the value of the comCscAllegedHarmDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscAllegedHarmDesc(String value) {
        this.comCscAllegedHarmDesc = value;
    }

}
