//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemDetail" minOccurs="0"/>
 *         &lt;element ref="{}ExcludedInd" minOccurs="0"/>
 *         &lt;element ref="{}ItemValuationTypeCd" minOccurs="0"/>
 *         &lt;element ref="{}ItemValueAmt" minOccurs="0"/>
 *         &lt;element ref="{}SalesReceiptAppraisal" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}AdditionalInterest" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}MinPremInd" minOccurs="0"/>
 *         &lt;element ref="{}Measure" minOccurs="0"/>
 *         &lt;element ref="{}RatingCalc" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}CommlCoverage" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ValueInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *       &lt;attribute name="VehRef" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemDetail",
    "excludedInd",
    "itemValuationTypeCd",
    "itemValueAmt",
    "salesReceiptAppraisal",
    "additionalInterest",
    "minPremInd",
    "measure",
    "ratingCalc",
    "commlCoverage",
    "valueInfo"
})
@XmlRootElement(name = "PropertyItem")
public class PropertyItem {

    @XmlElement(name = "ItemDetail")
    protected String itemDetail;
    @XmlElement(name = "ExcludedInd")
    protected Boolean excludedInd;
    @XmlElement(name = "ItemValuationTypeCd")
    protected String itemValuationTypeCd;
    @XmlElement(name = "ItemValueAmt")
    protected BigDecimal itemValueAmt;
    @XmlElement(name = "SalesReceiptAppraisal")
    protected List<SalesReceiptAppraisal> salesReceiptAppraisal;
    @XmlElement(name = "AdditionalInterest")
    protected List<AdditionalInterest> additionalInterest;
    @XmlElement(name = "MinPremInd")
    protected Boolean minPremInd;
    @XmlElement(name = "Measure")
    protected String measure;
    @XmlElement(name = "RatingCalc")
    protected List<RatingCalc> ratingCalc;
    @XmlElement(name = "CommlCoverage")
    protected List<CommlCoverage> commlCoverage;
    @XmlElement(name = "ValueInfo")
    protected List<ValueInfo> valueInfo;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "VehRef")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object vehRef;

    /**
     * Gets the value of the itemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDetail() {
        return itemDetail;
    }

    /**
     * Sets the value of the itemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDetail(String value) {
        this.itemDetail = value;
    }

    /**
     * Gets the value of the excludedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExcludedInd() {
        return excludedInd;
    }

    /**
     * Sets the value of the excludedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludedInd(Boolean value) {
        this.excludedInd = value;
    }

    /**
     * Gets the value of the itemValuationTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemValuationTypeCd() {
        return itemValuationTypeCd;
    }

    /**
     * Sets the value of the itemValuationTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemValuationTypeCd(String value) {
        this.itemValuationTypeCd = value;
    }

    /**
     * Gets the value of the itemValueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getItemValueAmt() {
        return itemValueAmt;
    }

    /**
     * Sets the value of the itemValueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setItemValueAmt(BigDecimal value) {
        this.itemValueAmt = value;
    }

    /**
     * Gets the value of the salesReceiptAppraisal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the salesReceiptAppraisal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSalesReceiptAppraisal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesReceiptAppraisal }
     * 
     * 
     */
    public List<SalesReceiptAppraisal> getSalesReceiptAppraisal() {
        if (salesReceiptAppraisal == null) {
            salesReceiptAppraisal = new ArrayList<SalesReceiptAppraisal>();
        }
        return this.salesReceiptAppraisal;
    }

    /**
     * Gets the value of the additionalInterest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInterest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInterest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInterest }
     * 
     * 
     */
    public List<AdditionalInterest> getAdditionalInterest() {
        if (additionalInterest == null) {
            additionalInterest = new ArrayList<AdditionalInterest>();
        }
        return this.additionalInterest;
    }

    /**
     * Gets the value of the minPremInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMinPremInd() {
        return minPremInd;
    }

    /**
     * Sets the value of the minPremInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMinPremInd(Boolean value) {
        this.minPremInd = value;
    }

    /**
     * Gets the value of the measure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasure() {
        return measure;
    }

    /**
     * Sets the value of the measure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasure(String value) {
        this.measure = value;
    }

    /**
     * Gets the value of the ratingCalc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ratingCalc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRatingCalc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RatingCalc }
     * 
     * 
     */
    public List<RatingCalc> getRatingCalc() {
        if (ratingCalc == null) {
            ratingCalc = new ArrayList<RatingCalc>();
        }
        return this.ratingCalc;
    }

    /**
     * Gets the value of the commlCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commlCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommlCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommlCoverage }
     * 
     * 
     */
    public List<CommlCoverage> getCommlCoverage() {
        if (commlCoverage == null) {
            commlCoverage = new ArrayList<CommlCoverage>();
        }
        return this.commlCoverage;
    }

    /**
     * Gets the value of the valueInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueInfo }
     * 
     * 
     */
    public List<ValueInfo> getValueInfo() {
        if (valueInfo == null) {
            valueInfo = new ArrayList<ValueInfo>();
        }
        return this.valueInfo;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the vehRef property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getVehRef() {
        return vehRef;
    }

    /**
     * Sets the value of the vehRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setVehRef(Object value) {
        this.vehRef = value;
    }

}
