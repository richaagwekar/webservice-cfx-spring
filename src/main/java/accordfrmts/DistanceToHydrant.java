package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
   "numUnits",
   "unitMeasurementCd"
   
})

@XmlRootElement(name = "DistanceToHydrant")
public class DistanceToHydrant {
	public String getNumUnits() {
		return numUnits;
	}
	public void setNumUnits(String numUnits) {
		this.numUnits = numUnits;
	}
	public String getUnitMeasurementCd() {
		return unitMeasurementCd;
	}
	public void setUnitMeasurementCd(String unitMeasurementCd) {
		this.unitMeasurementCd = unitMeasurementCd;
	}
	@XmlElement(name="NumUnits")
protected String numUnits;
	@XmlElement(name="UnitMeasurementCd")
protected String unitMeasurementCd;

}
