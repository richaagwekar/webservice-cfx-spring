//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_ChangePolicyBillPlanInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_BillPlanCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_PolicyBillPlanSelection" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SetPolicyPlanPendingInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EquityEditOverrideInd" minOccurs="0"/>
 *         &lt;element ref="{}DepositAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_ReinvoiceInd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscChangePolicyBillPlanInd",
    "comCscBillPlanCd",
    "comCscPolicyBillPlanSelection",
    "comCscSetPolicyPlanPendingInd",
    "comCscEquityEditOverrideInd",
    "depositAmt",
    "comCscReinvoiceInd"
})
@XmlRootElement(name = "com.csc_ChangePolicyBillPlan")
public class ComCscChangePolicyBillPlan {

    @XmlElement(name = "com.csc_ChangePolicyBillPlanInd")
    protected Boolean comCscChangePolicyBillPlanInd;
    @XmlElement(name = "com.csc_BillPlanCd")
    protected String comCscBillPlanCd;
    @XmlElement(name = "com.csc_PolicyBillPlanSelection")
    protected ComCscPolicyBillPlanSelection comCscPolicyBillPlanSelection;
    @XmlElement(name = "com.csc_SetPolicyPlanPendingInd")
    protected Boolean comCscSetPolicyPlanPendingInd;
    @XmlElement(name = "com.csc_EquityEditOverrideInd")
    protected Boolean comCscEquityEditOverrideInd;
    @XmlElement(name = "DepositAmt")
    protected BigDecimal depositAmt;
    @XmlElement(name = "com.csc_ReinvoiceInd")
    protected Boolean comCscReinvoiceInd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the comCscChangePolicyBillPlanInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscChangePolicyBillPlanInd() {
        return comCscChangePolicyBillPlanInd;
    }

    /**
     * Sets the value of the comCscChangePolicyBillPlanInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscChangePolicyBillPlanInd(Boolean value) {
        this.comCscChangePolicyBillPlanInd = value;
    }

    /**
     * Gets the value of the comCscBillPlanCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscBillPlanCd() {
        return comCscBillPlanCd;
    }

    /**
     * Sets the value of the comCscBillPlanCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscBillPlanCd(String value) {
        this.comCscBillPlanCd = value;
    }

    /**
     * Gets the value of the comCscPolicyBillPlanSelection property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscPolicyBillPlanSelection }
     *     
     */
    public ComCscPolicyBillPlanSelection getComCscPolicyBillPlanSelection() {
        return comCscPolicyBillPlanSelection;
    }

    /**
     * Sets the value of the comCscPolicyBillPlanSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscPolicyBillPlanSelection }
     *     
     */
    public void setComCscPolicyBillPlanSelection(ComCscPolicyBillPlanSelection value) {
        this.comCscPolicyBillPlanSelection = value;
    }

    /**
     * Gets the value of the comCscSetPolicyPlanPendingInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscSetPolicyPlanPendingInd() {
        return comCscSetPolicyPlanPendingInd;
    }

    /**
     * Sets the value of the comCscSetPolicyPlanPendingInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscSetPolicyPlanPendingInd(Boolean value) {
        this.comCscSetPolicyPlanPendingInd = value;
    }

    /**
     * Gets the value of the comCscEquityEditOverrideInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscEquityEditOverrideInd() {
        return comCscEquityEditOverrideInd;
    }

    /**
     * Sets the value of the comCscEquityEditOverrideInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscEquityEditOverrideInd(Boolean value) {
        this.comCscEquityEditOverrideInd = value;
    }

    /**
     * Gets the value of the depositAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepositAmt() {
        return depositAmt;
    }

    /**
     * Sets the value of the depositAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepositAmt(BigDecimal value) {
        this.depositAmt = value;
    }

    /**
     * Gets the value of the comCscReinvoiceInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscReinvoiceInd() {
        return comCscReinvoiceInd;
    }

    /**
     * Sets the value of the comCscReinvoiceInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscReinvoiceInd(Boolean value) {
        this.comCscReinvoiceInd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
