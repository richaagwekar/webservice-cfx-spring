//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}APIPSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}AutoMedicalPaymentsSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}CollisionSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}CombinedPhysicalDamageSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}ComprehensiveSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}EffectiveDt" minOccurs="0"/>
 *         &lt;element ref="{}LiabilitySymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}PIPNoFaultSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}PropertyProtectionCoverageSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}SpecifiedPerilsSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}TowingAndLaborSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}TrailerInterchangeCollisionSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}TrailerInterchangeComprehensiveSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}TrailerInterchangeSpecifiedPerilsSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}UnderinsuredMotoristSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{}UninsuredMotoristSymbolCd" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apipSymbolCd",
    "autoMedicalPaymentsSymbolCd",
    "collisionSymbolCd",
    "combinedPhysicalDamageSymbolCd",
    "comprehensiveSymbolCd",
    "effectiveDt",
    "liabilitySymbolCd",
    "pipNoFaultSymbolCd",
    "propertyProtectionCoverageSymbolCd",
    "specifiedPerilsSymbolCd",
    "towingAndLaborSymbolCd",
    "trailerInterchangeCollisionSymbolCd",
    "trailerInterchangeComprehensiveSymbolCd",
    "trailerInterchangeSpecifiedPerilsSymbolCd",
    "underinsuredMotoristSymbolCd",
    "uninsuredMotoristSymbolCd"
})
@XmlRootElement(name = "CoveredAutoSymbol")
public class CoveredAutoSymbol {

    @XmlElement(name = "APIPSymbolCd")
    protected List<String> apipSymbolCd;
    @XmlElement(name = "AutoMedicalPaymentsSymbolCd")
    protected List<String> autoMedicalPaymentsSymbolCd;
    @XmlElement(name = "CollisionSymbolCd")
    protected List<String> collisionSymbolCd;
    @XmlElement(name = "CombinedPhysicalDamageSymbolCd")
    protected List<String> combinedPhysicalDamageSymbolCd;
    @XmlElement(name = "ComprehensiveSymbolCd")
    protected List<String> comprehensiveSymbolCd;
    @XmlElement(name = "EffectiveDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDt;
    @XmlElement(name = "LiabilitySymbolCd")
    protected List<String> liabilitySymbolCd;
    @XmlElement(name = "PIPNoFaultSymbolCd")
    protected List<String> pipNoFaultSymbolCd;
    @XmlElement(name = "PropertyProtectionCoverageSymbolCd")
    protected List<String> propertyProtectionCoverageSymbolCd;
    @XmlElement(name = "SpecifiedPerilsSymbolCd")
    protected List<String> specifiedPerilsSymbolCd;
    @XmlElement(name = "TowingAndLaborSymbolCd")
    protected List<String> towingAndLaborSymbolCd;
    @XmlElement(name = "TrailerInterchangeCollisionSymbolCd")
    protected List<String> trailerInterchangeCollisionSymbolCd;
    @XmlElement(name = "TrailerInterchangeComprehensiveSymbolCd")
    protected List<String> trailerInterchangeComprehensiveSymbolCd;
    @XmlElement(name = "TrailerInterchangeSpecifiedPerilsSymbolCd")
    protected List<String> trailerInterchangeSpecifiedPerilsSymbolCd;
    @XmlElement(name = "UnderinsuredMotoristSymbolCd")
    protected List<String> underinsuredMotoristSymbolCd;
    @XmlElement(name = "UninsuredMotoristSymbolCd")
    protected List<String> uninsuredMotoristSymbolCd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the apipSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the apipSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAPIPSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAPIPSymbolCd() {
        if (apipSymbolCd == null) {
            apipSymbolCd = new ArrayList<String>();
        }
        return this.apipSymbolCd;
    }

    /**
     * Gets the value of the autoMedicalPaymentsSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoMedicalPaymentsSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoMedicalPaymentsSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAutoMedicalPaymentsSymbolCd() {
        if (autoMedicalPaymentsSymbolCd == null) {
            autoMedicalPaymentsSymbolCd = new ArrayList<String>();
        }
        return this.autoMedicalPaymentsSymbolCd;
    }

    /**
     * Gets the value of the collisionSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collisionSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollisionSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCollisionSymbolCd() {
        if (collisionSymbolCd == null) {
            collisionSymbolCd = new ArrayList<String>();
        }
        return this.collisionSymbolCd;
    }

    /**
     * Gets the value of the combinedPhysicalDamageSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the combinedPhysicalDamageSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCombinedPhysicalDamageSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCombinedPhysicalDamageSymbolCd() {
        if (combinedPhysicalDamageSymbolCd == null) {
            combinedPhysicalDamageSymbolCd = new ArrayList<String>();
        }
        return this.combinedPhysicalDamageSymbolCd;
    }

    /**
     * Gets the value of the comprehensiveSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comprehensiveSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComprehensiveSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getComprehensiveSymbolCd() {
        if (comprehensiveSymbolCd == null) {
            comprehensiveSymbolCd = new ArrayList<String>();
        }
        return this.comprehensiveSymbolCd;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDt(XMLGregorianCalendar value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the liabilitySymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the liabilitySymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLiabilitySymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLiabilitySymbolCd() {
        if (liabilitySymbolCd == null) {
            liabilitySymbolCd = new ArrayList<String>();
        }
        return this.liabilitySymbolCd;
    }

    /**
     * Gets the value of the pipNoFaultSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pipNoFaultSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPIPNoFaultSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPIPNoFaultSymbolCd() {
        if (pipNoFaultSymbolCd == null) {
            pipNoFaultSymbolCd = new ArrayList<String>();
        }
        return this.pipNoFaultSymbolCd;
    }

    /**
     * Gets the value of the propertyProtectionCoverageSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propertyProtectionCoverageSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropertyProtectionCoverageSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPropertyProtectionCoverageSymbolCd() {
        if (propertyProtectionCoverageSymbolCd == null) {
            propertyProtectionCoverageSymbolCd = new ArrayList<String>();
        }
        return this.propertyProtectionCoverageSymbolCd;
    }

    /**
     * Gets the value of the specifiedPerilsSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedPerilsSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedPerilsSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSpecifiedPerilsSymbolCd() {
        if (specifiedPerilsSymbolCd == null) {
            specifiedPerilsSymbolCd = new ArrayList<String>();
        }
        return this.specifiedPerilsSymbolCd;
    }

    /**
     * Gets the value of the towingAndLaborSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the towingAndLaborSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTowingAndLaborSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTowingAndLaborSymbolCd() {
        if (towingAndLaborSymbolCd == null) {
            towingAndLaborSymbolCd = new ArrayList<String>();
        }
        return this.towingAndLaborSymbolCd;
    }

    /**
     * Gets the value of the trailerInterchangeCollisionSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trailerInterchangeCollisionSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrailerInterchangeCollisionSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTrailerInterchangeCollisionSymbolCd() {
        if (trailerInterchangeCollisionSymbolCd == null) {
            trailerInterchangeCollisionSymbolCd = new ArrayList<String>();
        }
        return this.trailerInterchangeCollisionSymbolCd;
    }

    /**
     * Gets the value of the trailerInterchangeComprehensiveSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trailerInterchangeComprehensiveSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrailerInterchangeComprehensiveSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTrailerInterchangeComprehensiveSymbolCd() {
        if (trailerInterchangeComprehensiveSymbolCd == null) {
            trailerInterchangeComprehensiveSymbolCd = new ArrayList<String>();
        }
        return this.trailerInterchangeComprehensiveSymbolCd;
    }

    /**
     * Gets the value of the trailerInterchangeSpecifiedPerilsSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trailerInterchangeSpecifiedPerilsSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrailerInterchangeSpecifiedPerilsSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTrailerInterchangeSpecifiedPerilsSymbolCd() {
        if (trailerInterchangeSpecifiedPerilsSymbolCd == null) {
            trailerInterchangeSpecifiedPerilsSymbolCd = new ArrayList<String>();
        }
        return this.trailerInterchangeSpecifiedPerilsSymbolCd;
    }

    /**
     * Gets the value of the underinsuredMotoristSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the underinsuredMotoristSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnderinsuredMotoristSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUnderinsuredMotoristSymbolCd() {
        if (underinsuredMotoristSymbolCd == null) {
            underinsuredMotoristSymbolCd = new ArrayList<String>();
        }
        return this.underinsuredMotoristSymbolCd;
    }

    /**
     * Gets the value of the uninsuredMotoristSymbolCd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uninsuredMotoristSymbolCd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUninsuredMotoristSymbolCd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUninsuredMotoristSymbolCd() {
        if (uninsuredMotoristSymbolCd == null) {
            uninsuredMotoristSymbolCd = new ArrayList<String>();
        }
        return this.uninsuredMotoristSymbolCd;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
