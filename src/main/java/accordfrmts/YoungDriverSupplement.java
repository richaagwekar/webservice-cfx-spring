//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Manufacturer" minOccurs="0"/>
 *         &lt;element ref="{}Model" minOccurs="0"/>
 *         &lt;element ref="{}ModelYear" minOccurs="0"/>
 *         &lt;element ref="{}AllowOthersInd" minOccurs="0"/>
 *         &lt;element ref="{}ArrestedOrDetainedInd" minOccurs="0"/>
 *         &lt;element ref="{}AttendSchoolInd" minOccurs="0"/>
 *         &lt;element ref="{}CollegeGradeAverage" minOccurs="0"/>
 *         &lt;element ref="{}SignedDt" minOccurs="0"/>
 *         &lt;element ref="{}NumDaysDrivenPerWeekToSchool" minOccurs="0"/>
 *         &lt;element ref="{}NumDaysDrivenPerWeekToWork" minOccurs="0"/>
 *         &lt;element ref="{}DeclinedCanceledNonRenewedInd" minOccurs="0"/>
 *         &lt;element ref="{}DriverTrainingInd" minOccurs="0"/>
 *         &lt;element ref="{}ExpelledEtcInd" minOccurs="0"/>
 *         &lt;element ref="{}HighestGradeCompleted" minOccurs="0"/>
 *         &lt;element ref="{}HighSchoolGradeAverage" minOccurs="0"/>
 *         &lt;element ref="{}InAccidentInd" minOccurs="0"/>
 *         &lt;element ref="{}LicenseSuspendedRevokedInd" minOccurs="0"/>
 *         &lt;element ref="{}DistanceOneWayToSchool" minOccurs="0"/>
 *         &lt;element ref="{}DistanceOneWayToWork" minOccurs="0"/>
 *         &lt;element ref="{}ModifiedOrSpecialEquipmentInd" minOccurs="0"/>
 *         &lt;element ref="{}LengthTimeDriving" minOccurs="0"/>
 *         &lt;element ref="{}OwnOrPurchaseInd" minOccurs="0"/>
 *         &lt;element ref="{}ParentLimitationsInd" minOccurs="0"/>
 *         &lt;element ref="{}ResideWithParentsInd" minOccurs="0"/>
 *         &lt;element ref="{}SignatureInd" minOccurs="0"/>
 *         &lt;element ref="{}TicketReceivedInd" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "manufacturer",
    "model",
    "modelYear",
    "allowOthersInd",
    "arrestedOrDetainedInd",
    "attendSchoolInd",
    "collegeGradeAverage",
    "signedDt",
    "numDaysDrivenPerWeekToSchool",
    "numDaysDrivenPerWeekToWork",
    "declinedCanceledNonRenewedInd",
    "driverTrainingInd",
    "expelledEtcInd",
    "highestGradeCompleted",
    "highSchoolGradeAverage",
    "inAccidentInd",
    "licenseSuspendedRevokedInd",
    "distanceOneWayToSchool",
    "distanceOneWayToWork",
    "modifiedOrSpecialEquipmentInd",
    "lengthTimeDriving",
    "ownOrPurchaseInd",
    "parentLimitationsInd",
    "resideWithParentsInd",
    "signatureInd",
    "ticketReceivedInd"
})
@XmlRootElement(name = "YoungDriverSupplement")
public class YoungDriverSupplement {

    @XmlElement(name = "Manufacturer")
    protected String manufacturer;
    @XmlElement(name = "Model")
    protected String model;
    @XmlElement(name = "ModelYear")
    protected String modelYear;
    @XmlElement(name = "AllowOthersInd")
    protected Boolean allowOthersInd;
    @XmlElement(name = "ArrestedOrDetainedInd")
    protected Boolean arrestedOrDetainedInd;
    @XmlElement(name = "AttendSchoolInd")
    protected Boolean attendSchoolInd;
    @XmlElement(name = "CollegeGradeAverage")
    protected String collegeGradeAverage;
    @XmlElement(name = "SignedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar signedDt;
    @XmlElement(name = "NumDaysDrivenPerWeekToSchool")
    protected BigInteger numDaysDrivenPerWeekToSchool;
    @XmlElement(name = "NumDaysDrivenPerWeekToWork")
    protected BigInteger numDaysDrivenPerWeekToWork;
    @XmlElement(name = "DeclinedCanceledNonRenewedInd")
    protected Boolean declinedCanceledNonRenewedInd;
    @XmlElement(name = "DriverTrainingInd")
    protected Boolean driverTrainingInd;
    @XmlElement(name = "ExpelledEtcInd")
    protected Boolean expelledEtcInd;
    @XmlElement(name = "HighestGradeCompleted")
    protected String highestGradeCompleted;
    @XmlElement(name = "HighSchoolGradeAverage")
    protected String highSchoolGradeAverage;
    @XmlElement(name = "InAccidentInd")
    protected Boolean inAccidentInd;
    @XmlElement(name = "LicenseSuspendedRevokedInd")
    protected Boolean licenseSuspendedRevokedInd;
    @XmlElement(name = "DistanceOneWayToSchool")
    protected String distanceOneWayToSchool;
    @XmlElement(name = "DistanceOneWayToWork")
    protected String distanceOneWayToWork;
    @XmlElement(name = "ModifiedOrSpecialEquipmentInd")
    protected Boolean modifiedOrSpecialEquipmentInd;
    @XmlElement(name = "LengthTimeDriving")
    protected BigInteger lengthTimeDriving;
    @XmlElement(name = "OwnOrPurchaseInd")
    protected Boolean ownOrPurchaseInd;
    @XmlElement(name = "ParentLimitationsInd")
    protected Boolean parentLimitationsInd;
    @XmlElement(name = "ResideWithParentsInd")
    protected Boolean resideWithParentsInd;
    @XmlElement(name = "SignatureInd")
    protected Boolean signatureInd;
    @XmlElement(name = "TicketReceivedInd")
    protected Boolean ticketReceivedInd;
    @XmlAttribute
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the manufacturer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Sets the value of the manufacturer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturer(String value) {
        this.manufacturer = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModel(String value) {
        this.model = value;
    }

    /**
     * Gets the value of the modelYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelYear() {
        return modelYear;
    }

    /**
     * Sets the value of the modelYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelYear(String value) {
        this.modelYear = value;
    }

    /**
     * Gets the value of the allowOthersInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowOthersInd() {
        return allowOthersInd;
    }

    /**
     * Sets the value of the allowOthersInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowOthersInd(Boolean value) {
        this.allowOthersInd = value;
    }

    /**
     * Gets the value of the arrestedOrDetainedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getArrestedOrDetainedInd() {
        return arrestedOrDetainedInd;
    }

    /**
     * Sets the value of the arrestedOrDetainedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setArrestedOrDetainedInd(Boolean value) {
        this.arrestedOrDetainedInd = value;
    }

    /**
     * Gets the value of the attendSchoolInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAttendSchoolInd() {
        return attendSchoolInd;
    }

    /**
     * Sets the value of the attendSchoolInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAttendSchoolInd(Boolean value) {
        this.attendSchoolInd = value;
    }

    /**
     * Gets the value of the collegeGradeAverage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollegeGradeAverage() {
        return collegeGradeAverage;
    }

    /**
     * Sets the value of the collegeGradeAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollegeGradeAverage(String value) {
        this.collegeGradeAverage = value;
    }

    /**
     * Gets the value of the signedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignedDt() {
        return signedDt;
    }

    /**
     * Sets the value of the signedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignedDt(XMLGregorianCalendar value) {
        this.signedDt = value;
    }

    /**
     * Gets the value of the numDaysDrivenPerWeekToSchool property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumDaysDrivenPerWeekToSchool() {
        return numDaysDrivenPerWeekToSchool;
    }

    /**
     * Sets the value of the numDaysDrivenPerWeekToSchool property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumDaysDrivenPerWeekToSchool(BigInteger value) {
        this.numDaysDrivenPerWeekToSchool = value;
    }

    /**
     * Gets the value of the numDaysDrivenPerWeekToWork property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumDaysDrivenPerWeekToWork() {
        return numDaysDrivenPerWeekToWork;
    }

    /**
     * Sets the value of the numDaysDrivenPerWeekToWork property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumDaysDrivenPerWeekToWork(BigInteger value) {
        this.numDaysDrivenPerWeekToWork = value;
    }

    /**
     * Gets the value of the declinedCanceledNonRenewedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDeclinedCanceledNonRenewedInd() {
        return declinedCanceledNonRenewedInd;
    }

    /**
     * Sets the value of the declinedCanceledNonRenewedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeclinedCanceledNonRenewedInd(Boolean value) {
        this.declinedCanceledNonRenewedInd = value;
    }

    /**
     * Gets the value of the driverTrainingInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDriverTrainingInd() {
        return driverTrainingInd;
    }

    /**
     * Sets the value of the driverTrainingInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDriverTrainingInd(Boolean value) {
        this.driverTrainingInd = value;
    }

    /**
     * Gets the value of the expelledEtcInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExpelledEtcInd() {
        return expelledEtcInd;
    }

    /**
     * Sets the value of the expelledEtcInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpelledEtcInd(Boolean value) {
        this.expelledEtcInd = value;
    }

    /**
     * Gets the value of the highestGradeCompleted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHighestGradeCompleted() {
        return highestGradeCompleted;
    }

    /**
     * Sets the value of the highestGradeCompleted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHighestGradeCompleted(String value) {
        this.highestGradeCompleted = value;
    }

    /**
     * Gets the value of the highSchoolGradeAverage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHighSchoolGradeAverage() {
        return highSchoolGradeAverage;
    }

    /**
     * Sets the value of the highSchoolGradeAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHighSchoolGradeAverage(String value) {
        this.highSchoolGradeAverage = value;
    }

    /**
     * Gets the value of the inAccidentInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInAccidentInd() {
        return inAccidentInd;
    }

    /**
     * Sets the value of the inAccidentInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInAccidentInd(Boolean value) {
        this.inAccidentInd = value;
    }

    /**
     * Gets the value of the licenseSuspendedRevokedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLicenseSuspendedRevokedInd() {
        return licenseSuspendedRevokedInd;
    }

    /**
     * Sets the value of the licenseSuspendedRevokedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLicenseSuspendedRevokedInd(Boolean value) {
        this.licenseSuspendedRevokedInd = value;
    }

    /**
     * Gets the value of the distanceOneWayToSchool property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceOneWayToSchool() {
        return distanceOneWayToSchool;
    }

    /**
     * Sets the value of the distanceOneWayToSchool property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceOneWayToSchool(String value) {
        this.distanceOneWayToSchool = value;
    }

    /**
     * Gets the value of the distanceOneWayToWork property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceOneWayToWork() {
        return distanceOneWayToWork;
    }

    /**
     * Sets the value of the distanceOneWayToWork property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceOneWayToWork(String value) {
        this.distanceOneWayToWork = value;
    }

    /**
     * Gets the value of the modifiedOrSpecialEquipmentInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getModifiedOrSpecialEquipmentInd() {
        return modifiedOrSpecialEquipmentInd;
    }

    /**
     * Sets the value of the modifiedOrSpecialEquipmentInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModifiedOrSpecialEquipmentInd(Boolean value) {
        this.modifiedOrSpecialEquipmentInd = value;
    }

    /**
     * Gets the value of the lengthTimeDriving property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLengthTimeDriving() {
        return lengthTimeDriving;
    }

    /**
     * Sets the value of the lengthTimeDriving property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLengthTimeDriving(BigInteger value) {
        this.lengthTimeDriving = value;
    }

    /**
     * Gets the value of the ownOrPurchaseInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOwnOrPurchaseInd() {
        return ownOrPurchaseInd;
    }

    /**
     * Sets the value of the ownOrPurchaseInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOwnOrPurchaseInd(Boolean value) {
        this.ownOrPurchaseInd = value;
    }

    /**
     * Gets the value of the parentLimitationsInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getParentLimitationsInd() {
        return parentLimitationsInd;
    }

    /**
     * Sets the value of the parentLimitationsInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParentLimitationsInd(Boolean value) {
        this.parentLimitationsInd = value;
    }

    /**
     * Gets the value of the resideWithParentsInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getResideWithParentsInd() {
        return resideWithParentsInd;
    }

    /**
     * Sets the value of the resideWithParentsInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResideWithParentsInd(Boolean value) {
        this.resideWithParentsInd = value;
    }

    /**
     * Gets the value of the signatureInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSignatureInd() {
        return signatureInd;
    }

    /**
     * Sets the value of the signatureInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSignatureInd(Boolean value) {
        this.signatureInd = value;
    }

    /**
     * Gets the value of the ticketReceivedInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTicketReceivedInd() {
        return ticketReceivedInd;
    }

    /**
     * Sets the value of the ticketReceivedInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketReceivedInd(Boolean value) {
        this.ticketReceivedInd = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
