//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_SearchIdentifierType" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchIdentifierTypeSelection" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchIdentifier" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchPaymentMethodCd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchPaymentMethodSelection" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_StartPostedDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_EndPostedDt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchMinimumAmt" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SearchMaxiumAmt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscSearchIdentifierType",
    "comCscSearchIdentifierTypeSelection",
    "comCscSearchIdentifier",
    "comCscSearchPaymentMethodCd",
    "comCscSearchPaymentMethodSelection",
    "comCscStartPostedDt",
    "comCscEndPostedDt",
    "comCscSearchMinimumAmt",
    "comCscSearchMaxiumAmt"
})
@XmlRootElement(name = "com.csc_PaymentSearchInfo")
public class ComCscPaymentSearchInfo {

    @XmlElement(name = "com.csc_SearchIdentifierType")
    protected String comCscSearchIdentifierType;
    @XmlElement(name = "com.csc_SearchIdentifierTypeSelection")
    protected ComCscSearchIdentifierTypeSelection comCscSearchIdentifierTypeSelection;
    @XmlElement(name = "com.csc_SearchIdentifier")
    protected String comCscSearchIdentifier;
    @XmlElement(name = "com.csc_SearchPaymentMethodCd")
    protected String comCscSearchPaymentMethodCd;
    @XmlElement(name = "com.csc_SearchPaymentMethodSelection")
    protected String comCscSearchPaymentMethodSelection;
    @XmlElement(name = "com.csc_StartPostedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscStartPostedDt;
    @XmlElement(name = "com.csc_EndPostedDt")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar comCscEndPostedDt;
    @XmlElement(name = "com.csc_SearchMinimumAmt")
    protected BigDecimal comCscSearchMinimumAmt;
    @XmlElement(name = "com.csc_SearchMaxiumAmt")
    protected BigDecimal comCscSearchMaxiumAmt;

    /**
     * Gets the value of the comCscSearchIdentifierType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSearchIdentifierType() {
        return comCscSearchIdentifierType;
    }

    /**
     * Sets the value of the comCscSearchIdentifierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSearchIdentifierType(String value) {
        this.comCscSearchIdentifierType = value;
    }

    /**
     * Gets the value of the comCscSearchIdentifierTypeSelection property.
     * 
     * @return
     *     possible object is
     *     {@link ComCscSearchIdentifierTypeSelection }
     *     
     */
    public ComCscSearchIdentifierTypeSelection getComCscSearchIdentifierTypeSelection() {
        return comCscSearchIdentifierTypeSelection;
    }

    /**
     * Sets the value of the comCscSearchIdentifierTypeSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComCscSearchIdentifierTypeSelection }
     *     
     */
    public void setComCscSearchIdentifierTypeSelection(ComCscSearchIdentifierTypeSelection value) {
        this.comCscSearchIdentifierTypeSelection = value;
    }

    /**
     * Gets the value of the comCscSearchIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSearchIdentifier() {
        return comCscSearchIdentifier;
    }

    /**
     * Sets the value of the comCscSearchIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSearchIdentifier(String value) {
        this.comCscSearchIdentifier = value;
    }

    /**
     * Gets the value of the comCscSearchPaymentMethodCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSearchPaymentMethodCd() {
        return comCscSearchPaymentMethodCd;
    }

    /**
     * Sets the value of the comCscSearchPaymentMethodCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSearchPaymentMethodCd(String value) {
        this.comCscSearchPaymentMethodCd = value;
    }

    /**
     * Gets the value of the comCscSearchPaymentMethodSelection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSearchPaymentMethodSelection() {
        return comCscSearchPaymentMethodSelection;
    }

    /**
     * Sets the value of the comCscSearchPaymentMethodSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSearchPaymentMethodSelection(String value) {
        this.comCscSearchPaymentMethodSelection = value;
    }

    /**
     * Gets the value of the comCscStartPostedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscStartPostedDt() {
        return comCscStartPostedDt;
    }

    /**
     * Sets the value of the comCscStartPostedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscStartPostedDt(XMLGregorianCalendar value) {
        this.comCscStartPostedDt = value;
    }

    /**
     * Gets the value of the comCscEndPostedDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getComCscEndPostedDt() {
        return comCscEndPostedDt;
    }

    /**
     * Sets the value of the comCscEndPostedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setComCscEndPostedDt(XMLGregorianCalendar value) {
        this.comCscEndPostedDt = value;
    }

    /**
     * Gets the value of the comCscSearchMinimumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscSearchMinimumAmt() {
        return comCscSearchMinimumAmt;
    }

    /**
     * Sets the value of the comCscSearchMinimumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscSearchMinimumAmt(BigDecimal value) {
        this.comCscSearchMinimumAmt = value;
    }

    /**
     * Gets the value of the comCscSearchMaxiumAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComCscSearchMaxiumAmt() {
        return comCscSearchMaxiumAmt;
    }

    /**
     * Sets the value of the comCscSearchMaxiumAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComCscSearchMaxiumAmt(BigDecimal value) {
        this.comCscSearchMaxiumAmt = value;
    }

}
