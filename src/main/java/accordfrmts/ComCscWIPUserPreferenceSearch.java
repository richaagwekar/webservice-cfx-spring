//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.16 at 03:29:24 PM IST 
//


package accordfrmts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}com.csc_WIPInterfaceApplicationId"/>
 *         &lt;element ref="{}ItemIdInfo"/>
 *         &lt;element ref="{}com.csc_UserPreferenceNbr" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_SecurityProgramNbr" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WIPEmployeeValidInd" minOccurs="0"/>
 *         &lt;element ref="{}com.csc_WIPEmployeeLocateAccessInd" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comCscWIPInterfaceApplicationId",
    "itemIdInfo",
    "comCscUserPreferenceNbr",
    "comCscSecurityProgramNbr",
    "comCscWIPEmployeeValidInd",
    "comCscWIPEmployeeLocateAccessInd"
})
@XmlRootElement(name = "com.csc_WIPUserPreferenceSearch")
public class ComCscWIPUserPreferenceSearch {

    @XmlElement(name = "com.csc_WIPInterfaceApplicationId", required = true)
    protected String comCscWIPInterfaceApplicationId;
    @XmlElement(name = "ItemIdInfo", required = true)
    protected ItemIdInfo itemIdInfo;
    @XmlElement(name = "com.csc_UserPreferenceNbr")
    protected String comCscUserPreferenceNbr;
    @XmlElement(name = "com.csc_SecurityProgramNbr")
    protected String comCscSecurityProgramNbr;
    @XmlElement(name = "com.csc_WIPEmployeeValidInd")
    protected Boolean comCscWIPEmployeeValidInd;
    @XmlElement(name = "com.csc_WIPEmployeeLocateAccessInd")
    protected Boolean comCscWIPEmployeeLocateAccessInd;

    /**
     * Gets the value of the comCscWIPInterfaceApplicationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscWIPInterfaceApplicationId() {
        return comCscWIPInterfaceApplicationId;
    }

    /**
     * Sets the value of the comCscWIPInterfaceApplicationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscWIPInterfaceApplicationId(String value) {
        this.comCscWIPInterfaceApplicationId = value;
    }

    /**
     * Gets the value of the itemIdInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ItemIdInfo }
     *     
     */
    public ItemIdInfo getItemIdInfo() {
        return itemIdInfo;
    }

    /**
     * Sets the value of the itemIdInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemIdInfo }
     *     
     */
    public void setItemIdInfo(ItemIdInfo value) {
        this.itemIdInfo = value;
    }

    /**
     * Gets the value of the comCscUserPreferenceNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscUserPreferenceNbr() {
        return comCscUserPreferenceNbr;
    }

    /**
     * Sets the value of the comCscUserPreferenceNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscUserPreferenceNbr(String value) {
        this.comCscUserPreferenceNbr = value;
    }

    /**
     * Gets the value of the comCscSecurityProgramNbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCscSecurityProgramNbr() {
        return comCscSecurityProgramNbr;
    }

    /**
     * Sets the value of the comCscSecurityProgramNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCscSecurityProgramNbr(String value) {
        this.comCscSecurityProgramNbr = value;
    }

    /**
     * Gets the value of the comCscWIPEmployeeValidInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscWIPEmployeeValidInd() {
        return comCscWIPEmployeeValidInd;
    }

    /**
     * Sets the value of the comCscWIPEmployeeValidInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscWIPEmployeeValidInd(Boolean value) {
        this.comCscWIPEmployeeValidInd = value;
    }

    /**
     * Gets the value of the comCscWIPEmployeeLocateAccessInd property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComCscWIPEmployeeLocateAccessInd() {
        return comCscWIPEmployeeLocateAccessInd;
    }

    /**
     * Sets the value of the comCscWIPEmployeeLocateAccessInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComCscWIPEmployeeLocateAccessInd(Boolean value) {
        this.comCscWIPEmployeeLocateAccessInd = value;
    }

}
