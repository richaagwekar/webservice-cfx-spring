package com.csc.rest.changedmodelResponse;

import com.csc.rest.changedmodelResponse.Policies;
import com.csc.rest.changedmodelResponse.Status;
import com.csc.rest.changedmodelRq.Criteria;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@XmlRootElement
@XmlAccessorType(value=XmlAccessType.FIELD)
public class PolicySearchResponse implements Serializable  {
    @XmlElement
    protected Status status;
    @XmlElement
    protected Criteria criteria;
    @XmlElement
    protected Policies policies;

    public Policies getPolicies() {
        return this.policies;
    }

    public void setPolicies(Policies policies) {
        this.policies = policies;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Criteria getCriteria() {
        return this.criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }
}
