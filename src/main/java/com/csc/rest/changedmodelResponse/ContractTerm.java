package com.csc.rest.changedmodelResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(value=XmlAccessType.NONE)
public class ContractTerm {
    @XmlElement(name="EffectiveDt")
    public String effectiveDt = "";
    @XmlElement(name="ExpirationDt")
    public String expirationDt = "";

    public String getEffectiveDt() {
        return this.effectiveDt;
    }

    public void setEffectiveDt(String effectiveDt) {
        this.effectiveDt = effectiveDt;
    }

    public void setExpirationDt(String expirationDt) {
        this.expirationDt = expirationDt;
    }

    public String getExpirationDt() {
        return this.expirationDt;
    }
}