package com.csc.rest.changedmodelResponse;

import com.csc.rest.changedmodelResponse.Policy;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(value=XmlAccessType.FIELD)
public class Policies {
    @XmlElement
    protected Policy[] policy;

    public Policy[] getPolicy() {
        return this.policy;
    }

    public void setPolicy(Policy[] policy) {
        this.policy = policy;
    }
}

