package com.csc.rest.changedmodelResponse;

import com.csc.rest.changedmodelResponse.ContractTerm;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(value=XmlAccessType.NONE)
public class Policy {
    @XmlElement
    protected String policynum = "";
    @XmlElement
    protected String symbol = "";
    @XmlElement
    protected String module = "";
    @XmlElement
    protected String masterco = "";
    @XmlElement
    protected String location = "";
    @XmlElement
    protected ContractTerm contractTerm = new ContractTerm();
    @XmlElement
    protected String href = "";

   

    public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public ContractTerm getContractTerm() {
        return this.contractTerm;
    }

    public void setContractTerm(ContractTerm contractTerm) {
        this.contractTerm = contractTerm;
    }

    public String getModule() {
        return this.module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getMasterco() {
        return this.masterco;
    }

    public void setMasterco(String masterco) {
        this.masterco = masterco;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getPolicynum() {
        return this.policynum;
    }

    public void setPolicynum(String policynum) {
        this.policynum = policynum;
    }
}

