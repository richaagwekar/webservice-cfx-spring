

package com.csc.rest.changedmodelResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlAccessorType(value=XmlAccessType.FIELD)
@JsonPropertyOrder({
    "Code",
    "Message"
})
public class Status {
    @XmlElement
    @JsonProperty("Code")
    protected Integer Code;
    @XmlElement
    @JsonProperty("Message")
    private String Message;
	public Integer getCode() {
		return Code;
	}
	public void setCode(Integer code) {
		Code = code;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}


}

