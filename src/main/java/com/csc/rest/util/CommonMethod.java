package com.csc.rest.util;

import com.csc.rest.model.ResponseInfo;

public class CommonMethod

{
	static String value;

	public static String FormatDate(String d) {
		String date = "";
		date = "20" + d.substring(1, 3) + "-" + d.substring(3, 5) + "-"
				+ d.substring(5, 7);

		return date;
	}

	public static void setLob(ResponseInfo info) {
		value = info.persPolicy.getPolicynum();
	}

	public static String getLob() {
		return value;
	}

	public static String ConvertNumericStateCd(String Cd) {
		String Code = "";
		if (Cd.equals("15"))
			{Code = "KS";  }
		else if (Cd.equals("39"))
			{Code = "SC"; }
		
		return Code;

	}
}
