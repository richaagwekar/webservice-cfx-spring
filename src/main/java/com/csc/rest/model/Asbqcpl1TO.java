// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.rest.model;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "ASBQCPL1")
public class Asbqcpl1TO implements Serializable {
	


	private BigDecimal BQA5VA;
	@Id
	private String BQAGNB;
	private String BQBCCD;
	private String BQEETX;
	private String BQASCD;
	private String BQARCD;
	private String BQAQCD;
	private BigDecimal BQAUPC;
	private BigDecimal BQBTVA;
	private BigDecimal BQBSVA;
	private BigDecimal BQBRVA;
	private BigDecimal BQA3VA;
	private char BQD3ST;
	private char BQD2ST;
	private char BQD0ST;
	private char BQDZST;
	private String BQBATX;
	private String BQTYPE0ACT;
	private int BQAFDT;
	private int BQALDT;
	private short BQAFNB;
	private int BQANDT;
	private char BQC7ST;
	private char BQC6ST;
	private int BQAENB;
	private String BQANTX;
	private int BQEGNB;
	private int BQBRNB;
	@Id
	private String BQAGTX;
	@Id
	private String BQADNB;
	@Id
	private String BQASTX;
	private String BQARTX;
	private String BQABCD;
	private String BQAACD;
	private String BQAPNB;
	private String BQH2TX;
	private String BQBKCD;
	private String BQBJCD;
	private char BQBICD;
	private String BQH1TX;
	private char BQH0TX;
	private char BQHZTX;
	private char BQHYTX;
	private char BQHXTX;
	private String BQHWTX;
	private int BQDGNB;
	private int BQDFNB;
	private String BQDENB;
	private String BQDDNB;
	private String BQDCNB;
	private String BQDBNB;
	private char BQJCTX;
	private char BQJBTX;
	private String BQBSCD;
	private char BQBRCD;
	private char BQBQCD;
	private char BQBPCD;
	private char BQBOCD;
	private char BQBNCD;
	private char BQBMCD;
	private char BQBLCD;
	private int BQAEDT;
	private char BQJ1TX;
	private BigDecimal BQBUVA;
	private String BQM1TX;
	private char BQM2TX;
	private char BQM3TX;
	private BigDecimal BQB1VA;
	private BigDecimal BQB2VA;
	private BigDecimal BQAHVA;
	private BigDecimal BQAGVA;
	private int BQBLNB;
	private int BQBKNB;
	private BigDecimal BQAFPC;
	private BigDecimal BQAEPC;
	private String BQANCD;
	private String BQAMCD;
	private String BQALCD;
	private String BQAKCD;
	private char BQI5TX;
	private char BQI4TX;
	private char BQI3TX;
	private char BQI2TX;
	private char BQI1TX;
	private char BQI0TX;
	private char BQIZTX;
	private char BQIYTX;
	private char BQC2ST;
	private char BQC1ST;
	private char BQC0ST;
	private char BQCZST;
	private BigDecimal BQA9NB;
	private BigDecimal BQBJVA;
	private BigDecimal BQBIVA;
	private BigDecimal BQBHVA;
	private BigDecimal BQBGVA;
	private BigDecimal BQBFVA;
	private BigDecimal BQBEVA;
	private BigDecimal BQBDVA;
	private BigDecimal BQBCVA;
	private BigDecimal BQBBVA;
	
	private BigDecimal BQBAVA;
	private BigDecimal BQA9VA;
	private BigDecimal BQA8VA;
	private BigDecimal BQA7VA;
	private BigDecimal BQA6VA;
	private BigDecimal BQAXPC;
	private BigDecimal BQAWPC;
	private BigDecimal BQAVPC;
	private short BQDNNB;
	private short BQDMNB;
	private short BQDLNB;
	private int BQDKNB;
	private int BQDJNB;
	private int BQABNB;
	private int BQDINB;
	private int BQDHNB;

	public String getBqaacd() {
		return BQAACD;
	}

	public void setBqaacd(String bqaacd) {
		BQAACD=bqaacd;
	}

	public String getBqabcd() {
		return BQABCD;
	}

	public void setBqabcd(String bqabcd) {
		BQABCD=bqabcd;
	}

	public String getBqartx() {
		return BQARTX;
	}

	public void setBqartx(String bqartx) {
		BQARTX=bqartx;
	}

	public String getBqastx() {
		return BQASTX;
	}

	public void setBqastx(String bqastx) {
		BQASTX=bqastx;
	}

	public String getBqadnb() {
		return BQADNB;
	}

	public void setBqadnb(String bqadnb) {
		BQADNB=bqadnb;
	}

	public String getBqagtx() {
		return BQAGTX;
	}

	public void setBqagtx(String bqagtx) {
		BQAGTX=bqagtx;
	}

	public int getBqbrnb() {
		return BQBRNB;
	}

	public void setBqbrnb(int bqbrnb) {
		BQBRNB=bqbrnb;
	}

	public int getBqegnb() {
		return BQEGNB;
	}

	public void setBqegnb(int bqegnb) {
		BQEGNB=bqegnb;
	}

	public String getBqantx() {
		return BQANTX;
	}

	public void setBqantx(String bqantx) {
		BQANTX=bqantx;
	}

	public int getBqaenb() {
		return BQAENB;
	}

	public void setBqaenb(int bqaenb) {
		BQAENB=bqaenb;	
	}

	public char getBqc6st() {
		return BQC6ST;
	}

	public void setBqc6st(char bqc6st) {
		BQC6ST= bqc6st;
	}

	public char getBqc7st() {
		return BQC7ST;
	}

	public void setBqc7st(char bqc7st) {
		BQC7ST= bqc7st;
	}

	public int getBqandt() {
		return BQANDT;
	}

	public void setBqandt(int bqandt) {
		BQANDT=bqandt;
	}

	public short getBqafnb() {
		return BQAFNB;
	}

	public void setBqafnb(short bqafnb) {
		BQAFNB= bqafnb;
	}

	public int getBqaldt() {
		return BQALDT;
	}

	public void setBqaldt(int bqaldt) {
		BQALDT= bqaldt;
	}

	public int getBqafdt() {
		return BQAFDT;
	}

	public void setBqafdt(int bqafdt) {
		BQAFDT= bqafdt;
	}

	public String getBqtype0act() {
		return BQTYPE0ACT;
	}

	public void setBqtype0act(String bqtype0act) {
		BQTYPE0ACT= bqtype0act;
	}

	public String getBqbatx() {
		return BQBATX;
	}

	public void setBqbatx(String bqbatx) {
		BQBATX= bqbatx;
	}

	public char getBqdzst() {
		return BQDZST;
	}

	public void setBqdzst(char bqdzst) {
		BQDZST= bqdzst;
	}

	public char getBqd0st() {
		return BQD0ST;
	}

	public void setBqd0st(char bqd0st) {
		BQD0ST= bqd0st;
	}

	public char getBqd2st() {
		return BQD2ST;
	}

	public void setBqd2st(char bqd2st) {
		BQD2ST= bqd2st;
	}

	public char getBqd3st() {
		return BQD3ST;
	}

	public void setBqd3st(char bqd3st) {
		BQD3ST=bqd3st;
	}

	public BigDecimal getBqa3va() {
		return BQA3VA;
	}

	public void setBqa3va(BigDecimal bqa3va) {
		BQA3VA= bqa3va;
	}

	public BigDecimal getBqbrva() {
		return BQBRVA;
	}

	public void setBqbrva(BigDecimal bqbrva) {
		BQBRVA= bqbrva;
	}

	public BigDecimal getBqbsva() {
		return BQBSVA;
	}

	public void setBqbsva(BigDecimal bqbsva) {
		BQBSVA= bqbsva;
	}

	public BigDecimal getBqbtva() {
		return BQBTVA;
	}

	public void setBqbtva(BigDecimal bqbtva) {
		BQBTVA= bqbtva;
	}

	public BigDecimal getBqaupc() {
		return BQAUPC;
	}

	public void setBqaupc(BigDecimal bqaupc) {
		BQAUPC= bqaupc;
	}

	public String getBqaqcd() {
		return BQAQCD;
	}

	public void setBqaqcd(String bqaqcd) {
		BQAQCD= bqaqcd;
	}

	public String getBqarcd() {
		return BQARCD;
	}

	public void setBqarcd(String bqarcd) {
		BQARCD= bqarcd;
	}

	public String getBqascd() {
		return BQASCD;
	}

	public void setBqascd(String bqascd) {
		BQASCD= bqascd;
	}

	public String getBqeetx() {
		return BQEETX;
	}

	public void setBqeetx(String bqeetx) {
		BQEETX= bqeetx;
	}

	public String getBqbccd() {
		return BQBCCD;
	}

	public void setBqbccd(String bqbccd) {
		BQBCCD= bqbccd;
	}

	public String getBqagnb() {
		return BQAGNB;
	}

	public void setBqagnb(String bqagnb) {
		BQAGNB= bqagnb;
	}

	public BigDecimal getBqa5va() {
		return BQA5VA;
	}

	public void setBqa5va(BigDecimal bqa5va) {
		BQA5VA= bqa5va;
	}

	public String getBqhwtx() {
		return BQHWTX;
	}

	public void setBqhwtx(String bqhwtx) {
		BQHWTX= bqhwtx;
	}

	public char getBqhxtx() {
		return BQHXTX;
	}

	public void setBqhxtx(char bqhxtx) {
		BQHXTX= bqhxtx;
	}

	public char getBqhytx() {
		return BQHYTX;
	}

	public void setBqhytx(char bqhytx) {
		BQHYTX= bqhytx;
	}

	public char getBqhztx() {
		return BQHZTX;
	}

	public void setBqhztx(char bqhztx) {
		BQHZTX= bqhztx;
	}

	public char getBqh0tx() {
		return BQH0TX;
	}

	public void setBqh0tx(char bqh0tx) {
		BQH0TX= bqh0tx;
	}

	public String getBqh1tx() {
		return BQH1TX;
	}

	public void setBqh1tx(String bqh1tx) {
		BQH1TX= bqh1tx;
	}

	public char getBqbicd() {
		return BQBICD;
	}

	public void setBqbicd(char bqbicd) {
		BQBICD=bqbicd;
	}

	public String getBqbjcd() {
		return BQBJCD;
	}

	public void setBqbjcd(String bqbjcd) {
		BQBJCD= bqbjcd;
	}

	public String getBqbkcd() {
		return BQBKCD;
	}

	public void setBqbkcd(String bqbkcd) {
		BQBKCD= bqbkcd;
	}

	public String getBqh2tx() {
		return BQH2TX;
	}

	public void setBqh2tx(String bqh2tx) {
		BQH2TX= bqh2tx;
	}

	public String getBqapnb() {
		return BQAPNB;
	}

	public void setBqapnb(String bqapnb) {
		BQAPNB= bqapnb;
	}

	public char getBqblcd() {
		return BQBLCD;
	}

	public void setBqblcd(char bqblcd) {
		BQBLCD= bqblcd;
	}

	public char getBqbmcd() {
		return BQBMCD;
	}

	public void setBqbmcd(char bqbmcd) {
		BQBMCD= bqbmcd;
	}

	public char getBqbncd() {
		return BQBNCD;
	}

	public void setBqbncd(char bqbncd) {
		BQBNCD= bqbncd;
	}

	public char getBqbocd() {
		return BQBOCD;
	}

	public void setBqbocd(char bqbocd) {
		BQBOCD= bqbocd;
	}

	public char getBqbpcd() {
		return BQBPCD;
	}

	public void setBqbpcd(char bqbpcd) {
		BQBPCD= bqbpcd;
	}

	public char getBqbqcd() {
		return BQBQCD;
	}

	public void setBqbqcd(char bqbqcd) {
		BQBQCD= bqbqcd;
	}

	public char getBqbrcd() {
		return BQBRCD;
	}

	public void setBqbrcd(char bqbrcd) {
		BQBRCD= bqbrcd;
	}

	public String getBqbscd() {
		return BQBSCD;
	}

	public void setBqbscd(String bqbscd) {
		BQBSCD= bqbscd;
	}

	public char getBqjbtx() {
		return BQJBTX;
	}

	public void setBqjbtx(char bqjbtx) {
		BQJBTX= bqjbtx;
	}

	public char getBqjctx() {
		return BQJCTX;
	}

	public void setBqjctx(char bqjctx) {
		BQJCTX= bqjctx;
	}

	public String getBqdbnb() {
		return BQDBNB;
	}

	public void setBqdbnb(String bqdbnb) {
		BQDBNB= bqdbnb;
	}

	public String getBqdcnb() {
		return BQDCNB;
	}

	public void setBqdcnb(String bqdcnb) {
		BQDCNB= bqdcnb;
	}

	public String getBqddnb() {
		return BQDDNB;
	}

	public void setBqddnb(String bqddnb) {
		BQDDNB= bqddnb;
	}

	public String getBqdenb() {
		return BQDENB;
	}

	public void setBqdenb(String bqdenb) {
		BQDENB= bqdenb;
	}

	public int getBqdfnb() {
		return BQDFNB;
	}

	public void setBqdfnb(int bqdfnb) {
		BQDFNB= bqdfnb;
	}

	public int getBqdgnb() {
		return BQDGNB;
	}

	public void setBqdgnb(int bqdgnb) {
		BQDGNB= bqdgnb;
	}

	public int getBqdhnb() {
		return BQDHNB;
	}

	public void setBqdhnb(int bqdhnb) {
		BQDHNB= bqdhnb;
	}

	public int getBqdinb() {
		return BQDINB;
	}

	public void setBqdinb(int bqdinb) {
		BQDINB= bqdinb;
	}

	public int getBqabnb() {
		return BQABNB;
	}

	public void setBqabnb(int bqabnb) {
		BQABNB= bqabnb;
	}

	public int getBqdjnb() {
		return BQDJNB;
	}

	public void setBqdjnb(int bqdjnb) {
		BQDJNB= bqdjnb;
	}

	public int getBqdknb() {
		return BQDKNB;
	}

	public void setBqdknb(int bqdknb) {
		BQDKNB= bqdknb;
	}

	public short getBqdlnb() {
		return BQDLNB;
	}

	public void setBqdlnb(short bqdlnb) {
		BQDLNB= bqdlnb;
	}

	public short getBqdmnb() {
		return BQDMNB;
	}

	public void setBqdmnb(short bqdmnb) {
		BQDMNB= bqdmnb;
	}

	public short getBqdnnb() {
		return BQDNNB;
	}

	public void setBqdnnb(short bqdnnb) {
		BQDNNB= bqdnnb;
	}

	public BigDecimal getBqavpc() {
		return BQAVPC;
	}

	public void setBqavpc(BigDecimal bqavpc) {
		BQAVPC=bqavpc;
	}

	public BigDecimal getBqawpc() {
		return BQAWPC;
	}

	public void setBqawpc(BigDecimal bqawpc) {
		BQAWPC= bqawpc;
	}

	public BigDecimal getBqaxpc() {
		return BQAXPC;
	}

	public void setBqaxpc(BigDecimal bqaxpc) {
		BQAXPC= bqaxpc;
	}

	public BigDecimal getBqa6va() {
		return BQA6VA;
	}

	public void setBqa6va(BigDecimal bqa6va) {
		BQA6VA=bqa6va;
	}

	public BigDecimal getBqa7va() {
		return BQA7VA;
	}

	public void setBqa7va(BigDecimal bqa7va) {
		BQA7VA= bqa7va;
	}

	public BigDecimal getBqa8va() {
		return BQA8VA;
	}

	public void setBqa8va(BigDecimal bqa8va) {
		BQA8VA= bqa8va;
	}

	public BigDecimal getBqa9va() {
		return BQA9VA;
	}

	public void setBqa9va(BigDecimal bqa9va) {
		BQA9VA= bqa9va;
	}

	public BigDecimal getBqbava() {
		return BQBAVA;
	}

	public void setBqbava(BigDecimal bqbava) {
		BQBAVA= bqbava;
	}

	public BigDecimal getBqbbva() {
		return BQBBVA;
	}

	public void setBqbbva(BigDecimal bqbbva) {
		BQBBVA= bqbbva;
	}

	public BigDecimal getBqbcva() {
		return BQBCVA;
	}

	public void setBqbcva(BigDecimal bqbcva) {
		BQBCVA= bqbcva;
	}

	public BigDecimal getBqbdva() {
		return BQBDVA;
	}

	public void setBqbdva(BigDecimal bqbdva) {
		BQBDVA= bqbdva;
	}

	public BigDecimal getBqbeva() {
		return BQBEVA;
	}

	public void setBqbeva(BigDecimal bqbeva) {
		BQBEVA=bqbeva;
	}

	public BigDecimal getBqbfva() {
		return BQBFVA;
	}

	public void setBqbfva(BigDecimal bqbfva) {
		BQBFVA= bqbfva;
	}

	public BigDecimal getBqbgva() {
		return BQBGVA;
	}

	public void setBqbgva(BigDecimal bqbgva) {
		BQBGVA= bqbgva;
	}

	public BigDecimal getBqbhva() {
		return BQBHVA;
	}

	public void setBqbhva(BigDecimal bqbhva) {
		BQBHVA= bqbhva;
	}

	public BigDecimal getBqbiva() {
		return BQBIVA;
	}

	public void setBqbiva(BigDecimal bqbiva) {
		BQBIVA= bqbiva;
	}

	public BigDecimal getBqbjva() {
		return BQBJVA;
	}

	public void setBqbjva(BigDecimal bqbjva) {
		BQBJVA= bqbjva;
	}

	public BigDecimal getBqa9nb() {
		return BQA9NB;
	}

	public void setBqa9nb(BigDecimal bqa9nb) {
		BQA9NB= bqa9nb;
	}

	public char getBqczst() {
		return BQCZST;
	}

	public void setBqczst(char bqczst) {
		BQCZST= bqczst;
	}

	public char getBqc0st() {
		return BQC0ST;
	}

	public void setBqc0st(char bqc0st) {
		BQC0ST= bqc0st;
	}

	public char getBqc1st() {
		return BQC1ST;
	}

	public void setBqc1st(char bqc1st) {
		BQC1ST= bqc1st;
	}

	public char getBqc2st() {
		return BQC2ST;
	}

	public void setBqc2st(char bqc2st) {
		BQC2ST= bqc2st;
	}

	public char getBqiytx() {
		return BQIYTX;
	}

	public void setBqiytx(char bqiytx) {
		BQIYTX= bqiytx;
	}

	public char getBqiztx() {
		return BQIZTX;
	}

	public void setBqiztx(char bqiztx) {
		BQIZTX= bqiztx;
	}

	public char getBqi0tx() {
		return BQI0TX;
	}

	public void setBqi0tx(char bqi0tx) {
		BQI0TX= bqi0tx;
	}

	public char getBqi1tx() {
		return BQI1TX;
	}

	public void setBqi1tx(char bqi1tx) {
		BQI1TX= bqi1tx;
	}

	public char getBqi2tx() {
		return BQI2TX;
	}

	public void setBqi2tx(char bqi2tx) {
		BQI2TX= bqi2tx;
	}

	public char getBqi3tx() {
		return BQI3TX;
	}

	public void setBqi3tx(char bqi3tx) {
		BQI3TX= bqi3tx;
	}

	public char getBqi4tx() {
		return BQI4TX;
	}

	public void setBqi4tx(char bqi4tx) {
		BQI4TX= bqi4tx;
	}

	public char getBqi5tx() {
		return BQI5TX;
	}

	public void setBqi5tx(char bqi5tx) {
		BQI5TX= bqi5tx;
	}

	public String getBqakcd() {
		return BQAKCD;
	}

	public void setBqakcd(String bqakcd) {
		BQAKCD= bqakcd;
	}

	public String getBqalcd() {
		return BQALCD;
	}

	public void setBqalcd(String bqalcd) {
		BQALCD= bqalcd;
	}

	public String getBqamcd() {
		return BQAMCD;
	}

	public void setBqamcd(String bqamcd) {
		BQAMCD= bqamcd;
	}

	public String getBqancd() {
		return BQANCD;
	}

	public void setBqancd(String bqancd) {
		BQANCD= bqancd;
	}

	public BigDecimal getBqaepc() {
		return BQAEPC;
	}

	public void setBqaepc(BigDecimal bqaepc) {
		BQAEPC= bqaepc;
	}

	public BigDecimal getBqafpc() {
		return BQAFPC;
	}

	public void setBqafpc(BigDecimal bqafpc) {
		BQAFPC= bqafpc;
	}

	public int getBqbknb() {
		return BQBKNB;
	}

	public void setBqbknb(int bqbknb) {
		BQBKNB= bqbknb;
	}

	public int getBqblnb() {
		return BQBLNB;
	}

	public void setBqblnb(int bqblnb) {
		BQBLNB= bqblnb;
	}

	public BigDecimal getBqagva() {
		return BQAGVA;
	}

	public void setBqagva(BigDecimal bqagva) {
		BQAGVA= bqagva;
	}

	public BigDecimal getBqahva() {
		return BQAHVA;
	}

	public void setBqahva(BigDecimal bqahva) {
		BQAHVA= bqahva;
	}

	public char getBqj1tx() {
		return BQJ1TX;
	}

	public void setBqj1tx(char bqj1tx) {
		BQJ1TX= bqj1tx;
	}

	public int getBqaedt() {
		return BQAEDT;
	}

	public void setBqaedt(int bqaedt) {
		BQAEDT= bqaedt;
	}

	public BigDecimal getBqbuva() {
		return BQBUVA;
	}

	public void setBqbuva(BigDecimal bqbuva) {
		BQBUVA=bqbuva;
	}

	public String getBqm1tx() {
		return BQM1TX;
	}

	public void setBqm1tx(String bqm1tx) {
		BQM1TX= bqm1tx;
	}

	public char getBqm2tx() {
		return BQM2TX;
	}

	public void setBqm2tx(char bqm2tx) {
		BQM2TX= bqm2tx;
	}

	public char getBqm3tx() {
		return BQM3TX;
	}

	public void setBqm3tx(char bqm3tx) {
		BQM3TX= bqm3tx;
	}

	public BigDecimal getBqb1va() {
		return BQB1VA;
	}

	public void setBqb1va(BigDecimal bqb1va) {
		BQB1VA= bqb1va;
	}

	public BigDecimal getBqb2va() {
		return BQB2VA;
	}

	public void setBqb2va(BigDecimal bqb2va) {
		BQB2VA= bqb2va;
	}

	
	
}
