package com.csc.rest.model.CommlLine;

import java.util.ArrayList;
import java.util.List;

public class IMCPropertyInfo {
	String LocationRef;
	String SubLocRef;
	String TerritoryCd;
	String PMACd;
	String ClassCd;
	String ClassCdDesc;
	public List<CommlCov> CommlCoverage = new ArrayList<CommlCov>();

	public String getLocationRef() {
		return LocationRef;
	}

	public void setLocationRef(String locationRef) {
		LocationRef = locationRef;
	}

	public String getSubLocRef() {
		return SubLocRef;
	}

	public void setSubLocRef(String subLocRef) {
		SubLocRef = subLocRef;
	}

	public String getTerritoryCd() {
		return TerritoryCd;
	}

	public void setTerritoryCd(String territoryCd) {
		TerritoryCd = territoryCd;
	}

	public String getPMACd() {
		return PMACd;
	}

	public void setPMACd(String pMACd) {
		PMACd = pMACd;
	}

	public String getClassCd() {
		return ClassCd;
	}

	public void setClassCd(String classCd) {
		ClassCd = classCd;
	}

	public String getClassCdDesc() {
		return ClassCdDesc;
	}

	public void setClassCdDesc(String classCdDesc) {
		ClassCdDesc = classCdDesc;
	}

	public static class CommlCov {
		String SubjectInsuranceCd;
		String ClassCd;

		String CoverageCd;
		String IterationNumber;

		public String getSubjectInsuranceCd() {
			return SubjectInsuranceCd;
		}

		public void setSubjectInsuranceCd(String subjectInsuranceCd) {
			SubjectInsuranceCd = subjectInsuranceCd;
		}

		public String getClassCd() {
			return ClassCd;
		}

		public void setClassCd(String classCd) {
			ClassCd = classCd;
		}

		public String getIterationNumber() {
			return IterationNumber;
		}

		public void setIterationNumber(String iterationNumber) {
			IterationNumber = iterationNumber;
		}

		public String getCoverageCd() {
			return CoverageCd;
		}

		public void setCoverageCd(String coverageCd) {
			CoverageCd = coverageCd;
		}

	}
}
