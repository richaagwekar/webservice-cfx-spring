package com.csc.rest.model.CommlLine;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Spring;


import com.csc.rest.model.CommlLineBusiness;
import com.csc.rest.model.CommlLineBusiness.CommlInfo;
import com.csc.rest.model.PolicyDetails.ContractTerm;
import com.csc.rest.model.PolicyDetails.CreditScoreInfo;
import com.csc.rest.model.PolicyDetails.Location;

public class CommPolicy  {
	String LobType = "";
	public CommlPolicy commlPolicy;
	public List<Location> location;
	public InlandMarineLineBusiness inlandMarineLineBusiness;
	public CommlPropertyLineBusiness commlPropertyLineBusiness;
public CommPolicy()
{
	
}
	public CommPolicy(String type) {

		location = new ArrayList<Location>();
		if (type.equals("IMC")) {
			inlandMarineLineBusiness = new InlandMarineLineBusiness();

		}
		if (type.equals("CF")) {
			commlPropertyLineBusiness = new CommlPropertyLineBusiness();

		}

	}

	public CommlPolicy getCommlPolicy() {
		return commlPolicy;
	}

	public void setCommlPolicy(CommlPolicy commlPolicy) {
		this.commlPolicy = commlPolicy;
	}

	public static class CommlPolicy {

		String PolicyNumber;
		String LOBCd;
		String NAICCd;
		String ControllingStateProvCd;
		public ContractTerm contractTerm;

		String CurrentTermAmt;
		String OriginalInceptionDt;
		String RateEffectiveDt;
		public CreditScoreInfo creditScoreInfo;
		String PolicyTermCd;

		public ContractTerm getContractTerm() {
			return contractTerm;
		}

		public void setContractTerm(ContractTerm contractTerm) {
			this.contractTerm = contractTerm;
		}

		public String getCurrentTermAmt() {
			return CurrentTermAmt;
		}

		public void setCurrentTermAmt(String currentTermAmt) {
			CurrentTermAmt = currentTermAmt;
		}

		public String getOriginalInceptionDt() {
			return OriginalInceptionDt;
		}

		public void setOriginalInceptionDt(String originalInceptionDt) {
			OriginalInceptionDt = originalInceptionDt;
		}

		public String getRateEffectiveDt() {
			return RateEffectiveDt;
		}

		public void setRateEffectiveDt(String rateEffectiveDt) {
			RateEffectiveDt = rateEffectiveDt;
		}

		public String getPolicyTermCd() {
			return PolicyTermCd;
		}

		public void setPolicyTermCd(String policyTermCd) {
			PolicyTermCd = policyTermCd;
		}

		public String getPolicyNumber() {
			return PolicyNumber;
		}

		public void setPolicyNumber(String policyNumber) {
			PolicyNumber = policyNumber;
		}

		public String getControllingStateProvCd() {
			return ControllingStateProvCd;
		}

		public void setControllingStateProvCd(String controllingStateProvCd) {
			ControllingStateProvCd = controllingStateProvCd;
		}

		public CreditScoreInfo getCreditScoreInfo() {
			return creditScoreInfo;
		}

		public void setCreditScoreInfo(CreditScoreInfo creditScoreInfo) {
			this.creditScoreInfo = creditScoreInfo;
		}

		public String getLOBCd() {
			return LOBCd;
		}

		public void setLOBCd(String lOBCd) {
			LOBCd = lOBCd;
		}

		public String getNAICCd() {
			return NAICCd;
		}

		public void setNAICCd(String nAICCd) {
			NAICCd = nAICCd;
		}

	}

	public static void getObject(Object obj) {

	}

}
