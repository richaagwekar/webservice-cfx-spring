package com.csc.rest.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;




@XmlAccessorType(XmlAccessType.NONE)
public class PartialPolicy implements Serializable {
@XmlElement
	String CompanyProductCd ;
@XmlElement	
String LOBCd;
@XmlElement
	String NAICCd;
@XmlElement
	String ControllingStateProvCd;
@XmlElement
	String ContractTerm ;


	public String getLOBCd() {
		return LOBCd;
	}


	public void setLOBCd(String lOBCd) {
		LOBCd = lOBCd;
	}


	public String getNAICCd() {
		return NAICCd;
	}


	public void setNAICCd(String nAICCd) {
		NAICCd = nAICCd;
	}


	public String getControllingStateProvCd() {
		return ControllingStateProvCd;
	}


	public void setControllingStateProvCd(String controllingStateProvCd) {
		ControllingStateProvCd = controllingStateProvCd;
	}


	public String getContractTerm() {
		return ContractTerm;
	}


	public void setContractTerm(String contractTerm) {
		ContractTerm = contractTerm;
	}


	

	public String getCompanyProductCd() {
		return CompanyProductCd;
	}

	
	public void setCompanyProductCd(String companyProductCd) {
		CompanyProductCd = companyProductCd;
	}

	
}

