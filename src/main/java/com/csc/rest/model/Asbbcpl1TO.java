package com.csc.rest.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import accordfrmts.Boolean;

@Entity
@Table(name = "ASBBCPL1")
public class Asbbcpl1TO implements Serializable

{
	private static final long serialVersionUID = 1L;

	@Id
	private String BBASTX="";
	@Id
	private String BBAGTX="";
	@Id
	private String BBADNB="";
	private String BBA3VA="";
	private String BBARTX="";
	private String BBAACD="";
	private String BBABCD="";

	public BigDecimal getBBUSVA3() {
		return BBUSVA3;
	}
	public void setBBUSVA3(BigDecimal bBUSVA3) {
		BBUSVA3 = bBUSVA3;
	}
	private BigDecimal BBUSVA3 = BigDecimal.ZERO;

	
	public String getBBASTX() {
		return BBASTX;
	}
	public void setBBASTX(String bBASTX) {
		BBASTX = bBASTX;
	}
	public String getBBAGTX() {
		return BBAGTX;
	}
	public void setBBAGTX(String bBAGTX) {
		BBAGTX = bBAGTX;
	}
	public String getBBADNB() {
		return BBADNB;
	}
	public void setBBADNB(String bBADNB) {
		BBADNB = bBADNB;
	}
	public String getBBA3VA() {
		return BBA3VA;
	}
	public void setBBA3VA(String bBA3VA) {
		BBA3VA = bBA3VA;
	}
	public String getBBARTX() {
		return BBARTX;
	}
	public void setBBARTX(String bBARTX) {
		BBARTX = bBARTX;
	}
	public String getBBAACD() {
		return BBAACD;
	}
	public void setBBAACD(String bBAACD) {
		BBAACD = bBAACD;
	}
	public String getBBABCD() {
		return BBABCD;
	}
	public void setBBABCD(String bBABCD) {
		BBABCD = bBABCD;
	}
	private BigDecimal BBUSVA5 = BigDecimal.ZERO;

	

	

	private BigDecimal BBUSVA4 = BigDecimal.ZERO;



	private BigDecimal BBCXVA = BigDecimal.ZERO;

	private BigDecimal BBCYVA = BigDecimal.ZERO;

	private BigDecimal BBCZVA = BigDecimal.ZERO;

	private BigDecimal BBC0VA = BigDecimal.ZERO;

	private BigDecimal BBC1VA = BigDecimal.ZERO;

	private BigDecimal BBC2VA = BigDecimal.ZERO;
	
	private BigDecimal BBC3VA = BigDecimal.ZERO;

	public BigDecimal getBBUSVA5() {
		return BBUSVA5;
	}
	public void setBBUSVA5(BigDecimal bBUSVA5) {
		BBUSVA5 = bBUSVA5;
	}
	
	public BigDecimal getBBUSVA4() {
		return BBUSVA4;
	}
	public void setBBUSVA4(BigDecimal bBUSVA4) {
		BBUSVA4 = bBUSVA4;
	}
	public BigDecimal getBBCXVA() {
		return BBCXVA;
	}
	public void setBBCXVA(BigDecimal bBCXVA) {
		BBCXVA = bBCXVA;
	}
	public BigDecimal getBBCYVA() {
		return BBCYVA;
	}
	public void setBBCYVA(BigDecimal bBCYVA) {
		BBCYVA = bBCYVA;
	}
	public BigDecimal getBBCZVA() {
		return BBCZVA;
	}
	public void setBBCZVA(BigDecimal bBCZVA) {
		BBCZVA = bBCZVA;
	}
	public BigDecimal getBBC0VA() {
		return BBC0VA;
	}
	public void setBBC0VA(BigDecimal bBC0VA) {
		BBC0VA = bBC0VA;
	}
	public BigDecimal getBBC1VA() {
		return BBC1VA;
	}
	public void setBBC1VA(BigDecimal bBC1VA) {
		BBC1VA = bBC1VA;
	}
	public BigDecimal getBBC2VA() {
		return BBC2VA;
	}
	public void setBBC2VA(BigDecimal bBC2VA) {
		BBC2VA = bBC2VA;
	}
	public BigDecimal getBBC3VA() {
		return BBC3VA;
	}
	public void setBBC3VA(BigDecimal bBC3VA) {
		BBC3VA = bBC3VA;
	}

	private char BBC2ST;
	private char BBIYTX;

	private char BBC1ST;

	private int BBBLNB;

	public char getBBC2ST() {
		return BBC2ST;
	}
	public void setBBC2ST(char bBC2ST) {
		BBC2ST = bBC2ST;
	}
	public char getBBIYTX() {
		return BBIYTX;
	}
	public void setBBIYTX(char bBIYTX) {
		BBIYTX = bBIYTX;
	}
	public char getBBC1ST() {
		// TODO Auto-generated method stub
		return BBC1ST;
	}
	public void setBBC1ST(char bBC1ST) {
		BBC1ST = bBC1ST;
	}
	public int getBBBLNB() {
		// TODO Auto-generated method stub
		return BBBLNB;
	}


	
}
