package com.csc.rest.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ASBYCPL1")
public class Asbycpl1TO implements Serializable
{
private static final long serialVersionUID = 1L;
	
	@Id
	private String BYASTX="";
	@Id
	private int BYBRNB;
	@Id
	private String BYAGTX="";
	@Id
	
	private String BYARTX="";
	private String BYAACD="";
	private String BYABCD="";
	private String BYANTX="";
	
	private String BYADNB="";
	private String BYAGNB="";
	private String BYC0NB="";
	public String getBYC0NB() {
		return BYC0NB;
	}
	public void setBYC0NB(String bYC0NB) {
		BYC0NB = bYC0NB;
	}
	@Id
	private String BYAOTX="";

	private BigDecimal BYAGVA;

	private BigDecimal BBUSVA5;

	private BigDecimal BBUSVA3;

	private BigDecimal BYPOTX;

	private BigDecimal BYEGCD;

	private BigDecimal BYA9NB;

	private String BYPNTX;

	private BigDecimal BYUSVA5;

	private BigDecimal BYALV1VAL;

	private BigDecimal BYUSVA3;

	private BigDecimal BYA5VA;

	private BigDecimal BYALV0VAL;

	private String BYPPTX;

	private char BYIYTX;

	private char BYC0ST;
	
	
	public String getBYAOTX() {
		return BYAOTX;
	}
	public void setBYAOTX(String bYAOTX) {
		BYAOTX = bYAOTX;
	}
	public String getBYASTX() {
		return BYASTX;
	}
	public void setBYASTX(String bYASTX) {
		BYASTX = bYASTX;
	}
	public String getBYAGTX() {
		return BYAGTX;
	}
	public void setBYAGTX(String bYAGTX) {
		BYAGTX = bYAGTX;
	}
	public String getBYARTX() {
		return BYARTX;
	}
	public void setBYARTX(String bYARTX) {
		BYARTX = bYARTX;
	}
	public String getBYAACD() {
		return BYAACD;
	}
	public void setBYAACD(String bYAACD) {
		BYAACD = bYAACD;
	}
	public String getBYABCD() {
		return BYABCD;
	}
	public void setBYABCD(String bYABCD) {
		BYABCD = bYABCD;
	}
	public String getBYANTX() {
		return BYANTX;
	}
	public void setBYANTX(String bYANTX) {
		BYANTX = bYANTX;
	}
	public int getBYBRNB() {
		return BYBRNB;
	}
	public void setBYBRNB(int bYBRNB) {
		BYBRNB = bYBRNB;
	}
	public String getBYADNB() {
		return BYADNB;
	}
	public void setBYADNB(String bYADNB) {
		BYADNB = bYADNB;
	}
	public String getBYAGNB() {
		return BYAGNB;
	}
	public void setBYAGNB(String bYAGNB) {
		BYAGNB = bYAGNB;
	}
	public BigDecimal getBYAGVA() {
		// TODO Auto-generated method stub
		return BYAGVA;
	}
	public void setBYAGVA(BigDecimal bYAGVA) {
		BYAGVA = bYAGVA;
	}
	public BigDecimal getBBUSVA5() {
		// TODO Auto-generated method stub
		return BBUSVA5;
	}
	public BigDecimal getBBUSVA3() {
		// TODO Auto-generated method stub
		return BBUSVA3;
	}
	public BigDecimal getBYPOTX() {
		// TODO Auto-generated method stub
		return BYPOTX;
	}
	public BigDecimal getBYEGCD() {
		// TODO Auto-generated method stub
		return BYEGCD;
	}
	public BigDecimal getBYA9NB() {
		// TODO Auto-generated method stub
		return BYA9NB;
	}
	public void setBBUSVA5(BigDecimal bBUSVA5) {
		BBUSVA5 = bBUSVA5;
	}
	public void setBBUSVA3(BigDecimal bBUSVA3) {
		BBUSVA3 = bBUSVA3;
	}
	public void setBYPOTX(BigDecimal bYPOTX) {
		BYPOTX = bYPOTX;
	}
	public void setBYEGCD(BigDecimal bYEGCD) {
		BYEGCD = bYEGCD;
	}
	public void setBYA9NB(BigDecimal bYA9NB) {
		BYA9NB = bYA9NB;
	}
	public String getBYPNTX() {
		// TODO Auto-generated method stub
		return BYPNTX;
	}
	public void setBYPNTX(String bYPNTX) {
		BYPNTX = bYPNTX;
	}
	public BigDecimal getBYUSVA5() {
		// TODO Auto-generated method stub
		return BYUSVA5;
	}
	public BigDecimal getBYALV1VAL() {
		// TODO Auto-generated method stub
		return BYALV1VAL;
	}
	public BigDecimal getBYUSVA3() {
		// TODO Auto-generated method stub
		return BYUSVA3;
	}
	public BigDecimal getBYA5VA() {
		// TODO Auto-generated method stub
		return BYA5VA;
	}
	public BigDecimal getBYALV0VAL() {
		// TODO Auto-generated method stub
		return BYALV0VAL;
	}
	public String getBYPPTX() {
		// TODO Auto-generated method stub
		return BYPPTX;
	}
	public char getBYIYTX() {
		// TODO Auto-generated method stub
		return BYIYTX;
	}
	public char getBYC0ST() {
		// TODO Auto-generated method stub
		return BYC0ST;
	}
	
	
}
