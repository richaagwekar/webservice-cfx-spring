package com.csc.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;





@XmlAccessorType(XmlAccessType.NONE)
public class PersPolicy {
@XmlElement
	String policynum = "";
@XmlElement
	String symbol="";
@XmlElement
	String module="";
@XmlElement
	String masterco="";
@XmlElement
	String location="";

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getMasterco() {
		return masterco;
	}

	public void setMasterco(String masterco) {
		this.masterco = masterco;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	@XmlElement
	public ContractTerm contractTerm = new ContractTerm();

	public String getPolicynum() {
		return policynum;
	}

	public void setPolicynum(String policynum) {
		this.policynum = policynum;
	}
}
