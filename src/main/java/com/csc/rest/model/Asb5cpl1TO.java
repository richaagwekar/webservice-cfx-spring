package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import accordfrmts.PCJURISDICTION;

@Entity
@Table(name = "ASB5CPL1")
public class Asb5cpl1TO implements Serializable
{

private static final long serialVersionUID = 1L;
	
	@Id
	private String B5ASTX="";
	@Id
	private String B5AGTX="";
	@Id
	private String B5BRNB="";
	private String B5ARTX="";
	private String B5AACD="";
	private String B5ABCD="";
	private String B5ANTX="";
	private String B5EGNB="";
	private String B5H1TX="";
	public String getB5H1TX() {
		return B5H1TX;
	}
	public void setB5H1TX(String b5h1tx) {
		B5H1TX = b5h1tx;
	}
	private char B5HYTX;
	public char getB5HYTX() {
		return B5HYTX;
	}
	public void setB5HYTX(char b5hytx) {
		B5HYTX = b5hytx;
	}
	public String getB5EGNB() {
		return B5EGNB;
	}
	public void setB5EGNB(String b5egnb) {
		B5EGNB = b5egnb;
	}
	private String B5ADNB="";
	private String B5AGNB="";
	private String B5KWTX="";
	private String B5PTTX="";
	public String getB5KWTX() {
		return B5KWTX;
	}
	public void setB5KWTX(String b5kwtx) {
		B5KWTX = b5kwtx;
	}
	public String getB5PTTX() {
		return B5PTTX;
	}
	public void setB5PTTX(String b5pttx) {
		B5PTTX = b5pttx;
	}
	public String getB5KKTX() {
		return B5KKTX;
	}
	public void setB5KKTX(String b5kktx) {
		B5KKTX = b5kktx;
	}
	private String B5KKTX="";

	private char B5C6ST;

	private String B5BCCD;
	public String getB5AGNB() {
		return B5AGNB;
	}
	public void setB5AGNB(String b5agnb) {
		B5AGNB = b5agnb;
	}
	public String getB5ADNB() {
		return B5ADNB;
	}
	public void setB5ADNB(String b5adnb) {
		B5ADNB = b5adnb;
	}
	public String getB5BRNB() {
		return B5BRNB;
	}
	public void setB5BRNB(String b5brnb) {
		B5BRNB = b5brnb;
	}
	public String getB5ASTX() {
		return B5ASTX;
	}
	public void setB5ASTX(String b5astx) {
		B5ASTX = b5astx;
	}
	public String getB5AGTX() {
		return B5AGTX;
	}
	public void setB5AGTX(String b5agtx) {
		B5AGTX = b5agtx;
	}
	public String getB5ARTX() {
		return B5ARTX;
	}
	public void setB5ARTX(String b5artx) {
		B5ARTX = b5artx;
	}
	public String getB5AACD() {
		return B5AACD;
	}
	public void setB5AACD(String b5aacd) {
		B5AACD = b5aacd;
	}
	public String getB5ABCD() {
		return B5ABCD;
	}
	public void setB5ABCD(String b5abcd) {
		B5ABCD = b5abcd;
	}
	public String getB5ANTX() {
		return B5ANTX;
	}
	public void setB5ANTX(String b5antx) {
		B5ANTX = b5antx;
	}
	public char getB5C6ST() {
		// TODO Auto-generated method stub
		return B5C6ST;
	}
	public void setB5C6ST(char b5c6st) {
		B5C6ST = b5c6st;
	}
	public String getB5BCCD() {
		// TODO Auto-generated method stub
		return B5BCCD;
	}
	public void setB5BCCD(String b5bccd) {
		B5BCCD = b5bccd;
	}
	
	
}
