package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ASBUCPL1")
public class Asbucpl1TO implements Serializable 
{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String BUASTX="";
	
	private String BUARTX="";
	private String BUAACD="";
	private String BUABCD="";
	private String BUAPNB="";
	private String BUBRNB="";
	private String BUADNB="";
	private char BUC6ST;
	
	public char getBUC6ST() {
		return BUC6ST;
	}
	public void setBUC6ST(char bUC6ST) {
		BUC6ST = bUC6ST;
	}
	public String getBUADNB() {
		return BUADNB;
	}
	public void setBUADNB(String bUADNB) {
		BUADNB = bUADNB;
	}
	public String getBUBRNB() {
		return BUBRNB;
	}
	public void setBUBRNB(String bUBRNB) {
		BUBRNB = bUBRNB;
	}
	public String getBUAPNB() {
		return BUAPNB;
	}
	public void setBUAPNB(String bUAPNB) {
		BUAPNB = bUAPNB;
	}
	public String getBUASTX() {
		return BUASTX;
	}
	public void setBUASTX(String bUASTX) {
		BUASTX = bUASTX;
	}
	public String getBUARTX() {
		return BUARTX;
	}
	public void setBUARTX(String bUARTX) {
		BUARTX = bUARTX;
	}
	public String getBUAACD() {
		return BUAACD;
	}
	public void setBUAACD(String bUAACD) {
		BUAACD = bUAACD;
	}
	public String getBUABCD() {
		return BUABCD;
	}
	public void setBUABCD(String bUABCD) {
		BUABCD = bUABCD;
	}
	
	
}
