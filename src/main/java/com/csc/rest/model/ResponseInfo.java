package com.csc.rest.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class ResponseInfo {
@XmlElement
	public PersPolicy persPolicy;

	public ResponseInfo() {
		persPolicy = new PersPolicy();
	}
}
