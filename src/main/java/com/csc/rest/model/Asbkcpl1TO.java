package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ASBKCPL1")
public class Asbkcpl1TO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int BKAENB;
	@Id
	private String BKAOTX = "";
	
	
	
	private char BKC6ST;
	public char getBKC6ST() {
		return BKC6ST;
	}

	public void setBKC6ST(char bKC6ST) {
		BKC6ST = bKC6ST;
	}

	

	private String BKAACD = "";
	private String BKABCD = "";
	private String BKASTX = "";
	private String BKBBTX="";
	public String getBKBBTX() {
		return BKBBTX;
	}

	public void setBKBBTX(String bKBBTX) {
		BKBBTX = bKBBTX;
	}



	private String BKBRNB = "";

	
	private String BKARTX="";
	private String BKADNB="";
	
	private String BKA3VA;
	private String BKA9NB="";

	

	public String getBKA9NB() {
		return BKA9NB;
	}

	public void setBKA9NB(String bKA9NB) {
		BKA9NB = bKA9NB;
	}

	public String getBKA3VA() {
		return BKA3VA;
	}

	public void setBKA3VA(String bKA3VA) {
		BKA3VA = bKA3VA;
	}

	public int getBKAENB() {
		return BKAENB;
	}

	public void setBKAENB(int bKAENB) {
		BKAENB = bKAENB;
	}

	public String getBKADNB() {
		return BKADNB;
	}

	public void setBKADNB(String bKADNB) {
		BKADNB = bKADNB;
	}

	public String getBKARTX() {
		return BKARTX;
	}

	public void setBKARTX(String bKARTX) {
		BKARTX = bKARTX;
	}

	

	public String getBKASTX() {
		return BKASTX;
	}

	public void setBKASTX(String bKASTX) {
		BKASTX = bKASTX;
	}

	public String getBKAACD() {
		return BKAACD;
	}

	public void setBKAACD(String bKAACD) {
		BKAACD = bKAACD;
	}

	public String getBKABCD() {
		return BKABCD;
	}

	public void setBKABCD(String bKABCD) {
		BKABCD = bKABCD;
	}

	public String getBKBRNB() {
		return BKBRNB;
	}

	public void setBKBRNB(String bKBRNB) {
		BKBRNB = bKBRNB;
	}

	public String getBKAOTX() {
		return BKAOTX;
	}

	public void setBKAOTX(String bKAOTX) {
		BKAOTX = bKAOTX;
	}

}
