package com.csc.rest.model;

public class CommlLineBusiness {

	public Coverage coverage;
	public String LOBCd;
	public CommlInfo commlInfo = new CommlInfo();

	public void setLOBCd(String lOBCd) {
		LOBCd = lOBCd;
	}

	public Coverage getCoverage() {
		return coverage;
	}

	public void setCoverage(Coverage coverage) {
		this.coverage = coverage;
	}

	public class CommlInfo {
		String LocationRef;
		String SubLocationRef;
		String TerritoryCd;
		String PMACd;
		String ClassCd;
		String ClassCdDesc;
		public Coverage coverage = new Coverage();

		public String getLocationRef() {
			return LocationRef;
		}

		public void setLocationRef(String locationRef) {
			LocationRef = locationRef;
		}

		public String getSubLocationRef() {
			return SubLocationRef;
		}

		public void setSubLocationRef(String subLocationRef) {
			SubLocationRef = subLocationRef;
		}

		public String getTerritoryCd() {
			return TerritoryCd;
		}

		public void setTerritoryCd(String territoryCd) {
			TerritoryCd = territoryCd;
		}

		public String getPMACd() {
			return PMACd;
		}

		public void setPMACd(String pMACd) {
			PMACd = pMACd;
		}

		public String getClassCd() {
			return ClassCd;
		}

		public void setClassCd(String classCd) {
			ClassCd = classCd;
		}

		public String getClassCdDesc() {
			return ClassCdDesc;
		}

		public void setClassCdDesc(String classCdDesc) {
			ClassCdDesc = classCdDesc;
		}

	}
}
