package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ASBJCPL1")
public class Asbjcpl1TO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String BJAENB = "";
	
	private String BJASTX = "";
	private String BJAHNB = "";
	private String BJAZTX = "";
	private String BJAGNB = "";
	
	private String BJC5ST = "";
	private String BJAINB = "";
	private String BJAACD="";
	private String BJABCD="";
	private String BJARTX="";
	private String BJADNB="";
	private char BJC6ST;
	private char BJACST;
	public char getBJACST() {
		return BJACST;
	}

	public void setBJACST(char bJACST) {
		BJACST = bJACST;
	}

	public char getBJC6ST() {
		return BJC6ST;
	}

	public void setBJC6ST(char bJC6ST) {
		BJC6ST = bJC6ST;
	}

	public String getBJADNB() {
		return BJADNB;
	}

	public void setBJADNB(String bJADNB) {
		BJADNB = bJADNB;
	}

	public String getBJAACD() {
		return BJAACD;
	}

	public void setBJAACD(String bJAACD) {
		BJAACD = bJAACD;
	}

	public String getBJABCD() {
		return BJABCD;
	}

	public void setBJABCD(String bJABCD) {
		BJABCD = bJABCD;
	}

	public String getBJARTX() {
		return BJARTX;
	}

	public void setBJARTX(String bJARTX) {
		BJARTX = bJARTX;
	}

	public String getBJAINB() {
		return BJAINB;
	}

	public void setBJAINB(String bJAINB) {
		BJAINB = bJAINB;
	}

	public String getBJC5ST() {
		return BJC5ST;
	}

	public void setBJC5ST(String bJC5ST) {
		BJC5ST = bJC5ST;
	}

	public String getBJAENB() {
		return BJAENB;
	}

	public void setBJAENB(String bJAENB) {
		BJAENB = bJAENB;
	}

	public String getBJASTX() {
		return BJASTX;
	}

	public void setBJASTX(String bJASTX) {
		BJASTX = bJASTX;
	}

	public String getBJAHNB() {
		return BJAHNB;
	}

	public void setBJAHNB(String bJAHNB) {
		BJAHNB = bJAHNB;
	}

	public String getBJAZTX() {
		return BJAZTX;
	}

	public void setBJAZTX(String bJAZTX) {
		BJAZTX = bJAZTX;
	}

	public String getBJAGNB() {
		return BJAGNB;
	}

	public void setBJAGNB(String bJAGNB) {
		BJAGNB = bJAGNB;
	}

	public String getBJAJNB() {
		return BJAJNB;
	}

	public void setBJAJNB(String bJAJNB) {
		BJAJNB = bJAJNB;
	}

	private String BJAJNB = "";

}
