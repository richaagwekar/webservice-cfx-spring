package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ASBLCPL1")
public class Asblcpl1TO implements Serializable
{
private static final long serialVersionUID = 1L;
	
	@Id
	private String BLASTX="";
	private String BLA7ST="";
	private String BLAGDT="";
	private String BLA8ST="";
	private String BLBRNB="";
	private String BLAACD="";
	private String BLABCD="";
	private String BLARTX="";
	private String BLADNB="";
	private char BLC6ST;
	public String getBLADNB() {
		return BLADNB;
	}
	public char getBLC6ST() {
		return BLC6ST;
	}
	public void setBLC6ST(char bLC6ST) {
		BLC6ST = bLC6ST;
	}
	public void setBLADNB(String bLADNB) {
		BLADNB = bLADNB;
	}
	public String getBLARTX() {
		return BLARTX;
	}
	public void setBLARTX(String bLARTX) {
		BLARTX = bLARTX;
	}
	public String getBLAACD() {
		return BLAACD;
	}
	public void setBLAACD(String bLAACD) {
		BLAACD = bLAACD;
	}
	public String getBLABCD() {
		return BLABCD;
	}
	public void setBLABCD(String bLABCD) {
		BLABCD = bLABCD;
	}
	public String getBLBRNB() {
		return BLBRNB;
	}
	public void setBLBRNB(String bLBRNB) {
		BLBRNB = bLBRNB;
	}
	public String getBLASTX() {
		return BLASTX;
	}
	public void setBLASTX(String bLASTX) {
		BLASTX = bLASTX;
	}
	public String getBLA7ST() {
		return BLA7ST;
	}
	public void setBLA7ST(String bLA7ST) {
		BLA7ST = bLA7ST;
	}
	public String getBLAGDT() {
		return BLAGDT;
	}
	public void setBLAGDT(String bLAGDT) {
		BLAGDT = bLAGDT;
	}
	public String getBLA8ST() {
		return BLA8ST;
	}
	public void setBLA8ST(String bLA8ST) {
		BLA8ST = bLA8ST;
	}
}
