package com.csc.rest.model;

// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.


import java.io.Serializable;

public class Pmsp0200ID implements Serializable {
	private static final long serialVersionUID = -8604354225571274451L;

	private String LOCATION = "";
	private String POLICY0NUM = "";
	private String SYMBOL = "";
	private String MASTER0CO = "";
	private String MODULE = "";
	private char TRANS0STAT;

	public Pmsp0200ID()
	{
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((LOCATION == null) ? 0 : LOCATION.hashCode());
		result = prime * result
				+ ((MASTER0CO == null) ? 0 : MASTER0CO.hashCode());
		result = prime * result + ((MODULE == null) ? 0 : MODULE.hashCode());
		result = prime * result
				+ ((POLICY0NUM == null) ? 0 : POLICY0NUM.hashCode());
		result = prime * result + ((SYMBOL == null) ? 0 : SYMBOL.hashCode());
		result = prime * result + TRANS0STAT;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pmsp0200ID other = (Pmsp0200ID) obj;
		if (LOCATION == null) {
			if (other.LOCATION != null)
				return false;
		} else if (!LOCATION.equals(other.LOCATION))
			return false;
		if (MASTER0CO == null) {
			if (other.MASTER0CO != null)
				return false;
		} else if (!MASTER0CO.equals(other.MASTER0CO))
			return false;
		if (MODULE == null) {
			if (other.MODULE != null)
				return false;
		} else if (!MODULE.equals(other.MODULE))
			return false;
		if (POLICY0NUM == null) {
			if (other.POLICY0NUM != null)
				return false;
		} else if (!POLICY0NUM.equals(other.POLICY0NUM))
			return false;
		if (SYMBOL == null) {
			if (other.SYMBOL != null)
				return false;
		} else if (!SYMBOL.equals(other.SYMBOL))
			return false;
		if (TRANS0STAT != other.TRANS0STAT)
			return false;
		return true;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	public String getPOLICY0NUM() {
		return POLICY0NUM;
	}

	public void setPOLICY0NUM(String pOLICY0NUM) {
		POLICY0NUM = pOLICY0NUM;
	}

	public String getSYMBOL() {
		return SYMBOL;
	}

	public void setSYMBOL(String sYMBOL) {
		SYMBOL = sYMBOL;
	}

	public String getMASTER0CO() {
		return MASTER0CO;
	}

	public void setMASTER0CO(String mASTER0CO) {
		MASTER0CO = mASTER0CO;
	}

	public String getMODULE() {
		return MODULE;
	}

	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}

	public char getTRANS0STAT() {
		return TRANS0STAT;
	}

	public void setTRANS0STAT(char tRANS0STAT) {
		TRANS0STAT = tRANS0STAT;
	}

	
}
