package com.csc.rest.dao;

import java.util.List;
import java.util.Map;

import accordfrmts.PersAutoPolicyQuoteInqRq;

import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelRq.PolicySearchInquiry;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public interface DataDao {

	
	
	public Map GetAccord(Policy info) throws Exception;
	
	public List paSearch(PolicySearchInquiry search) throws Exception;
	
}
