package com.csc.rest.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import accordfrmts.PersAutoPolicyQuoteInqRq;

import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelRq.PolicySearchInquiry;
import com.csc.rest.model.Asb5cpl1TO;
import com.csc.rest.model.Asbbcpl1TO;
import com.csc.rest.model.Asbjcpl1TO;
import com.csc.rest.model.Asbkcpl1TO;
import com.csc.rest.model.Asblcpl1TO;
import com.csc.rest.model.Asbqcpl1TO;
import com.csc.rest.model.Asbucpl1TO;
import com.csc.rest.model.Asbycpl1TO;
import com.csc.rest.model.Pmsp0200TO;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public class DataDaoImpl implements DataDao {

	@Autowired
	SessionFactory sessionFactory ;

	Session session = null;
	Transaction tx = null;
	int loc, unit, cov;
	static final Logger logger = Logger.getLogger(DataDaoImpl.class);

	@SuppressWarnings("rawtypes")

	public List paSearch(PolicySearchInquiry search) throws Exception {

		logger.debug("Extracting Resultset...");
		int size = search.getCriteria().getMaxResultCount();
		String dt = search.getCriteria().getEffectiveDate();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);
		cr.setMaxResults(size);
if(search.getCriteria().getCompanyProductCd()!=null)
{
		cr.add(Restrictions.eq("SYMBOL",
				search.getCriteria().getCompanyProductCd()));
}
if(search.getCriteria().getNAICCd()!=null)
{
		cr.add(Restrictions.eq("MASTER0CO",
				search.getCriteria().getNAICCd()));
}
if(search.getCriteria().getControllingStateProvCd()!=null)
{
	/*//24052016 change| Start
		cr.add(Restrictions.eq("RISK0STATE",
				doMapStateCode(search.getCriteria().getControllingStateProvCd())));
		//24052016 change| End*/
	cr.add(Restrictions.eq("RISK0STATE",
			search.getCriteria().getControllingStateProvCd()));
}
if(search.getCriteria().getLOBCd()!=null)
		cr.add(Restrictions.eq("LOCATION",
				search.getCriteria().getLOBCd()));


		
		if(dt!=null)
		{
			//24052016 change| Start
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		
		/*Criterion cr1 =Restrictions.lt("EFF0YR", "1" + dt.substring(2, 4));

		Criterion cr2 =Restrictions.and(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)),
		Restrictions.lt("EFF0MO",  dt.substring(5, 7)));

		Criterion cr3 = Restrictions.and(Restrictions.and(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)),
		  Restrictions.eq("EFF0MO",  dt.substring(5, 7))),Restrictions.le("EFF0DA",  dt.substring(8, 10)));
		
		  
		 cr.add(Restrictions.or(Restrictions.or(cr1,cr2),cr3));
		//24052016 change| End*/
		}

		cr.add(Restrictions.eq("TRANS0STAT", 'V'));
		cr.add(Restrictions.ne("TYPE0ACT","CN"));
		@SuppressWarnings("unchecked")
		List resultList = (List<Pmsp0200TO>) cr.list();
		logger.debug("Returning resultset....");
		return resultList;

	}
	//24052016 change| Start
/*	String doMapStateCode(String STATECDKEY)
	{
		String STATEABBRV=null;
Query query = session.createSQLQuery(
				"select STATEABBRV from TBTS01 where STATECDKEY= :sck")
				.setParameter("sck",STATECDKEY);
				List result = query.list();
				STATEABBRV	=(String) result.get(0);
				return STATEABBRV;
	}*/
	//24052016 change| End
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	public List GetPolicyDetails(ResponseInfo responseInfo) throws Exception {

		List Resultset = new ArrayList<List>();
		logger.debug("Extracting Resultset...");
		String dt = responseInfo.persPolicy.contractTerm.getEffectiveDt();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);

		cr.add(Restrictions.eq("POLICY0NUM",
				responseInfo.persPolicy.getPolicynum()));
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		cr.add(Restrictions.eq("SYMBOL", responseInfo.persPolicy.getSymbol()));
		cr.add(Restrictions.eq("MODULE", responseInfo.persPolicy.getModule()));
		cr.add(Restrictions.eq("MASTER0CO",
				responseInfo.persPolicy.getMasterco()));
		cr.add(Restrictions.eq("LOCATION",
				responseInfo.persPolicy.getLocation()));

		List resultList = (List<Pmsp0200TO>) cr.list();

		logger.debug("Returning resultset....");
		Resultset.add(resultList);

		Criteria cr1 = session.createCriteria(Asbucpl1TO.class);
		cr1.add(Restrictions.eq("BUAACD", responseInfo.persPolicy.getModule()));
		cr1.add(Restrictions.eq("BUABCD", responseInfo.persPolicy.getMasterco()));
		cr1.add(Restrictions.eq("BUARTX", responseInfo.persPolicy.getSymbol()));
		cr1.add(Restrictions.eq("BUADNB", responseInfo.persPolicy.getModule()));
		cr1.add(Restrictions.eq("BUASTX",
				responseInfo.persPolicy.getPolicynum()));
		List resultList1 = (List<Asbucpl1TO>) cr1.list();
		loc = resultList1.size();
		Resultset.add(resultList1);

		if (responseInfo.persPolicy.getSymbol().equals("APV")) {
			Criteria cr2 = session.createCriteria(Asblcpl1TO.class);
			cr2.add(Restrictions.eq("BLAACD",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BLABCD",
					responseInfo.persPolicy.getMasterco()));
			cr2.add(Restrictions.eq("BLARTX",
					responseInfo.persPolicy.getSymbol()));
			cr2.add(Restrictions.eq("BLADNB",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BLASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);

			Criteria cr3 = session.createCriteria(Asbjcpl1TO.class);
			cr3.add(Restrictions.eq("BJAACD",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("BJABCD",
					responseInfo.persPolicy.getMasterco()));
			cr3.add(Restrictions.eq("BJARTX",
					responseInfo.persPolicy.getSymbol()));
			cr3.add(Restrictions.eq("BJADNB",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("BJASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList3 = (List<Asbjcpl1TO>) cr3.list();
			Resultset.add(resultList3);

			Criteria cr4 = session.createCriteria(Asbkcpl1TO.class);
			cr4.add(Restrictions.eq("BKAACD",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BKABCD",
					responseInfo.persPolicy.getMasterco()));
			cr4.add(Restrictions.eq("BKARTX",
					responseInfo.persPolicy.getSymbol()));
			cr4.add(Restrictions.eq("BKADNB",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BKASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList4 = (List<Asbkcpl1TO>) cr4.list();
			Resultset.add(resultList4);
		} 
		else if (responseInfo.persPolicy.getSymbol().equals("CPP"))
		
			{
			Criteria cr2 = session.createCriteria(Asbbcpl1TO.class);
			cr2.add(Restrictions.eq("BBAACD",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BBABCD",
					responseInfo.persPolicy.getMasterco()));
			cr2.add(Restrictions.eq("BBARTX",
					responseInfo.persPolicy.getSymbol()));
			cr2.add(Restrictions.eq("BBADNB",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BBASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);
			
			Criteria cr3 = session.createCriteria(Asb5cpl1TO.class);
			cr3.add(Restrictions.eq("B5AACD",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("B5ABCD",
					responseInfo.persPolicy.getMasterco()));
			cr3.add(Restrictions.eq("B5ARTX",
					responseInfo.persPolicy.getSymbol()));
			cr3.add(Restrictions.eq("B5ADNB",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("B5ASTX",
					responseInfo.persPolicy.getPolicynum()));
			
			List resultList3 = (List<Asbkcpl1TO>) cr3.list();
			Resultset.add(resultList3);
			
			Criteria cr4 = session.createCriteria(Asbycpl1TO.class);
			cr4.add(Restrictions.eq("BYAACD",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BYABCD",
					responseInfo.persPolicy.getMasterco()));
			cr4.add(Restrictions.eq("BYARTX",
					responseInfo.persPolicy.getSymbol()));
			cr4.add(Restrictions.eq("BYADNB",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BYASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList4 = (List<Asbjcpl1TO>) cr4.list();
			Resultset.add(resultList4);
			
			
			}
		return Resultset;

	}
	
	public List GetPolicyDetails(Policy responseInfo) throws Exception {

		List Resultset = new ArrayList<List>();
		logger.debug("Extracting Resultset...");
		String dt = responseInfo.getContractTerm().getEffectiveDt();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);

		cr.add(Restrictions.eq("POLICY0NUM",
				responseInfo.getPolicynum()));
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		cr.add(Restrictions.eq("SYMBOL", responseInfo.getSymbol()));
		cr.add(Restrictions.eq("MODULE", responseInfo.getModule()));
		cr.add(Restrictions.eq("MASTER0CO",
				responseInfo.getMasterco()));
		cr.add(Restrictions.eq("LOCATION",
				responseInfo.getLocation()));

		List resultList = (List<Pmsp0200TO>) cr.list();

		logger.debug("Returning resultset....");
		Resultset.add(resultList);

		Criteria cr1 = session.createCriteria(Asbucpl1TO.class);
		cr1.add(Restrictions.eq("BUAACD", responseInfo.getModule()));
		cr1.add(Restrictions.eq("BUABCD", responseInfo.getMasterco()));
		cr1.add(Restrictions.eq("BUARTX", responseInfo.getSymbol()));
		cr1.add(Restrictions.eq("BUADNB", responseInfo.getModule()));
		cr1.add(Restrictions.eq("BUASTX",
				responseInfo.getPolicynum()));
		List resultList1 = (List<Asbucpl1TO>) cr1.list();
		loc = resultList1.size();
		Resultset.add(resultList1);

		if (responseInfo.getSymbol().equals("APV")) {
			Criteria cr2 = session.createCriteria(Asblcpl1TO.class);
			cr2.add(Restrictions.eq("BLAACD",
					responseInfo.getModule()));
			cr2.add(Restrictions.eq("BLABCD",
					responseInfo.getMasterco()));
			cr2.add(Restrictions.eq("BLARTX",
					responseInfo.getSymbol()));
			cr2.add(Restrictions.eq("BLADNB",
					responseInfo.getModule()));
			cr2.add(Restrictions.eq("BLASTX",
					responseInfo.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);

			Criteria cr3 = session.createCriteria(Asbjcpl1TO.class);
			cr3.add(Restrictions.eq("BJAACD",
					responseInfo.getModule()));
			cr3.add(Restrictions.eq("BJABCD",
					responseInfo.getMasterco()));
			cr3.add(Restrictions.eq("BJARTX",
					responseInfo.getSymbol()));
			cr3.add(Restrictions.eq("BJADNB",
					responseInfo.getModule()));
			cr3.add(Restrictions.eq("BJASTX",
					responseInfo.getPolicynum()));
			List resultList3 = (List<Asbjcpl1TO>) cr3.list();
			Resultset.add(resultList3);

			Criteria cr4 = session.createCriteria(Asbkcpl1TO.class);
			cr4.add(Restrictions.eq("BKAACD",
					responseInfo.getModule()));
			cr4.add(Restrictions.eq("BKABCD",
					responseInfo.getMasterco()));
			cr4.add(Restrictions.eq("BKARTX",
					responseInfo.getSymbol()));
			cr4.add(Restrictions.eq("BKADNB",
					responseInfo.getModule()));
			cr4.add(Restrictions.eq("BKASTX",
					responseInfo.getPolicynum()));
			List resultList4 = (List<Asbkcpl1TO>) cr4.list();
			Resultset.add(resultList4);
		} 
		else if (responseInfo.getSymbol().equals("CPP"))
		
			{
			Criteria cr2 = session.createCriteria(Asbbcpl1TO.class);
			cr2.add(Restrictions.eq("BBAACD",
					responseInfo.getModule()));
			cr2.add(Restrictions.eq("BBABCD",
					responseInfo.getMasterco()));
			cr2.add(Restrictions.eq("BBARTX",
					responseInfo.getSymbol()));
			cr2.add(Restrictions.eq("BBADNB",
					responseInfo.getModule()));
			cr2.add(Restrictions.eq("BBASTX",
					responseInfo.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);
			
			Criteria cr3 = session.createCriteria(Asb5cpl1TO.class);
			cr3.add(Restrictions.eq("B5AACD",
					responseInfo.getModule()));
			cr3.add(Restrictions.eq("B5ABCD",
					responseInfo.getMasterco()));
			cr3.add(Restrictions.eq("B5ARTX",
					responseInfo.getSymbol()));
			cr3.add(Restrictions.eq("B5ADNB",
					responseInfo.getModule()));
			cr3.add(Restrictions.eq("B5ASTX",
					responseInfo.getPolicynum()));
			
			List resultList3 = (List<Asbkcpl1TO>) cr3.list();
			Resultset.add(resultList3);
			
			Criteria cr4 = session.createCriteria(Asbycpl1TO.class);
			cr4.add(Restrictions.eq("BYAACD",
					responseInfo.getModule()));
			cr4.add(Restrictions.eq("BYABCD",
					responseInfo.getMasterco()));
			cr4.add(Restrictions.eq("BYARTX",
					responseInfo.getSymbol()));
			cr4.add(Restrictions.eq("BYADNB",
					responseInfo.getModule()));
			cr4.add(Restrictions.eq("BYASTX",
					responseInfo.getPolicynum()));
			List resultList4 = (List<Asbjcpl1TO>) cr4.list();
			Resultset.add(resultList4);
			
			
			}
		return Resultset;

	}*/
	@Override
	public Map GetAccord(Policy info) throws Exception {
		// TODO Auto-generated method stub
		
		Map Resultmap = new HashMap();
		logger.debug("Extracting Resultset...");
		String dt = info.getContractTerm().getEffectiveDt();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);

		cr.add(Restrictions.eq("POLICY0NUM",
				info.getPolicynum()));
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		cr.add(Restrictions.eq("SYMBOL", info.getSymbol()));
		cr.add(Restrictions.eq("MODULE", info.getModule()));
		cr.add(Restrictions.eq("MASTER0CO",
				info.getMasterco()));
		cr.add(Restrictions.eq("LOCATION",
				info.getLocation()));
		cr.add(Restrictions.eq("TRANS0STAT",
				'V'));
		
		List resultList = (List<Pmsp0200TO>) cr.list();

		logger.debug("Returning resultset....");
		Resultmap.put("Pmsp0200TO",resultList);

		Criteria cr1 = session.createCriteria(Asbucpl1TO.class);
		cr1.add(Restrictions.eq("BUAACD", info.getLocation()));
		cr1.add(Restrictions.eq("BUABCD", info.getMasterco()));
		cr1.add(Restrictions.eq("BUARTX", info.getSymbol()));
		cr1.add(Restrictions.eq("BUADNB", info.getModule()));
		cr1.add(Restrictions.eq("BUASTX",
				info.getPolicynum()));
		cr1.add(Restrictions.eq("BUC6ST",
				'V'));
		
		List resultList1 = (List<Asbucpl1TO>) cr1.list();
		loc = resultList1.size();
		Resultmap.put("Asbucpl1TO",resultList1);

		if (info.getSymbol().equals("APV")) {
			Criteria cr2 = session.createCriteria(Asblcpl1TO.class);
			cr2.add(Restrictions.eq("BLAACD",
					info.getLocation()));
			cr2.add(Restrictions.eq("BLABCD",
					info.getMasterco()));
			cr2.add(Restrictions.eq("BLARTX",
					info.getSymbol()));
			cr2.add(Restrictions.eq("BLADNB",
					info.getModule()));
			cr2.add(Restrictions.eq("BLASTX",
					info.getPolicynum()));
			cr2.add(Restrictions.eq("BLC6ST",
					'V'));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultmap.put("Asblcpl1TO",resultList2);

			Criteria cr3 = session.createCriteria(Asbjcpl1TO.class);
			cr3.add(Restrictions.eq("BJAACD",
					info.getLocation()));
			cr3.add(Restrictions.eq("BJABCD",
					info.getMasterco()));
			cr3.add(Restrictions.eq("BJARTX",
					info.getSymbol()));
			cr3.add(Restrictions.eq("BJADNB",
					info.getModule()));
			cr3.add(Restrictions.eq("BJASTX",
					info.getPolicynum()));
			cr3.add(Restrictions.eq("BJC6ST",
					'V'));
			List resultList3 = (List<Asbjcpl1TO>) cr3.list();
			Resultmap.put("Asbjcpl1TO",resultList3);

			Criteria cr4 = session.createCriteria(Asbkcpl1TO.class);
			cr4.add(Restrictions.eq("BKAACD",
					info.getLocation()));
			cr4.add(Restrictions.eq("BKABCD",
					info.getMasterco()));
			cr4.add(Restrictions.eq("BKARTX",
					info.getSymbol()));
			cr4.add(Restrictions.eq("BKADNB",
					info.getModule()));
			cr4.add(Restrictions.eq("BKASTX",
					info.getPolicynum()));
			cr4.add(Restrictions.eq("BKC6ST",
					'V'));
			
			List resultList4 = (List<Asbkcpl1TO>) cr4.list();
			Resultmap.put("Asbkcpl1TO",resultList4);
		} 
		else if (info.getSymbol().equals("CPP"))
		
			{
			Criteria cr2 = session.createCriteria(Asbbcpl1TO.class);
			cr2.add(Restrictions.eq("BBAACD",
					info.getLocation()));
			cr2.add(Restrictions.eq("BBABCD",
					info.getMasterco()));
			cr2.add(Restrictions.eq("BBARTX",
					info.getSymbol()));
			cr2.add(Restrictions.eq("BBADNB",
					info.getModule()));
			cr2.add(Restrictions.eq("BBASTX",
					info.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultmap.put("Asbbcpl1TO",resultList2);
			
			Criteria cr3 = session.createCriteria(Asb5cpl1TO.class);
			cr3.add(Restrictions.eq("B5AACD",
					info.getLocation()));
			cr3.add(Restrictions.eq("B5ABCD",
					info.getMasterco()));
			cr3.add(Restrictions.eq("B5ARTX",
					info.getSymbol()));
			cr3.add(Restrictions.eq("B5ADNB",
					info.getModule()));
			cr3.add(Restrictions.eq("B5ASTX",
					info.getPolicynum()));
			
			List resultList3 = (List<Asbkcpl1TO>) cr3.list();
			Resultmap.put("Asb5cpl1TO",resultList3);
			
			Criteria cr4 = session.createCriteria(Asbycpl1TO.class);
			cr4.add(Restrictions.eq("BYAACD",
					info.getLocation()));
			cr4.add(Restrictions.eq("BYABCD",
					info.getMasterco()));
			cr4.add(Restrictions.eq("BYARTX",
					info.getSymbol()));
			cr4.add(Restrictions.eq("BYADNB",
					info.getModule()));
			cr4.add(Restrictions.eq("BYASTX",
					info.getPolicynum()));
			List resultList4 = (List<Asbjcpl1TO>) cr4.list();
			Resultmap.put("Asbycpl1TO",resultList4);
			
			
			}
		else if (info.getSymbol().equals("HP") || info.getSymbol().equals("FP") )
	    {
			
			Criteria cr2 = session.createCriteria(Asbqcpl1TO.class);
			cr2.add(Restrictions.eq("BQAACD",
					info.getLocation()));
			cr2.add(Restrictions.eq("BQABCD",
					info.getMasterco()));
			cr2.add(Restrictions.eq("BQARTX",
					info.getSymbol()));
			cr2.add(Restrictions.eq("BQADNB",
					info.getModule()));
			cr2.add(Restrictions.eq("BQASTX",
					info.getPolicynum()));
			List resultList2 = (List<Asbqcpl1TO>) cr2.list();
			Resultmap.put("Asbqcpl1TO",resultList2);

		/*	Criteria cr3 = session.createCriteria(ASBACPL1TO.class);
			cr3.add(Restrictions.eq("BJAACD",
					info.getModule()));
			cr3.add(Restrictions.eq("BJABCD",
					info.getMasterco()));
			cr3.add(Restrictions.eq("BJARTX",
					info.getSymbol()));
			cr3.add(Restrictions.eq("BJADNB",
					info.getModule()));
			cr3.add(Restrictions.eq("BJASTX",
					info.getPolicynum()));
			List resultList3 = (List<Asbjcpl1TO>) cr3.list();
			Resultmap.put("Asbjcpl1TO",resultList3);

			Criteria cr4 = session.createCriteria(BASPPD0100TO.class);
			cr4.add(Restrictions.eq("BKAACD",
					info.getModule()));
			cr4.add(Restrictions.eq("BKABCD",
					info.getMasterco()));
			cr4.add(Restrictions.eq("BKARTX",
					info.getSymbol()));
			cr4.add(Restrictions.eq("BKADNB",
					info.getModule()));
			cr4.add(Restrictions.eq("BKASTX",
					info.getPolicynum()));
			List resultList4 = (List<Asbkcpl1TO>) cr4.list();
			Resultmap.put("Asbkcpl1TO",resultList4);*/
		
		}
		return Resultmap;
		
	}
}
