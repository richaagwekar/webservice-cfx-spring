
package com.csc.rest.changedmodelRq;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicySearchInquiry implements Serializable {
@XmlElement
	protected Criteria criteria=new Criteria();

public Criteria getCriteria() {
	return criteria;
}

public void setCriteria(Criteria criteria) {
	this.criteria = criteria;
}


}
