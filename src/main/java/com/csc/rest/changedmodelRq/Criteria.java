package com.csc.rest.changedmodelRq;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;


@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class Criteria implements Serializable {

@XmlElement(name="CompanyProductCode")
protected	String CompanyProductCd ;
@XmlElement(name="LOBCode")	
protected String LOBCd;
@XmlElement
protected	String NAICCd;
@XmlElement(name="ControllingStateCode")
protected	String ControllingStateProvCd;
@XmlElement
protected	String EffectiveDate ;
@JsonProperty("MaxResultCount")
private Integer MaxResultCount;


	public String getEffectiveDate() {
	return EffectiveDate;
}


public void setEffectiveDate(String effectiveDate) {
	EffectiveDate = effectiveDate;
}





	public Integer getMaxResultCount() {
	return MaxResultCount;
}


public void setMaxResultCount(Integer maxResultCount) {
	MaxResultCount = maxResultCount;
}


	public String getLOBCd() {
		return LOBCd;
	}


	public void setLOBCd(String lOBCd) {
		LOBCd = lOBCd;
	}


	public String getNAICCd() {
		return NAICCd;
	}


	public void setNAICCd(String nAICCd) {
		NAICCd = nAICCd;
	}


	public String getControllingStateProvCd() {
		return ControllingStateProvCd;
	}


	public void setControllingStateProvCd(String controllingStateProvCd) {
		ControllingStateProvCd = controllingStateProvCd;
	}


	public String getCompanyProductCd() {
		return CompanyProductCd;
	}

	
	public void setCompanyProductCd(String companyProductCd) {
		CompanyProductCd = companyProductCd;
	}


}