package com.csc.rest.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;
import org.hibernate.cfg.CreateKeySecondPass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import accordfrmts.Addr;
import accordfrmts.AmountType;
import accordfrmts.AuditInfo;
import accordfrmts.AuditType;
import accordfrmts.BldgImprovements;
import accordfrmts.BldgProtection;
import accordfrmts.Boolean;
import accordfrmts.CURRENCY;
import accordfrmts.ComCscItemIdInfo;
import accordfrmts.ComCscOtherIdentifier;
import accordfrmts.CommlAutoHiredInfo;
import accordfrmts.CommlAutoLineBusiness;
import accordfrmts.CommlAutoNonOwnedInfo;
import accordfrmts.CommlAutoPolicyQuoteInqRq;
import accordfrmts.CommlCoverage;
import accordfrmts.CommlCoverageSupplement;
import accordfrmts.CommlIMInfo;
import accordfrmts.CommlIMPropertyInfo;
import accordfrmts.CommlInlandMarineLineBusiness;
import accordfrmts.CommlName;
import accordfrmts.CommlPkgPolicyQuoteInqRq;
import accordfrmts.CommlPolicy;
import accordfrmts.CommlRateState;
import accordfrmts.CommlSubLocation;
import accordfrmts.Communications;
import accordfrmts.Construction;
import accordfrmts.ContractTerm;
import accordfrmts.Coverage;
import accordfrmts.CoverageCategory;
import accordfrmts.CreditScoreInfo;
import accordfrmts.CurrentTermAmt;
import accordfrmts.Deductible;
import accordfrmts.DistanceToFireStation;
import accordfrmts.DistanceToHydrant;
import accordfrmts.DriverInfo;
import accordfrmts.Dwell;
import accordfrmts.DwellInspectionValuation;
import accordfrmts.DwellOccupancy;
import accordfrmts.DwellRating;
import accordfrmts.DwellUse;
import accordfrmts.FormatCurrencyAmt;
import accordfrmts.Frequency;
import accordfrmts.Gender;
import accordfrmts.GeneralLiabilityLineBusiness;
import accordfrmts.GeneralPartyInfo;
import accordfrmts.HomeLineBusiness;
import accordfrmts.HomePolicyQuoteInqRq;
import accordfrmts.InsuredOrPrincipal;
import accordfrmts.InsuredOrPrincipalInfo;
import accordfrmts.ItemIdInfo;
import accordfrmts.LiabilityInfo;
import accordfrmts.Limit;
import accordfrmts.MaritalStatus;
import accordfrmts.MarketValueAmt;
import accordfrmts.NameInfo;
import accordfrmts.NonOwnedInfo;
import accordfrmts.Option;
import accordfrmts.OtherIdentifier;
import accordfrmts.OtherOrPriorPolicy;
import accordfrmts.PCBASICWATERCRAFT;
import accordfrmts.PCCARRIERCODE;
import accordfrmts.PCCOVERAGE;
import accordfrmts.PCDEDUCTIBELTYPE;
import accordfrmts.PCDEDUCTIBLE;
import accordfrmts.PCDEDUCTIBLEAPPLIESTO;
import accordfrmts.PCISSUENATION;
import accordfrmts.PCJURISDICTION;
import accordfrmts.PCLIMITAPPLIESTO;
import accordfrmts.PCLINEOFBUSINESS;
import accordfrmts.PCPOLICYTERM;
import accordfrmts.PCVEHICLETYPE;
import accordfrmts.PCVEHICLEUSE;
import accordfrmts.PersApplicationInfo;
import accordfrmts.PersAutoLineBusiness;
import accordfrmts.PersAutoPolicyQuoteInqRq;
import accordfrmts.PersPolicy;
import accordfrmts.PersonInfo;
import accordfrmts.PersonName;
import accordfrmts.PhoneInfo;
import accordfrmts.Producer;
import accordfrmts.ProducerInfo;
import accordfrmts.PurchasePriceAmt;
import accordfrmts.RoofingMaterial;
import accordfrmts.SupplementaryNameInfo;
import accordfrmts.SwimmingPool;
import accordfrmts.TaxIdentity;
import accordfrmts.TotalArea;
import accordfrmts.VehiclePerformance;

import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelRq.PolicySearchInquiry;
import com.csc.rest.controller.RestController;
import com.csc.rest.dao.*;

import com.csc.rest.model.*;

import com.csc.rest.model.CommlLine.*;
import com.csc.rest.model.CommlLine.IMCPropertyInfo.CommlCov;
import com.csc.rest.model.Asb5cpl1TO;
import com.csc.rest.model.Asblcpl1TO;

import com.csc.rest.model.Asbbcpl1TO;
import com.csc.rest.model.Asbycpl1TO;
import com.csc.rest.model.CommlLineBusiness.CommlInfo;
import com.csc.rest.model.PersAutoLineBusiness.PersDriver;
import com.csc.rest.model.PersAutoLineBusiness.PersVeh;
import com.csc.rest.model.PolicyDetails.Location;

import com.csc.rest.util.CommonMethod;

public class DataServicesImpl implements DataServices {

	@Autowired
	DataDao dataDao;

	static final Logger logger = Logger.getLogger(DataDaoImpl.class);

	

	
	@Override
	public List<Policy> pASearch(PolicySearchInquiry search,String hostName,int port) throws Exception {
		List<Policy> persPolicyList = new ArrayList<Policy>();

		List ResultList = dataDao.paSearch(search);

		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();

		int size = search.getCriteria()
				.getMaxResultCount();

		/*if (ResultList.isEmpty()) {
			Policy policy = new Policy();
			policy.setPolicynum("No such Policy Exists");
			persPolicyList.add(policy);
			return persPolicyList;
		}*/
		if(size!=0)
		{
		for (Object rowResult : ResultList) {
			// Create persPolicy object and add to list
			pmsp0200TO = (Pmsp0200TO) rowResult;
			Policy policy = new Policy();
			policy.getContractTerm().setExpirationDt(CommonMethod
					.FormatDate(pmsp0200TO.getEXP0YR() + pmsp0200TO.getEXP0MO()
							+ pmsp0200TO.getEXP0DA()));
			policy.getContractTerm()
					.setEffectiveDt(CommonMethod
							.FormatDate(pmsp0200TO.getEFF0YR() + pmsp0200TO.getEFF0MO()
									+ pmsp0200TO.getEFF0DA()));
			policy.setPolicynum(pmsp0200TO.getPOLICY0NUM());
			policy.setSymbol(pmsp0200TO.getSYMBOL());
			policy.setModule(pmsp0200TO.getMODULE());
			policy.setMasterco(pmsp0200TO.getMASTER0CO());
			policy.setLocation(pmsp0200TO.getLOCATION());
			String params ="location="+policy.getLocation()+"&symbol="+policy.getSymbol()+"&contractterm.effectivedt="+policy.getContractTerm().getEffectiveDt()+"&contractterm.expirationdt="+policy.getContractTerm().getExpirationDt()+"&policynum="+policy.getPolicynum()+"&module="+policy.getModule()+"&masterco="+policy.getMasterco();
		
			
			policy.setHref("http://"+hostName+":"+port+"/webservice-cfx-spring/rest/PIJ/AccordPolDetail?"+params);
		
			persPolicyList.add(policy);
		}
		}
		return persPolicyList;

	}
	
	
	
	
	public PersAutoPolicyQuoteInqRq GetAccord(Policy info) throws Exception
	{
		
		PersAutoPolicyQuoteInqRq persAutoPolicyQuoteInqRq=null;
		Map<String,List> ResultMap = dataDao.GetAccord(info);
		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
		Asbucpl1TO asbucpl1TO = new Asbucpl1TO();
		Asblcpl1TO asblcpl1TO = new Asblcpl1TO();
		Asbjcpl1TO asbjcpl1TO = new Asbjcpl1TO();
		Asbkcpl1TO asbkcpl1TO = new Asbkcpl1TO();
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"/spring-config.xml");
		persAutoPolicyQuoteInqRq = (PersAutoPolicyQuoteInqRq) context
				.getBean("persAutoPolicyQuoteInqRq");
		
		
	List<Pmsp0200TO> pmsp0200=		ResultMap.get("Pmsp0200TO");
	List<Asbucpl1TO> Asbucpl1=		ResultMap.get("Asbucpl1TO");
	List<Asblcpl1TO> Asblcpl1=		ResultMap.get("Asblcpl1TO");
	List<Asbjcpl1TO> Asbjcpl1=		ResultMap.get("Asbjcpl1TO");
	List<Asbkcpl1TO> Asbkcpl1=		ResultMap.get("Asbkcpl1TO");
	

	
	
	
	DecimalFormat decimalFormat = new DecimalFormat();
	decimalFormat.setParseBigDecimal(true);

	
	pmsp0200TO=pmsp0200.get(0);

	//XMLGregorianCalendar value;	
	if(pmsp0200TO.getSYMBOL().equalsIgnoreCase("APV"))
	{
		accordfrmts.PersAutoLineBusiness palb=new PersAutoLineBusiness();
	persAutoPolicyQuoteInqRq.setTransactionEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
					.getEffectiveDt()));
	

	//persAuto
	PCLINEOFBUSINESS p=new PCLINEOFBUSINESS();
	p.setValue("APV");
	palb.setLOBCd(p);
	
	PCCARRIERCODE pccarriercode=new PCCARRIERCODE();
	pccarriercode.setValue(pmsp0200TO.getMASTER0CO());
	
	PCJURISDICTION pcjurisdiction=new PCJURISDICTION();
	pcjurisdiction.setValue(CommonMethod
					.ConvertNumericStateCd(pmsp0200TO
							.getRISK0STATE()));
	persAutoPolicyQuoteInqRq.setPersPolicy(new PersPolicy());
	persAutoPolicyQuoteInqRq.getPersPolicy().setPolicyNumber(pmsp0200TO
			.getPOLICY0NUM());
	persAutoPolicyQuoteInqRq.getPersPolicy().setLOBCd(p);
	persAutoPolicyQuoteInqRq.getPersPolicy().setCompanyProductCd(pmsp0200TO.getSYMBOL());
	persAutoPolicyQuoteInqRq.getPersPolicy().setNAICCd(pccarriercode);
	persAutoPolicyQuoteInqRq.getPersPolicy()
			.setControllingStateProvCd(pcjurisdiction);
	persAutoPolicyQuoteInqRq.getPersPolicy().setContractTerm(new ContractTerm());
	persAutoPolicyQuoteInqRq.getPersPolicy().getContractTerm()
			.setEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
					.getEffectiveDt()));
	
	persAutoPolicyQuoteInqRq.getPersPolicy().getContractTerm()
	.setExpirationDt(getXMLGregorianCalendar(info.getContractTerm()
			.getExpirationDt()));
	persAutoPolicyQuoteInqRq.getPersPolicy()
			.setOriginalInceptionDt(getXMLGregorianCalendar(info.getContractTerm()
					.getEffectiveDt()));
	persAutoPolicyQuoteInqRq.getPersPolicy()
			.setRateEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
					.getEffectiveDt()));
	
	PCPOLICYTERM pcpolicyterm=new PCPOLICYTERM();
	pcpolicyterm.setValue(pmsp0200TO
			.getINSTAL0TRM());
	persAutoPolicyQuoteInqRq.getPersPolicy().setPolicyTermCd(pcpolicyterm);
	persAutoPolicyQuoteInqRq.getPersPolicy().getOtherOrPriorPolicy().add(new OtherOrPriorPolicy());
	CreditScoreInfo csi=new CreditScoreInfo();
	csi.setCreditScore("");
	persAutoPolicyQuoteInqRq.getPersPolicy().getCreditScoreInfo().add(csi);
	//persAutoPolicyQuoteInqRq.getPersPolicy().setPolicyStatusCd();
	PersApplicationInfo persApplicationInfo=new PersApplicationInfo();
	persApplicationInfo.setApplicationReceivedByAgencyDt(getXMLGregorianCalendar(info.getContractTerm()
			.getEffectiveDt()));
	persAutoPolicyQuoteInqRq.getPersPolicy().setPersApplicationInfo(persApplicationInfo);
	
	ComCscItemIdInfo cciii=new ComCscItemIdInfo();
	//cciii.setInsurerId();
	persAutoPolicyQuoteInqRq.getPersPolicy().getComCscItemIdInfo().add(cciii);
	
	CurrentTermAmt currentTermAmt=new CurrentTermAmt();
	currentTermAmt.setAmt(pmsp0200TO.getTOT0AG0PRM());
	persAutoPolicyQuoteInqRq.getPersPolicy().setCurrentTermAmt(currentTermAmt);
	
	
/*	for(int i=0;i<Asbucpl1.size();i++)
	{
		asbucpl1TO=Asbucpl1.get(i);
	accordfrmts.Location sinloc = new accordfrmts.Location();
	sinloc.setId(asbucpl1TO.getBUBRNB());
	sinloc.setAddr(new Addr());
	sinloc.getAddr().setPostalCode(asbucpl1TO.getBUAPNB());
//	persAutoPolicyQuoteInqRq.getLocation().add(sinloc);
	
	}*/
	
	
	for(int k=0;k<Asblcpl1.size();k++)
	{String zeros="0";
	accordfrmts.PersDriver persDriver=new accordfrmts.PersDriver();
		asblcpl1TO=Asblcpl1.get(k);
		for(int m=2;m>Integer.toString(k).trim().length();m--)
			zeros=zeros+"0";
		persDriver.setId("D"+zeros+(k+1));
		persDriver.setDriverInfo(new DriverInfo());
		persDriver.getDriverInfo().setPersonInfo(new PersonInfo());
		Gender gender=new Gender();
		gender.setValue(asblcpl1TO.getBLA7ST());
	    persDriver.getDriverInfo().getPersonInfo().setGenderCd(gender);
		persDriver.getDriverInfo().getPersonInfo().setBirthDt(getXMLGregorianCalendarfrmtb(asblcpl1TO.getBLAGDT()));
		MaritalStatus ms=new MaritalStatus();
		ms.setValue(asblcpl1TO.getBLA8ST());
		
		persDriver.getDriverInfo().getPersonInfo().setMaritalStatusCd(ms);
		persDriver.getDriverInfo().getPersonInfo().setOccupationClassCd("");
		palb.getPersDriver().add(persDriver);
			
	}
	Map<String,accordfrmts.Location> ll=new HashMap<String, accordfrmts.Location>();
	//List<accordfrmts.Location> ll=new ArrayList<accordfrmts.Location>();
	for(int q=0;q<Asbjcpl1.size();q++)
	{
		asbjcpl1TO=Asbjcpl1.get(q);
	accordfrmts.Location l=new accordfrmts.Location();
	String zeros="0";
	for(int m=2;m>asbjcpl1TO.getBJAENB().trim().length();m--)
		zeros=zeros+"0";

	l.setId("L"+zeros+(asbjcpl1TO.getBJAENB()));
	l.setAddr(new Addr());
	//asbucpl1TO=Asbucpl1.get(q);
    l.getAddr().setPostalCode(asbjcpl1TO.getBJAENB());
    persAutoPolicyQuoteInqRq.getLocation().add(l);
	ll.put(asbjcpl1TO.getBJAENB(), l);
	}
	
	for(int i=0;i<Asbjcpl1.size();i++)
	{
asbjcpl1TO=Asbjcpl1.get(i);
palb.setLOBCd(p);
accordfrmts.PersVeh pv=new accordfrmts.PersVeh();

pv.setItemIdInfo(new ItemIdInfo());
pv.getItemIdInfo().setInsurerId(asbjcpl1TO.getBJAENB());
pv.setModelYear(asbjcpl1TO.getBJAHNB());
PCVEHICLETYPE pvtc= new PCVEHICLETYPE();
pvtc.setValue(asbjcpl1TO.getBJAZTX());
pv.setVehTypeCd(pvtc);
pv.setTerritoryCd(asbjcpl1TO.getBJAGNB());
pv.setVehSymbolCd(asbjcpl1TO.getBJAJNB());
pv.setVehSymbolCd(asbjcpl1TO.getBJAJNB());
//Will be changed
//pv.setExistingUnrepairedDamageInd(new Boolean(false));

VehiclePerformance vp=new VehiclePerformance();

vp.setValue(Character.toString(asbjcpl1TO.getBJACST()));

		
PCVEHICLEUSE pu=new PCVEHICLEUSE();
pu.setValue("P");
pv.setVehUseCd(pu);
pv.setVehPerformanceCd(vp);

//pv.setFullTermAmt((BigDecimal)decimalFormat.parse(asbjcpl1TO.getBJAINB()));

/*accordfrmts.Location l=new accordfrmts.Location();
String zeros="0";
for(int m=2;m>asbjcpl1TO.getBJAENB().trim().length();m--)
	zeros=zeros+"0";

l.setId("L"+zeros+( asbjcpl1TO.getBJAENB()));
l.setAddr(new Addr());
//l.getAddr().setPostalCode(asbjcpl1TO.getBJAENB());

for(int q=0;q<Asbucpl1.size();q++)
{
	asbucpl1TO=Asbucpl1.get(q);

if(asbucpl1TO.getBUBRNB().equals(asbjcpl1TO.getBJAENB()))
	l.getAddr().setPostalCode(asbucpl1TO.getBUAPNB());
}*/



for(int j=0;j<Asbkcpl1.size();j++)
{
	asbkcpl1TO=Asbkcpl1.get(j);
	if(asbkcpl1TO.getBKAENB() == Integer.parseInt(asbjcpl1TO.getBJAENB()))
	{
accordfrmts.Coverage cov= new accordfrmts.Coverage();
PCCOVERAGE pccov= new PCCOVERAGE();
pccov.setValue(asbkcpl1TO.getBKAOTX());
cov.setCoverageCd(pccov);
cov.setCoverageTypeCd("Coverage");
if(!asbkcpl1TO.getBKBBTX().trim().equals(""))
{
if(asbkcpl1TO.getBKBBTX().contains("/"))
{
	accordfrmts.Limit limit=new accordfrmts.Limit();
	limit.setFormatCurrencyAmt(new FormatCurrencyAmt());
	//limit.getFormatCurrencyAmt().setCURRENCY(new CURRENCY());
	String amt[]=asbkcpl1TO.getBKBBTX().split("/");
	limit.getFormatCurrencyAmt().setAmt((BigDecimal)decimalFormat.parse(amt[0]+"000"));
	
	cov.getLimit().add(limit);
	accordfrmts.Limit limit1=new accordfrmts.Limit();
	limit1.setFormatCurrencyAmt(new FormatCurrencyAmt());
//	limit1.getFormatCurrencyAmt().setCURRENCY(new CURRENCY());
	limit1.getFormatCurrencyAmt().setAmt((BigDecimal)decimalFormat.parse(amt[1]+"000"));
	cov.getLimit().add(limit1);
	
}
else
{
	accordfrmts.Limit limit=new accordfrmts.Limit();
	limit.setFormatCurrencyAmt(new FormatCurrencyAmt());
	//limit.getFormatCurrencyAmt().setCURRENCY(new CURRENCY());
	if(("PD").equals(cov.getCoverageCd().getValue()))
	limit.getFormatCurrencyAmt().setAmt((BigDecimal)decimalFormat.parse(asbkcpl1TO.getBKBBTX()+"000"));
	else
		limit.getFormatCurrencyAmt().setAmt((BigDecimal)decimalFormat.parse(asbkcpl1TO.getBKBBTX()));
	cov.getLimit().add(limit);
}
}

if(!asbkcpl1TO.getBKA9NB().trim().equals(""))
{
	Deductible deductible= new Deductible();
	//deductible.setPCDEDUCTIBLE(new PCDEDUCTIBLE());
	PCDEDUCTIBELTYPE pdt=new PCDEDUCTIBELTYPE();
	pdt.setValue("FL");
	deductible.setDeductibleTypeCd(pdt);
	deductible.setFormatCurrencyAmt(new FormatCurrencyAmt());
	//deductible.getFormatCurrencyAmt().setCURRENCY(new CURRENCY());
	deductible.getFormatCurrencyAmt().setAmt((BigDecimal)decimalFormat.parse(asbkcpl1TO.getBKA9NB()));
	
	cov.getDeductible().add(deductible);
}	
CurrentTermAmt currentTermAmtcov=new CurrentTermAmt();
currentTermAmtcov.setAmt((BigDecimal)decimalFormat.parse(asbkcpl1TO.getBKA3VA()));
		cov.setCurrentTermAmt(currentTermAmtcov);
		Option option=new Option();
		option.setOptionCd(Character.toString(asbkcpl1TO.getBKC6ST()));
		cov.getOption().add(option);
		pv.getCoverage().add(cov);
	
	String zeros="0";
		for(int m=2;m>asbjcpl1TO.getBJAENB().trim().length();m--)
			zeros=zeros+"0";
		pv.setId("V"+zeros+(asbjcpl1TO.getBJAENB()));
		for(int b=0;b<ll.size();b++)
		{
			//persAutoPolicyQuoteInqRq.getLocation().add(ll.get(b));
	   if(Integer.toString(b+1).equals(asbjcpl1TO.getBJAENB()))
	   {
		
		pv.setLocationRef(ll.get(Integer.toString(b+1)));
	   }
	   
		}
	}
	
	
}


palb.getPersVeh().add(pv);
//palb.getPersDriver().add(persDriver);
persAutoPolicyQuoteInqRq.setPersAutoLineBusiness(palb);

/*accordfrmts.Location l=new accordfrmts.Location();
l.setId("L00"+(i+1));
l.setAddr(new Addr());
l.getAddr().setPostalCode(asbjcpl1TO.getBJAENB());*/


	}
	}
	
	
	
return persAutoPolicyQuoteInqRq;
		
	}

	public HomePolicyQuoteInqRq GetAccordHO(Policy info) throws Exception
	{
		
		HomePolicyQuoteInqRq homePolicyQuoteInqRq=new HomePolicyQuoteInqRq();
		Map<String,List> ResultMap = dataDao.GetAccord(info);
		Asbqcpl1TO asbqcpl1TO = new Asbqcpl1TO();
		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
		Asbucpl1TO asbucpl1TO = new Asbucpl1TO();
		
		List<Pmsp0200TO> pmsp0200=		ResultMap.get("Pmsp0200TO");
		List<Asbqcpl1TO> Asbqcpl1=		ResultMap.get("Asbqcpl1TO");
		List<Asbucpl1TO> Asbucpl1=		ResultMap.get("Asbucpl1TO");
		pmsp0200TO=pmsp0200.get(0);

		//XMLGregorianCalendar value;	
		if(pmsp0200TO.getSYMBOL().equalsIgnoreCase("HP"))
		{
			accordfrmts.HomeLineBusiness hlb=new HomeLineBusiness();
			homePolicyQuoteInqRq.setTransactionEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
						.getEffectiveDt()));
		

		
		PCLINEOFBUSINESS p=new PCLINEOFBUSINESS();
		p.setValue("HP");
		hlb.setLOBCd(p);
		
		PCCARRIERCODE pccarriercode=new PCCARRIERCODE();
		pccarriercode.setValue(pmsp0200TO.getMASTER0CO());
		
		PCJURISDICTION pcjurisdiction=new PCJURISDICTION();
		pcjurisdiction.setValue(CommonMethod
						.ConvertNumericStateCd(pmsp0200TO
								.getRISK0STATE()));
		homePolicyQuoteInqRq.setPersPolicy(new PersPolicy());
		homePolicyQuoteInqRq.getPersPolicy().setId("policy");
		homePolicyQuoteInqRq.getPersPolicy().setPolicyNumber(pmsp0200TO
				.getPOLICY0NUM());
		homePolicyQuoteInqRq.getPersPolicy().setLOBCd(p);
		homePolicyQuoteInqRq.getPersPolicy().setCompanyProductCd(pmsp0200TO.getSYMBOL());
		homePolicyQuoteInqRq.getPersPolicy().setNAICCd(pccarriercode);
		homePolicyQuoteInqRq.getPersPolicy()
				.setControllingStateProvCd(pcjurisdiction);
		homePolicyQuoteInqRq.getPersPolicy().setContractTerm(new ContractTerm());
		homePolicyQuoteInqRq.getPersPolicy().getContractTerm()
				.setEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
						.getEffectiveDt()));
		
		homePolicyQuoteInqRq.getPersPolicy().getContractTerm()
		.setExpirationDt(getXMLGregorianCalendar(info.getContractTerm()
				.getExpirationDt()));
		homePolicyQuoteInqRq.getPersPolicy()
				.setOriginalInceptionDt(getXMLGregorianCalendar(info.getContractTerm()
						.getEffectiveDt()));
		homePolicyQuoteInqRq.getPersPolicy()
				.setRateEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
						.getEffectiveDt()));
		
		PCPOLICYTERM pcpolicyterm=new PCPOLICYTERM();
		pcpolicyterm.setValue(pmsp0200TO
				.getINSTAL0TRM());
		homePolicyQuoteInqRq.getPersPolicy().setPolicyTermCd(pcpolicyterm);
		homePolicyQuoteInqRq.getPersPolicy().getOtherOrPriorPolicy().add(new OtherOrPriorPolicy());
		CreditScoreInfo csi=new CreditScoreInfo();
		csi.setCreditScore("");
		homePolicyQuoteInqRq.getPersPolicy().getCreditScoreInfo().add(csi);
		//persAutoPolicyQuoteInqRq.getPersPolicy().setPolicyStatusCd();
		PersApplicationInfo persApplicationInfo=new PersApplicationInfo();
		persApplicationInfo.setApplicationReceivedByAgencyDt(getXMLGregorianCalendar(info.getContractTerm()
				.getEffectiveDt()));
		homePolicyQuoteInqRq.getPersPolicy().setPersApplicationInfo(persApplicationInfo);
		
		ComCscItemIdInfo cciii=new ComCscItemIdInfo();
		//cciii.setInsurerId();
		homePolicyQuoteInqRq.getPersPolicy().getComCscItemIdInfo().add(cciii);
		
		CurrentTermAmt currentTermAmt=new CurrentTermAmt();
		currentTermAmt.setAmt(pmsp0200TO.getTOT0AG0PRM());
		homePolicyQuoteInqRq.getPersPolicy().setCurrentTermAmt(currentTermAmt);
		

		InsuredOrPrincipal insuredOrPrincipal=new InsuredOrPrincipal();
		GeneralPartyInfo generalPartyInfo=new GeneralPartyInfo();
		NameInfo nameInfo=new NameInfo();
		PersonName personName=new PersonName();
		personName.setGivenName("");
		personName.setSurname("");
		personName.setTitlePrefix("");
		personName.setOtherGivenName("");
		personName.setNameSuffix("");
		
		nameInfo.setPersonName(personName);
		nameInfo.setComCscLongName(pmsp0200TO.getADD0LINE01());
		Addr addr=new Addr();
		addr.setAddrTypeCd("StreetAddress");
		addr.setAddr2(pmsp0200TO.getADD0LINE03());
		addr.setCity(pmsp0200TO.getADD0LINE04());
		PCJURISDICTION pcjurisdiction2=new PCJURISDICTION();
		pcjurisdiction2.setValue(pmsp0200TO.getADD0LINE04());
		addr.setStateProvCd(pcjurisdiction2);
		addr.setPostalCode(pmsp0200TO.getZIP0POST());
		addr.setCountry(null);
		addr.setCountryCd(null);
		
		Communications c =new Communications();
		PhoneInfo phoneInfo=new PhoneInfo();
		phoneInfo.setPhoneTypeCd("");
		phoneInfo.setPhoneNumber(null);
		phoneInfo.setCommunicationUseCd("");
		
		generalPartyInfo.setCommunications(c);
		generalPartyInfo.getAddr().add(addr);
		generalPartyInfo.getNameInfo().add(nameInfo);
		
		insuredOrPrincipal.setGeneralPartyInfo(generalPartyInfo);
		InsuredOrPrincipalInfo insuredOrPrincipalInfo=new InsuredOrPrincipalInfo();
		
		
	//	insuredOrPrincipalInfo.getContent().add()
	insuredOrPrincipal.setInsuredOrPrincipalInfo(insuredOrPrincipalInfo);
		
	Producer  producer=new Producer();
	ItemIdInfo itemIdInfo=new ItemIdInfo();
	itemIdInfo.setAgencyId(pmsp0200TO.getFILLR1()+pmsp0200TO.getRPT0AGT0NR()+pmsp0200TO.getFILLR2());
	producer.setItemIdInfo(itemIdInfo);
	
	NameInfo nameInfo2=new NameInfo();
	CommlName commlName=new CommlName();
	commlName.setCommercialName("");
	SupplementaryNameInfo sni=new SupplementaryNameInfo();
	sni.setSupplementaryName("");
	sni.setSupplementaryNameCd("");
	commlName.getSupplementaryNameInfo().add(sni);
	nameInfo2.setCommlName(commlName);
	nameInfo2.setLegalEntityCd("");
	TaxIdentity taxIdentity=new TaxIdentity();
	taxIdentity.setTaxIdTypeCd("");
	taxIdentity.setTaxId("");
	taxIdentity.setTaxIdSource("");
	taxIdentity.setStateProvCd(null);
	taxIdentity.setStateProv("");
	taxIdentity.setCountryCd(new PCISSUENATION());
	taxIdentity.setCountryCd(null);
	taxIdentity.setCounty(null);
	taxIdentity.setComCscEffectiveDt(null);
	taxIdentity.setComCscExpirationDt(null);
	ComCscOtherIdentifier comCscOtherIdentifier=new ComCscOtherIdentifier();
	comCscOtherIdentifier.setOtherId(null);
	comCscOtherIdentifier.setOtherIdTypeCd(null);
	taxIdentity.getComCscOtherIdentifier().add(comCscOtherIdentifier);
	taxIdentity.setComCscActionCd(null);
	nameInfo2.getTaxIdentity().add(taxIdentity);
	
	Addr addr1=new Addr();
	addr1.setAddrTypeCd("");
	addr1.setAddr2("");
	addr1.setCity("");
	PCJURISDICTION pcjurisdiction3=new PCJURISDICTION();
	pcjurisdiction3.setValue(pmsp0200TO.getADD0LINE04());
	addr1.setStateProvCd(pcjurisdiction3);
	addr1.setPostalCode("");
	addr1.setCountry(null);
	addr1.setCountryCd(null);
	
	Communications c1 =new Communications();
	PhoneInfo phoneInfo1=new PhoneInfo();
	phoneInfo1.setPhoneTypeCd(null);
	phoneInfo1.setPhoneNumber(null);
	phoneInfo1.setCommunicationUseCd("");
	
	ComCscItemIdInfo comCscItemIdInfo=new ComCscItemIdInfo();
	comCscItemIdInfo.setAgencyId("");
	comCscItemIdInfo.setInsurerId("");
	comCscItemIdInfo.setSystemId("");
	OtherIdentifier oi=new OtherIdentifier();
	oi.setOtherId("");
	oi.setOtherIdTypeCd("");
	comCscItemIdInfo.getOtherIdentifier().add(oi);
	
	
	
	
	GeneralPartyInfo genePartyInfo=new GeneralPartyInfo();
	genePartyInfo.getNameInfo().add(nameInfo2);
	genePartyInfo.setCommunications(c1);
	genePartyInfo.getAddr().add(addr1);
	genePartyInfo.setComCscItemIdInfo(comCscItemIdInfo);
	producer.setGeneralPartyInfo(genePartyInfo);
	ProducerInfo producerInfo=new ProducerInfo();
	producerInfo.setContractNumber("");
	producerInfo.setProducerSubCode("00");
	producer.setProducerInfo(producerInfo);
	
	
	
	homePolicyQuoteInqRq.getProducer().add(producer);
	homePolicyQuoteInqRq.getInsuredOrPrincipal().add(insuredOrPrincipal);
		for(int i=0;i<Asbqcpl1.size();i++)
		{
			asbqcpl1TO=Asbqcpl1.get(i);
		accordfrmts.Location sinloc = new accordfrmts.Location();
		
	String zeros="0";
		for(int k=2;k>Integer.toString(asbqcpl1TO.getBqaenb()).trim().length();k--)
			zeros=zeros+"0";
		
		sinloc.setId("D"+zeros+asbqcpl1TO.getBqaenb());
	
		sinloc.setItemIdInfo(new ItemIdInfo());
		sinloc.getItemIdInfo().setInsurerId("1");
		
		
		sinloc.setAddr(new Addr());
		
		sinloc.getAddr().setPostalCode(asbqcpl1TO.getBqapnb());
		sinloc.getAddr().setAddr1(asbqcpl1TO.getBqdcnb());
		sinloc.getAddr().setAddr2(asbqcpl1TO.getBqdenb());
		sinloc.getAddr().setCity(pmsp0200TO.getADD0LINE04());
		sinloc.getAddr().setStateProv(asbqcpl1TO.getBqbccd());
		sinloc.getAddr().setCountry(asbqcpl1TO.getBqh2tx());
		homePolicyQuoteInqRq.getLocation().add(sinloc);
		
		Dwell dwell=new Dwell();
		dwell.setPolicyTypeCd(Character.toString(asbqcpl1TO.getBqhxtx()));
		dwell.setBldgEffectivenessGradeTypeCd(asbqcpl1TO.getBqancd());
		dwell.setBldgCodeEffectivenessGradeCd(asbqcpl1TO.getBqbknb());
		dwell.setComCscNbrOfStories(asbqcpl1TO.getBqi1tx());
		dwell.setLocationRef(sinloc);
		dwell.setId("DW"+zeros+asbqcpl1TO.getBqaenb());
	dwell.setPurchaseDt(null);
		PurchasePriceAmt ppa=new PurchasePriceAmt();
		/*CURRENCY currency=new CURRENCY();
		currency.setAmt(asbqcpl1TO.getBqbuva());*/
		ppa.setAmt(asbqcpl1TO.getBqbuva());
	dwell.setPurchasePriceAmt(ppa);
	dwell.setFloorUnitLocated(null);
	dwell.setAreaTypeSurroundingsCd(asbqcpl1TO.getBqm1tx());	Construction construction=new Construction();
	
	construction.setConstructionCd(Character.toString(asbqcpl1TO.getBqhytx()));
	construction.setInsurerConstructionClassCd(Character.toString(asbqcpl1TO.getBqczst()));
	construction.setYearBuilt(String.valueOf(asbqcpl1TO.getBqdlnb()));	
	construction.setNumStories(new BigInteger("1"));
	construction.setRoofGeometryTypeCd(null);
	
	RoofingMaterial roofingMaterial=new RoofingMaterial();
	
	/*if(asbqcpl1TO.getBqamcd().substring(4, 6).equals(""))
	{
		roofingMaterial.setRoofMaterialCd(asbqcpl1TO.getBqdbnb());
	}
	else
	{
		roofingMaterial.setRoofMaterialCd(asbqcpl1TO.getBqamcd().substring(5, 7));
	}*/

	
	if(asbqcpl1TO.getBqamcd().length()<4 || asbqcpl1TO.getBqamcd().substring(4, 6).equals(""))
	{
		roofingMaterial.setRoofMaterialCd(asbqcpl1TO.getBqdbnb());
	}
	else
	{
		roofingMaterial.setRoofMaterialCd(asbqcpl1TO.getBqamcd().substring(5, 7));
	}
	
	construction.getRoofingMaterial().add(roofingMaterial);
	dwell.setConstruction(construction);
	
	DwellOccupancy dwellOccupancy=new DwellOccupancy();
	dwellOccupancy.setNumRooms(null);
	if(asbqcpl1TO.getBqbncd()=='N')
	{
	dwellOccupancy.setNumApartments("0");
	}
	else
	{
		dwellOccupancy.setNumApartments(Character.toString(asbqcpl1TO.getBqbncd()));
	}

	DwellUse dw=new DwellUse();
	dw.setValue(Character.toString(asbqcpl1TO.getBqiytx()));
	dwellOccupancy.setDwellUseCd(dw);
	dwellOccupancy.setOccupancyTypeCd(Character.toString(asbqcpl1TO.getBqh0tx()));
	dwell.setDwellOccupancy(dwellOccupancy);
	DwellRating dwellRating=new DwellRating();
	dwellRating.setTerritoryCd(asbqcpl1TO.getBqagnb());
    dwell.setDwellRating(dwellRating);

   
    
    
BldgProtection bldgProtection=new BldgProtection();
bldgProtection.setNumUnitsInFireDivisions(null);
bldgProtection.setFireProtectionClassCd(asbqcpl1TO.getBqh1tx());

DistanceToFireStation dfs=new DistanceToFireStation();
dfs.setNumUnits(Integer.toString(asbqcpl1TO.getBqdinb()));
dfs.setUnitMeasurementCd("MI");
bldgProtection.setDistanceToFireStation(dfs);

DistanceToHydrant dth=new DistanceToHydrant();
dth.setNumUnits(Integer.toString(asbqcpl1TO.getBqdhnb()));
dth.setUnitMeasurementCd("FT");

bldgProtection.setDistanceToHydrant(dth);
bldgProtection.setProtectionDeviceBurglarCd(Character.toString(asbqcpl1TO.getBqblcd()));
bldgProtection.setProtectionDeviceFireCd(Character.toString(asbqcpl1TO.getBqi3tx()));
bldgProtection.setProtectionDeviceSmokeCd(null);
bldgProtection.setProtectionDeviceSprinklerCd(Character.toString(asbqcpl1TO.getBqi4tx()));
	dwell.setBldgProtection(bldgProtection);
	
	dwell.setBldgImprovements(null);
	
	 DwellInspectionValuation dwellInspectionValuation=new DwellInspectionValuation();
	    dwellInspectionValuation.setEstimatedReplCostAmt(null);
	    dwellInspectionValuation.setHeatSourcePrimaryCd(null);
	    dwellInspectionValuation.setNumFamilies(new BigInteger("1"));
	    TotalArea ta=new TotalArea();
	    ta.setNumUnits(null);
	    ta.setUnitMeasurementCd("FT");
	    MarketValueAmt mva=new MarketValueAmt();
	    mva.setCURRENCY(null);
	    dwellInspectionValuation.setTotalArea(ta);
	    dwellInspectionValuation.setMarketValueAmt(mva);
	    
	    
	    
	    
	    SwimmingPool sp=new SwimmingPool();
	    sp.setAboveGroundInd(null);
	    sp.setApprovedFenceInd(null);
	    
	    dwell.setSwimmingPool(sp);
	    
	//Coverage DWELL
	accordfrmts.Coverage coverage=new Coverage();
	PCCOVERAGE pccoverage=new PCCOVERAGE();
	pccoverage.setValue("DWELL");
	coverage.setCoverageCd(pccoverage);
	Limit limit=new Limit();
	FormatCurrencyAmt amt=new FormatCurrencyAmt();
	amt.setAmt(asbqcpl1TO.getBqa6va());
    limit.setFormatCurrencyAmt(amt);
	coverage.getLimit().add(limit);
	
	Deductible deductible=new Deductible();
	FormatCurrencyAmt amt1=new FormatCurrencyAmt();
	amt1.setAmt(asbqcpl1TO.getBqa9nb());
	deductible.setFormatCurrencyAmt(amt1);
	PCDEDUCTIBELTYPE pcdeductibeltype=new PCDEDUCTIBELTYPE();
	pcdeductibeltype.setValue(Character.toString(asbqcpl1TO.getBqbrcd()).trim());
	deductible.setDeductibleTypeCd(pcdeductibeltype);
	PCDEDUCTIBLEAPPLIESTO pcdeductibleappliesto=new PCDEDUCTIBLEAPPLIESTO();
	pcdeductibleappliesto.setValue("AllPeril");
	deductible.setDeductibleAppliesToCd(pcdeductibleappliesto);
	coverage.getDeductible().add(deductible);
	
	CurrentTermAmt curreAmt=new CurrentTermAmt();
	curreAmt.setAmt(asbqcpl1TO.getBqa7va());
	coverage.setCurrentTermAmt(curreAmt);

	//Coverage OS
	
	accordfrmts.Coverage coverage1=new Coverage();
	PCCOVERAGE pccoverage1=new PCCOVERAGE();
	pccoverage1.setValue("OS");
	coverage1.setCoverageCd(pccoverage1);
	Limit limit1=new Limit();
	FormatCurrencyAmt amt2=new FormatCurrencyAmt();
	amt2.setAmt(asbqcpl1TO.getBqa8va());
    limit1.setFormatCurrencyAmt(amt2);
	coverage1.getLimit().add(limit1);
	
	
	CurrentTermAmt curreAmt1=new CurrentTermAmt();
	curreAmt1.setAmt(asbqcpl1TO.getBqa9va());
	coverage1.setCurrentTermAmt(curreAmt1);
	
	
	//Coverage PP
	
	accordfrmts.Coverage coverage2=new Coverage();
	PCCOVERAGE pccoverage2=new PCCOVERAGE();
	pccoverage2.setValue("PP");
	coverage2.setCoverageCd(pccoverage2);
	Limit limit2=new Limit();
	FormatCurrencyAmt amt3=new FormatCurrencyAmt();
	amt3.setAmt(asbqcpl1TO.getBqbava());
    limit2.setFormatCurrencyAmt(amt3);
	coverage2.getLimit().add(limit2);
	
	
	CurrentTermAmt curreAmt2=new CurrentTermAmt();
	curreAmt2.setAmt(asbqcpl1TO.getBqbbva());
	coverage2.setCurrentTermAmt(curreAmt2);
	
	//Coverage LOU
	
	accordfrmts.Coverage coverage3=new Coverage();
	PCCOVERAGE pccoverage3=new PCCOVERAGE();
	pccoverage3.setValue("LOU");
	coverage3.setCoverageCd(pccoverage3);
	Limit limit3=new Limit();
	FormatCurrencyAmt amt4=new FormatCurrencyAmt();
	amt4.setAmt(asbqcpl1TO.getBqbcva());
    limit3.setFormatCurrencyAmt(amt4);
	coverage3.getLimit().add(limit3);
	
	
	CurrentTermAmt curreAmt3=new CurrentTermAmt();
	curreAmt3.setAmt(asbqcpl1TO.getBqbdva());
	coverage3.setCurrentTermAmt(curreAmt3);
	
	//Coverage PL
	accordfrmts.Coverage coverage4=new Coverage();
	PCCOVERAGE pccoverage4=new PCCOVERAGE();
	pccoverage4.setValue("PL");
	coverage4.setCoverageCd(pccoverage4);
	Limit limit4=new Limit();
	FormatCurrencyAmt amt5=new FormatCurrencyAmt();
	amt5.setAmt(asbqcpl1TO.getBqbeva());
    limit4.setFormatCurrencyAmt(amt5);
	coverage4.getLimit().add(limit4);
	
	
	CurrentTermAmt curreAmt4=new CurrentTermAmt();
	curreAmt4.setAmt(asbqcpl1TO.getBqbfva());
	coverage4.setCurrentTermAmt(curreAmt4);
//	hlb.getDwell().add(dwell);
	
	//Coverage MEDPM
	accordfrmts.Coverage coverage5=new Coverage();
	PCCOVERAGE pccoverage5=new PCCOVERAGE();
	pccoverage5.setValue("MEDPM");
	coverage5.setCoverageCd(pccoverage5);
	Limit limit5=new Limit();
	FormatCurrencyAmt amt6=new FormatCurrencyAmt();
	amt6.setAmt(asbqcpl1TO.getBqbgva());
    limit5.setFormatCurrencyAmt(amt6);
	coverage5.getLimit().add(limit5);
	
	
	CurrentTermAmt curreAmt5=new CurrentTermAmt();
	curreAmt5.setAmt(asbqcpl1TO.getBqbhva());
	coverage5.setCurrentTermAmt(curreAmt5);
	dwell.getCoverage().add(coverage);
	dwell.getCoverage().add(coverage1);
	dwell.getCoverage().add(coverage2);
	dwell.getCoverage().add(coverage3);
	dwell.getCoverage().add(coverage4);
	dwell.getCoverage().add(coverage5);
	if(asbqcpl1TO.getBqhztx()=='Y')
	{
		accordfrmts.Coverage coverage6=new Coverage();
		PCCOVERAGE pccoverage6=new PCCOVERAGE();
		pccoverage6.setValue("FVREP");
		coverage6.setCoverageCd(pccoverage6);
		
		CurrentTermAmt curreAmt6=new CurrentTermAmt();
		curreAmt6.setAmt(null);
		coverage6.setCurrentTermAmt(curreAmt6);
		dwell.getCoverage().add(coverage6);
	}
	if(asbqcpl1TO.getBqawpc().compareTo(new BigDecimal('0'))>=1)
	{
		accordfrmts.Coverage coverage7=new Coverage();
		PCCOVERAGE pccoverage7=new PCCOVERAGE();
		pccoverage7.setValue("INFGD");
		coverage7.setCoverageCd(pccoverage7);
		Limit limit6=new Limit();
		limit6.setFormatPct(asbqcpl1TO.getBqawpc().doubleValue());
		coverage7.getLimit().add(limit6);
		
		
		CurrentTermAmt curreAmt7=new CurrentTermAmt();
		curreAmt7.setAmt(null);
		coverage7.setCurrentTermAmt(curreAmt7);
		dwell.getCoverage().add(coverage7);
	}
	if(asbqcpl1TO.getBqbpcd()=='Y')
	{
		accordfrmts.Coverage coverage6=new Coverage();
		PCCOVERAGE pccoverage6=new PCCOVERAGE();
		pccoverage6.setValue("WINDX");
		coverage6.setCoverageCd(pccoverage6);
		
		CurrentTermAmt curreAmt6=new CurrentTermAmt();
		curreAmt6.setAmt(null);
		coverage6.setCurrentTermAmt(curreAmt6);
		dwell.getCoverage().add(coverage6);
	}
	
		accordfrmts.Coverage coverage8=new Coverage();
		PCCOVERAGE pccoverage8=new PCCOVERAGE();
		pccoverage8.setValue("VALUP");
		coverage8.setCoverageCd(pccoverage8);
		
		CurrentTermAmt curreAmt8=new CurrentTermAmt();
		curreAmt8.setAmt(null);
		coverage8.setCurrentTermAmt(curreAmt8);
		dwell.getCoverage().add(coverage8);
	
	if(asbqcpl1TO.getBqm3tx()=='Y')
	{
		accordfrmts.Coverage coverage6=new Coverage();
		PCCOVERAGE pccoverage6=new PCCOVERAGE();
		pccoverage6.setValue("FLOOD");
		coverage6.setCoverageCd(pccoverage6);
		
		CurrentTermAmt curreAmt6=new CurrentTermAmt();
		curreAmt6.setAmt(null);
		coverage6.setCurrentTermAmt(curreAmt6);
		dwell.getCoverage().add(coverage6);
	}
	if(asbqcpl1TO.getBqc1st()=='Y')
	{
		accordfrmts.Coverage coverage6=new Coverage();
		PCCOVERAGE pccoverage6=new PCCOVERAGE();
		pccoverage6.setValue("BLDRK");
		coverage6.setCoverageCd(pccoverage6);
		
		CurrentTermAmt curreAmt6=new CurrentTermAmt();
		curreAmt6.setAmt(null);
		coverage6.setCurrentTermAmt(curreAmt6);
		dwell.getCoverage().add(coverage6);
	}
	
		accordfrmts.Coverage coverage9=new Coverage();
		PCCOVERAGE pccoverage9=new PCCOVERAGE();
		pccoverage9.setValue("HURR");
		coverage9.setCoverageCd(pccoverage9);
		
		Deductible deductible1=new Deductible();
		FormatCurrencyAmt amt10=new FormatCurrencyAmt();
		amt10.setAmt(asbqcpl1TO.getBqb1va());
		deductible1.setFormatCurrencyAmt(amt10);
		PCDEDUCTIBELTYPE pcdeductibeltype1=new PCDEDUCTIBELTYPE();
		pcdeductibeltype1.setValue(Character.toString(asbqcpl1TO.getBqbrcd()).trim());
		deductible1.setDeductibleTypeCd(pcdeductibeltype1);
		PCDEDUCTIBLEAPPLIESTO pcdeductibleappliesto1=new PCDEDUCTIBLEAPPLIESTO();
		pcdeductibleappliesto1.setValue("AllPeril");
		deductible1.setDeductibleAppliesToCd(pcdeductibleappliesto1);
		coverage9.getDeductible().add(deductible1);
		
		CurrentTermAmt curreAmt6=new CurrentTermAmt();
		curreAmt6.setAmt(asbqcpl1TO.getBqb2va());
		coverage9.setCurrentTermAmt(curreAmt6);
		dwell.getCoverage().add(coverage9);
	
		
	hlb.getDwell().add(dwell);
		
		
		}
		homePolicyQuoteInqRq.setHomeLineBusiness(hlb);
		}
		
		return homePolicyQuoteInqRq;
	}
	public CommlPkgPolicyQuoteInqRq GetAccordComml(Policy info) throws Exception 
	{
		
	
		Map<String,List> ResultMap = dataDao.GetAccord(info);
		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
		Asbucpl1TO asbucpl1TO = new Asbucpl1TO();
		Asbbcpl1TO asbbcpl1TO = new Asbbcpl1TO();
		Asb5cpl1TO asb5cpl1TO = new Asb5cpl1TO();
		Asbycpl1TO asbycpl1TO=new Asbycpl1TO();
		
		CommlPkgPolicyQuoteInqRq commlpkgPolicyQuoteInqRq=new CommlPkgPolicyQuoteInqRq();
		
	List<Pmsp0200TO> pmsp0200=		ResultMap.get("Pmsp0200TO");
	List<Asbucpl1TO> Asbucpl1=		ResultMap.get("Asbucpl1TO");
    List<Asbbcpl1TO> Asbbcpl1=		ResultMap.get("Asbbcpl1TO");
	List<Asb5cpl1TO> Asb5cpl1=		ResultMap.get("Asb5cpl1TO");
	List<Asbycpl1TO> Asbycpl1=		ResultMap.get("Asbycpl1TO");
	
	
	
	DecimalFormat decimalFormat = new DecimalFormat();
	decimalFormat.setParseBigDecimal(true);

	
	pmsp0200TO=pmsp0200.get(0);
	
	
			CommlAutoLineBusiness calb =new CommlAutoLineBusiness();
			
			PCLINEOFBUSINESS p=new PCLINEOFBUSINESS();
			if(pmsp0200TO.getLINE0BUS().equals("BO") || pmsp0200TO.getLINE0BUS().equals("BP"))
				p.setValue("BOP");
			else
			p.setValue(pmsp0200TO.getLINE0BUS());
			calb.setLOBCd(p);
			
			PCCARRIERCODE pccarriercode=new PCCARRIERCODE();
			pccarriercode.setValue(pmsp0200TO.getMASTER0CO());
			
			PCJURISDICTION pcjurisdiction=new PCJURISDICTION();
			pcjurisdiction.setValue(CommonMethod
							.ConvertNumericStateCd(pmsp0200TO
									.getRISK0STATE()));
			commlpkgPolicyQuoteInqRq.setCommlPolicy(new CommlPolicy());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setPolicyNumber(pmsp0200TO
					.getPOLICY0NUM());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setPolicyVersion(pmsp0200TO.getMODULE());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setPolicyStatusCd(pmsp0200TO.getTYPE0ACT());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setLOBCd(p);
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setCompanyProductCd(pmsp0200TO.getSYMBOL());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setNAICCd(pccarriercode);
			commlpkgPolicyQuoteInqRq.getCommlPolicy()
					.setControllingStateProvCd(pcjurisdiction);
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setContractTerm(new ContractTerm());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().getContractTerm()
					.setEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
							.getEffectiveDt()));
			
			commlpkgPolicyQuoteInqRq.getCommlPolicy().getContractTerm()
			.setExpirationDt(getXMLGregorianCalendar(info.getContractTerm()
					.getExpirationDt()));
			
			
			
			
		
			commlpkgPolicyQuoteInqRq.getCommlPolicy()
					.setRateEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
							.getEffectiveDt()));
			
			PCPOLICYTERM pcpolicyterm=new PCPOLICYTERM();
			pcpolicyterm.setValue(pmsp0200TO
					.getINSTAL0TRM());
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setPolicyTermCd(pcpolicyterm);
			commlpkgPolicyQuoteInqRq.getCommlPolicy().getOtherOrPriorPolicy().add(new OtherOrPriorPolicy());
			CreditScoreInfo csi=new CreditScoreInfo();
			csi.setCreditScore("");
			commlpkgPolicyQuoteInqRq.getCommlPolicy().getCreditScoreInfo().add(csi);
			commlpkgPolicyQuoteInqRq.getCommlPolicy().setCurrentTermAmt(pmsp0200TO.getTOT0AG0PRM());
			commlpkgPolicyQuoteInqRq.setTransactionEffectiveDt(getXMLGregorianCalendar(info.getContractTerm()
					.getEffectiveDt()));
			for(int i=0;i<Asbucpl1.size();i++)
			{
				asbucpl1TO=Asbucpl1.get(i);
			accordfrmts.Location sinloc = new accordfrmts.Location();
			sinloc.setId(asbucpl1TO.getBUBRNB());
			sinloc.setAddr(new Addr());
			sinloc.getAddr().setPostalCode(asbucpl1TO.getBUAPNB());
			commlpkgPolicyQuoteInqRq.getLocation().add(sinloc);
			
			}
			
			
			for(int i=0;i<Asbbcpl1.size();i++)
			{
				asbbcpl1TO=Asbbcpl1.get(i);
				if(asbbcpl1TO.getBBAGTX().trim().equals("IMC") || asbbcpl1TO.getBBAGTX().trim().equals("CF"))
				{
					for(int j=0;j<Asb5cpl1.size();j++)
					{
						asb5cpl1TO=Asb5cpl1.get(j);
					CommlSubLocation commlSubLocation=new CommlSubLocation();
					Locale locale = new Locale("000");
                    NumberFormat numberFormat = NumberFormat.getInstance(locale);
	                String number = numberFormat.format(asb5cpl1TO.getB5BRNB());
					commlSubLocation.setLocationRef("Loc"+number);
					 number = numberFormat.format(asb5cpl1TO.getB5EGNB());
					commlSubLocation.setSubLocationRef("SubLoc"+number);
					 number = numberFormat.format(asb5cpl1TO.getB5EGNB());
					commlSubLocation.setId("CommSubLoc"+number);
					
				
					Construction con=new Construction();
					con.setConstructionCd(String.valueOf(asb5cpl1TO.getB5HYTX()));
					commlSubLocation.setConstruction(con);
					
					if(asbbcpl1TO.getBBAGTX().trim().equals("CF"))
					{
					BldgProtection bldgProtection=new BldgProtection();
					bldgProtection.setFireProtectionClassCd(asb5cpl1TO.getB5H1TX());
					commlSubLocation.setBldgProtection(bldgProtection);
					}
					
					
					}
				}
				
			if(	asbbcpl1TO.getBBAGTX().trim().equals("GL"))
				{
				GeneralLiabilityLineBusiness generalLiabilityLineBusiness= new GeneralLiabilityLineBusiness();
				PCLINEOFBUSINESS pclineofbusiness=new PCLINEOFBUSINESS();
				pclineofbusiness.setValue("GL");
				generalLiabilityLineBusiness.setLOBCd(pclineofbusiness);
				 
				generalLiabilityLineBusiness.setCurrentTermAmt((BigDecimal)decimalFormat.parse(asbbcpl1TO.getBBA3VA()));
				
				
				LiabilityInfo liabilityInfo=new LiabilityInfo();
				
				CommlCoverage commlCoverage=new CommlCoverage();
	            PCCOVERAGE pccoverage=new PCCOVERAGE();
	            pccoverage.setValue("EAOCC");
				commlCoverage.setCoverageCd(pccoverage);
				commlCoverage.setCoverageDesc("");
				accordfrmts.Limit limit=new Limit();
				FormatCurrencyAmt formatCurrencyAmt=new FormatCurrencyAmt();
				formatCurrencyAmt.setAmt(asbbcpl1TO.getBBUSVA3());
				limit.setFormatCurrencyAmt(formatCurrencyAmt);
				commlCoverage.getLimit().add(limit);
				liabilityInfo.getCommlCoverage().add(commlCoverage);
				
				CommlCoverage commlCoverage1=new CommlCoverage();
	            PCCOVERAGE pccoverage1=new PCCOVERAGE();
	            pccoverage1.setValue("GENAG");
				commlCoverage1.setCoverageCd(pccoverage);
				commlCoverage1.setCoverageDesc("");
				accordfrmts.Limit limit1=new Limit();
				FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
				formatCurrencyAmt1.setAmt(asbbcpl1TO.getBBUSVA5());
				limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
				commlCoverage1.getLimit().add(limit1);
				liabilityInfo.getCommlCoverage().add(commlCoverage1);
				
			   //copy
				CommlCoverage commlCoverage2=new CommlCoverage();
	            PCCOVERAGE pccoverage2=new PCCOVERAGE();
	            pccoverage2.setValue("PIADV");
				commlCoverage2.setCoverageCd(pccoverage2);
				commlCoverage2.setCoverageDesc("");
				accordfrmts.Limit limit2=new Limit();
				FormatCurrencyAmt formatCurrencyAmt2=new FormatCurrencyAmt();
				formatCurrencyAmt2.setAmt(asbbcpl1TO.getBBC2VA());
				limit2.setFormatCurrencyAmt(formatCurrencyAmt2);
				commlCoverage2.getLimit().add(limit2);
				liabilityInfo.getCommlCoverage().add(commlCoverage2);
				
				CommlCoverage commlCoverage3=new CommlCoverage();
	            PCCOVERAGE pccoverage3=new PCCOVERAGE();
	            pccoverage3.setValue("PRDCO");
				commlCoverage3.setCoverageCd(pccoverage3);
				commlCoverage3.setCoverageDesc("");
				accordfrmts.Limit limit3=new Limit();
				FormatCurrencyAmt formatCurrencyAmt3=new FormatCurrencyAmt();
				formatCurrencyAmt3.setAmt(asbbcpl1TO.getBBUSVA4());
				limit3.setFormatCurrencyAmt(formatCurrencyAmt3);
				commlCoverage3.getLimit().add(limit3);
				liabilityInfo.getCommlCoverage().add(commlCoverage3);
				
				CommlCoverage commlCoverage4=new CommlCoverage();
	            PCCOVERAGE pccoverage4=new PCCOVERAGE();
	            pccoverage4.setValue("com.csc_LLOCC");
				commlCoverage4.setCoverageCd(pccoverage4);
				commlCoverage4.setCoverageDesc("");
				accordfrmts.Limit limit4=new Limit();
				FormatCurrencyAmt formatCurrencyAmt4=new FormatCurrencyAmt();
				formatCurrencyAmt4.setAmt(asbbcpl1TO.getBBCYVA());
				limit4.setFormatCurrencyAmt(formatCurrencyAmt4);
				commlCoverage4.getLimit().add(limit4);
				liabilityInfo.getCommlCoverage().add(commlCoverage4);
				
				CommlCoverage commlCoverage5=new CommlCoverage();
	            PCCOVERAGE pccoverage5=new PCCOVERAGE();
	            pccoverage5.setValue("com.csc_LLAGG");
				commlCoverage5.setCoverageCd(pccoverage5);
				commlCoverage5.setCoverageDesc("");
				accordfrmts.Limit limit5=new Limit();
				FormatCurrencyAmt formatCurrencyAmt5=new FormatCurrencyAmt();
				formatCurrencyAmt5.setAmt(asbbcpl1TO.getBBCZVA());
				limit5.setFormatCurrencyAmt(formatCurrencyAmt5);
				commlCoverage5.getLimit().add(limit1);
				liabilityInfo.getCommlCoverage().add(commlCoverage5);
				
				CommlCoverage commlCoverage6=new CommlCoverage();
	            PCCOVERAGE pccoverage6=new PCCOVERAGE();
	            pccoverage6.setValue("com.csc_EBLOCC");
				commlCoverage6.setCoverageCd(pccoverage6);
				commlCoverage6.setCoverageDesc("");
				accordfrmts.Limit limit6=new Limit();
				FormatCurrencyAmt formatCurrencyAmt6=new FormatCurrencyAmt();
				formatCurrencyAmt6.setAmt(asbbcpl1TO.getBBC0VA());
				limit6.setFormatCurrencyAmt(formatCurrencyAmt6);
				commlCoverage6.getLimit().add(limit6);
				liabilityInfo.getCommlCoverage().add(commlCoverage6);
				
				CommlCoverage commlCoverage7=new CommlCoverage();
	            PCCOVERAGE pccoverage7=new PCCOVERAGE();
	            pccoverage7.setValue("com.csc_EBLAGG");
				commlCoverage7.setCoverageCd(pccoverage7);
				commlCoverage7.setCoverageDesc("");
				accordfrmts.Limit limit7=new Limit();
				FormatCurrencyAmt formatCurrencyAmt7=new FormatCurrencyAmt();
				formatCurrencyAmt7.setAmt(asbbcpl1TO.getBBC1VA());
				limit7.setFormatCurrencyAmt(formatCurrencyAmt7);
				commlCoverage7.getLimit().add(limit7);
				liabilityInfo.getCommlCoverage().add(commlCoverage7);
				
				
				CommlCoverage commlCoverage8=new CommlCoverage();
	            PCCOVERAGE pccoverage8=new PCCOVERAGE();
	            pccoverage8.setValue("FIRDM");
				commlCoverage8.setCoverageCd(pccoverage8);
				commlCoverage8.setCoverageDesc("");
				accordfrmts.Limit limit8=new Limit();
				FormatCurrencyAmt formatCurrencyAmt8=new FormatCurrencyAmt();
				formatCurrencyAmt8.setAmt(asbbcpl1TO.getBBC3VA());
				limit8.setFormatCurrencyAmt(formatCurrencyAmt8);
				commlCoverage8.getLimit().add(limit8);
				liabilityInfo.getCommlCoverage().add(commlCoverage8);
				
				CommlCoverage commlCoverage9=new CommlCoverage();
	            PCCOVERAGE pccoverage9=new PCCOVERAGE();
	            pccoverage9.setValue("MEDEX");
				commlCoverage9.setCoverageCd(pccoverage9);
				commlCoverage9.setCoverageDesc("");
				accordfrmts.Limit limit9=new Limit();
				FormatCurrencyAmt formatCurrencyAmt9=new FormatCurrencyAmt();
				formatCurrencyAmt9.setAmt(asbbcpl1TO.getBBCXVA());
				limit9.setFormatCurrencyAmt(formatCurrencyAmt9);
				commlCoverage9.getLimit().add(limit9);
				liabilityInfo.getCommlCoverage().add(commlCoverage9);
				
				
				AuditInfo auditInfo=new AuditInfo();
				AuditType at=new AuditType();
				at.setValue(String.valueOf(asbbcpl1TO.getBBC2ST()));
				auditInfo.setAuditTypeCd(at);
				Frequency f= new Frequency();
				f.setValue(String.valueOf(asbbcpl1TO.getBBIYTX()));
				auditInfo.setAuditFrequencyCd(f);
	
				generalLiabilityLineBusiness.getAuditInfo().add(auditInfo);
		
				generalLiabilityLineBusiness.setLiabilityInfo(liabilityInfo);
				
			
				commlpkgPolicyQuoteInqRq.setGeneralLiabilityLineBusiness(generalLiabilityLineBusiness);
				
				}
				
			if(	asbbcpl1TO.getBBAGTX().trim().equals("IMC"))
			{
				CommlInlandMarineLineBusiness commlInlandMarineLineBusiness=new CommlInlandMarineLineBusiness();
				PCLINEOFBUSINESS pclineofbusiness=new PCLINEOFBUSINESS();
				pclineofbusiness.setValue("INMRC");
				commlInlandMarineLineBusiness.setLOBCd(pclineofbusiness);
				 
				commlInlandMarineLineBusiness.setCurrentTermAmt((BigDecimal)decimalFormat.parse(asbbcpl1TO.getBBA3VA()));
				
				CommlIMInfo commlIMInfo =new CommlIMInfo();
				CommlIMPropertyInfo commlIMPropertyInfo=new CommlIMPropertyInfo();
				/*commlIMPropertyInfo.setP
				
				
				LiabilityInfo liabilityInfo=new LiabilityInfo();
				
				CommlCoverage commlCoverage=new CommlCoverage();
	            PCCOVERAGE pccoverage=new PCCOVERAGE();
	            pccoverage.setValue("EAOCC");
				commlCoverage.setCoverageCd(pccoverage);
				commlCoverage.setCoverageDesc("");
				accordfrmts.Limit limit=new Limit();
				FormatCurrencyAmt formatCurrencyAmt=new FormatCurrencyAmt();
				formatCurrencyAmt.setAmt(asbbcpl1TO.getBBUSVA3());
				limit.setFormatCurrencyAmt(formatCurrencyAmt);
				commlCoverage.getLimit().add(limit);
				liabilityInfo.getCommlCoverage().add(commlCoverage);*/
				
				
					asb5cpl1TO=Asb5cpl1.get(0);
					commlIMPropertyInfo.setClassCd(asb5cpl1TO.getB5PTTX().trim());
				
					for(int k=0;k<Asbycpl1.size();k++)
					{
						asbycpl1TO=Asbycpl1.get(k);
						
						CommlCoverage commlCoverage=new CommlCoverage();
			            PCCOVERAGE pccoverage=new PCCOVERAGE();
			           
			            if(asbycpl1TO.getBYAOTX().trim().equals("BR"))
			            	pccoverage.setValue("BRWOR");
			            if(asbycpl1TO.getBYAOTX().trim().equals("COMMLA"))
			            	pccoverage.setValue("FINEA");
			            if(asbycpl1TO.getBYAOTX().trim().equals("COMMLC"))
			            	pccoverage.setValue("CAMRA");
			            if(asbycpl1TO.getBYAOTX().trim().equals("COMMLO"))
			            	pccoverage.setValue("com.csc_ORGAN");
			            if(asbycpl1TO.getBYAOTX().trim().equals("COMMLM"))
			            	pccoverage.setValue("MUSFL");
			            if(asbycpl1TO.getBYAOTX().trim().equals("EDP"))
			            	pccoverage.setValue("EDPEQ");
			            if(asbycpl1TO.getBYAOTX().trim().equals("EQPDO"))
			            	pccoverage.setValue("com.csc_EQDOP");
			            if(asbycpl1TO.getBYAOTX().trim().equals("EQPDP"))
			            	pccoverage.setValue("EQDFL");
			            if(asbycpl1TO.getBYAOTX().trim().equals("EQPDS"))
			            	pccoverage.setValue("com.csc_EQDS");
			            if(asbycpl1TO.getBYAOTX().trim().equals("EQUIP"))
			            	pccoverage.setValue("EQUFL");
			            if(asbycpl1TO.getBYAOTX().trim().equals("PHYSUR"))
			            	pccoverage.setValue("PHYS");
			            if(asbycpl1TO.getBYAOTX().trim().equals("PAPER"))
			            	pccoverage.setValue("VALPAP");
			            
			            if(asbycpl1TO.getBYAOTX().trim().equals("ACCTS"))
			            	pccoverage.setValue("ACCTS");
			            if(asbycpl1TO.getBYAOTX().trim().equals("BAILEE"))
			            	pccoverage.setValue("BAILEE");
			            if(asbycpl1TO.getBYAOTX().trim().equals("BOAT"))
			            	pccoverage.setValue("BOAT");
			            if(asbycpl1TO.getBYAOTX().trim().equals("RDOTV"))
			            	pccoverage.setValue("RDOTV");
			            if(asbycpl1TO.getBYAOTX().trim().equals("SIGN"))
			            	pccoverage.setValue("SIGN");
			            	  if(asbycpl1TO.getBYAOTX().trim().equals("THEATR"))
			            	pccoverage.setValue("THEATR");
			            else
			            	pccoverage.setValue("xxx");
			            commlCoverage.setCoverageCd(pccoverage);
			            commlCoverage.setCoverageDesc(null);
			            commlCoverage.setIterationNumber(asbycpl1TO.getBYC0NB());
			            
			            CommlCoverageSupplement commlCoverageSupplement=new CommlCoverageSupplement();
			            commlCoverageSupplement.setCoinsurancePct(new Integer(80).doubleValue());
			            commlCoverage.getCommlCoverageSupplement().add(commlCoverageSupplement);
					if(asbycpl1TO.getBYA9NB()!= new BigDecimal(0.00))
					{
						accordfrmts.Limit limit1=new Limit();
						FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
						formatCurrencyAmt1.setAmt(asbycpl1TO.getBYA9NB());
						limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
						PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
						pclimitappliesto.setValue("com.csc_SCH");
						limit1.setLimitAppliesToCd(pclimitappliesto);
						commlCoverage.getLimit().add(limit1);
		           }
					if(asbycpl1TO.getBYUSVA5()!= new BigDecimal(0.00))
					{
						accordfrmts.Limit limit1=new Limit();
						FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
						formatCurrencyAmt1.setAmt(asbycpl1TO.getBYUSVA5());
						limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
						PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
						pclimitappliesto.setValue("com.csc_UNSCH");
						limit1.setLimitAppliesToCd(pclimitappliesto);
						commlCoverage.getLimit().add(limit1);
		           }
					if(asbycpl1TO.getBYAGVA()!= new BigDecimal(0.00))
					{
						accordfrmts.Limit limit1=new Limit();
						FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
						formatCurrencyAmt1.setAmt(asbycpl1TO.getBYAGVA());
						limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
						PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
						pclimitappliesto.setValue("PREM");
						limit1.setLimitAppliesToCd(pclimitappliesto);
						commlCoverage.getLimit().add(limit1);
		           }
					if(commlCoverage.getCoverageCd().getValue().trim().equals("TRANS"))
					{
						if(asbycpl1TO.getBYALV1VAL()!= new BigDecimal(0.00))
						{
							accordfrmts.Limit limit1=new Limit();
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(asbycpl1TO.getBYALV1VAL());
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
							PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
							pclimitappliesto.setValue(null);
							limit1.setLimitAppliesToCd(pclimitappliesto);
							commlCoverage.getLimit().add(limit1);
			           }
					}
					if(commlCoverage.getCoverageCd().getValue().trim().equals("TRIP"))
					{
						
						if(asbycpl1TO.getBYUSVA3()!= new BigDecimal(0.00))
						{
							accordfrmts.Limit limit1=new Limit();
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(asbycpl1TO.getBYUSVA3());
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
							PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
							pclimitappliesto.setValue(null);
							limit1.setLimitAppliesToCd(pclimitappliesto);
							commlCoverage.getLimit().add(limit1);
			           }
					}
					
						if(asbycpl1TO.getBYA5VA()!= new BigDecimal(0.00))
						{
							accordfrmts.Limit limit1=new Limit();
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(asbycpl1TO.getBYA5VA());
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
							PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
							pclimitappliesto.setValue(null);
							limit1.setLimitAppliesToCd(pclimitappliesto);
							commlCoverage.getLimit().add(limit1);
			           }	
					
			
						if(asbycpl1TO.getBYALV0VAL()!= new BigDecimal(0.00))
						{
							accordfrmts.Limit limit1=new Limit();
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(asbycpl1TO.getBYALV0VAL());
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
							PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
							pclimitappliesto.setValue("OFF");
							limit1.setLimitAppliesToCd(pclimitappliesto);
							commlCoverage.getLimit().add(limit1);
			           }
						if(asbycpl1TO.getBYALV1VAL()!= new BigDecimal(0.00))
						{
							accordfrmts.Limit limit1=new Limit();
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(asbycpl1TO.getBYALV1VAL());
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
							PCLIMITAPPLIESTO pclimitappliesto=new PCLIMITAPPLIESTO();
							pclimitappliesto.setValue("OFF");
							limit1.setLimitAppliesToCd(pclimitappliesto);
							commlCoverage.getLimit().add(limit1);
			           }
						if(commlCoverage.getCoverageCd().getValue().trim().equals("SIGN"))
						{
							
						Deductible deductible=new Deductible();
						deductible.setFormatPct(new Double(asbycpl1TO.getBYPPTX()));
						Option option=new Option();
						String op;
						if(String.valueOf(asbycpl1TO.getBYIYTX()).equals("I"));
						op="Inside";
						if(String.valueOf(asbycpl1TO.getBYIYTX()).equals("O"))
							op="Outside";
						option.setOptionCd(op);
						commlCoverage.getDeductible().add(deductible);
						commlCoverage.getOption().add(option);
					    }
						if(commlCoverage.getCoverageCd().getValue().trim().equals("PAPER"))
						{
							
						Deductible deductible=new Deductible();
						deductible.setFormatPct(new Double(asbycpl1TO.getBYPPTX()));
						Option option=new Option();
						String op;
						if(String.valueOf(asbycpl1TO.getBYC0ST()).equals("Y"));
						op="Scheduled";
						if(String.valueOf(asbycpl1TO.getBYC0ST()).equals("O"))
					    op="Unscheduled";
						
						commlCoverage.getDeductible().add(deductible);

						option.setOptionCd(op);
						
						commlCoverage.getOption().add(option);
						
						accordfrmts.Limit limit1=new Limit();
						
						FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
						formatCurrencyAmt1.setAmt(new BigDecimal(asbycpl1TO.getBYPPTX()));
						limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
					
						
						commlCoverage.getLimit().add(limit1);
						
						
					    }
						else
						{
							accordfrmts.Limit limit1=new Limit();
							
							FormatCurrencyAmt formatCurrencyAmt1=new FormatCurrencyAmt();
							formatCurrencyAmt1.setAmt(new BigDecimal(asbycpl1TO.getBYPPTX()));
							limit1.setFormatCurrencyAmt(formatCurrencyAmt1);
														
							commlCoverage.getLimit().add(limit1);
						}
						if(commlCoverage.getCoverageCd().getValue().trim().equals("RDOTV"))
						{
							Deductible deductible=new Deductible();
							PCDEDUCTIBELTYPE pcdeductibeltype=new PCDEDUCTIBELTYPE();
							pcdeductibeltype.setValue("com.csc_XEXP");
							deductible.setDeductibleTypeCd(pcdeductibeltype);
							
						}
						else
						{Deductible deductible=new Deductible();
						PCDEDUCTIBELTYPE pcdeductibeltype=new PCDEDUCTIBELTYPE();
					
							deductible.setDeductibleTypeCd(null);
						}
					}
				
			}
			if(	asbbcpl1TO.getBBAGTX().trim().equals("CA"))
			{
				CommlAutoLineBusiness commlAutoLineBusiness=new CommlAutoLineBusiness();
				PCLINEOFBUSINESS pclineofbusiness1=new PCLINEOFBUSINESS();
				pclineofbusiness1.setValue("CA");
				commlAutoLineBusiness.setLOBCd(pclineofbusiness1);
				
				for(int j=0;j<Asb5cpl1.size();j++)
				{
					asb5cpl1TO=Asb5cpl1.get(j);
				if(asb5cpl1TO.getB5ANTX().equals("CA") && asb5cpl1TO.getB5C6ST()=='P')
				{
					CommlRateState commlRateState=new CommlRateState();
					PCJURISDICTION pcjurisdiction2=new PCJURISDICTION();
					pcjurisdiction2.setValue(asb5cpl1TO.getB5BCCD());
				commlRateState.setStateProvCd(pcjurisdiction2);
					commlAutoLineBusiness.getCommlRateState().add(commlRateState);
					if(j==0)
					{
						CommlAutoHiredInfo commlAutoHiredInfo=new CommlAutoHiredInfo();
						commlAutoHiredInfo.setHiredLiabilityCostAmt(asbbcpl1TO.getBBC1VA());
						for(int k=0;k<Asbycpl1.size();k++)
						{
							asbycpl1TO=Asbycpl1.get(k);
							if(asbycpl1TO.getBYAOTX().substring(0, 4).equals("HIRE"))
							{
							CommlCoverage commlCoverage=new CommlCoverage();
							PCCOVERAGE pccoverage=new PCCOVERAGE();
							pccoverage.setValue(asbycpl1TO.getBYAOTX());
							commlCoverage.setCoverageCd(pccoverage);
							commlCoverage.setCoverageDesc("");
							if(asbycpl1TO.getBYAGVA().toString().length()>0)
							{
								Limit limit=new Limit();
								FormatCurrencyAmt formatCurrencyAmt=new FormatCurrencyAmt();
								
								if(asbycpl1TO.getBYAOTX().trim().equals("HIRE"))
								formatCurrencyAmt.setAmt(asbycpl1TO.getBBUSVA5());
								if(asbycpl1TO.getBYAOTX().trim().equals("HIREUM"))
								formatCurrencyAmt.setAmt(asbycpl1TO.getBBUSVA3());
								if(asbycpl1TO.getBYAOTX().trim().equals("HIRECM"))
								formatCurrencyAmt.setAmt(asbycpl1TO.getBYPOTX());
								if(asbycpl1TO.getBYAOTX().trim().equals("HIRECO"))
								formatCurrencyAmt.setAmt(asbycpl1TO.getBYEGCD());
								else
								formatCurrencyAmt.setAmt(asbycpl1TO.getBYAGVA());
								
								limit.setFormatCurrencyAmt(formatCurrencyAmt);
								commlCoverage.getLimit().add(limit);
							}
							
							if(asbycpl1TO.getBYA9NB().toString().length()>0)
							{
								Deductible deductible=new Deductible();
								FormatCurrencyAmt formatCurrencyAmt=new FormatCurrencyAmt();
								
								formatCurrencyAmt.setAmt((BigDecimal)decimalFormat.parse(asbycpl1TO.getBYPNTX()));
								
								deductible.setFormatCurrencyAmt(formatCurrencyAmt);
								PCDEDUCTIBELTYPE pdt=new PCDEDUCTIBELTYPE();
								pdt.setValue("FL");
								deductible.setDeductibleTypeCd(pdt);
								commlCoverage.getDeductible().add(deductible);
							}
							
							commlAutoHiredInfo.getCommlCoverage().add(commlCoverage);
							}
							
						}
					
						commlRateState.setCommlAutoHiredInfo(commlAutoHiredInfo);
						
						CommlAutoNonOwnedInfo commlAutoNonOwnedInfo=new CommlAutoNonOwnedInfo();
						Boolean boolean1=new Boolean();
						boolean1.setValue(String.valueOf(asbbcpl1TO.getBBC1ST()));
						commlAutoNonOwnedInfo.setIndividualLiabilityForEmployeesInd(boolean1);
						NonOwnedInfo noi=new NonOwnedInfo();
						noi.setNonOwnedGroupTypeCd("V");
						//noi.setNumNonOwned(asbbcpl1TO.getBBBLNB());
						
						commlAutoNonOwnedInfo.getNonOwnedInfo().add(noi);
						
						NonOwnedInfo noi1=new NonOwnedInfo();
						noi1.setNonOwnedGroupTypeCd("com.csc_NumDonor");
						//noi1.setNumNonOwned(asbbcpl1TO.getBBIFNB());
						
						commlAutoNonOwnedInfo.getNonOwnedInfo().add(noi1);
						
						NonOwnedInfo noi2=new NonOwnedInfo();
						noi2.setNonOwnedGroupTypeCd("E");
					//	noi2.setNumNonOwned(asbbcpl1TO.getBBBKNB());
						
						commlAutoNonOwnedInfo.getNonOwnedInfo().add(noi2);
						
						
						
						
						
					}
					
				}
				}
				
			}
			if(	asbbcpl1TO.getBBAGTX().trim().equals("IMC"))
			{
				
			}
			if(	asbbcpl1TO.getBBAGTX().trim().equals("CF"))
			{
				
			}
			if(	asbbcpl1TO.getBBAGTX().trim().equals("CR"))
			{
				
			}
			}
			
			return commlpkgPolicyQuoteInqRq;
			
		
	}
	public static XMLGregorianCalendar getXMLGregorianCalendar(String date) throws DatatypeConfigurationException, ParseException {
		Date dob=null;
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		dob=df.parse(date);
		GregorianCalendar cal = new GregorianCalendar();

		cal.setTime(dob);
		XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0, 0).normalize();
	
		xmlDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
		
   

		return xmlDate;

	}
	public static XMLGregorianCalendar getXMLGregorianCalendarfrmtb(String date) throws ParseException, DatatypeConfigurationException
	{
		Date dob=null;
		DateFormat df=new SimpleDateFormat("yyMMdd");
		dob=df.parse(date);
		GregorianCalendar cal = new GregorianCalendar();

		cal.setTime(dob);
		XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR)+1, cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0, 0).normalize();
	
		xmlDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
		
   

		return xmlDate;
	}

}
