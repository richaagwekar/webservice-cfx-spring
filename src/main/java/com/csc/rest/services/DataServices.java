package com.csc.rest.services;

import java.util.List;

import accordfrmts.CommlPkgPolicyQuoteInqRq;
import accordfrmts.HomePolicyQuoteInqRq;
import accordfrmts.PersAutoPolicyQuoteInqRq;

import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelRq.PolicySearchInquiry;
import com.csc.rest.model.CommlLine.CommPolicy;
import com.csc.rest.model.PolicyDetails;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public interface DataServices {

	

	

	public List<Policy> pASearch(PolicySearchInquiry search,String hostName,int port) throws Exception;

	public PersAutoPolicyQuoteInqRq GetAccord(Policy info) throws Exception;
	
	public CommlPkgPolicyQuoteInqRq GetAccordComml(Policy info) throws Exception;

	public HomePolicyQuoteInqRq GetAccordHO(Policy info) throws Exception;
}
