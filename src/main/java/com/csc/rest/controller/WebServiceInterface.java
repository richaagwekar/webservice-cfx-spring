package com.csc.rest.controller;





import java.io.IOException;

import accordfrmts.PersAutoPolicyQuoteInqRq;
import accordfrmts.PolicyQuoteInqRq;

import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelResponse.Response;

import com.csc.rest.changedmodelResponse.PolicySearchResponse;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceContext;

import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Path("/PIJ")


public interface WebServiceInterface {
	

	
	@GET
	@Path("/Policy")
	@Produces({  "application/json" })
	
	Response PolicySearchInquiry(@QueryParam("") com.csc.rest.changedmodelRq.Criteria cr) throws JAXBException, IOException;


	@GET
	@Path("/AccordPolDetail")
	@Produces({ "application/xml" })
	
	PolicyQuoteInqRq GetPolicyDetails(@QueryParam("") Policy info) throws JAXBException, IOException;

}
