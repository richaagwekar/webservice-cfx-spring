package com.csc.rest.controller;


import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;



import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONArray;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;






import accordfrmts.CommlPkgPolicyQuoteInqRq;
import accordfrmts.HomePolicyQuoteInqRq;
import accordfrmts.PersAutoPolicyQuoteInqRq;
import accordfrmts.PolicyQuoteInqRq;

import com.csc.rest.changedmodelResponse.Policies;
import com.csc.rest.changedmodelResponse.Policy;
import com.csc.rest.changedmodelResponse.PolicySearchResponse;
import com.csc.rest.changedmodelResponse.Response;
import com.csc.rest.changedmodelResponse.Status;
import com.csc.rest.model.*;
import com.csc.rest.model.CommlLine.CommPolicy;
import com.csc.rest.model.CommlLine.CommPolicy.CommlPolicy;
import com.csc.rest.services.DataServices;




@Service("WebServiceInterface")
public class RestController implements WebServiceInterface {
@Autowired
	DataServices dataServices;

@Context
HttpServletRequest request;

public HttpServletRequest getRequest() {
	return request;
}
public void setRequest(HttpServletRequest request) {
	this.request = request;
}
	static final Logger logger = Logger.getLogger(RestController.class);

		@Override
	public Response PolicySearchInquiry(
			com.csc.rest.changedmodelRq.Criteria cr)
			throws JAXBException, IOException {
		// TODO Auto-generated method stub
		List<Policy> result = new ArrayList<Policy>();
		PolicySearchResponse psr=new PolicySearchResponse();
		try {
			
int flag_emp_res=0;
		  String  hostName = request.getServerName();
			  int port = request.getServerPort();

			  com.csc.rest.changedmodelRq.PolicySearchInquiry info=new com.csc.rest.changedmodelRq.PolicySearchInquiry();
			  info.setCriteria(cr);
			
			
			result = dataServices.pASearch(info,hostName,port);
			if (result.isEmpty()) {
			/*	Policy policy = new Policy();
				policy.setPolicynum("No such Policy Exists");
				result.add(policy);*/
				flag_emp_res=1;

			}
			else
			{
Policies policies=new Policies();
Policy []arraypolicy=new Policy[result.size()];

for(int i=0;i<result.size();i++)
{
Policy policy=new Policy();
policy=result.get(i);
arraypolicy[i]=policy;
}

policies.setPolicy(arraypolicy);
psr.setPolicies(policies);
			}
psr.setCriteria(info.getCriteria());

Status status=new Status();
if(flag_emp_res==0)
{
status.setCode(200);
status.setMessage("OK");
}
if(flag_emp_res==1)
{
status.setCode(100);
status.setMessage("EMPTY");
}
psr.setStatus(status);



		} catch (Exception e) {
			logger.error("Exception caught : ", e);
			e.printStackTrace();
		}
		Response res=new Response();
		res.setPolicySearchResponse(psr);
		return res;

	}
	@Override
	public
	PolicyQuoteInqRq GetPolicyDetails(Policy info) throws JAXBException, IOException{
		
		
		PersAutoPolicyQuoteInqRq persAutoPolicyQuoteInqRq=null;
		PolicyQuoteInqRq pqir=null;
		CommlPkgPolicyQuoteInqRq commlPkgPolicyQuoteInqRq=null;
		HomePolicyQuoteInqRq homePolicyQuoteInqRq=null;
		
		try {
			
if(info.getSymbol().equals("APV"))
{
			persAutoPolicyQuoteInqRq = dataServices.GetAccord(info);
			pqir=persAutoPolicyQuoteInqRq;
}
if(info.getSymbol().equals("CPP"))
{
	commlPkgPolicyQuoteInqRq = dataServices.GetAccordComml(info);
	pqir=commlPkgPolicyQuoteInqRq;
}
if(info.getSymbol().equals("HP"))
{
	homePolicyQuoteInqRq=dataServices.GetAccordHO(info);
			pqir=homePolicyQuoteInqRq;
}
		} catch (Exception e) {
			logger.error("Exception caught : ", e);
			e.printStackTrace();
		}
return pqir;

	}

}
